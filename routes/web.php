<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', function () {
    return view('Main.index');
})->name('index');

//main
Route::get('/Recipes','MainController@showRecipes')->name('Main.recipes');
Route::get('/Products','MainController@showProducts')->name('Main.products');
//checkpoint
Route::get('/AQUATA/Checkpoint/Reportes', function () {
    return view('checkpoint.reportes');
})->name('Checkpoint.Reportes');


Route::get('/AQUATA/Perfil','ProfileController@showProfile')->name('Profile');

//redireccionamiento por enlace
Route::get('/AQUATA/Inventario',function(){
  return redirect('/AQUATA/Inventario/Alimento');
})->name('Inventory');


//vistas principales
Route::get('/AQUATA/Historial/Articulos','HistoryController@showArticlesHistory')->name('History.Articles');
Route::post('/AQUATA/Historial/Articulos','HistoryController@showArticlesHistoryByDate')->name('History.ArticlesHistoryByDate');
Route::get('/AQUATA/Historial/Alimento','HistoryController@showFoodHistory')->name('History.Food');
Route::post('/AQUATA/Historial/Alimento','HistoryController@showFoodHistoryByDate')->name('History.FoodHistoryByDate');

Route::get('/AQUATA/Inventario/Articulos','InventoryController@showArticlesInventory')->name('Inventory.Articles');
Route::get('/AQUATA/Inventario/Alimento','InventoryController@showFoodInventory')->name('Inventory.Food');
Route::get('/AQUATA/Inventario/Productos','InventoryController@showProductsInventory')->name('Inventory.Products');

Route::get('/AQUATA/Inventario/Producto/Editar/{id}','InventoryController@editInventoryProduct')->name('InventoryProduct.Edit');
Route::post('/AQUATA/Inventario/Producto/Actualizar', 'InventoryController@updateInventoryProduct')->name('InventoryProduct.Update');
Route::get('/AQUATA/Inventario/Producto/Cantidad/{id}','InventoryController@upgradeInventoryProduct')->name('InventoryProduct.Upgrade');
Route::post('/AQUATA/Inventario/Producto/Cantidad/Actualizar', 'InventoryController@upgradeValidationInventoryProduct')->name('InventoryProduct.UpgradeValidation');
Route::get('/AQUATA/Inventario/Producto/Desastre/{id}', 'InventoryController@disasterInventoryProduct')->name('InventoryProduct.Disaster');
Route::post('/AQUATA/Inventario/Producto/Desastre/Actualizar', 'InventoryController@disasterValidationInventoryProduct')->name('InventoryProduct.DisasterValidation');
Route::get('/AQUATA/Inventario/Producto/Agregar','InventoryController@addInventoryProduct')->name('InventoryProduct.Add');
Route::get('/AQUATA/Inventario/Producto/Convertir','InventoryController@convertInventoryProduct')->name('InventoryProduct.Convert');
Route::post('/AQUATA/Inventario/Producto/Convertir/Actualizar','InventoryController@convertValidationInventoryProduct')->name('InventoryProduct.ConvertValidation');
Route::post('/AQUATA/Inventario/Producto/Agregar/Actualizar','InventoryController@addValidationInventoryProduct')->name('InventoryProduct.AddValidation');
Route::get('/AQUATA/TipoAlimento/Eliminar/{id}', 'InventoryController@deleteType')->name('Type.Delete');
Route::get('/AQUATA/TipoAlimento/Activar/{id}', 'InventoryController@activeType')->name('Type.Active');
Route::get('/AQUATA/TipoAlimento/Editar/{id}', 'InventoryController@editType')->name('Type.Edit');
Route::get('/AQUATA/TipoAlimento/Agregar', 'InventoryController@addType')->name('Type.Add');
Route::post('/AQUATA/TipoAlimento/Agregado', 'InventoryController@addUpdateType')->name('Type.AddUpdate');
Route::post('/AQUATA/TipoAlimento/Actualizar', 'InventoryController@updateType')->name('Type.Update');

Route::get('/AQUATA/Costal/Agregar', 'InventoryController@addBag')->name('Bag.Add');
Route::post('/AQUATA/Costal/Agregar/Actualizar', 'InventoryController@addUpdateBag')->name('Bag.AddUpdate');
Route::get('/AQUATA/Costal/Editar/{id}', 'InventoryController@editBag')->name('Bag.Edit');
Route::post('/AQUATA/Costal/Actualizar', 'InventoryController@updateBag')->name('Bag.Update');
Route::get('/AQUATA/Costal/Eliminar/{id}', 'InventoryController@deleteBag')->name('Bag.Delete');
Route::post('/AQUATA/Costal/EliminadoCompleto', 'InventoryController@updateDeleteBag')->name('Bag.UpdateDelete');

Route::get('/AQUATA/Motivo/Eliminar/{id}', 'InventoryController@deleteMotive')->name('Motive.Delete');
Route::get('/AQUATA/Motivo/Activar/{id}', 'InventoryController@activeMotive')->name('Motive.Active');
Route::get('/AQUATA/Motivo/Editar/{id}', 'InventoryController@editMotive')->name('Motive.Edit');
Route::post('/AQUATA/Motivo/Actualizar', 'InventoryController@updateMotive')->name('Motive.Update');
Route::get('/AQUATA/Motivo/Agregar', 'InventoryController@addMotive')->name('Motive.Add');
Route::post('/AQUATA/Motivo/Agregado', 'InventoryController@addUpdateMotive')->name('Motive.AddUpdate');

Route::get('/AQUATA/Articulo/Agregar', 'InventoryController@addArticle')->name('Article.Add');
Route::post('/AQUATA/Articulo/Agregado', 'InventoryController@addUpdateArticle')->name('Article.AddUpdate');
Route::get('/AQUATA/Articulo/Eliminar/{id}', 'InventoryController@deleteArticle')->name('Article.Delete');
Route::get('/AQUATA/Articulo/Editar/{id}', 'InventoryController@editArticle')->name('Article.Edit');
Route::post('/AQUATA/Articulo/Actualizar', 'InventoryController@updateArticle')->name('Article.Update');
Route::get('/AQUATA/Articulo/Activar/{id}', 'InventoryController@activeArticle')->name('Article.Activate');
Route::get('/AQUATA/InventarioArticulo/Eliminar/{id}', 'InventoryController@deleteInventoryArticle')->name('InventoryArticle.Delete');
Route::get('/AQUATA/InventarioArticulo/Editar/{id}', 'InventoryController@editInventoryArticle')->name('InventoryArticle.Edit');
Route::post('/AQUATA/InventarioArticulo/Actualizar', 'InventoryController@updateInventoryArticle')->name('InventoryArticle.Update');
Route::get('/AQUATA/InventarioArticulo/Agregar', 'InventoryController@addInventoryArticle')->name('InventoryArticle.Add');
Route::post('/AQUATA/InventarioArticulo/Agregado', 'InventoryController@addUpdateInventoryArticle')->name('InventoryArticle.AddUpdate');
Route::get('/AQUATA/Inventario/Articulo/Cantidad/{id}','InventoryController@upgradeInventoryArticle')->name('InventoryArticle.Upgrade');
Route::post('/AQUATA/Inventario/Articulo/Cantidad/Actualizar', 'InventoryController@upgradeValidationInventoryArticle')->name('InventoryArticle.UpgradeValidation');
Route::get('/AQUATA/Inventario/Articulo/Restar/{id}','InventoryController@takeOffInventoryArticle')->name('InventoryArticle.TakeOff');
Route::post('/AQUATA/Inventario/Articulo/Restar/Actualizar', 'InventoryController@takeOffValidationInventoryArticle')->name('InventoryArticle.TakeOffValidation');

Route::get('/AQUATA/Unidad/Eliminar/{id}', 'InventoryController@deleteUnit')->name('Unit.Delete');
Route::get('/AQUATA/Unidad/Activar/{id}', 'InventoryController@activeUnit')->name('Unit.Active');
Route::get('/AQUATA/Unidad/Editar/{id}', 'InventoryController@editUnit')->name('Unit.Edit');
Route::post('/AQUATA/Unidad/Actualizar', 'InventoryController@updateUnit')->name('Unit.Update');
Route::get('/AQUATA/Unidad/Agregar', 'InventoryController@addUnit')->name('Unit.Add');
Route::post('/AQUATA/Unit/Agregado', 'InventoryController@addUpdateUnit')->name('Unit.AddUpdate');

Route::get('/AQUATA/CategoriaArticulo/Eliminar/{id}', 'InventoryController@deleteArticleCategory')->name('ArticleCategory.Delete');
Route::get('/AQUATA/CategoriaArticulo/Activar/{id}', 'InventoryController@activeArticleCategory')->name('ArticleCategory.Active');
Route::get('/AQUATA/CategoriaArticulo/Editar/{id}', 'InventoryController@editArticleCategory')->name('ArticleCategory.Edit');
Route::post('/AQUATA/CategoriaArticulo/Actualizar', 'InventoryController@updateArticleCategory')->name('ArticleCategory.Update');
Route::get('/AQUATA/CategoriaArticulo/Agregar', 'InventoryController@addArticleCategory')->name('ArticleCategory.Add');
Route::post('/AQUATA/CategoriaArticulo/Agregado', 'InventoryController@addUpdateArticleCategory')->name('ArticleCategory.AddUpdate');

Route::get('/AQUATA/Producto/Agregar/', 'InventoryController@addProduct')->name('Product.Add');
Route::post('/AQUATA/Producto/Agregar/Actualizar', 'InventoryController@addValidationProduct')->name('Product.AddValidation');
Route::get('/AQUATA/Producto/Eliminar/{id}', 'InventoryController@deleteProduct')->name('Product.Delete');
Route::get('/AQUATA/Producto/Activar/{id}', 'InventoryController@activeProduct')->name('Product.Active');
Route::get('/AQUATA/Producto/Editar/{id}', 'InventoryController@editProduct')->name('Product.Edit');
Route::post('/AQUATA/Producto/Actualizar', 'InventoryController@updateProduct')->name('Product.Update');

Route::get('/AQUATA/inicio','PondsController@show')->name('ponds');
Route::get('/AQUATA/Empleados', 'JobsController@showInfo')->name('Employee');
Route::get('/AQUATA/Recetas','RecipeController@show')->name('recipes');

/*Route::get('/AQUATA/Empleados', function(){
  $user=Auth::user();

  if($user->socio()){
    echo "No fallo";
    return redirect('/AQUATA/inicio');
  }
  else{
    echo "Fallo";
    return view('home');
  }
});*/

/*Route::group([
    'middleware' => 'socio'
], function () {

  Route::get('/AQUATA/inicio','PondsController@show')->name('ponds');
  Route::get('/AQUATA/Empleados', 'JobsController@showInfo')->name('Employee');

});*/



// NOTE: rutas para trabajar con los estanques
Route::get('/AQUATA/pondAdd', 'PondsController@addForm')->name('Estanque.AddForm');
Route::post('/AQUATA/pondAdd', 'PondsController@add')->name('Estanque.Add');
Route::post('/AQUATA/pondFind', 'PondsController@find')->name('Estanque.Find');
Route::post('/AQUATA/pondEdit', 'PondsController@update')->name('Estanque.Update');
Route::post('/AQUATA/pondDelete', 'PondsController@delete')->name('Estanque.Delete');
Route::post('/AQUATA/pondActivate', 'PondsController@activate')->name('Estanque.Activate');


// NOTE: rutas para trabajar con las truchas
Route::get('/AQUATA/troutEdit/{id}', 'PondsController@findTrout')->name('Trucha.Find');
Route::post('/AQUATA/troutEdit/{id}', 'PondsController@updateTrout')->name('Trucha.Update');
Route::post('/AQUATA/eliminarTrucha','PondsController@deleteTrout')->name('Delete.Trout');
Route::get('/AQUATA/troutAdd', 'PondsController@addFormTrout')->name('Trout.AddForm');
Route::post('/AQUATA/troutAdd', 'PondsController@addTrout')->name('Trout.Add');
Route::post('/AQUATA/troutActivate', 'PondsController@troutActivate')->name('Trout.Activate');

//  NOTE: rutas para trabajar con los puestos
Route::get('/AQUATA/Puesto/Agregar', 'JobsController@addJobView')->name('Job.Add.View');
Route::post('/AQUATA/Puesto/Agregar', 'JobsController@addJob')->name('Job.Add');
Route::get('/AQUATA/Puesto/Editar/{id}', 'JobsController@editJob')->name('Job.Edit');
Route::post('/AQUATA/Puesto/Actualizar', 'JobsController@updateJob')->name('Job.Update');
Route::get('/AQUATA/Puesto/Eliminar/{id}', 'JobsController@deleteJob')->name('Job.Delete');
Route::get('/AQUATA/Puesto/Activar/{id}', 'JobsController@activateJob')->name('Job.Activate');

//  NOTE: rutas para trabajar con los empleados
Route::get('/AQUATA/Empleado/Agregar', 'JobsController@addEmployeeView')->name('Employee.Add.View');
Route::post('/AQUATA/Empleado/Agregar', 'JobsController@addEmployee')->name('Employee.Add');
Route::get('/AQUATA/Empleado/Editar/{id}', 'JobsController@editEmployee')->name('Employee.Edit');
Route::post('/AQUATA/Empleado/Actualizar', 'JobsController@updateEmployee')->name('Employee.Update');
Route::get('/AQUATA/Empleado/Eliminar/{id}', 'JobsController@deleteEmployee')->name('Employee.Delete');
Route::get('/AQUATA/Empleado/Activar/{id}', 'JobsController@activateEmployee')->name('Employee.Activate');

// NOTE: Rutas para trabajar con economia
Route::get('/AQUATA/Economia',function(){
  return redirect('/AQUATA/Economia/Gastos');
})->name('Economy');

Route::get('/AQUATA/Economia/Gastos','EconomyController@showExpenses')->name('Economy.Expense');
Route::post('/AQUATA/Economia/Gastos','EconomyController@showExpensesByDate')->name('Economy.ExpenseByDate');
Route::get('/AQUATA/Gasto/Agregar','EconomyController@addExpenseView')->name('Expense.Add.View');
Route::post('/AQUATA/Gasto/Agregar','EconomyController@addExpense')->name('Expense.Add');
Route::post('/AQUATA/Gasto/Cancelar','EconomyController@cancelExpense')->name('Expense.Cancel');
Route::get('/AQUATA/CategoriaGasto/Agregar', 'EconomyController@addExpenseCategoryView')->name('ExpenseCategory.Add.View');
Route::post('/AQUATA/CategoriaGasto/Agregar', 'EconomyController@addExpenseCategory')->name('ExpenseCategory.Add');
Route::post('/AQUATA/CategoriaGasto/Editar', 'EconomyController@editExpenseCategory')->name('ExpenseCategory.Edit');
Route::post('/AQUATA/CategoriaGasto/Actualizar', 'EconomyController@updateExpenseCategory')->name('ExpenseCategory.Update');
Route::post('/AQUATA/CategoriaGasto/Eliminar', 'EconomyController@deleteExpenseCategory')->name('ExpenseCategory.Delete');
Route::post('/AQUATA/CategoriaGasto/Activar', 'EconomyController@activateExpenseCategory')->name('ExpenseCategory.Activate');

Route::get('/AQUATA/Economia/Ventas','EconomyController@showSales')->name('Economy.Sale');
Route::post('/AQUATA/Economia/Ventas','EconomyController@showSalesByDate')->name('Economy.SaleByDate');
Route::get('/AQUATA/Venta/Agregar','EconomyController@addSaleView')->name('Sale.Add.View');
Route::post('/AQUATA/Venta/Agregar','EconomyController@addSale')->name('Sale.Add');
Route::post('/AQUATA/Venta/Cancelar','EconomyController@cancelSale')->name('Sale.Cancel');

Route::get('/AQUATA/Economia/Estadisticas','EconomyController@showStatistics')->name('Economy.Statistic');
Route::post('/AQUATA/Economia/Estadisticas','EconomyController@showStatisticsByDate')->name('Economy.StatisticByDate');

Route::get('/AQUATA/recetaAgregar', 'RecipeController@addForm')->name('Receta.AddForm');
Route::post('/AQUATA/recetaAñadir', 'RecipeController@add')->name('Receta.Add');
Route::post('/AQUATA/recetaEliminar', 'RecipeController@delete')->name('Receta.Delete');
Route::post('/AQUATA/recetaActivar', 'RecipeController@activate')->name('Receta.Activate');
Route::post('/AQUATA/recetaEditada', 'RecipeController@find')->name('Recipe.Find');
Route::post('/AQUATA/recetaEditar', 'RecipeController@update')->name('Receta.Update');


Route::post('/AQUATA/Estanques/Historial','PondsController@pondHistory')->name('Ponds.History');
Route::any('/AQUATA/Estanques/Alimentar', 'PondsController@pondFeed')->name('Ponds.Feed');

/*Route::post('/AQUATA/Estanques/Alimentar', [
    'as' => 'Ponds.Feed',
    'middleware' => 'auth',
    'uses' => 'AuthController@anyRegister'
]);*/

Route::post('/AQUATA/Estanques/Historial/Fecha','PondsController@pondHistoryByDate')->name('Ponds.HistoryByDate');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
