@extends('Layouts.Menu')
@section('head','usuario')
@section('title','Estadisticas')
@section('content')

  <ul class="tabs tabs-fixed-width">

    <li class="tab col s4 m4 l4"><a target="_self" href="{{ route('Economy.Expense') }}">Gastos</a></li>
    <li class="tab col s4 m4 l4"><a target="_self" href="{{ route('Economy.Sale') }}">Ventas</a></li>
    <li class="tab col s4 m4 l4"><a class="active" href="#swipe-estadisticas">Estadisticas</a></li>

  </ul>

  <div id="swipe-estadisticas">
    <!--Panel de control-->
    <div class="row">
      <div class="col s12 m4 l4">
        <div class="card white">
          <div class="card-content blue-text text-darken-2">
            <span class="card-title">Panel de control</span>
            <br>
            <ul class="collapsible popout">
              <!-- NOTE: Filtro de fechas -->
              <li>
                <div class="collapsible-header"><i class="material-icons">date_range</i>Selecionar periodo</div>
                <div class="collapsible-body">
                  <!-- NOTE: formulario para cambiar fechas -->
                  <form class="" id="form-fechas" action="{{ route('Economy.StatisticByDate') }}" method="post">
                    @csrf
                    <div class="row">
                      <!-- NOTE: datepicker 1 -->
                      <div class="col s12 m12 l12">
                        <h6 class="center-align black-text">Fecha inicial:</h6>
                        <input type="text" name="fecha1" class="datepicker center-align" id="fecha1">
                      </div>
                      <!-- NOTE: datepicker 2 -->
                      <div class="col s12 m12 l12">
                        <h6 class="center-align black-text">Fecha final:</h6>
                        <input type="text" name="fecha2" class="datepicker center-align" id="fecha2">
                      </div>
                    </div>
                    <div class="row">
                      <!-- NOTE: boton para actualizar los datos con las fechas de los pickers -->
                      <div class="col s12 m12 l12 center-align">
                        <br>
                        <a class="waves-effect waves-light btn teal" onclick="document.getElementById('form-fechas').submit();">Consultar</a>
                      </div>
                    </div>
                  </form>
              </li>

            </ul>
          </div>
          <div class="card-action center-align">
            <div class="row">
              <div class="col s12 m12 l12 center-align">
                <a id="export_button" class="waves-effect waves-light btn teal" ><i class="material-icons right">file_download</i>Exportar</a>
              </div>
            </div>

          </div>
        </div>
      </div>
      <!-- NOTE: CONTENIDO -->
      <div class="col s12 m8 l8">
        <!-- NOTE: poner aqui los tabs -->
        <div class="card">
          <div class="card-content">
            <div id="descripcion-estadisticas">
              <!-- NOTE: aqui se pondra una descripcin diferente segun el tab que se muestre -->
            </div>
          </div>
          <!-- NOTE: aqui se definen los tabs -->
          <div class="card-tabs">
            <ul class="tabs">
              <li class="tab col s6"><a href="#tabla-estadisticas" class="active" id="tab-tabla">Tabla</a></li>
              <li class="tab col s6"><a href="#div-grafica-1" id="tab-grafica-1">Grafica Pastel</a></li>
            </ul>
          </div>
          <!-- NOTE: aqui va el contenido de los tabs -->
          <div class="card-content">
            <div id="tabla-estadisticas">
              <!-- NOTE: AQUI VA LA TABLA -->
              <table class="centered responsive-table highlight">
                @if (!$stats->isEmpty())
                  <thead>
                    <tr>
                      <th class="hide">Tipo</th>
                      <th class="noexport">Tipo</th>
                      <th class="hide">Descripcion</th>
                      <th>Fecha</th>
                      <th>Monto</th>
                      <th class="noexport"></th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach ($stats as $stat)
                      <tr>
                        @if($stat->nombre == 'Venta')
                          <td class="hide">{{$stat->nombre}}</td>
                          <td class="noexport">
                            <i class="material-icons green-text">arrow_upward</i>
                          </td>
                          <td class="hide">{{$stat->descripcion}}</td>
                          <td>{{ $stat->fecha }}</td>
                          <td>${{ $stat->monto }}</td>

                          <td><a class="btn-flat SELL noexport" id='info-venta-{{$stat->id}}' href="#modal-info"><i class="material-icons">info_outline</i></a></td>
                        @else
                          <td class="hide">{{$stat->nombre}}</td>
                          <td class="noexport">
                            <i class="material-icons red-text">arrow_downward</i>
                          </td>
                          <td class="hide">{{$stat->descripcion}}</td>
                          <td>{{ $stat->fecha }}</td>
                          <td>${{ $stat->monto }}</td>
                          <td><a class="btn-flat EXPENSE noexport" id='info-gasto-{{$stat->id}}' href="#modal-info"><i class="material-icons">info_outline</i></a></td>
                        @endif
                      </tr>
                    @endforeach
                  </tbody>
                @else
                  <tbody>
                    <h4>No hay suficiente informacion para generar las estadisticas.</h3>
                  </tbody>
                @endif
              </table>
            </div>
            <div id="div-grafica-1">
              <div class="chart" id="grafica-1">
                <!-- NOTE: AQUI VA LA GRAFICA 1 -->
              </div>
            </div>
          </div>
        </div>

      </div>
    </div>
  </div>

@endsection

@section('modals')
  <div id="modal-info" class="modal modal-fixed-footer">
    <div class="modal-content center-align">

    </div>
    <div class="modal-footer">
      <a class='waves-effect waves-light btn-flat grey-text text-darken-3 modal-close'>Cerrar</a>
    </div>
  </div>
@endsection

@section('scripts')
  <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
  <script src="//cdn.rawgit.com/rainabba/jquery-table2excel/1.1.0/dist/jquery.table2excel.min.js"></script>

  <script type="text/javascript" id="scripts-gastos">
    $(document).ready(function() {
      // NOTE: Validar que fecha 1 sea menor a fecha 2
      $( "#form-fechas" ).submit(function( event ) {
        alert($('#fecha1').datepicker('getDate'));
        if ($('#fecha1').datepicker('getDate') <= $('#fecha2').datepicker('getDate')) {
          return;
        }
        //$("#error-select").html("Seleccione una categoria");
        M.toast({html: 'Seleccione un periodo valido',classes:'red darken-3'});
        event.preventDefault();
      });
      $('.useless-category').hide();

      $('#tab-tabla').click();

      //  NOTE: aqui se inicializan los picker 1 y 2
      var hoy = new Date();
      dia = hoy.getDate();
      mes = hoy.getMonth()-1;
      ano = hoy.getFullYear();

      @if(!empty($f1))
        //  NOTE: fehcas solicitadas por el usuario
        dia1 = {{ substr($f1,0,2) }};
        mes1 = {{ substr($f1,3,2) }};
        ano1 = {{ substr($f1,6,4) }};
        dia2 = {{ substr($f2,0,2) }};
        mes2 = {{ substr($f2,3,2) }};
        ano2 = {{ substr($f2,6,4) }};
        $('#fecha1').datepicker({
          format: 'dd/mm/yyyy',
          setDefaultDate: true,
          defaultDate: new Date(ano1,mes1-1,dia1),
          minDate: new Date(2020,03,23),
          maxDate: hoy,
          i18n: {
            months: ['Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'],
            monthsShort: ['Ene','Feb','Mar','Abr','May','Jun','Jul','Ago','Sep','Oct','Nov','Dic'],
            weekdays: ['Domingo','Lunes','Martes','Miércoles','Jueves','Viernes','Sábado'],
            weekdaysShort: ['Dom','Lun','Mar','Mier','Jue','Vier','Sab'],
            weekdaysAbbrev: ['D','L','M','M','J','V','S']
          },

        });

        $('#fecha2').datepicker({
          format: 'dd/mm/yyyy',
          setDefaultDate: true,
          defaultDate: new Date(ano2,mes2-1,dia2),
          minDate: new Date(2020,03,23),
          maxDate: hoy,
          i18n: {
            months: ['Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'],
            monthsShort: ['Ene','Feb','Mar','Abr','May','Jun','Jul','Ago','Sep','Oct','Nov','Dic'],
            weekdays: ['Domingo','Lunes','Martes','Miércoles','Jueves','Viernes','Sábado'],
            weekdaysShort: ['Dom','Lun','Mar','Mier','Jue','Vier','Sab'],
            weekdaysAbbrev: ['D','L','M','M','J','V','S']
          },

        });
      @else
        //  NOTE: fecha de hoy y de hace un mes
        $('#fecha1').datepicker({
          format: 'dd/mm/yyyy',
          setDefaultDate: true,
          defaultDate: new Date(ano,mes,dia),
          minDate: new Date(2020,03,23),
          maxDate: hoy,
          i18n: {
            months: ['Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'],
            monthsShort: ['Ene','Feb','Mar','Abr','May','Jun','Jul','Ago','Sep','Oct','Nov','Dic'],
            weekdays: ['Domingo','Lunes','Martes','Miércoles','Jueves','Viernes','Sábado'],
            weekdaysShort: ['Dom','Lun','Mar','Mier','Jue','Vier','Sab'],
            weekdaysAbbrev: ['D','L','M','M','J','V','S']
          },

        });

        $('#fecha2').datepicker({
          format: 'dd/mm/yyyy',
          setDefaultDate: true,
          defaultDate: hoy,
          minDate: new Date(2020,03,23),
          maxDate: hoy,
          i18n: {
            months: ['Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'],
            monthsShort: ['Ene','Feb','Mar','Abr','May','Jun','Jul','Ago','Sep','Oct','Nov','Dic'],
            weekdays: ['Domingo','Lunes','Martes','Miércoles','Jueves','Viernes','Sábado'],
            weekdaysShort: ['Dom','Lun','Mar','Mier','Jue','Vier','Sab'],
            weekdaysAbbrev: ['D','L','M','M','J','V','S']
          },

        });
      @endif

      $('.SELL').click(function(){
      $id = $(this).attr('id');
      $id = $id.replace('info-venta-','');

      @foreach($stats as $stat)
        @if($stat->nombre == 'Venta')
          if({{$stat->id}} == $id){
            $nombre = '{{$stat->nombre}}';
            $descripcion = '{{$stat->descripcion}}';
            $monto = '{{$stat->monto}}';
          }
        @endif
      @endforeach

      $tipo = 'Venta';

      modalStats($id,$nombre,$descripcion,$monto,$tipo);
    });

    $('.EXPENSE').click(function(){
      $id = $(this).attr('id');
      $id = $id.replace('info-gasto-','');
      //alert('gasto: ' + $id);

      @foreach($stats as $stat)
        @if($stat->nombre != 'Venta')
          if({{$stat->id}} == $id){
            $nombre = '{{$stat->nombre}}';
            $descripcion = '{{$stat->descripcion}}';
            $monto = '{{$stat->monto}}';
          }
        @endif
      @endforeach
      $tipo = 'Gasto';

      modalStats($id,$nombre,$descripcion,$monto,$tipo);
    });

    function modalStats($id,$nombre,$descripcion,$monto,$tipo){
      $('.modal-content').empty();
      $('.modal-content').append(
        "<h3>INFO  <i class='material-icons small teal-text'>info_outline</i></h3>"+
        "<div class='divider'></div>"+
        "<p class='grey-text'>"+$tipo+"</p>"+
        "<div class='divider'></div>"+
        "<table class='striped'>"+
          "<tbody>"+
            "<tr>"+
              "<th class='center-align'>Categoria:</th>"+
              "<td>"+$nombre+"</td>"+
            "</tr>"+
            "<tr>"+
              "<th class='center-align'>Descripción:</th>"+
              "<td>"+$descripcion+"</td>"+
            "</tr>"+
            "<tr>"+
              "<th class='center-align'>Monto:</th>"+
              "<td>$"+$monto+"</td>"+
            "</tr>"+
          "</tbody>"+
        "</table>"
      );

      $('.modal-footer').empty();
      $('.modal-footer').append(
        "<a class='waves-effect waves-light btn-flat grey-text text-darken-3 modal-close'>Cerrar</a>"
      );
      $('#modal-info').modal('open');
    }

      //los métodos para mostrar o no a los inútiles
      $('#categorys-lever').click(function(){
         if ($("#categorys-switch").prop("checked") == true) {
           $('.useless-category').hide();
           $('.useful-category').show();
         }
         else{
           $('.useless-category').show();
           $('.useful-category').hide();
         }
       });

       @if(session('success'))
          M.toast({html: '{{session('success')}}',classes:'green darken-2'})
       @endif
       @if(session('error'))
          red_toast("{{session('error')}}");
       @endif

       var init_tabs = M.Tabs.init($('.tabs'), { onShow: drawCharts });
    });

    document.addEventListener('DOMContentLoaded', function() {
      var elems = document.querySelectorAll('.dropdown-trigger');
      var options =
        {
          constrainWidth:false,
          alignment:'right'
        };
      var instances = M.Dropdown.init(elems, options);
    });

    //  NOTE: GRAFICAS
    google.load("visualization", "1", {packages:["corechart"]});
    google.charts.load('current', {'packages':['corechart']});

    google.setOnLoadCallback(drawCharts);

    //  NOTE: grafica de pastel
    function drawChart1() {
      var data = google.visualization.arrayToDataTable([
        ['Tipo', 'Total'],

          @foreach($statsChart as $sc)
            ['{{ $sc->nombre }}',{{ $sc->total }}],
          @endforeach
      ]);

      var options = {
        title: '',
        is3D: true,
      };

        var chart = new google.visualization.PieChart(document.getElementById('grafica-1'));
        chart.draw(data, options);
    }

    function drawCharts(){
      drawChart1();
    }
    window.addEventListener("resize", drawCharts);

    //  NOTE: mesajes para los Gastos
    $('#tab-tabla').click(function(){
      @if(!empty($f1))
        //  NOTE: fehcas solicitadas por el usuario
        dia1 = {{ substr($f1,0,2) }};
        mes1 = {{ substr($f1,3,2) }};
        ano1 = {{ substr($f1,6,4) }};
        dia2 = {{ substr($f2,0,2) }};
        mes2 = {{ substr($f2,3,2) }};
        ano2 = {{ substr($f2,6,4) }};

        $('#descripcion-estadisticas').empty();
        $('#descripcion-estadisticas').append(
          "En la siguiente tabla se muestran las estadisticas registradas entre las fechas " + dia1 + "/" + mes1 + "/" + ano1 + " y " +
          dia2 + "/" + mes2 + "/" + ano2 + "."
        );
      @else
        var hoy = new Date();
        dia = hoy.getDate();
        mes = hoy.getMonth();
        ano = hoy.getFullYear();
        $('#descripcion-estadisticas').empty();
        $('#descripcion-estadisticas').append(
          "En la siguiente tabla se muestran las estadisticas registrados entre las fechas " + dia + "/" + mes + "/" + ano + " y " +
           + dia + "/" + (mes+1) + "/" + ano + "."
        );
      @endif

    });
    $('#tab-grafica-1').click(function(){
      @if(!empty($f1))
        //  NOTE: fehcas solicitadas por el usuario
        dia1 = {{ substr($f1,0,2) }};
        mes1 = {{ substr($f1,3,2) }};
        ano1 = {{ substr($f1,6,4) }};
        dia2 = {{ substr($f2,0,2) }};
        mes2 = {{ substr($f2,3,2) }};
        ano2 = {{ substr($f2,6,4) }};

        $('#descripcion-estadisticas').empty();
        $('#descripcion-estadisticas').append(
          "En la siguiente grafica se muestran las estadisticas registradas entre las fechas " + dia1 + "/" + mes1 + "/" + ano1 + " y " +
          dia2 + "/" + mes2 + "/" + ano2 + "."
        );
      @else
        var hoy = new Date();
        dia = hoy.getDate();
        mes = hoy.getMonth();
        ano = hoy.getFullYear();
        $('#descripcion-estadisticas').empty();
        $('#descripcion-estadisticas').append(
          "En la siguiente grafica se muestran las estadisticas registradas entre las fechas " + dia + "/" + mes + "/" + ano + " y " +
           + dia + "/" + (mes+1) + "/" + ano + "."
        );
      @endif
    });

    $('body').on('click', '.form-submit', function() {
      document.getElementById("Form-ExpenseCancel-"+$(this).attr('id')+"").submit();
    });

    $("#export_button").click(function(){
      $("#tabla-estadisticas").table2excel({
          // exclude CSS class
          exclude:".noexport",
          name:"Worksheet Name",
          filename:"Estadisticas",//do not include extension
          fileext:".xlsx"
      });
    });

  </script>
@endsection
