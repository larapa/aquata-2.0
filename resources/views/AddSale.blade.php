@extends('Layouts.Menu')
@section('title','Nueva venta')

@section('content')

  <form action="{{route('Sale.Add')}}" method="post">
    @csrf

    <h4 class="center-align">Registrar venta</h4>
    <br>
    <div class="row">
      <div class="container">
        <!-- NOTE: Descripcion de la pagina -->
        <div class="row">
          <div class="col s12 m4 l4 center-align">
            <h4><i class="material-icons">help_outline</i> INFO</h4>

          </div>
          <div class="col s12 m8 l8">
            <p  ALIGN="justify">El correcto registro de las ventas respresenta una parte importante del adecuado funcionamiento
              del sistema. Cada venta se registrará con la fecha y hora del día actual, además del usuario que lo registro.
            </p>
            <p  ALIGN="justify">
              Tenga en cuenta que una vez registrada una venta esta<span class="red-text text-darken-2"> no se podrá modificar</span>.
            </p>
          </div>
        </div>
        <!-- NOTE: Descipcion de la venta -->
        <div class="row">
          <div class="input-field col s12">
            <textarea id="descripcion" class="validate materialize-textarea" name="descripcion" type="text" data-length="120" maxlength="120" required="" aria-required="true"></textarea>
            <label for="descripcion">Descripción</label>
            <span class="help-text" data-error="Incorrecto" data-success="Correcto">Ingrese la descripción</span>
          </div>
        </div>
        <!-- NOTE: Productos-->
        <div class="row">
          <div class="container">
            <div class="input-field col s12">
              <select multiple class="productos">
                <option value="" disabled>Seleccione uno o más productos</option>
                @foreach($products as $product)
                  <option value="{{$product->idProducto}}">{{$product->nombre}}</option>
                @endforeach
              </select>
              <label>Seleccione uno o más productos</label>
            </div>
          </div>
        </div>
        <!-- NOTE: Preview de la venta -->
        <div class="row">
          <div class="col s12">
            <span>Detalles:</span>
          </div>
        </div>

              @foreach($products as $product)
                <div class="row oculto center-align blue-grey lighten-5" id='oculto-{{$product->idProducto}}'>

                  <br>
                  <div class="row left-align valign-wrapper">
                    <div class="col s10">
                      <b>Producto:</b> {{$product->nombre}}
                    </div>
                    <div class="col s2 center-align" id='precio-{{$product->idProducto}}' value='{{$product->precio}}'>
                      <b>Precio:</b> ${{$product->precio}}/kg
                    </div>
                  </div>
                  <div class="divider grey"></div>
                  <br>
                  <div class="row valign-wrapper ">
                    <div class="col s3">
                      <b>Peso (kg):</b>
                      <div class="input-field">
                        <input id='peso-{{$product->idProducto}}' type="number" class="validate calcular" name="peso-{{$product->idProducto}}" value="0.01" min="0.01" required="" aria-required="true" step=0.01>
                        <label for="peso-{{$product->idProducto}}">Peso</label>
                        <span class="help-text" data-error="Incorrecto" data-success="Correcto">Peso en kg</span>
                      </div>
                    </div>
                    <div class="col s3">
                      <b>Piezas:</b>
                      <div class="input-field">
                        <input id='cantidad-{{$product->idProducto}}' type="number" class="validate calcular" name="cantidad-{{$product->idProducto}}" value="1" min="1" max="{{$product->cantidadElementos}}" required="" aria-required="true">
                        <label for="cantidad-{{$product->idProducto}}">Cantidad</label>
                        <span class="help-text" data-error="No hay suficiente producto en el inventario" data-success="Correcto">Cantidad en unidades</span>
                      </div>
                    </div>
                    <div class="col s3">
                      <b>Descuento:</b>
                      <div class="input-field">
                        <input id='descuento-{{$product->idProducto}}' type="number" class="validate calcular" name="descuento-{{$product->idProducto}}" value="0" min="0" required="" aria-required="true">
                        <label for="descuento-{{$product->idProducto}}">Descuento</label>
                        <span class="help-text" data-error="Descuento mayor que subtotal" data-success="Correcto">Descuento en pesos</span>
                      </div>
                    </div>
                    <div class="col s3">
                      <b>Subtotal:</b><p id='subtotal-{{$product->idProducto}}' value='0'>$0</p>
                    </div>
                  </div>

                  <input type="hidden" name="idProducto-{{$product->idProducto}}" value="{{$product->idProducto}}">
                  <input type="hidden" name="idInventarioProducto-{{$product->idProducto}}" value="{{$product->idInventarioProducto}}">
                  <input id="bandera-{{$product->idProducto}}" type="hidden" name="bandera-{{$product->idProducto}}" value="oculto">
                </div>
              @endforeach
              <div class="row right-align ">
                <div class="divider"></div>
                <div class="col s9">
                  <p><b>Total:</b></p>
                </div>
                <div class=" col s3 center-align">
                  <p id='total' value='0'>$0</p>
                </div>
              </div>

        <!-- NOTE: Boton de enviar -->
        <div class="row center-align">
          <br>
          <button id='boton-enviar' type="submit" class="waves-effect waves-light btn blue darken-3 center-align">Aceptar</button>
        </div>
      </div>
    </div>
  </form>

@endsection

@section('scripts')
  <script>
    $(document).ready(function() {
      $(".oculto").hide();
    });


    $('input, textarea').characterCounter();

    //  NOTE: calculo de subtotales
    $(".calcular").change(function(){
      $id = $(this).attr('id');
      $id = $id.replace('peso-','');
      $id = $id.replace('descuento-','');

      $precio = $('#precio-'+$id).attr('value');
      $peso = $('#peso-'+$id).val();
      $descuento = $('#descuento-'+$id).val();
      $subtotal = ($precio*$peso)-$descuento;

      $('#subtotal-'+$id).html("$" + $subtotal );
      $('#subtotal-'+$id).val($subtotal);


      $selectedValues = $('.productos').val();

      $total = 0;
      for (var i = 0; i < $selectedValues.length; i++) {
        $precio = $('#precio-'+$selectedValues[i]).attr('value');
        $peso = $('#peso-'+$selectedValues[i]).val();
        $descuento = $('#descuento-'+$selectedValues[i]).val();
        $subtotal = ($precio*$peso)-$descuento;
        $total += $subtotal;
      }
      $('#total').html("$" + $total );
    });

    //  NOTE: mostrar y ocultar productos de la venta
    $('.productos').change(function() {
      $selectedValues = $(this).val();
      $total=0;

      @foreach ($products as $product)
        $('#oculto-{{$product->idProducto}}').hide();
        $('#bandera-{{$product->idProducto}}').val('oculto');
      @endforeach
      for (var i = 0; i < $selectedValues.length; i++) {
        $('#oculto-'+$selectedValues[i]).fadeIn();
        $('#bandera-'+$selectedValues[i]).val('visible');
        $precio = $('#precio-'+$selectedValues[i]).attr('value');
        $peso = $('#peso-'+$selectedValues[i]).val();
        $descuento = $('#descuento-'+$selectedValues[i]).val();
        $subtotal = ($precio*$peso)-$descuento;
        $total += $subtotal;
      }
      $('#total').html("$" + $total );

    });

  </script>
@endsection
