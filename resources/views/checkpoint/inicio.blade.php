<!DOCTYPE html>
<html lang="en" dir="ltr">

<!-- TODO:
  -> Comando para compilar Sass
    -> node-sass -w sass -o css
  - Implementar "GOOGLE CHARTS" o "morris.js" para la creación de gráficas
  - Arreglar color del navbar
  - Considerar el efecto parallax para la página principal
-->
  <head>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    {!! MaterializeCSS::include_all() !!}
    <meta name="viewport" charset="utf-8" content="width=device-width, initial-scale=1.0"/>
    <title>@Usuario</title>
  </head>

  <body>

    <header>
      <div  class="navbar-fixed">
        <nav>
          <div class="nav-wrapper blue darken-3">
            <a href="inicio.blade.php" class="brand-logo">AQUATA</a>
            <a href="#" data-target="menu-responsive" class="sidenav-trigger">
              <i class="material-icons">menu</i>
            </a>
            <ul class="right hide-on-med-and-down">
              <li><a href="inicio.blade.php"><i class="material-icons tooltipped" data-position="bottom" data-tooltip="Inicio">home</i></a></li>
              <li><a href="inventario.blade.php"><i class="material-icons tooltipped" data-position="bottom" data-tooltip="Inventario">storage</i></a></li>
              <li><a href="trabajadores.blade.php"><i class="material-icons tooltipped" data-position="bottom" data-tooltip="Trabajadores">people</i></a></li>
              <li><a href="reportes.blade.php"><i class="material-icons tooltipped" data-position="bottom" data-tooltip="Reportes">trending_up</i></a></li>
              <li><a href="productos.blade.php"><i class="material-icons tooltipped" data-position="bottom" data-tooltip="Productos">work</i></a></li>
              <li><a href="recetas.blade.php"><i class="material-icons tooltipped" data-position="bottom" data-tooltip="Recetas">web</i></a></li>
              <li><a href="perfil.blade.php"><i class="material-icons tooltipped" data-position="bottom" data-tooltip="Perfil">face</i></a></li>
            </ul>
          </div>
        </nav>
      </div>

      <nav class="sidenav" id="menu-responsive">
        <li><a href="inicio.blade.php"><i class="material-icons">home</i>                    Inicio </a></li>
        <li><a href="inventario.blade.php"><i class="material-icons">storage</i>             Inventario</a></li>
        <li><a href="trabajadores.blade.php"><i class="material-icons">people</i>            Trabajadores </a></li>
        <li><a href="reportes.blade.php"><i class="material-icons">trending_up</i>           Reportes</a></li>
        <li><a href="productos.blade.php"><i class="material-icons">work</i>                 Productos</a></li>
        <li><a href="recetas.blade.php"><i class="material-icons">web</i>                    Recetas </a></li>
        <li><a href="perfil.blade.php"><i class="material-icons">face</i>                    Perfil </a></li>
      </nav>
    </header>

    <main>
      <!-- NOTE: titulo de la seccion -->
      <div class=" center-align">
        <div class="section col s12 m12 l12 blue darken-2">
          <span class="flow-text white-text">Inicio</span>
        </div>
      </div>

      <ul id="tabs-swipe-demo" class="tabs tabs-fixed-width">

        <li class="tab col s6 m6 l6"><a href="#swipe-ponds">Estanques</a></li>
        <li class="tab col s6 m6 l6"><a href="#swipe-types">Tipos de peces</a></li>

      </ul>

      <div class="row">
       <div class="col s2">
         <div class="card blue-grey darken-1">
           <div class="card-content white-text">
             <span class="card-title">Card Title</span>
             <p>I am a very simple card. I am good at containing small bits of information.
             I am convenient because I require little markup to use effectively.</p>
           </div>
           <div class="card-action">
             <a href="#">This is a link</a>
             <a href="#">This is a link</a>
           </div>
         </div>
       </div>
     </div>

      <div class="row">
        <div id="swipe-ponds" class="col s12 l12 m12">
          <div class="swipe-content">
            <!-- TODO: Generar cards a partir de un scripts, es necesaria la base de datos -->
            <!-- NOTE: CARDS -->
            @yield('content');
          </div>
        </div>
      </div>
      <div class="row">
        <div id="swipe-types" class="col s12 l12 m12">
            <!-- NOTE: TIPO 1 -->
            <div class="row">
              <div class="col s12 m4 l4">
                <div class="card white">
                  <div class="card-content grey-text text-darken-4">
                    <!-- NOTE: NOMBRE DE TIPO -->
                    <span class="card-title truncate">Trucha arcoiris salmonada</span>
                    <!-- NOTE: DESCRIPCIÓN DE TIPO -->
                    <p>Descripción del tipo de trucha</p>
                  </div>
                  <div class="card-action center-align">
                    <div class="row">
                      <div class="col s6 m6 l6">
                        <a class="waves-effect waves-light btn red darken-4 center-align modal-trigger" href="#modal_delete_type"><i class="material-icons right">delete</i>Eliminar</a>
                      </div>
                      <div class="col s6 m6 l6">
                        <!-- NOTE: Se tienen que mostrar los datos del tipo en el formato y cambiar el texto del MODAL -->
                        <a class="waves-effect waves-light btn yellow darken-3 center-align modal-trigger" href="#modal_type"><i class="material-icons right">edit</i>Editar</a>
                      </div>
                    </div>

                  </div>
                </div>
              </div>
            </div>

        </div>
      </div>

        <!-- NOTE: FLOATING BUTTON -->
        <div class="fixed-action-btn">
          <a class="btn-floating btn-large  green">
            <i class="large material-icons">add</i>
          </a>
          <ul>
            <li><a class="btn-floating green darken-3 modal-trigger" href="#modal_type"><i class="material-icons">grade</i></a></li>
            <li><a class="btn-floating green darken-1 modal-trigger" href="#modal_pond"><i class="material-icons">waves</i></a></li>
          </ul>
        </div>
    </main>

    <!-- NOTE: Añadir estanque -->
    <section>
      <div id="modal_pond" class="modal bottom-sheet">
        <div class="modal-content">
          <h4 class="center-align">Nuevo estanque</h4>
          <div class="row">
            <div class="container">
              <div class="col s12">
                <!-- NOTE: NOMBRE -->
                <div class="row">
                  <div class="input-field col s12">
                    <input id="pond_name" type="text" class="validate">
                    <label for="pond_name">Nombre</label>
                    <span class="help-text">Ingrese el nombre del estanque</span>
                  </div>
                </div>
                <!-- NOTE: DESCRIPCIÓN -->
                <div class="row">
                  <div class="input-field col s12">
                    <textarea id="pond_description" class="materialize-textarea"></textarea>
                    <label for="pond_description">Descripción</label>
                    <span class="help-text">Ingrese una descripción</span>
                  </div>
                </div>
                <!-- NOTE: TIPO -->
                <div class="row">
                  <div class="input-field col s12">
                    <select>
                      <option value="" disabled selected>Seleccione un tipo</option>
                      <option value="1">Trucha arcoiris salmonada</option>
                      <option value="2">Trucha arcoiris blanca</option>
                      <option value="3">Cría no. 2.5</option>
                    </select>
                    <label>Tipo</label>
                  </div>
                </div>
                <!-- NOTE: PESO -->
                <div class="row">
                  <div class="input-field col s12">
                    <input id="pond_weight" type="number" step="0.1" min="0" class="validate">
                    <label for="pond_weight">Peso</label>
                    <span class="helper-text">Ingrese el peso en gramos</span>
                  </div>
                </div>
                <!-- NOTE: NIVEL DE AGUA -->
                <div class="row">
                  <div class="input-field col s12">
                    <input id="pond_water_lvl" type="number" step="0.1" min="0" class="validate">
                    <label for="pond_water_lvl">Nivel de agua</label>
                    <span class="helper-text">Ingrese el peso en litros</span>
                  </div>
                </div>
                <!-- NOTE: TEMPERATURA -->
                <div class="row">
                  <div class="input-field col s12">
                    <input id="pond_temperature" type="number" step="0.1" min="0" class="validate">
                    <label for="pond_temperature">Temperatura</label>
                    <span class="helper-text">Ingrese la temperatura en °C</span>
                  </div>
                </div>
                <!-- NOTE: OXÍGENO -->
                <div class="row">
                  <div class="input-field col s12">
                    <input id="pond_oxigen" type="number" step="0.1" min="0" max="100" class="validate">
                    <label for="pond_oxigen">Oxígeno</label>
                    <span class="helper-text">Ingrese el porcentaje de oxígeno</span>
                  </div>
                </div>
                <!-- NOTE: PH -->
                <div class="row">
                  <div class="input-field col s12">
                    <input id="pond_ph" type="number" step="0.1" min="0" max="14" class="validate">
                    <label for="pond_ph">PH</label>
                    <span class="helper-text">Ingrese el nivel de PH</span>
                  </div>
                </div>
                <!-- NOTE: TAMAÑO -->
                <div class="row">
                  <div class="input-field col s12">
                    <input id="pond_size" type="number" step="0.1" min="0" class="validate">
                    <label for="pond_size">Tamaño</label>
                    <span class="helper-text">Ingrese el tamaño promedio en centímetros</span>
                  </div>
                </div>
                <!-- NOTE: CANTIDAD -->
                <div class="row">
                  <div class="input-field col s12">
                    <input id="pond_quantity" type="number" step="1" min="0" class="validate">
                    <label for="pond_quantity">Cantidad</label>
                    <span class="helper-text">Ingrese la cantidad de truchas</span>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <div class="row center-align">
              <div class="col s6 m6 l6">
                <a class="waves-effect waves-light btn-flat red-text text-darken-3 modal-close">Cancelar</a>
              </div>
              <div class="col s6 m6 l6">
                <a class="waves-effect waves-light btn-flat green-text text-darken-3 modal-close" onclick="add_pond()">Añadir</a>
              </div>
          </div>
        </div>
      </div>
    </section>

    <!-- NOTE: Añadir tipo -->
    <section>
      <div id="modal_type" class="modal bottom-sheet">
        <div class="modal-content">
          <h4 class="center-align">Nuevo tipo de pez</h4>
          <div class="row">
            <div class="container">
              <div class="col s12">
                <!-- NOTE: NOMBRE -->
                <div class="row">
                  <div class="input-field col s12">
                    <input id="type_name" type="text" class="validate">
                    <label for="type_name">Nombre</label>
                    <span class="help-text">Ingrese el nombre del tipo</span>
                  </div>
                </div>
                <!-- NOTE: DESCRIPCIÓN -->
                <div class="row">
                  <div class="input-field col s12">
                    <textarea id="type_description" class="materialize-textarea"></textarea>
                    <label for="type_description">Descripción</label>
                    <span class="help-text">Ingrese una descripción</span>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <div class="row center-align">
              <div class="col s6 m6 l6">
                <a class="waves-effect waves-light btn-flat red-text text-darken-3 modal-close">Cancelar</a>
              </div>
              <div class="col s6 m6 l6">
                <a class="waves-effect waves-light btn-flat green-text text-darken-3 modal-close" onclick="add_type()">Añadir</a>
              </div>
          </div>
        </div>
      </div>
    </section>

    <!-- NOTE: Eliminar estanque -->
    <section>
      <div id="modal_delete_pond" class="modal">
          <div class="modal-content center-align">
            <h4>Advertencia</h4>
            <p>Esta acción no se puede deshacer.</p>
            <p>¿Desdea continuar?</p>
          </div>
          <div class="modal-footer">
            <a class="waves-effect waves-light btn-flat grey-text text-darken-3 modal-close">Cancelar</a>
            <a class="waves-effect waves-light btn-flat red-text text-darken-3 modal-close" onclick="delete_pond()">Eliminar</a>
          </div>
        </div>
    </section>

    <!-- NOTE: Eliminar tipo -->
    <section>
      <div id="modal_delete_type" class="modal">
          <div class="modal-content center-align">
            <h4>Advertencia</h4>
            <p>Esta acción no se puede deshacer.</p>
            <p>¿Desdea continuar?</p>
          </div>
          <div class="modal-footer">
            <a class="waves-effect waves-light btn-flat grey-text text-darken-3 modal-close">Cancelar</a>
            <a class="waves-effect waves-light btn-flat red-text text-darken-3 modal-close" onclick="delete_type()">Eliminar</a>
          </div>
        </div>
    </section>

    <!-- NOTE: FOOTER -->
    <footer class="page-footer blue darken-4">
      <p class="white-text center-align">Sistema desarrollado por DEV-T</p>
    </footer>

    <!-- NOTE: SCRIPTS -->
    <script type="text/javascript" src="./js/jquery-3.4.1.min.js"></script>
    <!-- <script type="text/javascript" src="./js/bin/materialize.min.js"></script> -->

    <script>
      document.addEventListener('DOMContentLoaded', function() {
        var elems = document.querySelectorAll('.sidenav');
        var instances = M.Sidenav.init(elems);
      });
    </script>
    <script>
      document.addEventListener('DOMContentLoaded', function() {
        M.AutoInit();
      });
      document.addEventListener('DOMContentLoaded', function() {
        var elems = document.querySelectorAll('.horizontal-fab');
        var instances = M.FloatingActionButton.init(elems, {
          direction: 'left'
        });
      });
      document.addEventListener('DOMContentLoaded', function() {
        var elems = document.querySelectorAll('.modal');
        var options = {'inDuration':250}
        var instances = M.Modal.init(elems, options);
      });
    </script>
    <!-- NOTE: DEFINIR LOS MÉTODOS DE LOS BOTONES AQUÍ Y LLAMARLOS DESDE EL BOTÓN -->
    <script>
      function add_pond(){
        // NOTE: validar en la base de datos y verificar si fue añadido o modificado
        M.toast({html: 'El estanque se ha añadido'})
      }
      function add_type(){
        // NOTE: validar en la base de datos y verificar si fue añadido o modificado
        M.toast({html: 'El tipo se ha añadido'})
      }
      function delete_type(){
        // NOTE: validar en la base de datos y verificar si fue añadido o modificado
        M.toast({html: 'El tipo se ha eliminado'})
      }
      function delete_pond(){
        // NOTE: validar en la base de datos y verificar si fue añadido o modificado
        M.toast({html: 'El estanque se ha eliminado'})
      }
    </script>
  </body>
</html>
