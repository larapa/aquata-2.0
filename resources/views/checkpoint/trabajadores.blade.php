<!DOCTYPE html>
<html lang="en" dir="ltr">

  <head>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    {!! MaterializeCSS::include_all() !!}


    <meta name="viewport" charset="utf-8" content="width=device-width, initial-scale=1.0"/>
    <title>@Usuario</title>
  </head>
  <body>
    <header>
      <div  class="navbar-fixed">
        <nav>
          <div class="nav-wrapper blue darken-3">
            <a href="inicio.blade.php" class="brand-logo">AQUATA</a>
            <a href="#" data-target="menu-responsive" class="sidenav-trigger">
              <i class="material-icons">menu</i>
            </a>
            <ul class="right hide-on-med-and-down">
              <li><a href="inicio.blade.php"><i class="material-icons tooltipped" data-position="bottom" data-tooltip="Inicio">home</i></a></li>
              <li><a href="inventario.blade.php"><i class="material-icons tooltipped" data-position="bottom" data-tooltip="Inventario">storage</i></a></li>
              <li><a href="trabajadores.blade.php"><i class="material-icons tooltipped" data-position="bottom" data-tooltip="Trabajadores">people</i></a></li>
              <li><a href="reportes.blade.php"><i class="material-icons tooltipped" data-position="bottom" data-tooltip="Reportes">trending_up</i></a></li>
              <li><a href="productos.blade.php"><i class="material-icons tooltipped" data-position="bottom" data-tooltip="Productos">work</i></a></li>
              <li><a href="recetas.blade.php"><i class="material-icons tooltipped" data-position="bottom" data-tooltip="Recetas">web</i></a></li>
              <li><a href="perfil.blade.php"><i class="material-icons tooltipped" data-position="bottom" data-tooltip="Perfil">face</i></a></li>
            </ul>
          </div>
        </nav>
      </div>

      <nav class="sidenav" id="menu-responsive">
        <li><a href="inicio.blade.php"><i class="material-icons">home</i>                    Inicio </a></li>
        <li><a href="inventario.blade.php"><i class="material-icons">storage</i>             Inventario</a></li>
        <li><a href="trabajadores.blade.php"><i class="material-icons">people</i>            Trabajadores </a></li>
        <li><a href="reportes.blade.php"><i class="material-icons">trending_up</i>           Reportes</a></li>
        <li><a href="productos.blade.php"><i class="material-icons">work</i>      Productos</a></li>
        <li><a href="recetas.blade.php"><i class="material-icons">web</i>                    Recetas </a></li>
        <li><a href="perfil.blade.php"><i class="material-icons">face</i>                    Perfil </a></li>
      </nav>
    </header>

    <main>
      <!-- NOTE: titulo de la seccion -->
      <div class=" center-align">
        <div class="section col s12 m12 l12 blue darken-2">
          <span class="flow-text white-text">Trabajadores</span>
        </div>
      </div>

      <!-- NOTE: CARDS -->
      <!-- NOTE: Cada CARD se generará por cada categoria del inventario  -->
        <div class="row">
          <!-- NOTE: PUESTO 1 -->
          <div class="col s12 m6 l4">
            <div class="card">
              <div class="card-image waves-effect waves-block waves-light">
                <img class="activator" src="images/Trabajadores/trabajador.jpg">
              </div>
              <div class="card-content">
                <!-- NOTE: NOMBRE -->
                <span class="card-title activator grey-text text-darken-4">Puesto 1<i class="material-icons right">more_vert</i></span>
                <!-- NOTE: DESCRIPCIÓN -->
                <p class="blue-text">Descripción de puesto 1</p>
              </div>
              <div class="card-reveal">
                <!-- NOTE: NOMBRE -->
                <span class="card-title grey-text text-darken-4">Puesto 1<i class="material-icons right">close</i></span>
                <div class="custom-div">
                  <!-- NOTE: COLECCIÓN -->
                  <ul class="collection">
                    <li class="collection-item avatar">
                      <i class="material-icons circle blue">person</i>
                      <span class="title">Nombre de trabajador</span>
                      <p>correo@electrónico</p>
                      <a href="#!" class="secondary-content"><i class="material-icons">info_outline</i></a>
                    </li>
                    <li class="collection-item avatar">
                      <i class="material-icons circle blue">person</i>
                      <span class="title">Nombre de trabajador</span>
                      <p>correo@electrónico</p>
                      <a href="#!" class="secondary-content"><i class="material-icons">info_outline</i></a>
                    </li>
                    <li class="collection-item avatar">
                      <i class="material-icons circle blue">person</i>
                      <span class="title">Nombre de trabajador</span>
                      <p>correo@electrónico</p>
                      <a href="#!" class="secondary-content"><i class="material-icons">info_outline</i></a>
                    </li>
                    <li class="collection-item avatar">
                      <i class="material-icons circle blue">person</i>
                      <span class="title">Nombre de trabajador</span>
                      <p>correo@electrónico</p>
                      <a href="#!" class="secondary-content"><i class="material-icons">info_outline</i></a>
                    </li>
                    <li class="collection-item avatar">
                      <i class="material-icons circle blue">person</i>
                      <span class="title">Nombre de trabajador</span>
                      <p>correo@electrónico</p>
                      <a href="#!" class="secondary-content"><i class="material-icons">info_outline</i></a>
                    </li>
                    <li class="collection-item avatar">
                      <i class="material-icons circle blue">person</i>
                      <span class="title">Nombre de trabajador</span>
                      <p>correo@electrónico</p>
                      <a href="#!" class="secondary-content"><i class="material-icons">info_outline</i></a>
                    </li>
                  </ul>
                </div>
                <div class="fixed-action-btn horizontal-fab">
                  <a class="btn-floating btn-large  blue darken-3">
                    <i class="large material-icons">more_vert</i>
                  </a>
                  <ul>
                    <li><a class="btn-floating btn-large green"><i class="material-icons">add</i></a></li>
                    <li><a class="btn-floating yellow darken-3"><i class="material-icons">edit</i></a></li>
                    <li><a class="btn-floating red"><i class="material-icons">delete</i></a></li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>

        <!-- NOTE: FLOATING BUTTON -->
        <!-- NOTE: Para agregar otra categoria -->
        <div class="fixed-action-btn">
          <a class="btn-floating btn-large waves-effect waves-light green"><i class="material-icons">add</i></a>
        </div>

    </main>

    <footer class="page-footer blue darken-4">
      <p class="white-text center-align">Sistema desarrollado por DEV-T</p>
    </footer>

    <script type="text/javascript" src="./js/bin/materialize.min.js"></script>

    <script>
      document.addEventListener('DOMContentLoaded', function() {
        var elems = document.querySelectorAll('.sidenav');
        var instances = M.Sidenav.init(elems);
      });
    </script>
    <script>
      document.addEventListener('DOMContentLoaded', function() {
        M.AutoInit();
      });
      document.addEventListener('DOMContentLoaded', function() {
        var elems = document.querySelectorAll('.horizontal-fab');
        var instances = M.FloatingActionButton.init(elems, {
          direction: 'left'
        });
      });
    </script>


  </body>
</html>
