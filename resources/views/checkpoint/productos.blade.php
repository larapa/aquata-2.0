@extends('Layouts.Menu')
@section('title','Reportes')
@section('content')
    <main>
      <!-- NOTE: titulo de la seccion -->
      <div class=" center-align">
        <div class="section col s12 m12 l12 blue darken-2">
          <span class="flow-text white-text">Productos</span>
        </div>
      </div>

      <!-- NOTE: listado de las pestañas -->
      <ul id="tabs-swipe-demo" class="tabs tabs-fixed-width">

        <li class="tab"><a href="#swipe-categories-articles">Categorias articulos</a></li>
        <li class="tab"><a href="#swipe-units-articles">Unidades articulos</a></li>
        <li class="tab"><a href="#swipe-feed-reason">Motivos Alimentacion</a></li>
        <li class="tab"><a href="#swipe-expense-category">Categorias gastos</a></li>

      </ul>

      <div class="row">
        <!-- NOTE: CATEGORIAS ARTICULOS -->
        <div id="swipe-categories-articles" class="col s12 m12 l12">
          <!-- NOTE: tabla -->
          <table class="responsive-table highlight striped centered">
            <thead>
              <th class="teal-text">Nombre</th>
              <th class="teal-text">Descripcion</th>
              <th class="teal-text">Modificar</th>
              <th class="teal-text">Eliminar</th>
            </thead>
            <tbody>
              <tr>
                <td>Limpieza</td>
                <td class="left-align">
                  <div class="left-align truncate">Articulos basicos de limpieza</div>
                </td>
                <td class="yellow-text text-darken-3"><i class="material-icons">edit</i></td>
                <td><a href="#modal_delete_categories_articles" class="waves-effect waves-teal btn-flat red-text modal-trigger" ><i class="material-icons">delete</i></a></td>
              </tr>
              <tr>
                <td>Herramientas</td>
                <td class="left-align">
                  <div class="left-align truncate">Herramientas que se usan en la granja</div>
                </td>
                <td class="yellow-text text-darken-3"><i class="material-icons">edit</i></td>
                <td><a href="#modal_delete_categories_articles" class="waves-effect waves-teal btn-flat red-text modal-trigger" ><i class="material-icons">delete</i></a></td>
              </tr>
              <tr>
                <td>Pesticidas</td>
                <td class="left-align">
                  <div class="left-align truncate">Articulso para matar plagas</div>
                </td>
                <td class="yellow-text text-darken-3"><i class="material-icons">edit</i></td>
                <td><a href="#modal_delete_categories_articles" class="waves-effect waves-teal btn-flat red-text modal-trigger" ><i class="material-icons">delete</i></a></td>
              </tr>
            </tbody>
          </table>
        </div>
        <!-- NOTE: UNIDADES ARTICULOS -->
        <div id="swipe-units-articles" class="col s12 m12 l12">
          <!-- NOTE: tabla -->
          <table class="responsive-table highlight striped centered">
            <thead>
              <th class="teal-text">Nombre unidad</th>
              <th class="teal-text">Modificar</th>
              <th class="teal-text">Eliminar</th>
            </thead>
            <tbody>
              <tr>
                <td>Gramos</td>
                <td class="yellow-text text-darken-3"><i class="material-icons">edit</i></td>
                <td><a href="#modal_delete_units_articles" class="waves-effect waves-teal btn-flat red-text modal-trigger" ><i class="material-icons">delete</i></a></td>
              </tr>
              <tr>
                <td>Mililitros</td>
                <td class="yellow-text text-darken-3"><i class="material-icons">edit</i></td>
                <td><a href="#modal_delete_units_articles" class="waves-effect waves-teal btn-flat red-text modal-trigger" ><i class="material-icons">delete</i></a></td>
              </tr>
              <tr>
                <td>Kilogramos</td>
                <td class="yellow-text text-darken-3"><i class="material-icons">edit</i></td>
                <td><a href="#modal_delete_units_articles" class="waves-effect waves-teal btn-flat red-text modal-trigger" ><i class="material-icons">delete</i></a></td>
              </tr>
            </tbody>
          </table>
        </div>
        <!-- NOTE: MOTIVOS ALIMENTACION -->
        <div id="swipe-feed-reason" class="col s12 m12 l12">
          <!-- NOTE: tabla -->
          <table class="responsive-table highlight striped centered">
            <thead>
              <th class="teal-text">Nombre</th>
              <th class="teal-text">Direccion</th>
              <th class="teal-text">Descripcion</th>
              <th class="teal-text">Modificar</th>
              <th class="teal-text">Eliminar</th>
            </thead>
            <tbody>
              <tr>
                <td>Venta</td>
                <td>Salida</td>
                <td class="left-align">
                  <div class="left-align truncate">Se vendieron costales de alimento</div>
                </td>
                <td class="yellow-text text-darken-3"><i class="material-icons">edit</i></td>
                <td><a href="#modal_delete_feed_reason" class="waves-effect waves-teal btn-flat red-text modal-trigger" ><i class="material-icons">delete</i></a></td>
              </tr>
              <tr>
                <td>Alimentar estanque</td>
                <td>Salida</td>
                <td class="left-align">
                  <div class="left-align truncate">Se le dio de comer a un estanque</div>
                </td>
                <td class="yellow-text text-darken-3"><i class="material-icons">edit</i></td>
                <td><a href="#modal_delete_feed_reason" class="waves-effect waves-teal btn-flat red-text modal-trigger" ><i class="material-icons">delete</i></a></td>
              </tr>
              <tr>
                <td>Regalo</td>
                <td>Entrada</td>
                <td class="left-align">
                  <div class="left-align truncate">Alguien nos regalo alimento</div>
                </td>
                <td class="yellow-text text-darken-3"><i class="material-icons">edit</i></td>
                <td><a href="#modal_delete_feed_reason" class="waves-effect waves-teal btn-flat red-text modal-trigger" ><i class="material-icons">delete</i></a></td>
              </tr>
              <tr>
                <td>Caduco el alimento</td>
                <td>Salida</td>
                <td class="left-align">
                  <div class="left-align truncate">El alimento se echo a perder</div>
                </td>
                <td class="yellow-text text-darken-3"><i class="material-icons">edit</i></td>
                <td><a href="#modal_delete_feed_reason" class="waves-effect waves-teal btn-flat red-text modal-trigger" ><i class="material-icons">delete</i></a></td>
              </tr>
            </tbody>
          </table>
        </div>
        <!-- NOTE: CATEGORIAS GASTOS -->
        <div id="swipe-expense-category" class="col s12 m12 l12">
          <!-- NOTE: tabla -->
          <table class="responsive-table highlight striped centered">
            <thead>
              <th class="teal-text">Nombre</th>
              <th class="teal-text">Descripcion</th>
              <th class="teal-text">Modificar</th>
              <th class="teal-text">Eliminar</th>
            </thead>
            <tbody>
              <tr>
                <td>Salarios</td>
                <td class="left-align">
                  <div class="left-align truncate">Sueldo de los empleados</div>
                </td>
                <td class="yellow-text text-darken-3"><i class="material-icons">edit</i></td>
                <td><a href="#modal_delete_expense_category" class="waves-effect waves-teal btn-flat red-text modal-trigger" ><i class="material-icons">delete</i></a></td>
              </tr>
              <tr>
                <td>Servicios</td>
                <td class="left-align">
                  <div class="left-align truncate">Electricidad, agua, internet</div>
                </td>
                <td class="yellow-text text-darken-3"><i class="material-icons">edit</i></td>
                <td><a href="#modal_delete_expense_category" class="waves-effect waves-teal btn-flat red-text modal-trigger" ><i class="material-icons">delete</i></a></td>
              </tr>
              <tr>
                <td>Propaganda</td>
                <td class="left-align">
                  <div class="left-align truncate">Anuncios</div>
                </td>
                <td class="yellow-text text-darken-3"><i class="material-icons">edit</i></td>
                <td><a href="#modal_delete_expense_category" class="waves-effect waves-teal btn-flat red-text modal-trigger" ><i class="material-icons">delete</i></a></td>
              </tr>
              <tr>
                <td>Compra de crias</td>
                <td class="left-align">
                  <div class="left-align truncate">Descripcion</div>
                </td>
                <td class="yellow-text text-darken-3"><i class="material-icons">edit</i></td>
                <td><a href="#modal_delete_expense_category" class="waves-effect waves-teal btn-flat red-text modal-trigger" ><i class="material-icons">delete</i></a></td>
              </tr>
            </tbody>
          </table>
        </div>

      </div>
        <!-- NOTE: FLOATING BUTTON -->
        <!-- NOTE: Para agregar otra categoria -->
        <div class="fixed-action-btn">
          <a class="btn-floating btn-large waves-effect waves-light green"><i class="material-icons">add</i></a>
        </div>

    </main>

    <!-- NOTE: MODALS DE ELIMINACIONES -->
    <!-- NOTE: Eliminar categoria de articulo -->
    <div id="modal_delete_categories_articles" class="modal">
      <div class="modal-content center-align">
        <h4>Advertencia</h4>
        <p>Esta acción no se puede deshacer.</p>
        <p>¿Desdea continuar?</p>
      </div>
      <div class="modal-footer">
        <a class="waves-effect waves-light btn-flat grey-text text-darken-3 modal-close">Cancelar</a>
        <a class="waves-effect waves-light btn-flat red-text text-darken-3 modal-close" onclick="delete_categories_articles()">Eliminar</a>
      </div>
    </div>

    <!-- NOTE: Eliminar unidad de articulos -->
    <div id="modal_delete_units_articles" class="modal">
      <div class="modal-content center-align">
        <h4>Advertencia</h4>
        <p>Esta acción no se puede deshacer.</p>
        <p>¿Desdea continuar?</p>
      </div>
      <div class="modal-footer">
        <a class="waves-effect waves-light btn-flat grey-text text-darken-3 modal-close">Cancelar</a>
        <a class="waves-effect waves-light btn-flat red-text text-darken-3 modal-close" onclick="delete_units_articles()">Eliminar</a>
      </div>
    </div>
    <!-- NOTE: Eliminar razon de alimentacion -->
    <div id="modal_delete_feed_reason" class="modal">
      <div class="modal-content center-align">
        <h4>Advertencia</h4>
        <p>Esta acción no se puede deshacer.</p>
        <p>¿Desdea continuar?</p>
      </div>
      <div class="modal-footer">
        <a class="waves-effect waves-light btn-flat grey-text text-darken-3 modal-close">Cancelar</a>
        <a class="waves-effect waves-light btn-flat red-text text-darken-3 modal-close" onclick="delete_feed_reason()">Eliminar</a>
      </div>
    </div>
    <!-- NOTE: Eliminar categoria de gasto -->
    <div id="modal_delete_expense_category" class="modal">
      <div class="modal-content center-align">
        <h4>Advertencia</h4>
        <p>Esta acción no se puede deshacer.</p>
        <p>¿Desdea continuar?</p>
      </div>
      <div class="modal-footer">
        <a class="waves-effect waves-light btn-flat grey-text text-darken-3 modal-close">Cancelar</a>
        <a class="waves-effect waves-light btn-flat red-text text-darken-3 modal-close" onclick="delete_expense_category()">Eliminar</a>
      </div>
    </div>


    <script>
      document.addEventListener('DOMContentLoaded', function() {
        var elems = document.querySelectorAll('.sidenav');
        var instances = M.Sidenav.init(elems);
      });
    </script>
    <script>
      document.addEventListener('DOMContentLoaded', function() {
        M.AutoInit();
      });
      document.addEventListener('DOMContentLoaded', function() {
        var elems = document.querySelectorAll('.horizontal-fab');
        var instances = M.FloatingActionButton.init(elems, {
          direction: 'left'
        });
      });
    </script>
    <script>
    function delete_categories_articles(){
      // NOTE: validar en la base de datos y verificar si fue añadido o modificado
      M.toast({html: 'La categoria de articulo se ha eliminado'})
    }
    function delete_units_articles(){
      // NOTE: validar en la base de datos y verificar si fue añadido o modificado
      M.toast({html: 'La unidad de articulo se ha eliminado'})
    }
    function delete_feed_reason(){
      // NOTE: validar en la base de datos y verificar si fue añadido o modificado
      M.toast({html: 'El motivo de alimentacion se ha eliminado'})
    }
    function delete_expense_category(){
      // NOTE: validar en la base de datos y verificar si fue añadido o modificado
      M.toast({html: 'La categoria de gasto se ha eliminado'})
    }
    </script>

@endsection
