@extends('Layouts.Menu')
@section('title','Reportes')
@section('content')

    <main>
      <!-- NOTE: titulo de la seccion -->
      <div class="center-align">
        <h5 class="grey-text">Este apartado está en desarrollo</h5>
      </div>

      <ul id="tabs-swipe-demo" class="tabs tabs-fixed-width">
        <li class="tab col s3"><a href="#swipe-ponds">Estanques</a></li>
        <li class="tab col s3"><a href="#swipe-inventory">Inventario</a></li>
        <li class="tab col s3"><a href="#swipe-economy">Economía</a></li>
      </ul>
      <div class="row">
        <div id="swipe-ponds" class="col s12 m12 l12">
          <div class="row">
            <!-- NOTE: GRÁFICA -->
            <div class="col s12 m12 l6">
              <div class="card white">
                <div class="card-content">
                  <span class="card-title">Título de la gráfica</span>
                  <p>Descripción de la gráfica</p>
                  <!-- NOTE: el id: chart_div es en donde se dibujará -->
                  <div id="chart_div" class="chart"></div>
                </div>
                <div class="card-action center-align">
                  <div class="row">
                    <div class="col s4 m4 l4">
                      <a href="#" class="waves-effect waves-light btn">Filtrar</a>
                    </div>
                    <div class="col s4 m4 l4">
                      <a href="#" class="waves-effect waves-light btn">XLSL</a>
                    </div>
                    <div class="col s4 m4 l4">
                      <a href="#" class="waves-effect waves-light btn">PDF</a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <!-- NOTE: TABLA -->
            <div class="col s12 m12 l6">
              <div class="card white">
                <div class="card-content">
                  <span class="card-title">Título de la Tabla</span>
                  <p>Descripción de la tabla</p>
                  <!-- NOTE: el id: chart_div es en donde se dibujará -->
                  <div id="chart_div_2" class="chart"></div>
                </div>
                <div class="card-action center-align">
                  <div class="row">
                    <div class="col s4 m4 l4">
                      <a href="#" class="waves-effect waves-light btn">Filtrar</a>
                    </div>
                    <div class="col s4 m4 l4">
                      <a href="#" class="waves-effect waves-light btn">XLSL</a>
                    </div>
                    <div class="col s4 m4 l4">
                      <a href="#" class="waves-effect waves-light btn">PDF</a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div class="row">
        <div id="swipe-inventory" class="col s12 m12 l12">
          Gráficas relacionadas al inventario
        </div>
      </div>

      <div class="row">
        <div id="swipe-economy" class="col s12 m12 l12">
          Gráficas relacionadas a lo económico
        </div>
      </div>

    </main>


    <script>
      document.addEventListener('DOMContentLoaded', function() {
        var elems = document.querySelectorAll('.sidenav');
        var instances = M.Sidenav.init(elems);
      });
    </script>
    <script>
      document.addEventListener('DOMContentLoaded', function() {
        M.AutoInit();
      });
    </script>

    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">
      google.load("visualization", "1", {packages:["corechart"]});
      google.charts.load('current', {'packages':['table']});

      google.setOnLoadCallback(drawCharts);
      function drawChart1() {
        var data = google.visualization.arrayToDataTable([
          ['Year', 'Sales', 'Expenses'],
          ['2004',  1000,      400],
          ['2005',  1170,      460],
          ['2006',  660,       1120],
          ['2007',  1030,      540]
        ]);

        var options = {
          title: 'Company Performance',
          hAxis: {title: 'Year', titleTextStyle: {color: 'red'}}
       };

      var chart = new google.visualization.ColumnChart(document.getElementById('chart_div'));
        chart.draw(data, options);
      }

      /* NOTE:  Para que los charts sean resposivos se debe llamar al metodo cada vez que se
                redimensione la pantalla, para volverlos a dibujar
      */


      function drawChart2() {

        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Name');
        data.addColumn('number', 'Salary');
        data.addColumn('boolean', 'Full Time Employee');
        data.addRows([
          ['Mike',  {v: 10000, f: '$10,000'}, true],
          ['Jim',   {v:8000,   f: '$8,000'},  false],
          ['Alice', {v: 12500, f: '$12,500'}, true],
          ['Bob',   {v: 7000,  f: '$7,000'},  true]
        ]);

        var table = new google.visualization.Table(document.getElementById('chart_div_2'));

        table.draw(data, {showRowNumber: true, width: '100%', height: '100%'});

      }


      function drawCharts(){
        drawChart1();
        drawChart2();
        // NOTE: drawChart2... 3... 4...
      }
      window.addEventListener("resize", drawCharts);

    </script>

@endsection
