<!DOCTYPE html>
<html lang="en" dir="ltr">

  <head>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    {!! MaterializeCSS::include_all() !!}


    <?php // TODO: Agregar un bool a los usuarios para ver si tienen el tutorial visto ?>

    <meta name="viewport" charset="utf-8" content="width=device-width, initial-scale=1.0"/>
    <title>@Usuario</title>
  </head>
  <body>
    <header>
      <div  class="navbar-fixed">
        <nav>
          <div class="nav-wrapper blue darken-3">
            <a href="inicio.blade.php" class="brand-logo">AQUATA</a>
            <a href="#" data-target="menu-responsive" class="sidenav-trigger">
              <i class="material-icons">menu</i>
            </a>
            <ul class="right hide-on-med-and-down">
              <li><a href="inicio.blade.php"><i class="material-icons tooltipped" data-position="bottom" data-tooltip="Inicio">home</i></a></li>
              <li><a href="inventario.blade.php"><i class="material-icons tooltipped" data-position="bottom" data-tooltip="Inventario">storage</i></a></li>
              <li><a href="trabajadores.blade.php"><i class="material-icons tooltipped" data-position="bottom" data-tooltip="Trabajadores">people</i></a></li>
              <li><a href="reportes.blade.php"><i class="material-icons tooltipped" data-position="bottom" data-tooltip="Reportes">trending_up</i></a></li>
              <li><a href="productos.blade.php"><i class="material-icons tooltipped" data-position="bottom" data-tooltip="Productos">work</i></a></li>
              <li><a href="recetas.blade.php"><i class="material-icons tooltipped" data-position="bottom" data-tooltip="Recetas">web</i></a></li>
              <li><a href="perfil.blade.php"><i class="material-icons tooltipped" data-position="bottom" data-tooltip="Perfil">face</i></a></li>
            </ul>
          </div>
        </nav>
      </div>

      <nav class="sidenav" id="menu-responsive">
        <li><a href="inicio.blade.php"><i class="material-icons">home</i>                    Inicio </a></li>
        <li><a href="inventario.blade.php"><i class="material-icons">storage</i>             Inventario</a></li>
        <li><a href="trabajadores.blade.php"><i class="material-icons">people</i>            Trabajadores </a></li>
        <li><a href="reportes.blade.php"><i class="material-icons">trending_up</i>           Reportes</a></li>
        <li><a href="productos.blade.php"><i class="material-icons">work</i>                 Productos</a></li>
        <li><a href="recetas.blade.php"><i class="material-icons">web</i>                    Recetas </a></li>
        <li><a href="perfil.blade.php"><i class="material-icons">face</i>                    Perfil </a></li>
      </nav>
    </header>

    <main>
      <!-- NOTE: titulo de la seccion -->
      <div class=" center-align">
        <div class="section col s12 m12 l12 blue darken-2">
          <span class="flow-text white-text">Inventario</span>
        </div>
      </div>

      <ul class="tabs tabs-fixed-width">

        <li class="tab col s4 m4 l4"><a href="#swipe-food">Alimento</a></li>
        <li class="tab col s4 m4 l4"><a href="#swipe-products">Productos</a></li>
        <li class="tab col s4 m4 l4"><a href="#swipe-articles">Articulos</a></li>

      </ul>

      <!-- NOTE: CARDS -->
      <!-- NOTE: Cada CARD se generará por cada categoria del inventario  -->
        <div class="row">
          <!-- NOTE: ALIMENTO -->
          <div id="swipe-food">
            <!-- NOTE: switch para ver los costales que tienen o no existencia en el invnetario -->
            <div class="col s12 m12 l12">
              <div class="switch center-align">
                <label>
                  <span class="teal-text">Costales en existencia</span>
                  <input type="checkbox">
                  <span class="lever"></span>
                  <span class="teal-text">Costales vacios</span>
                </label>
              </div>
            </div>
            <!-- NOTE: tabla -->
            <table class="responsive-table highlight striped centered">
              <thead>
                <tr>
                  <th class="teal-text">Etiqueta</th>
                  <th class="teal-text">Tamaño</th>
                  <th class="teal-text">Alimento restante</th>
                  <th class="teal-text">Tipo Alimento</th>
                  <th class="teal-text">Descripcion</th>
                  <th class="teal-text">Modificar</th>
                  <th class="teal-text">Eliminar</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>jkljk3m2</td>
                  <td>20</td>
                  <td>5</td>
                  <td>Alimento 2.5</td>
                  <td class="left-align">
                    <div class="left-align">Descripcion</div>
                  </td>
                  <td class="yellow-text text-darken-3"><i class="material-icons">edit</i></td>
                  <td><a href="#modal_delete_food" class="waves-effect waves-teal btn-flat red-text modal-trigger" ><i class="material-icons">delete</i></a></td>
                </tr>
                <tr>
                  <td>jhiutek3122</td>
                  <td>5</td>
                  <td>1</td>
                  <td>Alimento 3.0</td>
                  <td class="left-align">
                    <div class="left-align">Descripcion</div>
                  </td>
                  <td class="yellow-text text-darken-3"><i class="material-icons">edit</i></td>
                  <td><a href="#modal_delete_food" class="waves-effect waves-teal btn-flat red-text modal-trigger" ><i class="material-icons">delete</i></a></td>
                </tr>
                <tr>
                  <td>ds4fak3i9</td>
                  <td>10</td>
                  <td>8</td>
                  <td>Alimento 4.5</td>
                  <td class="left-align">
                    <div class="left-align">Descripcion</div>
                  </td>
                  <td class="yellow-text text-darken-3"><i class="material-icons">edit</i></td>
                  <td><a href="#modal_delete_food" class="waves-effect waves-teal btn-flat red-text modal-trigger" ><i class="material-icons">delete</i></a></td>
                </tr>
                <tr>
                  <td>mjyv74ll2h</td>
                  <td>40</td>
                  <td>10</td>
                  <td>Alimento 1.5</td>
                  <td class="left-align">
                    <div class="left-align">Descripcion</div>
                  </td>
                  <td class="yellow-text text-darken-3"><i class="material-icons">edit</i></td>
                  <td><a href="#modal_delete_food" class="waves-effect waves-teal btn-flat red-text modal-trigger" ><i class="material-icons">delete</i></a></td>
                </tr>
              </tbody>
            </table>

          </div>

          <!-- NOTE: PRODUCTOS -->
          <div id="swipe-products" class="col s12 m12 l12">
            <!-- NOTE: tabla de productos -->
            <table class="responsive-table highlight striped centered">
              <thead>
                <th class="teal-text">Nombre</th>
                <th class="teal-text">Precio</th>
                <th class="teal-text">Descripcion</th>
                <th class="teal-text">Modificar</th>
                <th class="teal-text">Eliminar</th>
              </thead>
              <tbody>
                <tr>
                  <td>Trucha Arcoíris</td>
                  <td>$50.00</td>
                  <td class="left-align">
                    <div class="left-align truncate">Descripcion</div>
                  </td>
                  <td class="yellow-text text-darken-3"><i class="material-icons">edit</i></td>
                  <td><a href="#modal_delete_product" class="waves-effect waves-teal btn-flat red-text modal-trigger" ><i class="material-icons">delete</i></a></td>
                </tr>
                <tr>
                  <td>Trucha Arcoíris Salmonada</td>
                  <td>$75.00</td>
                  <td class="left-align">
                    <div class="left-align truncate">Descripcion</div>
                  </td>
                  <td class="yellow-text text-darken-3"><i class="material-icons">edit</i></td>
                  <td><a href="#modal_delete_product" class="waves-effect waves-teal btn-flat red-text modal-trigger" ><i class="material-icons">delete</i></a></td>
                </tr>
                <tr>
                  <td>Filete Trucha Arcoíris</td>
                  <td>$100.00</td>
                  <td class="left-align">
                    <div class="left-align truncate">Descripcion</div>
                  </td>
                  <td class="yellow-text text-darken-3"><i class="material-icons">edit</i></td>
                  <td><a href="#modal_delete_product" class="waves-effect waves-teal btn-flat red-text modal-trigger" ><i class="material-icons">delete</i></a></td>
                </tr>
              </tbody>
            </table>
          </div>
          <!-- NOTE: ARTICULOS -->
          <div id="swipe-articles" class="col s12 m12 l12 ">
            <!-- NOTE: tabla -->
            <table class="responsive-table highlight striped centered">
              <thead>
                <tr>
                  <th class="teal-text">Nombre</th>
                  <!-- NOTE: concatenacion de 'cantidadUnidad' y ' ' y 'nombreUnidad' -->
                  <th class="teal-text">Unidad</th>
                  <th class="teal-text">Categoria</th>
                  <th class="teal-text">Existencia</th>
                  <th class="teal-text">Descripcion</th>
                  <th class="teal-text">Modificar</th>
                  <th class="teal-text">Eliminar</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>Insecticida</td>
                  <td>750 ml</td>
                  <td>Limpieza</td>
                  <td>5</td>
                  <td class="left-align">
                    <div class="left-align truncate">Descripcion</div>
                  </td>
                  <td class="yellow-text text-darken-3"><i class="material-icons">edit</i></td>
                  <td><a href="#modal_delete_article" class="waves-effect waves-teal btn-flat red-text modal-trigger" ><i class="material-icons">delete</i></a></td>
                </tr>
                <tr>
                  <td>Guantes</td>
                  <td>1 par</td>
                  <td>Limpieza</td>
                  <td>54</td>
                  <td class="left-align">
                    <div class="left-align">Descripcion</div>
                  </td>
                  <td class="yellow-text text-darken-3"><i class="material-icons">edit</i></td>
                  <td><a href="#modal_delete_article" class="waves-effect waves-teal btn-flat red-text modal-trigger" ><i class="material-icons">delete</i></a></td>
                </tr>
                <tr>
                  <td>Polvo para las hormigas</td>
                  <td>250 gr</td>
                  <td>Limpieza</td>
                  <td>10</td>
                  <td class="left-align">
                    <div class="left-align">Descripcion</div>
                  </td>
                  <td class="yellow-text text-darken-3"><i class="material-icons">edit</i></td>
                  <td><a href="#modal_delete_article" class="waves-effect waves-teal btn-flat red-text modal-trigger" ><i class="material-icons">delete</i></a></td>
                </tr>
              </tbody>
            </table>

          </div>

        </div>

        <!-- NOTE: FLOATING BUTTON -->
        <div class="fixed-action-btn">
          <a class="btn-floating btn-large  blue darken-3">
            <i class="large material-icons">more_vert</i>
          </a>
          <ul>
            <li><a class="btn-floating purple darken-3"><i class="material-icons">event_note</i></a></li>
            <li><a class="btn-floating green"><i class="material-icons">add</i></a></li>
          </ul>
        </div>

    </main>

    <footer class="page-footer blue darken-4">
      <p class="white-text center-align">Sistema desarrollado por DEV-T</p>
    </footer>

    <!-- NOTE: MODALS DE EDICIONES -->

    <!-- NOTE: MODALS DE ELIMINACIONES -->
    <!-- NOTE: Eliminar alimento -->
    <div id="modal_delete_food" class="modal">
      <div class="modal-content center-align">
        <h4>Advertencia</h4>
        <p>Esta acción no se puede deshacer.</p>
        <p>¿Desdea continuar?</p>
      </div>
      <div class="modal-footer">
        <a class="waves-effect waves-light btn-flat grey-text text-darken-3 modal-close">Cancelar</a>
        <a class="waves-effect waves-light btn-flat red-text text-darken-3 modal-close" onclick="delete_food()">Eliminar</a>
      </div>
    </div>
    <!-- NOTE: Eliminar producto -->
    <div id="modal_delete_product" class="modal">
      <div class="modal-content center-align">
        <h4>Advertencia</h4>
        <p>Esta acción no se puede deshacer.</p>
        <p>¿Desdea continuar?</p>
      </div>
      <div class="modal-footer">
        <a class="waves-effect waves-light btn-flat grey-text text-darken-3 modal-close">Cancelar</a>
        <a class="waves-effect waves-light btn-flat red-text text-darken-3 modal-close" onclick="delete_product()">Eliminar</a>
      </div>
    </div>

    <!-- NOTE: Eliminar articulo -->
    <div id="modal_delete_article" class="modal">
      <div class="modal-content center-align">
        <h4>Advertencia</h4>
        <p>Esta acción no se puede deshacer.</p>
        <p>¿Desdea continuar?</p>
      </div>
      <div class="modal-footer">
        <a class="waves-effect waves-light btn-flat grey-text text-darken-3 modal-close">Cancelar</a>
        <a class="waves-effect waves-light btn-flat red-text text-darken-3 modal-close" onclick="delete_article()">Eliminar</a>
      </div>
    </div>



    <script type="text/javascript" src="./js/bin/materialize.min.js"></script>

    <script>
      document.addEventListener('DOMContentLoaded', function() {
        var elems = document.querySelectorAll('.sidenav');
        var instances = M.Sidenav.init(elems);
      });
    </script>
    <script>
      document.addEventListener('DOMContentLoaded', function() {
        M.AutoInit();
      });
    </script>
    <script>
      document.addEventListener('DOMContentLoaded', function() {
        M.AutoInit();
      });
      document.addEventListener('DOMContentLoaded', function() {
        var elems = document.querySelectorAll('.horizontal-fab');
        var instances = M.FloatingActionButton.init(elems, {
          direction: 'left'
        });
      });
    </script>

    <script>
    function delete_food(){
      // NOTE: validar en la base de datos y verificar si fue añadido o modificado
      M.toast({html: 'El alimento se ha eliminado'})
    }
    function delete_product(){
      // NOTE: validar en la base de datos y verificar si fue añadido o modificado
      M.toast({html: 'El producto se ha eliminado'})
    }
    function delete_article(){
      // NOTE: validar en la base de datos y verificar si fue añadido o modificado
      M.toast({html: 'El articulo se ha eliminado'})
    }
    </script>

  </body>
</html>
