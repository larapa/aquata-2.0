<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    {!! MaterializeCSS::include_all() !!}

    <title> El Tepetate</title>
  </head>
  <body>

    <header>

      <nav>
        <div class="nav-wrapper white">
          <a href="#" class="brand-logo center black-text">EL TEPETATE</a>
          <ul id="nav-mobile" class="right hide-on-med-and-down">
            <li><a href="{{route('recipes')}}" class=" black-text">Recetas</a></li>
            <li><a href="{{route('products')}}" class=" black-text">Productos</a></li>
          </ul>
        </div>
      </nav>

    </header>

    <main>
      <div class="parallax-container">
        <div class="parallax">
          <img src="./images/Main/img10.jpg">
        </div>
      </div>
      <div class="section white">
        <div class="row container">

          <h2 class="header">¿Quiénes somos?</h2>
          <div class="row">
            <div class="col s6 m6 l6">
              <p class="grey-text text-darken-3 lighten-3" align="justify">
                Somos una empresa mexicana dedicada al cultivo y crianza de
                <span class="teal-text text-lighten-1">truchas arcoiris</span>.
                En la granaja <span> El Tepetate </span>nos encargamos de cuidar el bienestar de quienes más quieres.
                Ofrenciendo productos orgánicos ricos en proteína y omega 3 a un precio accesible para todo público.
              </p>

              <h5 class="header"> Nuestra misión</h5>

              <p align="justify">
                Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
              </p>
            </div>
            <div class="col s6 m6 l6">
              <div class="container">
                <img src="./images/Logos/Tepetates.png" style="width:100%;">
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="parallax-container">
        <div class="parallax">
          <img src="./images/Main/img2.jpg">
        </div>
      </div>
    </main>

  </body>
  <script>
    M.AutoInit();
  </script>
</html>
