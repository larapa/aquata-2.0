<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    {!! MaterializeCSS::include_all() !!}


    <meta name="viewport" charset="utf-8" content="width=device-width, initial-scale=1.0"/>
    <title>Iniciar sesión</title>
  </head>
  <body>
    <header>
      <div class="card-panel hoverable blue darken-3">
          <h4 class="center-align white-text">Bienvenido al sistema administrativo AQUATA</h4>
      </div>
    </header>
    <main class="valign-wrapper">
        <div class="row div-no-overflow">
          <div class="container">
            <form class="col s12 m8 offset-m2">
              <div class="row">
                <div class=" input-field">
                  <i class="material-icons prefix">account_circle</i>
                  <input type="email" class="validate">
                  <label for="icon-prefix">Correo electrónico</label>
                </div>
              </div>
              <div class="row">
                <div class=" input-field">
                  <i class="material-icons prefix">lock</i>
                  <input type="password" class="validate">
                  <label for="icon-prefix">Contraseña</label>
                </div>
              </div>
              <div class="center-align">
                <a href="inicio.blade.php" class="waves-effect waves-light btn-large blue darken-3">Iniciar</a>
                <p><a href="#">No puedo iniciar sesión</a></p>
              </div>
            </form>
          </div>
        </div>
    </main>

    <footer class="page-footer blue darken-4">
      <p class="white-text center-align">Sistema desarrollado por DEV-T</p>
    </footer>
  </body>
</html>
