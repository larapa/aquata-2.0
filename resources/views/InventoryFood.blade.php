@extends('Layouts.Menu')
@section('head','usuario')
@section('title','Inventario')
@section('content')

  <ul class="tabs tabs-fixed-width">

    <li class="tab col s4 m4 l4"><a class="active" href="#swipe-food">Alimento</a></li>
    <li class="tab col s4 m4 l4"><a target="_self"  href="{{route('Inventory.Articles')}}">Articulos</a></li>

    <li class="tab col s4 m4 l4"><a target="_self" href="{{route('Inventory.Products')}}">Productos</a></li>

  </ul>

    <div id="swipe-food">
      <!-- Aqui es el panel de control -->
      <div class="row">
        <div class="col s12 m12 l4">
          <div class="card white">
            <div class="card-content blue-text text-darken-2">
              <span class="card-title">Panel de control</span>
              <div class="row">
                <div class="col s8 m8 l8">
                  ¿Mostrar costales vacios?
                </div>
                <div class="col s4 m4 l4">
                  <div class="switch right-align">
                   <label>
                     <input type="checkbox" id="bags-switch">
                     <span class="lever" id="bags-lever"></span>
                   </label>
                 </div>
                </div>
              </div>
              <ul class="collapsible popout">
                <li>
                  <div class="collapsible-header"><i class="material-icons">restaurant</i>Tipos de alimento</div>
                  <div class="collapsible-body">
                    <div class="row">
                      <div class="col s8 m8 l8">
                        ¿Mostrar tipos inactivos?
                      </div>
                      <div class="col s4 m4 l4">
                        <div class="switch right-align">
                         <label>
                           <input type="checkbox" id="types-switch" >
                           <span class="lever" id="types-lever"></span>
                         </label>
                       </div>
                      </div>
                    </div>
                    <!-- ver los tipos de alimento -->
                      <ul class="collection" id="types-collection">
                        @foreach($types as $type)
                          @if($type->T == '1')
                            <li class="collection-item avatar usefull-type">
                              <div class="row">
                                <div class="col s10">
                                  <i class="material-icons circle blue">restaurant</i>
                                  <span class="title">{{$type->nombre}}</span>
                                  <p class="grey-text truncate">{{$type->descripcion}}</p>
                                </div>
                                <div class="s2">
                                  <a class="secondary-content dropdown-trigger btn-flat teal-text" data-target="TYPE-{{$type->id}}"><i class="material-icons right">arrow_drop_down</i></a>
                                </div>
                              </div>
                            </li>
                          @else
                            <li class="collection-item avatar useless-type">
                              <div class="row">
                                <div class="col s10">
                                  <i class="material-icons circle indigo lighten-4">restaurant</i>
                                  <span class="title">{{$type->nombre}}</span>
                                  <p class="grey-text truncate">{{$type->descripcion}}</p>
                                </div>
                                <div class="col s2">
                                  <a class="secondary-content dropdown-trigger btn-flat teal-text" data-target="TYPE-{{$type->id}}"><i class="material-icons right">arrow_drop_down</i></a>
                                </div>
                              </div>
                            </li>
                          @endif
                        @endforeach
                      </ul>
                    <div class="row">
                      <div class="col s12 m12 l12 center-align">
                        <br>
                        <a class="waves-effect waves-light btn-small teal" href="{{ route('Type.Add') }}"><i class="material-icons right">add</i>Agregar tipo</a>
                      </div>
                    </div>
                  </div>
                </li>
                <li>
                  <div class="collapsible-header"><i class="material-icons">archive</i>Motivos de uso</div>
                  <div class="collapsible-body">
                    <div class="row">
                      <div class="col s8 m8 l8">
                        ¿Mostrar motivos inactivos?
                      </div>
                      <div class="col s4 m4 l4">
                        <div class="switch right-align">
                         <label>
                           <input type="checkbox" id="motives-switch">
                           <span class="lever" id="motives-lever"></span>
                         </label>
                       </div>
                      </div>
                    </div>
                    <!-- ver los tipos de alimento -->
                      <ul class="collection">
                        @foreach($motives as $motive)
                          @if($motive->T == '1')
                            <li class="collection-item avatar usefull-motive">
                              <div class="row">
                                <div class="col s10">
                                  <i class="material-icons circle blue">archive</i>
                                  <span class="title">{{$motive->nombre}}</span>
                                  <p class="grey-text truncate">{{$motive->descripcion}}</p><br>
                                </div>
                                <div class="col s2">
                                  <a class="secondary-content dropdown-trigger btn-flat teal-text" data-target="MOTIVE-{{$motive->id}}"><i class="material-icons right">arrow_drop_down</i></a>
                                </div>
                              </div>
                            </li>
                          @else
                            <li class="collection-item avatar useless-motive">
                              <div class="row">
                                <div class="col s10">
                                  <i class="material-icons circle indigo lighten-4">archive</i>
                                  <span class="title">{{$motive->nombre}}</span>
                                  <p class="grey-text truncate">{{$motive->descripcion}}</p><br>
                                </div>
                                <div class="col s2">
                                  <a class="secondary-content dropdown-trigger btn-flat teal-text" data-target="MOTIVE-{{$motive->id}}"><i class="material-icons right">arrow_drop_down</i></a>
                                </div>
                              </div>
                            </li>
                          @endif
                        @endforeach
                      </ul>
                    <div class="row">
                      <div class="col s12 m12 l12 center-align">
                        <br>
                        <a class="waves-effect waves-light btn-small teal white-text" href="{{ route('Motive.Add') }}"><i class="material-icons right">add</i>Agregar motivo</a>
                      </div>
                    </div>
                  </div>
                </li>
              </ul>
            </div>
            <div class="card-action center-align">
              <a class="waves-effect waves-light btn-small teal white-text" href="{{route('Bag.Add')}}"><i class="material-icons right">add</i>Agregar costal</a>
            </div>
          </div>
        </div>

        <div class="col s12 m12 l8 center-align">
          <h4>Inventario Alimento</h4>
          <table class=" highlight centered responsive-table">
            <thead>
              <tr>
                <th>Etiqueta</th>
                <th>Total (gramos)</th>
                <th>Restante (gramos)</th>
                <th>Tipo</th>
                <th class="hide-on-med-and-down"> </th>
                <th class="hide-on-med-and-down"> </th>
                <th class="hide-on-large-only">Editar</th>
                <th class="hide-on-large-only">Eliminar</th>
              </tr>
            </thead>
            <tbody id="bags-table">
              @foreach($bags as $bag)
                @if($bag->alimentoRestante > '0')
                  <tr id="BAG-{{$bag->id}}" class="bag">
                    <td id="TAG-BAG-{{$bag->id}}">{{$bag->etiqueta}}</td>
                    <td id="SIZE-BAG-{{$bag->id}}">{{$bag->tamano}}</td>
                    <td id="REMAINING-BAG-{{$bag->id}}">{{$bag->alimentoRestante}}</td>
                    <td id="TYPE-BAG-{{$bag->id}}">{{$bag->nombre}}</td>
                    <td id="EDIT-BAG-{{$bag->id}}"><a id="EDIT-BAG-{{$bag->id}}" class="waves-effect waves-light" href="{{ route('Bag.Edit', $bag->id ) }}"><i class="material-icons">edit</i></td>
                    <td id="DELETE-BAG-{{$bag->id}}"><a id="DELETE-BAG-{{$bag->id}}" class="waves-effect waves-light" href="{{ route('Bag.Delete', $bag->id ) }}"><i class="material-icons">delete</i></td>
                  </tr>
                @else
                  <tr id="BAG-{{$bag->id}}" class="bag grey-text text-lighten-2 useless-bag">
                    <td id="TAG-BAG-{{$bag->id}}">{{$bag->etiqueta}}</td>
                    <td id="SIZE-BAG-{{$bag->id}}">{{$bag->tamano}}</td>
                    <td id="REMAINING-BAG-{{$bag->id}}">{{$bag->alimentoRestante}}</td>
                    <td id="TYPE-BAG-{{$bag->id}}">{{$bag->nombre}}</td>
                    <td id="EDIT-BAG-{{$bag->id}}"><a id="EDIT-BAG-{{$bag->id}}" class="grey-text" ><i class="material-icons">edit</i></td>
                    <td id="DELETE-BAG-{{$bag->id}}"><a id="DELETE-BAG-{{$bag->id}}" class="grey-text" ><i class="material-icons">delete</i></td>
                  </tr>
                @endif
              @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>

    <div class="pages center-align">
      <p>{{$bags->links()}}</p>
    </div>


@endsection

@section('modals')
  <div id="test" class="modal modal-fixed-footer">
    <div class="modal-content center-align">

    </div>
    <div class="modal-footer">

    </div>
  </div>
  <!-- Definir las rutas -->
  @foreach($types as $type)
    @if($type->T == '1')
      <ul id='TYPE-{{$type->id}}' class='dropdown-content'>
          <li><a id='TYPE-{{$type->id}}' href="#!" class="black-text TYPE"><i class="material-icons left blue-text">error_outline</i>INFO</a></li>
          <li><a href="{{route('Type.Edit', $type->id)}}" class="black-text"><i class="material-icons left blue-text">edit</i>Editar</a></li>
          <li><a href="{{ route('Type.Delete', $type->id ) }}" class="black-text"><i class="material-icons left blue-text">delete</i>Eliminar</a></li>
      </ul>
    @else
      <ul id='TYPE-{{$type->id}}' class='dropdown-content'>
        <li><a id='TYPE-{{$type->id}}' href="#!" class="black-text TYPE"><i class="material-icons left blue-text">error_outline</i>INFO</a></li>
        <li><a href="{{ route('Type.Active', $type->id ) }}" class="black-text"><i class="material-icons left blue-text">cached</i>Activar</a></li>
      </ul>
    @endif
    <input type="hidden" name="" value="{{$type->id}}" id="type-id-{{$type->id}}">
    <input type="hidden" name="" value="{{$type->nombre}}" id="type-nombre-{{$type->id}}">
    <input type="hidden" name="" value="{{$type->descripcion}}" id="type-descripcion-{{$type->id}}">
    <input type="hidden" name="" value="{{$type->T}}" id="type-T-{{$type->id}}">
  @endforeach

  @foreach($motives as $motive)
    @if($motive->T == '1')
    <ul id='MOTIVE-{{$motive->id}}' class='dropdown-content'>
        <li><a id='MOTIVE-{{$motive->id}}' href="#!" class="black-text MOTIVE"><i class="material-icons left blue-text">error_outline</i>INFO</a></li>
        @if ($motive->id > 4)
          <li><a href="{{route('Motive.Edit', $motive->id)}}" class="black-text"><i class="material-icons left blue-text">edit</i>Editar</a></li>
          <li><a href="{{ route('Motive.Delete', $motive->id ) }}" class="black-text"><i class="material-icons left blue-text">delete</i>Eliminar</a></li>
        @endif
    </ul>
    @else
      <ul id='MOTIVE-{{$motive->id}}' class='dropdown-content'>
        <li><a id='MOTIVE-{{$motive->id}}' href="#!" class="black-text MOTIVE"><i class="material-icons left blue-text">error_outline</i>INFO</a></li>
        <li><a href="{{ route('Motive.Active', $motive->id ) }}" class="black-text"><i class="material-icons left blue-text">cached</i>Activar</a></li>
      </ul>
    @endif
    <input type="hidden" name="" value="{{$motive->id}}" id="motive-id-{{$motive->id}}">
    <input type="hidden" name="" value="{{$motive->nombre}}" id="motive-nombre-{{$motive->id}}">
    <input type="hidden" name="" value="{{$motive->descripcion}}" id="motive-descripcion-{{$motive->id}}">
    <input type="hidden" name="" value="{{$motive->direccion}}" id="motive-direccion-{{$motive->id}}">
    <input type="hidden" name="" value="{{$motive->T}}" id="motive-T-{{$motive->id}}">
  @endforeach

@endsection

@section('scripts')
  <script type="text/javascript" id="scripts-alimento">
    $(document).ready(function() {
      @if(session('success'))
         M.toast({html: '{{session('success')}}',classes:'green darken-2'})
      @endif
      @if(session('non-success'))
         M.toast({html: '{{session('non-success')}}',classes:'red darken-2'})
      @endif
      @if(session('error'))
         red_toast("{{session('error')}}");
      @endif
      //ocultar los inútiles
      $('.useless-type').hide();
      $('.useless-motive').hide();
      $('.useless-bag').hide();
      //los métodos para mostrar o no a los inútiles
      $('#types-lever').click(function(){
         if ($("#types-switch").prop("checked") == true) {
           $('.useless-type').hide();
           $('.usefull-type').show();
         }
         else{
           $('.useless-type').show();
           $('.usefull-type').hide();
         }
       });

       $('#motives-lever').click(function(){
          if ($("#motives-switch").prop("checked") == true) {
            $('.useless-motive').hide();
            $('.usefull-motive').show();
          }
          else{
            $('.useless-motive').show();
            $('.usefull-motive').hide();
          }
        });

      $('#bags-lever').click(function(){
           if ($("#bags-switch").prop("checked") == true) {
             $('.useless-bag').hide();
           }
           else{
            $('.useless-bag').show();
           }
       });

       //poner la palanca de los costales en ON
       $('#bags-lever').click();

       $('.MOTIVE').click(function(){
        $id = $(this).attr("id");
        $id = $id.substring(7,$id.lenght);
        $nombre = $('#motive-nombre-'+$id).attr("value");
        $descripcion = $('#motive-descripcion-'+$id).attr("value");
        $t = $('#motive-T-'+$id).attr("value");
        if($t == '0'){
          $t = "Deshabilitado";
        }
        else {
          $t = "Habilitado";
        }
        $direccion = $('#motive-direccion-'+$id).attr("value");
        if($direccion == '0'){
          $direccion = "Salida";
        }
        else {
          $direccion = "Entrada";
        }

        motiveInfo($id,$nombre,$descripcion,$t,$direccion);
      });

      $('.TYPE').click(function(){
       $id = $(this).attr("id");
       $id = $id.substring(5,$id.lenght);
       $nombre = $('#type-nombre-'+$id).attr("value");
       $descripcion = $('#type-descripcion-'+$id).attr("value");
       $t = $('#type-T-'+$id).attr("value");
       if($t == '0'){
         $t = "Inactivo";
       }
       else {
         $t = "Activo";
       }

       typeInfo($id,$nombre,$descripcion,$t);
     });

  function typeInfo($id,$nombre,$descripcion,$t){
    $('.modal-content').empty();
    $('.modal-content').append(
      "<h3>INFO  <i class='material-icons small teal-text'>info_outline</i></h3>"+
      "<div class='divider'></div>"+
      "<p class='grey-text'>Tipo "+$t+"</p>"+
      "<div class='divider'></div>"+
      "<table class='striped'>"+
        "<tbody>"+
          "<tr>"+
            "<th class='center-align'>Nombre:</th>"+
            "<td>"+$nombre+"</td>"+
          "</tr>"+
          "<tr>"+
            "<th class='center-align'>Descripción:</th>"+
            "<td>"+$descripcion+"</td>"+
          "</tr>"+
        "</tbody>"+
      "</table>"
    );
    $('.modal-footer').empty();
    $('.modal-footer').append(
      "<a class='waves-effect waves-light btn-flat grey-text text-darken-3 modal-close'>Cerrar</a>"
    );
    $('#test').modal('open');
  }

  function motiveInfo($id,$nombre,$descripcion,$t,$direccion){
    $('.modal-content').empty();
    $('.modal-content').append(
      "<h3>INFO  <i class='material-icons small teal-text'>info_outline</i></h3>"+
      "<div class='divider'></div>"+
      "<p class='grey-text'>Motivo "+$t+"</p>"+
      "<div class='divider'></div>"+
      "<table class='striped'>"+
        "<tbody>"+
          "<tr>"+
            "<th class='center-align'>Nombre:</th>"+
            "<td>"+$nombre+"</td>"+
          "</tr>"+
          "<tr>"+
            "<th class='center-align'>Descripción:</th>"+
            "<td>"+$descripcion+"</td>"+
          "</tr>"+
          "<tr>"+
            "<th class='center-align'>Tipo:</th>"+
            "<td>"+$direccion+"</td>"+
          "</tr>"+
        "</tbody>"+
      "</table>"
    );
    $('.modal-footer').empty();
    $('.modal-footer').append(
      "<a class='waves-effect waves-light btn-flat grey-text text-darken-3 modal-close'>Cerrar</a>"
    );
    $('#test').modal('open');
  }


       @if (session('message-activate-type'))
        test("{{session('message-activate-type')}}");
       @endif

       @if (session('message-delete-type'))
        test("{{session('message-delete-type')}}");
       @endif

       @if (session('message-edit-type'))
        test("{{session('message-edit-type')}}");
       @endif

       @if (session('message-activate-motive'))
        test("{{session('message-activate-motive')}}");
       @endif

       @if (session('message-delete-motive'))
        test("{{session('message-delete-motive')}}");
       @endif

       @if (session('message-edit-motive'))
        test("{{session('message-edit-motive')}}");
       @endif

       @if (session('message-edit-bag'))
        test("{{session('message-edit-bag')}}");
       @endif

       @if (session('message-delete-bag'))
        test("{{session('message-delete-bag')}}");
       @endif

       @if (session('message-add-type'))
        test("{{session('message-add-type')}}");
       @endif

       @if (session('message-add-motive'))
        test("{{session('message-add-motive')}}");
       @endif

     });

     function test($info){
       M.toast({html: $info, classes: 'green darken-2'})
     }

     function test2($info){
       M.toast({html: $info, classes: 'red darken-2'})
     }

    //no tocar, es para que funcionen los dropdown
    document.addEventListener('DOMContentLoaded', function() {
      var elems = document.querySelectorAll('.dropdown-trigger');
      var options =
        {
          constrainWidth:false,
          alignment:'right'
        };
      var instances = M.Dropdown.init(elems, options);
    });

  </script>
@endsection
