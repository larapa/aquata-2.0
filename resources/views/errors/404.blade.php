<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <script type="text/javascript" src="/js/jquery-3.4.1.min.js"></script>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    {!! MaterializeCSS::include_all() !!}
    <meta name="viewport" charset="utf-8" content="width=device-width, initial-scale=1.0"/>
    <title>AQUATA</title>
  </head>
  <body>
    <div class="" style="width:100%; top:30%; display: block; position: absolute;">
      <div class="row center-align">
        <img class="hide-on-med-and-down" src="/images/Logos/DevT/Racoon_black.png" width="15%">
        <img class="hide-on-large-only" src="/images/Logos/DevT/Racoon_black.png" width="40%">
        <h2>Lo sentimos, la página que buscas no existe</h2>
      </div>
    </div>
  </body>
</html>
