@extends('Layouts.Menu')
@section('title','Desastre')
@section('content')

  <form action="{{route('InventoryProduct.DisasterValidation')}}" method="POST">
    <input type="hidden" name="usuario" value="1">
    <input type="hidden" name="id" value="{{$inventoryProduct->id}}">
    <input type="hidden" name="idInventario" value="{{$inventoryProduct->idInventario}}">
    @csrf

    <h4 class="center-align">Ha ocurrido un desastre </h4>
    <p class="center-align"><i class="material-icons medium ">mood_bad</i></p>

    <div class="row">
      <div class="container">
        <div class="col s12">
          <!-- NOTE: Estanque -->
          <div class="row">
            <div class="col s12 m4 l4 center-align">
              <h4><i class="material-icons">help_outline</i> INFO</h4>

            </div>
            <div class="col s12 m8 l8">
              <p  ALIGN="justify">Lamentamos escuchar que tus productos hayan perecido,
                 haremos un registro de este suceso en tu historial de ventas, hacemos esto con
                 el fin de que puedas llevar un mejor control de tus gastos y ventas. Por favor, indica la
                 cantidad de piezas que perecieron y el peso promedio de estas.
              </p>
            </div>
          </div>
        </div>
        <div class="center-align">
          <div class="row">
            <div class="input-field col s12 m12 l8">
              <input disabled value="{{$info->producto}}" id="estanque" type="text">
              <label for="estanque">Producto</label>
            </div>
            <div class="input-field col s12 m12 l4">
              <input disabled value="{{$inventory->cantidadElementos}}" id="estanque" type="text">
              <label for="estanque">Disponibles</label>
            </div>
          </div>
          <div class="row">
            <div class="input-field col s12 m12 l6 " id='cantidad-div'>
                <input name="cantidad" id="cantidad" type="number" step="1" min="0" max="{{$inventory->cantidadElementos}}" class="validate" value="" required="" aria-required="true">
              <label for="cantidad">Cantidad</label>
              <span class="helper-text" id="helper-cantidad">Ingrese la cantidad de elementos</span>
            </div>

            <div class="input-field col s12 m12 l6" id='cantidad-div'>
                <input name="peso" id="peso" type="number" step="1" min="0" class="validate" required="required">
              <label for="peso">Peso promedio</label>
              <span class="helper-text" id="helper-cantidad">Ingrese el peso promedio en gramos</span>
            </div>
          </div>
          <button type="submit" class="waves-effect waves-light btn blue darken-3 center-align">Aceptar</button>
        </div>
      </div>
    </div>
  </form>
@endsection
<?php // TODO: poener max cuando el switch de cancelación esté encendido ?>
@section('scripts')
  <script type="text/javascript">
    $(document).ready(function() {

      $('#cancelation-lever').click(function(event) {
        if($('#cancelacion').prop("checked") == false){
          $('#cantidad').attr({'max':'{{$inventory->cantidadElementos}}'});
          $('#cantidad').val('');
          $('#helper-cantidad').empty();
          $('#helper-cantidad').append('Ingrese la cantidad a cancelar');
        }
        else {
          $('#cantidad').attr({'max':''});
          $('#cantidad').val('');
          $('#helper-cantidad').empty();
          $('#helper-cantidad').append('Ingrese la cantidad a añadir');
        }
      });

    });

  </script>
@endsection
