@extends('Layouts.Menu')
@section('title','Nuevo gasto')
@section('content')


  <form action="{{route('Expense.Add')}}" method="POST">
    @csrf

    <h4 class="center-align">Registrar Gasto</h4>
    <br>
    <div class="row">
      <div class="container">
        <div class="col s12">
          <div class="row">
            <div class="col s12 m4 l4 center-align">
              <h4><i class="material-icons">help_outline</i> INFO</h4>

            </div>
            <div class="col s12 m8 l8">
              <p  ALIGN="justify">El correcto registro de los gastos respresenta una parte importante del adecuado funcionamiento
                del sistema. Cada gasto se registrará con la fecha y hora del día actual, además del usuario que lo registro.
              </p>
              <p  ALIGN="justify">
                Tenga en cuenta que una vez registrado un gasto este <span class="red-text text-darken-2"> no se podrá modificar</span>.
              </p>
            </div>
          </div>
          <!-- NOTE: CONCEPTO -->
          <div class="row">
            <div class="input-field col s12">
              <textarea id="concepto" class="validate materialize-textarea" name="concepto" type="text" data-length="120" maxlength="120" required="" aria-required="true"></textarea>
              <label for="concepto">Concepto</label>
              <span class="help-text" data-error="Incorrecto" data-success="Correcto">Ingrese el concepto</span>
            </div>
          </div>
          <!-- NOTE: MONTO -->
          <div class="row">
            <div class="input-field col s12">
              <input id="monto" name="monto" type="number" step="0.01" min="0.01" max="9999999.99" required="" aria-required="true" class="validate">
              <label for="monto">Monto</label>
              <span class="help-text" data-error="Incorrecto" data-success="Correcto">Ingrese el monto total</span>
            </div>
          </div>
          <!-- NOTE: CATEGORIA -->
          <div class="row">
            <div class="input-field col s12">
              <select id="select_category" name="idCategoriaGasto">
                <option id="first_select" value="" selected disabled>Seleccione una categoria</option>
                  @if(!empty($categorys))
                    @foreach($categorys as $category)
                      @if($category->T == '1')
                          <option value="{{ $category->id }}">{{ $category->nombre }}</option>
                      @endif
                    @endforeach
                  @endif
                </select>
                <label data-error="Escoge una opcion">Tipo</label>
                <span id="error-select" class="red-text darken-2"></span>
            </div>
          </div>
          <!-- NOTE: FECHA-->
          <div class="row">
            <div class="input-field col s12">
              <input id="fecha" name="fecha" type="text" class="datepicker" disabled>
              <label for="fecha">Fecha</label>
              <span class="help-text" data-error="Incorrecto" data-success="Correcto">El gasto se registrará con la fecha actual</span>
            </div>
          </div>
        </div>
        <div class="center-align">
          <button type="submit" class="waves-effect waves-light btn blue darken-3 center-align">Aceptar</button>
        </div>
      </div>
    </div>
  </form>

@endsection

@section('scripts')
  <script type="text/javascript">

    $(document).ready(function() {
      $('input, textarea').characterCounter();

      $('#fecha').datepicker({
        format: 'dd/mm/yyyy',
        setDefaultDate: true,
        defaultDate: new Date(),
        i18n: {
          months: ['Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'],
          monthsShort: ['Ene','Feb','Mar','Abr','May','Jun','Jul','Ago','Sep','Oct','Nov','Dic'],
          weekdays: ['Domingo','Lunes','Martes','Miércoles','Jueves','Viernes','Sábado'],
          weekdaysShort: ['Dom','Lun','Mar','Mier','Jue','Vier','Sab'],
          weekdaysAbbrev: ['D','L','M','M','J','V','S']
        }
      });

      $( "Form" ).submit(function( event ) {
        if ( $( "select" ).first().val() != null ) {
          return;
        }
        $("#error-select").html("Seleccione una categoria");
        M.toast({html: 'Seleccione una categoria',classes:'red darken-3'});
        event.preventDefault();
      });

    });

  </script>
@endsection
