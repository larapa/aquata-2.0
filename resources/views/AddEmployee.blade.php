@extends('Layouts.Menu')

@section('title','Nuevo empleado')

@section('content')
  <form action="{{ route('Employee.Add') }}" method="POST" enctype="multipart/form-data">
    @csrf

    <h4 class="center-align">Añadir empleado</h4>

    <div class="row">
      <div class="container">
        <div class="col s12">
          <div class="row">
            <div class="col s12 m4 l4 center align">
              <h4><i class="material-icons">help_outline</i> INFO</h4>
            </div>
            <div class="col s12 m8 l8">
              <p  ALIGN="justify">
                En este apartado puedes agregar un <span class="blue-text">empleado</span>, esto significa que haz contratado a personal nuevo.
                Es importante que ingreses sus datos correctamente, además podrás seleccionar si tu empleado tiene o no acceso al
                <span class="blue-text">Sistema Administrativo AQUATA</span> asignandole un usario.
                <p>Podrás ver tus empleados en el apartado <span class="blue-text">Trabajadores</span>.</p>
              </p>
            </div>
          </div>
          <!-- NOTE: NOMBRE -->
          <div class="row">
            <div class="input-field col s12">
              <input name="nombre" id="employee_name" type="text" data-length="30" maxlength="30" required="required" aria-required="true" class="validate">
              <label for="employee_name">Nombre</label>
              <span class="help-text">Ingrese el nombre del empleado</span>
            </div>
          </div>
          <!-- NOTE: APELLIDO PATERNO -->
          <div class="row">
            <div class="input-field col s12">
              <input name="aPaterno" id="employee_aPat" type="text" data-length="20" maxlength="20" required="required" aria-required="true" class="validate">
              <label for="employee_aPat">Apellido Paterno</label>
              <span class="help-text">Ingrese el apellido paterno del empleado</span>
            </div>
          </div>
          <!-- NOTE: APELLIDO MATERNO -->
          <div class="row">
            <div class="input-field col s12">
              <input name="aMaterno" id="employee_aMat" type="text" data-length="20" maxlength="20" required="required" aria-required="true" class="validate">
              <label for="employee_aMat">Apellido Materno</label>
              <span class="help-text">Ingrese el apellido materno del empleado</span>
            </div>
          </div>
          <!-- NOTE: TELEFONO -->
          <div class="row">
            <div class="input-field col s12">
              <input name="telefono" id="employee_phone" type="text" data-length="10" maxlength="10" required="required" aria-required="true" class="validate">
              <label for="employee_phone">Telefono</label>
              <span class="help-text">Ingrese el telefono del empleado</span>
            </div>
          </div>
          <!-- NOTE: CORREO ELECTRONICO -->
          <div class="row">
            <div class="input-field col s12">
              <input name="email" id="employee_email" type="email" data-length="45" maxlength="45" required="required" aria-required="true" class="validate">
              <label for="employee_email">Correo electronico</label>
              <span class="help-text">Ingrese el correo electronico del empleado</span>
            </div>
          </div>
          <!-- NOTE: SUELDO -->
          <div class="row">
            <div class="input-field col s12">
              <input name="sueldo" id="employee_salary" type="number" data-length="10" maxlength="10" step="0.01" required="required" aria-required="true" class="validate">
              <label for="employee_salary">Sueldo</label>
              <span class="help-text">Ingrese el sueldo del empleado</span>
            </div>
          </div>
          <!-- NOTE: PUESTO -->
          <div class="row">
            <div class="input-field col s12">
              <label>Puesto</label>
              <br>
              <!-- NOTE: select con todos los puestos (para elegir 1) -->
              <div class="input-field col s12">
                <select name="idPuesto">
                  <option value="" disabled selected>Elija el puesto del empleado</option>
                  @foreach($jobs as $job)
                    @if($job->id == '1')
                      <?php continue; ?>
                    @endif
                    <option value="{{$job->id}}">{{$job->nombre}}</option>
                  @endforeach
                </select>
              </div>
              <label data-error="Escoje una opcion">Puesto</label>
              <span id="error-select" class="red-text darken-2"></span>
            </div>
          </div>
          <!-- NOTE: FOTOGRAFIA DEL EMPLEADO -->
          <div class="row">
            <div class="file-field input-field">
              <div class="btn teal">
                <span>Subir imagen</span>
                <input type="file" name="imagen" accept="image/jpeg,image/jpg,image/png">
              </div>
              <div class="file-path-wrapper">
                <input type="text" class="file-path validate" required="required" aria-required="true" accept="image/gif,image/jpeg,image/jpg,image/png">
              </div>
            </div>
          </div>
          <!-- NOTE: POR SI ES EMPLEADO -->
          <div class="row">
            <div class="col s3 left-align switch">
              <br>
              <label>
                ¿Agregar usuario?
                <input name="activo" type="checkbox" id='user-switch'>
                <span class="lever" id='user-lever'></span>
              </label>
            </div>
          </div>

            <div id="quitarCampo">

              <div class="row">
                <div class="col s12">
                  <span class="blue-text">El usuario podrá acceder al sistema con su email</span>
                </div>
              </div>

              <div class="row">
                <div class="input-field col s12">
                  <input name="password" value="" id="password" type="text" data-length="255" maxlength="255">
                  <label for="password">Contraseña</label>
                  <span class="helper-text">Ingrese la contraseña del usuario</span>
                </div>
              </div>

              <div class="row">
                <div class="input-field col s12">
                  <!-- NOTE: select con todos los puestos (para elegir 1) -->
                  <select id="select_rol" name="idRol">
                    <option id="first_select_rol" value="" disabled selected>Elija el tipo de usuario</option>
                    @foreach($roles as $rol)
                      <option value="{{$rol->id}}">{{$rol->nombre}}</option>
                    @endforeach
                  </select>
                  <span id="error-select" class="red-text darken-2"></span>
                </div>
              </div>
            </div>

          <div class="center-align">
            <button type="submit" class="waves-effect waves-light btn blue darken-3 center-align">Aceptar</button>
          </div>
        </div>
      </div>
    </div>
  </form>
@endsection

@section('scripts')
  <script>
    $(document).ready(function() {
      $('#employee_name,'+
        '#employee_aPat,'+
        '#employee_aMat,'+
        '#employee_phone,'+
        '#employee_email,'+
        '#password').characterCounter();

        $( "Form" ).submit(function( event ) {
          if ( $( "select" ).first().val() != null ) {
            return;
          }
          $("#error-select").html("Seleccione un puesto para el empleado");
          red_toast('Seleccione un puesto para el empleado');
          event.preventDefault();
        });

        $('#quitarCampo').hide();

        $('#user-lever').click(function(){
           if ($("#user-switch").prop("checked") == false) {
             $('#quitarCampo').show();
           }
           else{
             $('#quitarCampo').hide();
           }
         });
       });


  </script>
@endsection
