@extends('Layouts.Menu')

@section('title','Editar empleado')

@section('content')

  <form action="{{ route('Employee.Update') }}" method="POST" enctype="multipart/form-data">
    @csrf
    <input type="hidden" name="id" value="{{$employee->id}}">
    <h4 class="center-align">Editar: {{$employee->nombre}} {{$employee->aPaterno}} {{$employee->aMaterno}}</h4>

    <div class="row">
      <div class="container">
        <div class="col s12">
          <div class="row">
            <div class="col s12 m4 l4 center align">
              <h4><i class="material-icons">help_outline</i> INFO</h4>

            </div>
            <div class="col s12 m8 l8">
              <p  ALIGN="justify">
                En este apartado puedes editar la información de <span class="blue-text">
                {{$employee->nombre}} {{$employee->aPaterno}} {{$employee->aMaterno}}</span>.
                Asegurate de asignarle un usuario a aquellos trabajadores que necesiten acceder al
                <span class="blue-text">Sistema Administrativo AQUATA</span>. Es importante administrar
                con cuidado el rol que desempeñarán tus empleados ya que este le permitirá el acceso
                 a diversas funciones del sistema.
              </p>

            </div>
          </div>
          <!-- NOTE: NOMBRE -->
          <div class="row">
            <div class="input-field col s12">
              <input name="nombre" id="employee_name" type="text" data-length="30" maxlength="30" required="required" aria-required="true" class="validate" value="{{$employee->nombre}}">
              <label for="employee_name">Nombre</label>
              <span class="help-text">Ingrese el nombre del empleado</span>
            </div>
          </div>
          <!-- NOTE: APELLIDO PATERNO -->
          <div class="row">
            <div class="input-field col s12">
              <input name="aPaterno" id="employee_aPat" type="text" data-length="20" maxlength="20" required="required" aria-required="true" class="validate" value="{{$employee->aPaterno}}">
              <label for="employee_aPat">Apellido Paterno</label>
              <span class="help-text">Ingrese el apellido paterno del empleado</span>
            </div>
          </div>
          <!-- NOTE: APELLIDO MATERNO -->
          <div class="row">
            <div class="input-field col s12">
              <input name="aMaterno" id="employee_aMat" type="text" data-length="20" maxlength="20" required="required" aria-required="true" class="validate" value="{{$employee->aMaterno}}">
              <label for="employee_aMat">Apellido Materno</label>
              <span class="help-text">Ingrese el apellido materno del empleado</span>
            </div>
          </div>
          <!-- NOTE: TELEFONO -->
          <div class="row">
            <div class="input-field col s12">
              <input name="telefono" id="employee_phone" type="text" data-length="10" maxlength="10" required="required" aria-required="true" class="validate" value="{{$employee->telefono}}">
              <label for="employee_phone">Telefono</label>
              <span class="help-text">Ingrese el telefono del empleado</span>
            </div>
          </div>
          <!-- NOTE: CORREO ELECTRONICO -->
          <div class="row">
            <div class="input-field col s12">
              <input name="email" id="employee_email" type="text" data-length="45" maxlength="45" required="required" aria-required="true" class="validate" value="{{$employee->email}}">
              <label for="employee_email">Correo electronico</label>
              <span class="help-text">Ingrese el correo electronico del empleado</span>
            </div>
          </div>
          <!-- NOTE: SUELDO -->
          <div class="row">
            <div class="input-field col s12">
              <input name="sueldo" id="employye_salary" type="text" required="required" aria-required="true" class="validate" value="{{$employee->sueldo}}">
              <label for="employye_salary">Sueldo</label>
              <span class="help-text">Ingrese el sueldo del empleado</span>
            </div>
          </div>
          <!-- NOTE: PUESTO -->
          <div class="row">
            <div class="input-field col s12">
              <label>Puesto</label>
              <br>
              <!-- NOTE: select con todos los puestos (para elegir 1) -->
              <div class="input-field col s12">
                <select name="idPuesto">
                  <option value="" disabled selected>Elija el puesto del empleado</option>
                  @foreach($jobs as $job)
                    @if($job->id == '1')
                      <?php continue; ?>
                    @endif
                    @if($job->id == $employee->idPuesto)
                      <option value="{{$job->id}}" selected>{{$job->nombre}}</option>
                    @else
                      <option value="{{$job->id}}">{{$job->nombre}}</option>
                    @endif
                  @endforeach
                </select>
              </div>
              <label data-error="Escoje una opcion">Puesto</label>
              <span id="error-select" class="red-text darken-2"></span>
            </div>
          </div>
          <!-- NOTE: FOTOGRAFIA DEL EMPLEADO -->
          <div class="row">
            <div class="file-field input-field">
              <div class="btn teal">
                <span>Subir imagen</span>
                <input type="file" name="imagen" accept="image/jpeg,image/jpg,image/png">
              </div>
              <div class="file-path-wrapper">
                <input type="text" class="file-path validate" required="required" aria-required="true" value="{{$employee->imagen}}">
              </div>
            </div>
          </div>

                @if(empty($user))
                  <div class="row">
                    <div class="col s3 left-align switch">
                      <br>
                      <label>
                        ¿Agregar usuario?
                        <input name="activo" type="checkbox" id='user-switch'>
                        <span class="lever" id='user-lever'></span>
                      </label>
                    </div>
                  </div>

                  <input type="hidden" name="idU" value="0">

                  <div id="quitarCampo">

                    <div class="row">
                      <div class="col s12">
                        <span class="blue-text">El usuario podrá acceder al sistema con su email</span>
                      </div>
                    </div>

                    <div class="row">
                      <div class="input-field col s12">
                        <input name="password" value="" id="password" type="password" data-length="255" maxlength="255">
                        <label for="password">Contraseña</label>
                        <span class="helper-text">Ingrese la contraseña del usuario</span>
                      </div>
                    </div>

                    <div class="row">
                      <div class="input-field col s12">
                        <!-- NOTE: select con todos los puestos (para elegir 1) -->
                        <select id="select_rol" name="idRol">
                          <option id="first_select_rol" value="" disabled selected>Elija el tipo de usuario</option>
                          @foreach($roles as $rol)
                            <option value="{{$rol->id}}">{{$rol->nombre}}</option>
                          @endforeach
                        </select>
                        <span id="error-select" class="red-text darken-2"></span>
                      </div>
                    </div>
                  </div>
                @else
                  @if(auth()->user()->id == $user->id)
                    <div class="row">
                      <div class="col s3 left-align switch">
                        <br>
                        <label>
                          ¿Agregar usuario?
                          <input name="activo" type="checkbox" id='user-switch2' disabled checked="checked">
                          <span class="lever" id='user-lever2'></span>
                        </label>
                      </div>
                    </div>
                    <input type="hidden" name="idU" value="{{$user->id}}">

                    <div id="quitarCampo2">

                      <div class="row">
                        <div class="input-field col s12">
                          <!-- NOTE: select con todos los puestos (para elegir 1) -->
                          <select name="idRol">
                            @foreach($roles as $rol)
                              @if($rol->id == $user->idRol)
                                <option value="{{$rol->id}}" selected>{{$rol->nombre}}</option>
                              @else
                                <option value="{{$rol->id}}">{{$rol->nombre}}</option>
                              @endif
                            @endforeach
                          </select>
                          <label>Si cambias tu <span class="blue-text"> tipo de usuario </span>automaticamente saldrás de la aplicación para actualizar los permisos.</label>
                        </div>
                      </div>

                      <div class="row">
                        <div class="col s12">
                          <p>Puedes cambiar tu contraseña utilizando el botón<span class="blue-text"> No puedo iniciar sesión </span>ubicado en la pantalla de login.</p>
                        </div>
                      </div>
                    </div>
                  @else
                    @if($user->T == 1)
                      <div class="row">
                        <div class="col s3 left-align switch">
                          <br>
                          <label>
                            ¿Agregar usuario?
                            <input name="activo" type="checkbox" id='user-switch2' checked="checked">
                            <span class="lever" id='user-lever2'></span>
                          </label>
                        </div>
                      </div>
                      <input type="hidden" name="idU" value="{{$user->id}}">

                      <div id="quitarCampo2">
                        <div class="row">
                          <div class="input-field col s12">
                            <!-- NOTE: select con todos los puestos (para elegir 1) -->
                            <select name="idRol">
                              @foreach($roles as $rol)
                                @if($rol->id == $user->idRol)
                                  <option value="{{$rol->id}}" selected>{{$rol->nombre}}</option>
                                @else
                                  <option value="{{$rol->id}}">{{$rol->nombre}}</option>
                                @endif
                              @endforeach
                            </select>
                            <label>Cambiar tipo de usuario</label>
                          </div>
                        </div>

                        <div class="row">
                          <div class="col s12">
                            <p>La contraseña puede ser cambiada por el usuario utilizando el botón<span class="blue-text"> No puedo iniciar sesión </span>ubicado en la pantalla de login.</p>
                          </div>
                        </div>
                      </div>
                    @else
                      <div class="row">
                        <div class="col s3 left-align switch">
                          <br>
                          <label>
                            ¿Agregar usuario?
                            <input name="activo" type="checkbox" id='user-switch3'>
                            <span class="lever" id='user-lever3'></span>
                          </label>
                        </div>
                      </div>
                      <input type="hidden" name="idU" value="{{$user->id}}">

                      <div id="quitarCampo3">
                        <div class="row">
                          <div class="input-field col s12">
                            <!-- NOTE: select con todos los puestos (para elegir 1) -->
                            <select name="idRol">
                              @foreach($roles as $rol)
                                @if($rol->id == $user->idRol)
                                  <option value="{{$rol->id}}" selected>{{$rol->nombre}}</option>
                                @else
                                  <option value="{{$rol->id}}">{{$rol->nombre}}</option>
                                @endif
                              @endforeach
                            </select>
                            <label>Cambiar tipo de usuario</label>
                          </div>
                        </div>

                        <div class="row">
                          <div class="col s12">
                            <p>La contraseña puede ser cambiada por el usuario utilizando el botón<span class="blue-text"> No puedo iniciar sesión </span>ubicado en la pantalla de login.</p>
                          </div>
                        </div>
                      </div>
                    @endif

                  @endif


                @endif

          <div class="center-align">
            <button type="submit" class="waves-effect waves-light btn blue darken-3 center-align">Aceptar</button>
          </div>
        </div>
      </div>
    </div>
  </form>

@endsection

@section('scripts')
  <script>
    $(document).ready(function() {
      $('#employee_name,'+
        '#employee_aPat,'+
        '#employee_aMat,'+
        '#employee_phone,'+
        '#employee_email').characterCounter();

      $( "Form" ).submit(function( event ) {
        if ( $( "select" ).first().val() != null ) {
          return;
        }
        $("#error-select").html("Seleccione un puesto para el empleado");
        red_toast('Seleccione un puesto para el empleado');
        event.preventDefault();
      });

      $('#quitarCampo').hide();
      $('#quitarCampo3').hide();

      $('#user-lever').click(function(){
         if ($("#user-switch").prop("checked") == false) {
           $('#quitarCampo').show();
         }
         else{
           $('#quitarCampo').hide();
         }
       });

       $('#user-lever3').click(function(){
          if ($("#user-switch3").prop("checked") == false) {
            $('#quitarCampo3').show();
          }
          else{
            $('#quitarCampo3').hide();
          }
        });

       $('#user-switch2').change(function(){
          if ($("#user-switch2").prop("checked") == false) {
            $('#quitarCampo2').hide();
          }
          else{
            $('#quitarCampo2').show();
          }
        });
    });
  </script>
@endsection
