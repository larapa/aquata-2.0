@extends('Layouts.Menu')
@section('head','usuario')
@section('title','Economia')
@section('content')

  <ul class="tabs tabs-fixed-width">

    <li class="tab col s4 m4 l4"><a class="active" href="#swipe-expenses">Gastos</a></li>
    <li class="tab col s4 m4 l4"><a target="_self" href="{{ route('Economy.Sale') }}">Ventas</a></li>
    <li class="tab col s4 m4 l4"><a target="_self" href="{{ route('Economy.Statistic') }}">Estadisticas</a></li>

  </ul>

  <div id="swipe-expenses">
    <!--Panel de control-->
    <div class="row">
      <div class="col s12 m4 l4">
        <div class="card white">
          <div class="card-content blue-text text-darken-2">
            <span class="card-title">Panel de control</span>
            <br>
            <ul class="collapsible popout">
              <!-- NOTE: Filtro de fechas -->
              <li>
                <div class="collapsible-header"><i class="material-icons">date_range</i>Selecionar periodo</div>
                <div class="collapsible-body">
                  <!-- NOTE: formulario para cambiar fechas -->
                  <form class="" id="form-fechas" action="{{route('Economy.ExpenseByDate')}}" method="post">
                    @csrf
                    <div class="row">
                      <!-- NOTE: datepicker 1 -->
                      <div class="col s12 m12 l12">
                        <h6 class="center-align black-text">Fecha inicial:</h6>
                        <input type="text" name="fecha1" class="datepicker center-align" id="fecha1">
                      </div>
                      <!-- NOTE: datepicker 2 -->
                      <div class="col s12 m12 l12">
                        <h6 class="center-align black-text">Fecha final:</h6>
                        <input type="text" name="fecha2" class="datepicker center-align" id="fecha2">
                      </div>
                    </div>
                    <div class="row">
                      <!-- NOTE: boton para actualizar los datos con las fechas de los pickers -->
                      <div class="col s12 m12 l12 center-align">
                        <br>
                        <a class="waves-effect waves-light btn teal" onclick="document.getElementById('form-fechas').submit();">Consultar</a>
                      </div>
                    </div>
                  </form>
              </li>
              <!-- NOTE: categorias -->
              <li>
                <div class="collapsible-header"><i class="material-icons">kitchen</i>Categorias</div>
                <div class="collapsible-body">
                  <div class="row">
                    <div class="col s8 m8 l8">
                      ¿Mostrar categorias inactivas?
                    </div>
                    <div class="col s4 m4 l4">
                      <div class="switch right-align">
                       <label>
                         <input type="checkbox" id="categorys-switch" >
                         <span class="lever" id="categorys-lever"></span>
                       </label>
                     </div>
                    </div>
                  </div>
                  <!-- ver los productos -->
                  <ul class="collection" id="categorys-collection">
                    @foreach($categorys as $category)
                      @if($category -> id > 3)
                        @if($category->T == '1')
                          <li class="collection-item avatar useful-category">
                            <div class="row">
                              <div class="col s10">
                                <i class="material-icons circle blue">kitchen</i>
                                <span class="title" id="NAME-CATEGORY-{{$category->id}}">{{$category->nombre}}</span>
                                <p class="grey-text truncate" id="DESCRIPTION-CATEGORY-{{$category->id}}" T="{{$category->T}}">{{$category->descripcion}}</p>
                              </div>
                              <div class="col s2">
                                <a class="secondary-content dropdown-trigger btn-flat teal-text" data-target="CATEGORY-{{$category->id}}"><i class="material-icons right">arrow_drop_down</i></a>
                              </div>
                              <!-- Dropdown content -->
                            </div>
                          </li>
                        @else
                          <li class="collection-item avatar useless-category">
                            <div class="row">
                              <div class="col s10">
                                <i class="material-icons circle grey">kitchen</i>
                                <span class="title grey-text" id="NAME-CATEGORY-{{$category->id}}">{{$category->nombre}}</span>
                                <p class="grey-text truncate" id="DESCRIPTION-CATEGORY-{{$category->id}}">{{$category->descripcion}}</p>
                              </div>
                              <div class="col s2">
                                <a class="secondary-content dropdown-trigger btn-flat teal-text" data-target="CATEGORY-{{$category->id}}"><i class="material-icons right">arrow_drop_down</i></a>
                              </div>
                            </div>
                          </li>
                        @endif
                      @endif
                    @endforeach
                  </ul>
                  <div class="row">
                    <div class="col s12 m12 l12 center-align">
                      <br>
                      <form id="Form-CategoryAdd" action="{{ route('ExpenseCategory.Add.View') }}" method="get">
                        @csrf
                        <a class="waves-effect waves-light btn-small teal" onclick="document.getElementById('Form-CategoryAdd').submit();"><i class="material-icons right">add</i>Agregar categoria</a>
                      </form>
                    </div>
                  </div>
                </div>
              </li>

            </ul>
          </div>
          <div class="card-action center-align">
            <div class="row">
              <div class="col s6">
                <form id="Form-ExpenseAdd" action="{{ route('Expense.Add.View') }}" method="get">

                  <a class="waves-effect waves-light btn teal" onclick="document.getElementById('Form-ExpenseAdd').submit();"><i class="material-icons right">add</i>Agregar gasto</a>
                </form>

              </div>
              <div class="col s6">
                <a id="export_button" class="waves-effect waves-light btn teal" ><i class="material-icons right">file_download</i>Exportar</a>
              </div>
            </div>

          </div>
        </div>
      </div>
      <!-- NOTE: CONTENIDO -->
      <div class="col s12 m8 l8">
        <!-- NOTE: poner aqui los tabs -->
        <div class="card">
          <div class="card-content">
            <div id="descripcion-gastos">
              <!-- NOTE: aqui se pondra una descripcin diferente segun el tab que se muestre -->
            </div>
          </div>
          <!-- NOTE: aqui se definen los tabs -->
          <div class="card-tabs">
            <ul class="tabs">
              <li class="tab col s6"><a href="#tabla-gastos" class="active" id="tab-tabla">Tabla</a></li>
              <li class="tab col s6"><a href="#div-grafica-1" id="tab-grafica-1">Grafica Pastel</a></li>
            </ul>
          </div>
          <!-- NOTE: aqui va el contenido de los tabs -->
          <div class="card-content">
            <div id="tabla-gastos">
              <!-- NOTE: AQUI VA LA TABLA -->
              <table class="centered responsive-table highlight">
                @if (!$expenses->isEmpty())
                  <thead>
                    <tr>
                      <th>Categoria</th>
                      <th class="hide">Concepto</th>
                      <th>Monto</th>
                      <th>Fecha</th>
                      <th class="hide">Usuario</th>
                      <th class="noexport"></th>
                      <th class="noexport"></th>
                    </tr>
                  </thead>
                  <tbody>

                    @foreach($expenses as $expense)
                      <tr>
                        <td>{{$expense->categoria}}</td>
                        <td class="hide">{{$expense->concepto}}</td>
                        <td>${{$expense->monto}}</td>
                        <td>{{$expense->fecha}}</td>
                        <td class="hide">{{$expense->nombre}} {{$expense->aPaterno}} {{$expense->aMaterno}}</td>
                        <td><a class="btn-flat EXPENSE noexport" id='info-gasto-{{$expense->id}}' href="#modal-info"><i class="material-icons">info_outline</i></a></td>
                        <td class="noexport">
                          <form id="Form-ExpenseCancel-{{$expense->id}}" action="{{ route('Expense.Cancel') }}" method="post">
                            @csrf
                            <input type="hidden" name="id" value="{{$expense->id}}">
                              <a id="CANCEL-EXPENSE-{{$expense->id}}" class="waves-effect waves-light cancel-expense"><i class="material-icons">cancel</i>
                          </form>
                        </td>
                      </tr>
                      <input type="hidden" value="{{$expense->fecha}}" id="expense-fecha-{{$expense->id}}">
                      <input type="hidden" value="{{$expense->concepto}}" id="expense-concepto-{{$expense->id}}">
                      <input type="hidden" value="{{$expense->monto}}" id="expense-monto-{{$expense->id}}">
                      <input type="hidden" value="{{$expense->categoria}}" id="expense-categoria-{{$expense->id}}">
                      <input type="hidden" value="{{$expense->nombre}} {{$expense->aPaterno}} {{$expense->aMaterno}}" id="expense-nombre-{{$expense->id}}">
                    @endforeach
                  </tbody>
                @else
                  <tbody>
                    <h4>No hay informacion. ¡Vamos a registrar algunos gastos!</h3>
                  </tbody>
                @endif
              </table>
            </div>
            <div id="div-grafica-1">
              <div class="chart" id="grafica-1">
                <!-- NOTE: AQUI VA LA GRAFICA 1 -->
              </div>
            </div>
          </div>
        </div>

      </div>
    </div>
  </div>

@endsection

@section('modals')
  <div id="modal-info" class="modal modal-fixed-footer">
    <div class="modal-content center-align">

    </div>
    <div class="modal-footer">
      <a class='waves-effect waves-light btn-flat grey-text text-darken-3 modal-close'>Cerrar</a>
    </div>
  </div>

  @foreach($categorys as $category)
    @if($category->T == '1')
      <ul id='CATEGORY-{{$category->id}}' class='dropdown-content'>
          <li><a id='CATEGORY-{{$category->id}}' href="#!" class="black-text CATEGORY"><i class="material-icons left blue-text">error_outline</i>INFO</a></li>
          <form id="Form-CategoryEdit-{{ $category -> id }}" action="{{ route('ExpenseCategory.Edit') }}" method="post">
            @csrf
            <input type="hidden" name="id" value="{{ $category -> id }}">
            <li><a onclick="document.getElementById('Form-CategoryEdit-{{ $category -> id }}').submit();" class="black-text"><i class="material-icons left blue-text">edit</i>Editar</a></li>
          </form>
          <form id="Form-CategoryDelete-{{ $category -> id }}" action="{{ route('ExpenseCategory.Delete') }}" method="post">
            @csrf
            <input type="hidden" name="id" value="{{ $category -> id }}">
            <li><a onclick="document.getElementById('Form-CategoryDelete-{{ $category -> id }}').submit();" class="black-text"><i class="material-icons left blue-text">delete</i>Eliminar</a></li>
          </form>
      </ul>
    @else
      <ul id='CATEGORY-{{$category->id}}' class='dropdown-content'>
        <li><a id='CATEGORY-{{$category->id}}' href="#!" class="black-text CATEGORY"><i class="material-icons left blue-text">error_outline</i>INFO</a></li>
        <form id="Form-CategoryActivate-{{ $category -> id }}" action="{{ route('ExpenseCategory.Activate') }}" method="post">
          @csrf
          <input type="hidden" name="id" value="{{ $category -> id }}">
          <li><a onclick="document.getElementById('Form-CategoryActivate-{{ $category -> id }}').submit();" class="black-text"><i class="material-icons left blue-text">cached</i>Activar</a></li>
        </form>
      </ul>
    @endif
    <input type="hidden" name="" value="{{$category->id}}" id="category-id-{{$category->id}}">
    <input type="hidden" name="" value="{{$category->nombre}}" id="category-nombre-{{$category->id}}">
    <input type="hidden" name="" value="{{$category->descripcion}}" id="category-descripcion-{{$category->id}}">
    <input type="hidden" name="" value="{{$category->T}}" id="category-T-{{$category->id}}">
  @endforeach
@endsection

@section('scripts')
  <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script src="//cdn.rawgit.com/rainabba/jquery-table2excel/1.1.0/dist/jquery.table2excel.min.js"></script>

  <script type="text/javascript" id="scripts-gastos">
    $(document).ready(function() {
      // NOTE: Validar que fecha 1 sea menor a fecha 2
      $( "#form-fechas" ).submit(function( event ) {
        alert($('#fecha1').datepicker('getDate'));
        if ($('#fecha1').datepicker('getDate') <= $('#fecha2').datepicker('getDate')) {
          return;
        }
        //$("#error-select").html("Seleccione una categoria");
        M.toast({html: 'Seleccione un periodo valido',classes:'red darken-3'});
        event.preventDefault();
      });
      $('.useless-category').hide();

      $('#tab-tabla').click();

      //  NOTE: aqui se inicializan los picker 1 y 2
      var hoy = new Date();
      dia = hoy.getDate();
      mes = hoy.getMonth()-1;
      ano = hoy.getFullYear();

      @if(!empty($f1))
        //  NOTE: fehcas solicitadas por el usuario
        dia1 = {{ substr($f1,0,2) }};
        mes1 = {{ substr($f1,3,2) }};
        ano1 = {{ substr($f1,6,4) }};
        dia2 = {{ substr($f2,0,2) }};
        mes2 = {{ substr($f2,3,2) }};
        ano2 = {{ substr($f2,6,4) }};
        $('#fecha1').datepicker({
          format: 'dd/mm/yyyy',
          setDefaultDate: true,
          defaultDate: new Date(ano1,mes1-1,dia1),
          minDate: new Date(2020,03,23),
          maxDate: hoy,
          i18n: {
            months: ['Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'],
            monthsShort: ['Ene','Feb','Mar','Abr','May','Jun','Jul','Ago','Sep','Oct','Nov','Dic'],
            weekdays: ['Domingo','Lunes','Martes','Miércoles','Jueves','Viernes','Sábado'],
            weekdaysShort: ['Dom','Lun','Mar','Mier','Jue','Vier','Sab'],
            weekdaysAbbrev: ['D','L','M','M','J','V','S']
          },

        });

        $('#fecha2').datepicker({
          format: 'dd/mm/yyyy',
          setDefaultDate: true,
          defaultDate: new Date(ano2,mes2-1,dia2),
          minDate: new Date(2020,03,23),
          maxDate: hoy,
          i18n: {
            months: ['Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'],
            monthsShort: ['Ene','Feb','Mar','Abr','May','Jun','Jul','Ago','Sep','Oct','Nov','Dic'],
            weekdays: ['Domingo','Lunes','Martes','Miércoles','Jueves','Viernes','Sábado'],
            weekdaysShort: ['Dom','Lun','Mar','Mier','Jue','Vier','Sab'],
            weekdaysAbbrev: ['D','L','M','M','J','V','S']
          },

        });
      @else
        //  NOTE: fecha de hoy y de hace un mes
        $('#fecha1').datepicker({
          format: 'dd/mm/yyyy',
          setDefaultDate: true,
          defaultDate: new Date(ano,mes,dia),
          minDate: new Date(2020,03,23),
          maxDate: hoy,
          i18n: {
            months: ['Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'],
            monthsShort: ['Ene','Feb','Mar','Abr','May','Jun','Jul','Ago','Sep','Oct','Nov','Dic'],
            weekdays: ['Domingo','Lunes','Martes','Miércoles','Jueves','Viernes','Sábado'],
            weekdaysShort: ['Dom','Lun','Mar','Mier','Jue','Vier','Sab'],
            weekdaysAbbrev: ['D','L','M','M','J','V','S']
          },

        });

        $('#fecha2').datepicker({
          format: 'dd/mm/yyyy',
          setDefaultDate: true,
          defaultDate: hoy,
          minDate: new Date(2020,03,23),
          maxDate: hoy,
          i18n: {
            months: ['Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'],
            monthsShort: ['Ene','Feb','Mar','Abr','May','Jun','Jul','Ago','Sep','Oct','Nov','Dic'],
            weekdays: ['Domingo','Lunes','Martes','Miércoles','Jueves','Viernes','Sábado'],
            weekdaysShort: ['Dom','Lun','Mar','Mier','Jue','Vier','Sab'],
            weekdaysAbbrev: ['D','L','M','M','J','V','S']
          },

        });
      @endif

      //los métodos para mostrar o no a los inútiles
      $('#categorys-lever').click(function(){
         if ($("#categorys-switch").prop("checked") == true) {
           $('.useless-category').hide();
           $('.useful-category').show();
         }
         else{
           $('.useless-category').show();
           $('.useful-category').hide();
         }
       });

       @if(session('success'))
          M.toast({html: '{{session('success')}}',classes:'green darken-2'})
       @endif
       @if(session('error'))
          red_toast("{{session('error')}}");
       @endif

       var init_tabs = M.Tabs.init($('.tabs'), { onShow: drawCharts });
    });

    document.addEventListener('DOMContentLoaded', function() {
      var elems = document.querySelectorAll('.dropdown-trigger');
      var options =
        {
          constrainWidth:false,
          alignment:'right'
        };
      var instances = M.Dropdown.init(elems, options);
    });

    //  NOTE: GRAFICAS
    google.load("visualization", "1", {packages:["corechart"]});
    google.charts.load('current', {'packages':['corechart']});

    google.setOnLoadCallback(drawCharts);

    //  NOTE: grafica de pastel
    function drawChart1() {
      var data = google.visualization.arrayToDataTable([
        ['Categoria', 'Monto'],
        @foreach($expenseschart as $expchart)
          ['{{$expchart->nombre}}',{{$expchart->monto}}],
        @endforeach
      ]);

      var options = {
        title: '',
        is3D: true,
      };

        var chart = new google.visualization.PieChart(document.getElementById('grafica-1'));
        chart.draw(data, options);
    }

    function drawCharts(){
      drawChart1();
    }
    window.addEventListener("resize", drawCharts);



    //  NOTE: mesajes para los Gastos
    $('#tab-tabla').click(function(){
      @if(!empty($f1))
        //  NOTE: fehcas solicitadas por el usuario
        dia1 = {{ substr($f1,0,2) }};
        mes1 = {{ substr($f1,3,2) }};
        ano1 = {{ substr($f1,6,4) }};
        dia2 = {{ substr($f2,0,2) }};
        mes2 = {{ substr($f2,3,2) }};
        ano2 = {{ substr($f2,6,4) }};

        $('#descripcion-gastos').empty();
        $('#descripcion-gastos').append(
          "En la siguiente tabla se muestran los gastos registrados entre las fechas " + dia1 + "/" + mes1 + "/" + ano1 + " y " +
          dia2 + "/" + mes2 + "/" + ano2 + "."
        );
      @else
        var hoy = new Date();
        dia = hoy.getDate();
        mes = hoy.getMonth();
        ano = hoy.getFullYear();
        $('#descripcion-gastos').empty();
        $('#descripcion-gastos').append(
          "En la siguiente tabla se muestran los gastos registrados entre las fechas " + dia + "/" + mes + "/" + ano + " y " +
           + dia + "/" + (mes+1) + "/" + ano + "."
        );
      @endif

    });
    $('#tab-grafica-1').click(function(){
      @if(!empty($f1))
        //  NOTE: fehcas solicitadas por el usuario
        dia1 = {{ substr($f1,0,2) }};
        mes1 = {{ substr($f1,3,2) }};
        ano1 = {{ substr($f1,6,4) }};
        dia2 = {{ substr($f2,0,2) }};
        mes2 = {{ substr($f2,3,2) }};
        ano2 = {{ substr($f2,6,4) }};

        $('#descripcion-gastos').empty();
        $('#descripcion-gastos').append(
          "En la siguiente grafica se muestran los gastos registrados entre las fechas " + dia1 + "/" + mes1 + "/" + ano1 + " y " +
          dia2 + "/" + mes2 + "/" + ano2 + "."
        );
      @else
        var hoy = new Date();
        dia = hoy.getDate();
        mes = hoy.getMonth();
        ano = hoy.getFullYear();
        $('#descripcion-gastos').empty();
        $('#descripcion-gastos').append(
          "En la siguiente grafica se muestran los gastos registrados entre las fechas " + dia + "/" + mes + "/" + ano + " y " +
           + dia + "/" + (mes+1) + "/" + ano + "."
        );
      @endif
    });
    //  NOTE: modals con la informacion
    //  categorias de gastos
    $('.CATEGORY').click(function(){
      $id = $(this).attr("id");
      $id = $id.substr(9,$id.lenght);
      $nombre = $('#category-nombre-'+$id).attr("value");
      $descripcion = $('#category-descripcion-'+$id).attr("value");
      $t = $('#category-T-'+$id).attr("value");
      if($t == '0'){
        $t = "Inactiva";
      }
      else {
        $t = "Activa";
      }
      categoryExpensesInfo($id,$nombre,$descripcion,$t);
    });

    function categoryExpensesInfo($id,$nombre,$descripcion,$t){
      $('.modal-content').empty();
      $('.modal-content').append(
        "<h3>INFO  <i class='material-icons small teal-text'>info_outline</i></h3>"+
        "<div class='divider'></div>"+
        "<p class='grey-text'>Categoria "+$t+"</p>"+
        "<div class='divider'></div>"+
        "<table class='striped'>"+
          "<tbody>"+
            "<tr>"+
              "<th class='center-align'>Nombre:</th>"+
              "<td>"+$nombre+"</td>"+
            "</tr>"+
            "<tr>"+
              "<th class='center-align'>Descripción:</th>"+
              "<td>"+$descripcion+"</td>"+
            "</tr>"+
          "</tbody>"+
        "</table>"
      );
      $('.modal-footer').empty();
      $('.modal-footer').append(
        "<a class='waves-effect waves-light btn-flat grey-text text-darken-3 modal-close'>Cerrar</a>"
      );
      $('#modal-info').modal('open');
    }
//  gastos
    $('.EXPENSE').click(function(){
      $id = $(this).attr("id");
      $id = $id.substr(11,$id.lenght);
      $fecha = $('#expense-fecha-'+$id).attr("value");
      $concepto = $('#expense-concepto-'+$id).attr("value");
      $monto = $('#expense-monto-'+$id).attr("value");
      $categoria = $('#expense-categoria-'+$id).attr("value");
      $nombre = $('#expense-nombre-'+$id).attr("value");

      expensesInfo($id,$fecha,$concepto,$monto,$categoria,$nombre);
    });

    function expensesInfo($id,$fecha,$concepto,$monto,$categoria,$nombre){
      $('.modal-content').empty();
      $('.modal-content').append(
        "<h3>INFO  <i class='material-icons small teal-text'>info_outline</i></h3>"+
        "<div class='divider'></div>"+
        "<table class='striped'>"+
          "<tbody>"+
          "<tr>"+
            "<th class='center-align'>Fecha:</th>"+
            "<td>"+$fecha+"</td>"+
          "</tr>"+
          "<tr>"+
            "<th class='center-align'>Concepto:</th>"+
            "<td>"+$concepto+"</td>"+
          "</tr>"+
          "<tr>"+
            "<th class='center-align'>Monto:</th>"+
            "<td>"+$monto+"</td>"+
          "</tr>"+
          "<tr>"+
            "<th class='center-align'>Categoria:</th>"+
            "<td>"+$categoria+"</td>"+
          "</tr>"+
          "<tr>"+
            "<th class='center-align'>Nombre:</th>"+
            "<td>"+$nombre+"</td>"+
          "</tr>"+
          "</tbody>"+
        "</table>"
      );
      $('.modal-footer').empty();
      $('.modal-footer').append(
        "<a class='waves-effect waves-light btn-flat grey-text text-darken-3 modal-close'>Cerrar</a>"
      );
      $('#modal-info').modal('open');
    }

    $('.cancel-expense').click(function(event) {
      $id = $(this).attr('id');
      $id = $id.substring(15, $id.lenght);

      $('.modal-content').empty();
      $('.modal-content').append(
        "<h3>Cancelar  <i class='material-icons small teal-text'>cancel</i></h3>"+
        "<div class='row'>"+
          "<p>Esta a punto de cancelar un gasto, debe considerar que esta accion no se puede deshacer</p>"+
          "<p>(Si el gasto se debe a una compra de articulos estos articulos se quitarán del inventario)</p>"+
          "<br>"+
          "<p>¿Esta seguro que desea continuar?</p>"+
        "</div>"
      );
      $('.modal-footer').empty();
      $('.modal-footer').append(
        "<a class='waves-effect waves-light btn-flat grey-text text-darken-3 modal-close'>Cerrar</a>"+
        "<a id='"+$id+"' class='waves-effect waves-light btn-flat red-text form-submit'>Eliminar</a>"
      );
      $('#modal-info').modal('open');


    });

    $('body').on('click', '.form-submit', function() {
      document.getElementById("Form-ExpenseCancel-"+$(this).attr('id')+"").submit();
    });

    $("#export_button").click(function(){
      $("#tabla-gastos").table2excel({
          // exclude CSS class
          exclude:".noexport",
          name:"Worksheet Name",
          filename:"Gastos"//do not include extension
      });
    });

  </script>



@endsection
