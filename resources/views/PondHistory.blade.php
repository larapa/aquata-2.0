@extends('Layouts.Menu')
@section('head','usuario')
@section('title','Historial de '.$pond->nombre)
@section('content')

    <div class="row">
      <div class="col s12 m12 l4">
        <div class="card white">
          <div class="card-content blue-text text-darken-2">
            <span class="card-title">Panel de control</span>
            <br>
            <ul class="collapsible popout">
              <!-- NOTE: Filtro de fechas -->
              <li>
                <div class="collapsible-header"><i class="material-icons">date_range</i>Selecionar periodo</div>
                <div class="collapsible-body">
                  <!-- NOTE: formulario para cambiar fechas -->
                  <form class="" id="form-fechas" action="{{route('Ponds.HistoryByDate')}}" method="post">
                    @csrf
                    <div class="row">
                      <!-- NOTE: datepicker 1 -->
                      <input type="hidden" name="id" value="{{$pond->id}}">
                      <div class="col s12 m12 l12">
                        <h6 class="center-align black-text">Fecha inicial:</h6>
                        <input type="text" name="fecha1" class="datepicker center-align" id="fecha1">
                      </div>
                      <!-- NOTE: datepicker 2 -->
                      <div class="col s12 m12 l12">
                        <h6 class="center-align black-text">Fecha final:</h6>
                        <input type="text" name="fecha2" class="datepicker center-align" id="fecha2">
                      </div>
                    </div>
                    <div class="row">
                      <!-- NOTE: boton para actualizar los datos con las fechas de los pickers -->
                      <div class="col s12 m12 l12 center-align">
                        <br>
                        <a class="waves-effect waves-light btn teal" onclick="document.getElementById('form-fechas').submit();">Consultar</a>
                      </div>
                    </div>
                  </form>
              </li>
              <li>
                <div class="collapsible-header"><i class="material-icons">insert_chart</i>Selecionar gráfico</div>
                <div class="collapsible-body">
                  <p>
                    <label>
                      <input class="with-gap" name="group3" type="radio" id="radio-biomass" checked/>
                      <span>Biomasa</span>
                    </label>
                  </p>
                  <p>
                    <label>
                      <input class="with-gap" name="group3" type="radio" id="radio-weight" />
                      <span>Peso promedio</span>
                    </label>
                  </p>
                  <p>
                    <label>
                      <input class="with-gap" name="group3" type="radio" id="radio-size" />
                      <span>Tamaño promedio</span>
                    </label>
                  </p>
                  <p>
                    <label>
                      <input class="with-gap" name="group3" type="radio" id="radio-temperature" />
                      <span>Temperatura</span>
                    </label>
                  </p>
              </li>
            </ul>
          </div>
          <div class="card-action center-align">
            <a id="export_button" class="waves-effect waves-light btn teal" ><i class="material-icons right">file_download</i>Exportar</a>
          </div>
        </div>
      </div>
      <!-- NOTE: CONTENIDO -->
      <div class="col s12 m12 l8">
        <!-- NOTE: poner aqui los tabs -->
        <div class="card">
          <div class="card-content">
            <h5>{{$pond->nombre}}</h5>
            <div id='info-div'>
              <p>Datos del historial</p>
            </div>
          </div>
          <!-- NOTE: aqui se definen los tabs -->
          <div class="card-tabs">
            <ul class="tabs">
              <li class="tab col s6"><a href="#tabla-gastos" class="active" id="tab-tabla">Historial</a></li>
              <li class="tab col s6"><a href="#div-grafica" id="tab-grafica">Gráficas</a></li>
            </ul>
          </div>
          <!-- NOTE: aqui va el contenido de los tabs -->
          <div class="card-content">
            <div id="tabla-gastos">
              <!-- NOTE: AQUI VA LA TABLA -->
              <table class="centered responsive-table highlight">
                <thead>
                  <tr>
                    <th class="hide-on-med-and-down"></th>
                    <th class="hide-on-large-only">Razón</th>
                    <th>Fecha</th>
                    <th>Etapa</th>
                    <th>Cantidad</th>
                    <th>Tamaño</th>
                    <th>Peso</th>
                    <th>Biomasa</th>
                    <th class="hide-on-med-and-down"></th>
                    <th class="hide-on-large-only">INFO</th>
                  </tr>
                </thead>
                <tbody>

                  @foreach ($history as $date)
                    <tr>
                      @if ($date->motivo == '1')
                        <td> <i class="material-icons small tooltipped grey-text" data-position="bottom" data-tooltip="Nuevo estanque">add</i></td>
                      @elseif ($date->motivo == '2')
                        <td> <i class="material-icons small tooltipped grey-text" data-position="bottom" data-tooltip="Edición">edit</i></td>
                      @elseif ($date->motivo == '3')
                        <td> <i class="material-icons small tooltipped grey-text" data-position="bottom" data-tooltip="Eliminación">delete</i></td>
                      @elseif ($date->motivo == '4')
                        <td> <i class="material-icons small tooltipped grey-text" data-position="bottom" data-tooltip="Alimentación">restaurant_menu</i></td>
                      @elseif ($date->motivo == '5')
                        <td> <i class="material-icons small tooltipped grey-text" data-position="bottom" data-tooltip="Activación">cached</i></td>
                      @elseif ($date->motivo == '6')
                        <td> <i class="material-icons small tooltipped grey-text" data-position="bottom" data-tooltip="Producción">forward</i></td>
                      @elseif ($date->motivo == '7')
                        <td> <i class="material-icons small tooltipped grey-text" data-position="bottom" data-tooltip="Cancelación">reply</i></td>
                      @endif
                      <td>{{$date->fecha}}</td>
                      <td>{{$date->tipo}}</td>
                      <td>{{$date->cantidad}}</td>
                      <td>{{$date->tamano}} <span class="grey-text">cm</span></td>
                      <td>{{$date->peso}} <span class="grey-text">gr</span></td>
                      <td>{{$date->peso * $date->cantidad /1000}} <span class="grey-text">kg</span></td>
                      <td>
                        <a class="waves-effect waves-light modal-trigger" href="#modal-{{$date->id}}"> <i class="material-icons">info_outline</i> </a>
                      </td>
                    </tr>
                  @endforeach
                </tbody>
              </table>

            </div>
            <div id="div-grafica">
              <div class="chart " id="chart-div">
                <!-- NOTE: AQUI VA LA GRAFICA 1 -->
              </div>
            </div>
          </div>
        </div>

      </div>
    </div>
  </div>

@endsection

@section('modals')
  @foreach ($history as $date)
    <!-- Modal Structure -->
    <div id="modal-{{$date->id}}" class="modal">
      <div class="modal-content center-align">
        <h3>INFO  <i class='material-icons small'>info_outline</i></h3>
        <div class="divider"></div>
        @if ($date->motivo == '1')
          <p class="grey-text">Estanque agregado con las siguiente información:</p>
        @elseif ($date->motivo == '2')
          <p class="grey-text">Estanque editado con las siguiente información:</p>
        @elseif ($date->motivo == '3')
          <p class="grey-text">Estanque eliminado con las siguiente información:</p>
        @elseif ($date->motivo == '4')
          <p class="grey-text">Estanque alimentado con las siguiente información:</p>
        @elseif ($date->motivo == '5')
          <p class="grey-text">Estanque activado con las siguiente información:</p>
        @elseif ($date->motivo == '6')
          <p class="grey-text">Productos generados con las siguiente información:</p>
        @elseif ($date->motivo == '7')
          <p class="grey-text">Productos cancelados con las siguiente información:</p>
        @endif

        <table class="striped centered">

          <tbody class="">
            <tr>
              <td><b>Fecha</b></td>
              <td>{{$date->fecha}}</td>
            </tr>
            <tr>
              <td> <b>Etapa</b> </td>
              <td>{{$date->tipo}}</td>
            </tr>
            <tr>
              <td> <b>Cantidad</b> </td>
              <td>{{$date->cantidad}} unidades</td>
            </tr>
            <tr>
              <td> <b>Tamaño promedio</b> </td>
              <td>{{$date->tamano}} centímetros</td>
            </tr>
            <tr>
              <td> <b>Peso promedio</b> </td>
              <td>{{$date->peso}} gramos</td>
            </tr>
            <tr>
              <td> <b>Biomasa</b> </td>
              <td>{{$date->peso * $date->cantidad /1000}} kilogramos</td>
            </tr>
            <tr>
              <td> <b>Temperatura</b> </td>
              <td>{{$date->temperatura}} °C</td>
            </tr>
            <tr>
              <td> <b>Oxígeno</b> </td>
              <td>{{$date->oxigeno}} unidades</td>
            </tr>
            <tr>
              <td> <b>Volumen de agua</b> </td>
              <td>{{$date->volumenAgua}} litros</td>
            </tr>
            <tr>
              <td> <b>PH</b> </td>
              <td>{{$date->ph}} unidades</td>
            </tr>
          </tbody>
        </table>
      </div>
      <div class="modal-footer">
        <a href="#!" class="modal-close waves-effect btn-flat">Aceptar</a>
      </div>
    </div>
  @endforeach
@endsection

@section('scripts')
  <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
  <script src="//cdn.rawgit.com/rainabba/jquery-table2excel/1.1.0/dist/jquery.table2excel.min.js"></script>
  <script type="text/javascript">
    $(document).ready(function() {

      $( "#form-fechas" ).submit(function( event ) {
        alert($('#fecha1').datepicker('getDate'));
        if ($('#fecha1').datepicker('getDate') <= $('#fecha2').datepicker('getDate')) {
          return;
        }
        //$("#error-select").html("Seleccione una categoria");
        M.toast({html: 'Seleccione un periodo valido',classes:'red darken-3'});
        event.preventDefault();
      });

      //  NOTE: aqui se inicializan los picker 1 y 2
      var hoy = new Date();
      dia = hoy.getDate();
      mes = hoy.getMonth()-1;
      ano = hoy.getFullYear();

      @if(!empty($f1))
        //  NOTE: fehcas solicitadas por el usuario
        dia1 = {{ substr($f1,0,2) }};
        mes1 = {{ substr($f1,3,2) }};
        ano1 = {{ substr($f1,6,4) }};
        dia2 = {{ substr($f2,0,2) }};
        mes2 = {{ substr($f2,3,2) }};
        ano2 = {{ substr($f2,6,4) }};
        $('#fecha1').datepicker({
          format: 'dd/mm/yyyy',
          setDefaultDate: true,
          defaultDate: new Date(ano1,mes1-1,dia1),
          minDate: new Date(2020,03,23),
          maxDate: hoy,
          i18n: {
            months: ['Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'],
            monthsShort: ['Ene','Feb','Mar','Abr','May','Jun','Jul','Ago','Sep','Oct','Nov','Dic'],
            weekdays: ['Domingo','Lunes','Martes','Miércoles','Jueves','Viernes','Sábado'],
            weekdaysShort: ['Dom','Lun','Mar','Mier','Jue','Vier','Sab'],
            weekdaysAbbrev: ['D','L','M','M','J','V','S']
          },

        });

        $('#fecha2').datepicker({
          format: 'dd/mm/yyyy',
          setDefaultDate: true,
          defaultDate: new Date(ano2,mes2-1,dia2),
          minDate: new Date(2020,03,23),
          maxDate: hoy,
          i18n: {
            months: ['Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'],
            monthsShort: ['Ene','Feb','Mar','Abr','May','Jun','Jul','Ago','Sep','Oct','Nov','Dic'],
            weekdays: ['Domingo','Lunes','Martes','Miércoles','Jueves','Viernes','Sábado'],
            weekdaysShort: ['Dom','Lun','Mar','Mier','Jue','Vier','Sab'],
            weekdaysAbbrev: ['D','L','M','M','J','V','S']
          },

        });
      @else
        //  NOTE: fecha de hoy y de hace un mes
        $('#fecha1').datepicker({
          format: 'dd/mm/yyyy',
          setDefaultDate: true,
          defaultDate: new Date(ano,mes,dia),
          minDate: new Date(2020,03,23),
          maxDate: hoy,
          i18n: {
            months: ['Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'],
            monthsShort: ['Ene','Feb','Mar','Abr','May','Jun','Jul','Ago','Sep','Oct','Nov','Dic'],
            weekdays: ['Domingo','Lunes','Martes','Miércoles','Jueves','Viernes','Sábado'],
            weekdaysShort: ['Dom','Lun','Mar','Mier','Jue','Vier','Sab'],
            weekdaysAbbrev: ['D','L','M','M','J','V','S']
          },

        });

        $('#fecha2').datepicker({
          format: 'dd/mm/yyyy',
          setDefaultDate: true,
          defaultDate: hoy,
          minDate: new Date(2020,03,23),
          maxDate: hoy,
          i18n: {
            months: ['Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'],
            monthsShort: ['Ene','Feb','Mar','Abr','May','Jun','Jul','Ago','Sep','Oct','Nov','Dic'],
            weekdays: ['Domingo','Lunes','Martes','Miércoles','Jueves','Viernes','Sábado'],
            weekdaysShort: ['Dom','Lun','Mar','Mier','Jue','Vier','Sab'],
            weekdaysAbbrev: ['D','L','M','M','J','V','S']
          },

        });
      @endif

      @if(session('success'))
         M.toast({html: '{{session('success')}}',classes:'green darken-2'})
      @endif
      @if(session('error'))
         M.toast({html: '{{session('error')}}',classes:'red darken-2'})
      @endif

      var init_tabs = M.Tabs.init($('.tabs'), { onShow: function(e){
        $('#radio-biomass').click();
      } });

      $('#radio-biomass').click(function(event) {
        drawBiomassChart();
        clearListener();
        window.addEventListener("resize", drawBiomassChart);

      });
      $('#radio-weight').click(function(event) {
        drawWeightChart();
        clearListener();
        window.addEventListener("resize", drawWeightChart);

      });
      $('#radio-size').click(function(event) {
        drawSizeChart();
        clearListener();
        window.addEventListener("resize", drawSizeChart);

      });
      $('#radio-temperature').click(function(event) {
        drawTemperatureChart();
        clearListener();
        window.addEventListener("resize", drawTemperatureChart);

      });
    });

    function clearListener(){
      window.removeEventListener("resize",drawBiomassChart);
      window.removeEventListener("resize",drawWeightChart);
      window.removeEventListener("resize",drawSizeChart);
      window.removeEventListener("resize",drawTemperatureChart);
    }
    google.charts.load('current', {packages: ['corechart', 'line']});
    function drawBiomassChart(){
      var biomass = new google.visualization.DataTable();
      biomass.addColumn('date','Fecha');
      biomass.addColumn('number','Biomasa');

      biomass.addRows([
        @foreach ($history as $date)
          @php
            $t = date_parse_from_format("Y-m-d H:i:s", $date->fecha);
          @endphp
          [new Date(Date.UTC({{$t['year']}},{{$t['month']}},{{$t['day']}},{{$t['hour']}},{{$t['minute']}},{{$t['second']}})),{{$date->cantidad * $date->peso /1000}}],
        @endforeach
      ]);
      var biomassOptions = {
        width: '100%',
        height: '100%',
        hAxis: {
          title: 'Fecha'
        },
        vAxis: {
          title: 'Kilogramos'
        },
        colors: ['#009688']
      };
      var chart = new google.visualization.LineChart(document.getElementById('chart-div'));
      chart.draw(biomass, biomassOptions);
    }
    function drawWeightChart(){
      var weightP = new google.visualization.DataTable();
      weightP.addColumn('date','Fecha');
      weightP.addColumn('number','Peso promedio');

      weightP.addRows([
        @foreach ($history as $date)
          @php
            $t = date_parse_from_format("Y-m-d H:i:s", $date->fecha);
          @endphp
          [new Date(Date.UTC({{$t['year']}},{{$t['month']}},{{$t['day']}},{{$t['hour']}},{{$t['minute']}},{{$t['second']}})),{{$date->peso}}],
        @endforeach
      ]);
      var weightPOptions = {
        width: '100%',
        height: '100%',
        hAxis: {
          title: 'Fecha'
        },
        vAxis: {
          title: 'Gramos'
        },
        colors: ['#f06292']
      };
      var chart = new google.visualization.LineChart(document.getElementById('chart-div'));
      chart.draw(weightP, weightPOptions);
    }
    function drawSizeChart(){
      var sizeP = new google.visualization.DataTable();
      sizeP.addColumn('date','Fecha');
      sizeP.addColumn('number','Tamaño promedio');

      sizeP.addRows([
        @foreach ($history as $date)
          @php
            $t = date_parse_from_format("Y-m-d H:i:s", $date->fecha);
          @endphp
          [new Date(Date.UTC({{$t['year']}},{{$t['month']}},{{$t['day']}},{{$t['hour']}},{{$t['minute']}},{{$t['second']}})),{{$date->tamano}}],
        @endforeach
      ]);
      var sizePOptions = {
        width: '100%',
        height: '100%',
        hAxis: {
          title: 'Fecha'
        },
        vAxis: {
          title: 'Centímetros'
        },
        colors: ['#ba68c8']
      };
      var chart = new google.visualization.LineChart(document.getElementById('chart-div'));
      chart.draw(sizeP, sizePOptions);
    }
    function drawTemperatureChart(){
      var temperatureP = new google.visualization.DataTable();
      temperatureP.addColumn('date','Fecha');
      temperatureP.addColumn('number','Temperatura');

      temperatureP.addRows([
        @foreach ($history as $date)
          @php
            $t = date_parse_from_format("Y-m-d H:i:s", $date->fecha);
          @endphp
          [new Date(Date.UTC({{$t['year']}},{{$t['month']}},{{$t['day']}},{{$t['hour']}},{{$t['minute']}},{{$t['second']}})),{{$date->temperatura}}],
        @endforeach
      ]);
      var temperaturePOptions = {
        width: '100%',
        height: '100%',
        hAxis: {
          title: 'Fecha'
        },
        vAxis: {
          title: 'Grados centígrados'
        },
        colors: ['#64b5f6']
      };
      var chart = new google.visualization.LineChart(document.getElementById('chart-div'));
      chart.draw(temperatureP, temperaturePOptions);
    }

  </script>

@endsection
