@extends('Layouts.Menu')

@section('title','Nuevo puesto')

@section('content')
  <!-- NOTE: formulario para añadir un puesto a la base de datos -->
  <form action="{{ route('Job.Add') }}" method="POST">
    @csrf

    <h4 class="center-align">Añadir puesto</h4>
    <div class="row">
      <div class="container">
        <div class="col s12">
          <div class="row">
            <div class="col s12 m4 l4 center align">
              <h4><i class="material-icons">help_outline</i> INFO</h4>
            </div>
            <div class="col s12 m8 l8">
              <p  ALIGN="justify">
                En este apartado puedes agregar un <span class="blue-text">puesto</span>, este elemento sirve para
                admnistrar a tus empleados con mayor facilidad. Asegurate de ingresar información lo más descriptiva y simple posible.
                <p>Podrás ver tus puestos en panel de control del apartado <span class="blue-text">Trabajadores</span>.</p>
              </p>
            </div>
          </div>
          <!-- NOTE: NOMBRE -->
          <div class="row">
            <div class="input-field col s12">
              <input id= job_nombre name="nombre" type="text" data-length="30" maxlength="30" required="required" aria-required="true" class="validate" value="">
              <label for="type_nombre">Nombre</label>
              <span class="help-text">Ingrese el nombre del puesto</span>
            </div>
          </div>
        </div>
        <div class="center-align">
          <button type="submit" class="waves-effect waves-light btn blue darken-3 center-align">Aceptar</button>
        </div>
      </div>
    </div>

  </form>
@endsection

@section('scripts')
  <script>
    $(document).ready(function() {
      $('#job_nombre').characterCounter();

      @if(session('message-duplicated-job'))
        red_toast("{{session('message-duplicated-job')}}");
      @endif
    });
  </script>
@endsection
