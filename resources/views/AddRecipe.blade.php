@extends('Layouts.Menu')
@section('title','Nueva receta')
@section('content')


  <form id="addForm" action="{{ route('Receta.Add') }}" method="POST" enctype="multipart/form-data">
    @csrf

    <h4 class="center-align">Agregar receta</h4>
    <div class="row">
      <div class="container">
        <div class="col s12">
          <div class="row">
            <div class="col s12 m4 l4 center align">
              <h4><i class="material-icons">help_outline</i> INFO</h4>

            </div>
            <div class="col s12 m8 l8">
              <p  ALIGN="justify">
                En este apartado puedes agregar una nueva <span class="blue-text">receta</span>.
                Esta información se mostrará en la página de presentación así que
                asegurate de agregar recetas deliciosas e imágenes atractivas. Además de seleccionar los
                productos de con los que recomiedas elaborar los platillos.
              </p>

            </div>
          </div>
          <!-- NOTE: NOMBRE -->
          <div class="row">
            <div class="input-field col s12">
              <input id="recipe_name" name="nombre" type="text" class="validate" data-length="30" maxlength="30" required="required">
              <label for="recipe_name">Nombre</label>
              <span class="help-text" data-error="Incorrecto" data-success="Correcto">Ingrese el nombre de la receta</span>
            </div>
          </div>
          <!-- NOTE: desc -->



             <div class="row">
               <div class="input-field col s12">
                 <textarea id="recipe_des"   name="descripcion" type="text" class="validate materialize-textarea" data-length="120" maxlength="120" required="required"></textarea>
                 <label for="recipe_des">Descripción</label>
                   <span class="help-text" data-error="Incorrecto" data-success="Correcto">Ingrese una descripción breve de la receta</span>
               </div>
             </div>

          {{-- Ingredientes --}}
          <div class="row">
            <div class="input-field col s12">
              <h5>Ingredientes</h5>
              <table >
                <thead>
                  <tr>
                      <th class="col s4">Cantidad</th>
                      <th class="col s8">Ingrediente</th>
                      <th class="text align center"><a  class="btn teal addRow0">+</a></th>
                  </tr>
                </thead>

                <tbody id="tb0">
                  <tr id="tr0">

                  </tr>

                </tbody>
              </table>
            </div>
          </div>


          <!-- NOTE: tiempo-->
          <div class="row">
            <div class="input-field col s12">
              <input id="recipe_time" name="tiempo" type="number" step="1" min="0" max="99999999999" class="validate" required="required">
              <label for="recipe_time">Tiempo</label>
              <span class="helper-text" data-error="Incorrecto" data-success="Correcto">Ingrese el tiempo en minutos</span>
            </div>
          </div>

          <div class="row">
            <div class="input-field col s12">
              <input id="recipe_port" name="porciones" type="text" class="validate" data-length="45" maxlength="45" required="required">
              <label for="recipe_port">Porciones</label>
              <span class="help-text" data-error="Incorrecto" data-success="Correcto">Ingrese la cantidad de porciones que se obtendrán</span>
            </div>
          </div>
          <!-- NOTE: TIPO producto -->
          <div class="row">
            <div class="input-field col s12">
              <select id="select_product" name="idProducto[]" multiple>
                <option id="first_select" value="" selected disabled>Seleccione al menos un producto a utilizar</option>
                  @if(!empty($products))
                    @foreach($products as $product)
                      @if($product->T == '1')
                          <option value="{{ $product-> id }}">{{ $product-> nombre }}</option>
                      @endif
                    @endforeach
                  @endif
                </select>
                <label data-error="Escoje una opcion">Productos</label>
                <span id="error-select" class="red-text darken-2"></span>
            </div>
          </div>

          <div class="row">
            <div class="input-field col s12">
              <h5>Instrucciones</h5>
              <table >
                <thead>
                  <tr>

                      <th>Instrucción</th>
                      <th class="text align center"><a  class="btn teal addRow1">+</a></th>
                  </tr>
                </thead>

                <tbody id="tb1">
                  <tr id="tr1">

                  </tr>

                </tbody>
              </table>
            </div>
          </div>

          <div class="row">
            <div class="file-field input-field">
              <div class="btn teal">
                <span>Subir imagen</span>
                <input type="file" name="imagen">
              </div>
              <div class="file-path-wrapper">
                <input type="text" class="file-path validate" required="required" aria-required="true" >
              </div>
            </div>
          </div>


          <div class="row">
            <div class="col s12 m12 l12 center-align">
              <button type="submit" class="waves-effect waves-light btn blue darken-3 center-align">Agregar</button>
            </div>
          </div>
        </div>
      </div>

    </div>
  </form>

@endsection

@section('scripts')
  <script type="text/javascript">
    $(document).ready(function() {
      $('input#recipe_name').characterCounter();
      $('textarea#recipe_des').characterCounter();
      $('textarea#recipe_ins').characterCounter();
      $('input#recipe_cant').characterCounter();
      $('input#recipe_ingre').characterCounter();
      $('input#recipe_port').characterCounter();
      $('input#recipe_idpro').characterCounter();
      $('input#recipe_ins').characterCounter();
      $( "Form" ).submit(function( event ) {
        if ( $( "select" ).val().length > 0 ) {
          return;
        }
        $("#error-select").html("Seleccione al menos un producto");
        M.toast({html: 'Seleccione un producto',classes:'red darken-3'});
        event.preventDefault();
      });

      $('.addRow0').on('click',function(){
        addRow();
      });
      $('.addRow1').on('click',function(){
        addRow1();
      });
      $i=1;
      function addRow(){
        var tr0 = '<tr>'+
          '<td class="col s4"><input id="recipe_cant" type="text"  name="cantidad[]" class="form-control" "validate" data-length="30" maxlength="30" required="required"></td>'+
          '<td class="col s8"><textarea id="recipe_ingre" type="text"  name="ingrediente[]" class="form-control materialize-textarea validate" data-length="120" maxlength="120" required="required"></textarea></td>'+
          '<td class="text align center"><a  class="btn teal lighten-2 remove">- </td>'+
        '</tr>';
        $('tbody#tb0').append(tr0);
      };
      $('tbody#tb0').on('click','.remove', function(){
        $(this).parent().parent().remove();
      });

      function addRow1(){
        var tr1 = '<tr>'+
          '<input id="recipe_idpro" type="hidden" name="id_paso[]" value="'+$i+'" class="form-control" "validate" data-length="30" maxlength="30" required="required">'+
          '<td><textarea id="recipe_ins"   name="instruccion[]" type="text" class="validate materialize-textarea" data-length="300" maxlength="300" required="required"></textarea></td>'+
          '<td class="text align center"><a  class="btn teal lighten-2 remove">- </td>'+
        '</tr>';
        $('tbody#tb1').append(tr1);
        $i++;
      };
      $('tbody#tb1').on('click','.remove', function(){
        $(this).parent().parent().remove();
        $i--;
      });

    });
  </script>
@endsection
