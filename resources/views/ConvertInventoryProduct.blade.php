@extends('Layouts.Menu')
@section('title','Convertir producto')
@section('content')
  <form action="{{route('InventoryProduct.ConvertValidation')}}" method="POST">
    @csrf
    <h4 class="center-align">Convertir producto del inventario </h4>
    <div class="row">
      <div class="container">
        <div class="col s12">
          <div class="row">
            <div class="col s12 m4 l4 center-align">
              <h4><i class="material-icons">help_outline</i> INFO</h4>

            </div>
            <div class="col s12 m8 l8">
              <p  ALIGN="justify">
                Si deseas crear un producto a partir de otro puedes realizarlo desde esta ventana. Selecciona cual es
                el <span class="blue-text">producto origen</span> y el <span class="blue-text">producto destino</span>.
                Corrobora los datos de tu selección, estos se mostrarán cuando escogas un origen.
              </p>

            </div>
          </div>
          <div class="row">
            <div class="col s12 m12 l12">
              <div class="input-field col s12 m12 l8">
                <select name='origin' id='origin'>
                  <option value="" disabled selected>Selecciona el producto</option>
                  @foreach ($inventoryProducts as $inventoryProduct)
                    <option class="truncate" value="{{$inventoryProduct->id}}">{{$inventoryProduct->producto}}</option>
                  @endforeach

                </select>
                <label>Origen</label>
                <span id="error-origin" class="red-text text-darken-3"></span>
              </div>
              @foreach ($inventoryProducts as $inventoryProduct)
                <div class="col s12 m12 l4 origin-info" id='ORIGIN-{{$inventoryProduct->id}}'>
                  <div class="input-field col s12">
                    <input disabled value="{{$inventoryProduct->cantidadMaxima}}" id="MAX-{{$inventoryProduct->id}}" type="number">
                    <label for="max">Cantidad disponible</label>
                  </div>
                </div>
              @endforeach
            </div>
          <!-- ========================================================================== -->
          <div class="col s12 m12 l12">
            <div class="input-field col s12">
              <select name='destination' id='destination'>
                <option value="" disabled selected>Selecciona el producto</option>
                @foreach ($products as $product)
                  <option class="truncate" value="{{$product->id}}">{{$product->nombre}}</option>
                @endforeach

              </select>
              <label>Destino</label>
              <span id="error-destination" class="red-text text-darken-3"></span>
            </div>
            <div class="col s12">
              <div class="input-field col s12 m12 l6">
                <input id="quantity" name='quantityOrigin' type="number" step="1" max="0" min="0" class="validate" required="required">
                <label for="quantity">Cantidad utilizados</label>
              </div>
              <div class="input-field col s12 m12 l6">
                <input id="quantityd" name='quantityDestination' type="number" step="1" min="0" class="validate" required="required">
                <label for="quantityd">Cantidad producidos</label>
              </div>
            </div>
          </div>
          </div>
        </div>
        <div class="center-align">
          <button type="submit" class="waves-effect waves-light btn blue darken-3 center-align">Aceptar</button>
        </div>
      </div>
    </div>
  </form>
@endsection
@section('scripts')
  <script>
    $(document).ready(function() {
      $('.origin-info').hide();
      $('#origin').change(function(event) {
        $('.origin-info').hide();
        $('#ORIGIN-'+$('#origin').val()).show();
        $('#quantity').attr('max',$('#MAX-'+$('#origin').val()).val())
      });
    });

    $( "Form" ).submit(function( event ) {
        if ( $( "select#origin" ).first().val() != null ) {
          $("#error-origin").empty();

        }
        else{
          $("#error-origin").html("Seleccione un producto");
          M.toast({html: 'Seleccione un producto origen',classes:'red darken-3'});
          $(document).scrollTop(0);
          event.preventDefault();
        }
        if ( $( "select#destination" ).first().val() != null ) {
          $("#error-destination").empty();
          return;
        }
        else{
          $("#error-destination").html("Seleccione un producto");
          M.toast({html: 'Seleccione un producto destino',classes:'red darken-3'});
          $(document).scrollTop(0);
          event.preventDefault();
        }
      });

  </script>
@endsection
