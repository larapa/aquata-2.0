@extends('Layouts.Menu')
@section('title','Editar costal')
@section('content')

  @if (session('mensaje'))
    <div class="alert"> {{session('mensaje')}} </div>
  @endif

  <form action="{{route('Bag.Update')}}" method="POST">
    <input type="hidden" name="id" value="{{$bag->id}}">
    @csrf

    <h4 class="center-align">Costal: {{ $bag->etiqueta }}</h4>
    <div class="row">
      <div class="container">
        <div class="col s12">
          <!-- NOTE: Estanque -->
          <div class="row">
            <div class="col s12 m4 l4 center-align">
              <h4><i class="material-icons">help_outline</i> INFO</h4>

            </div>
            <div class="col s12 m8 l8">
              <p  ALIGN="justify">La modificación de un elemento del inventario de costales de alimento consiste en
                modificar todas las caracteristicas correspondientes al costal de alimento.
              </p>
              <p  ALIGN="justify">
                Considera que si el costal ha sido abierto con anterioridad, o se han alimentado estanques con este
                (en otras palabras que la cantidad total del costal sea diferente a la restante) la unica modificacion
                posible será la de cambiarle el nombre de etiqueta a dicho costal de alimento.
              </p>
              <p  ALIGN="justify">
                Todo esto ha sido considerado para llevar un estricto control sobre el alimento.
              </p>
            </div>
          </div>
          <!-- NOTE: Etiqueta -->
          <div class="row">
            <div class="input-field col s12">
              <input id="bag_etiqueta" name="etiqueta" type="text" data-length="30" maxlength="30" required="" aria-required="true" class="validate" value="{{ $bag->etiqueta }}">
              <label for="bag_etiqueta">Etiqueta</label>
              <span class="help-text" data-error="Incorrecto" data-success="Correcto">Ingrese la etiqueta del costal</span>
            </div>
          </div>
          <!-- NOTE: Tamano -->
          <div class="row">
            <div class="input-field col s12">
              <input id="bag_tamano"  name="tamano" type="number" step="0.01" min="0" data-length="11" maxlength="11" required="" aria-required="true" value="{{ $bag->tamano }}" @if($bag->tamano != $bag->alimentoRestante) disabled @endif>
              @if($bag->tamano != $bag->alimentoRestante)  <input type="hidden" name="tamano" value="{{ $bag->tamano }}">@endif
              <label for="bag_tamano">Cantidad total en gramos del costal</label>
              <span class="help-text" data-error="Incorrecto" data-success="Correcto">Ingrese la cantidad total en gramos del costal</span>
            </div>
          </div>
          <!-- NOTE: Alimento restante -->
          <div class="row">
            <div class="input-field col s12">
              <input id="bag_alimentoRestante"  name="alimentoRestante" type="number" step="0.01" min="0" data-length="11" maxlength="11" required="" aria-required="true" value="{{ $bag->alimentoRestante }}" disabled>
              @if($bag->tamano != $bag->alimentoRestante) <input type="hidden" name="alimentoRestante" value="{{$bag->alimentoRestante}}"> @endif
              <label for="bag_alimentoRestante">Cantidad restante en gramos del costal</label>
              <span class="help-text" data-error="Incorrecto" data-success="Correcto">Ingrese la cantidad restante en gramos del costal</span>
            </div>
          </div>
          <!-- NOTE: Tipo alimento -->
          <div class="row">
            <div class="input-field col s12">
              <select name='idTipoAlimento'@if($bag->tamano != $bag->alimentoRestante) disabled @endif>
                <option value="">Selecciona un tipo de alimento</option>
                @foreach ($types as $type)
                  <option class="truncate" value="{{$type->id}}"@if($type->id == $bag->idTipoAlimento) selected @endif>{{$type->nombre}}</option>
                @endforeach
              </select>
              <label>Tipo alimento</label>
              @if($bag->tamano != $bag->alimentoRestante) <input type="hidden" name="idTipoAlimento" value="{{$bag->idTipoAlimento}}"> @endif
            </div>
          </div>
        <div class="center-align">
          <button type="submit" class="waves-effect waves-light btn blue darken-3 center-align">Aceptar</button>
        </div>
      </div>
    </div>
  </form>

@endsection

@section('scripts')
  <script type="text/javascript">

    $(document).ready(function() {
      $('input').characterCounter();
    });

  </script>
@endsection
