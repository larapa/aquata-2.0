@extends('Layouts.Menu')
@section('head','usuario')
@section('title','Nuevo costal')
@section('content')
  <form action="{{route('Bag.AddUpdate')}}" method="POST">
    @csrf
    <h4 class="center-align">Agregar un costal</h4>
    <div class="row">
      <div class="container">
        <div class="row">
          <div class="col s12 m4 l4 center-align">
            <h4><i class="material-icons">help_outline</i> INFO</h4>

          </div>
          <div class="col s12 m8 l8">
            <p  ALIGN="justify">
              Para agregar un costal es necesario que especifiques el tamaño de este en <span class="blue-text">kilogramos</span> y
              que selecciones el tipo de alimento del costal. Nosotros le asignaremos una etiqueta con la fecha del día de hoy y
              un indice para diferenciarlo de los demás. Si deseas cambiar esto puedes hacerlo con la función <span class="blue-text">editar</span>.
            </p>
            <p align="justify">
              Haremos un registro de este acontecimiento en tu <span class="blue-text">historial de inventario de alimentos</span> y
              en tu <span class="blue-text">historial de gastos</span>, ingresa el precio <span class="blue-text">TOTAL</span> que gastaste por estos costales.
            </p>

          </div>
        </div>
        <div class="row">
          <div class="input-field col s12">
            <input id="size" name="size" type="number" class="validate" required="required" >
            <label for="size">Tamaño</label>
            <span class="helper-text" data-error="Incorrecto" data-success="Correcto">Ingresa el tamaño en kilogramos</span>
          </div>
        </div>
        <div class="row">
          <div class="input-field col s6">
            <input id="quantity" name="quantity" type="number" step="1" class="validate" required="required" >
            <label for="quantity">Cantidad</label>
            <span class="helper-text" data-error="Incorrecto" data-success="Correcto">Ingresa la cantidad de costales</span>
          </div>
          <div class="input-field col s6">
            <input id="cost" name="cost" type="number" step="0.01" class="validate" required="required" >
            <label for="cost">Costo</label>
            <span class="helper-text" data-error="Incorrecto" data-success="Correcto">Ingresa el costo total</span>
          </div>
        </div>
        <div class="input-field col s12">
          <select name='type' id='type'>
            <option value="" disabled selected>Selecciona un tipo de alimento</option>
            @foreach ($foodTypes as $type)
              <option class="truncate" value="{{$type->id}}">{{$type->nombre}}</option>
            @endforeach

          </select>
          <label>Tipo de alimento</label>
          <span id="error-type" class="red-text text-darken-3"></span>
        </div>
        <div class="center-align">
          <button type="submit" class="waves-effect waves-light btn blue darken-3 center-align">Aceptar</button>
        </div>
      </div>
    </div>
  </form>
@endsection


@section('scripts')
  <script>
    $( "Form" ).submit(function( event ) {
        if ( $( "select#type" ).first().val() != null ) {
          $("#error-type").empty();
        }
        else{
          $("#error-type").html("Seleccione un tipo de alimento");
          M.toast({html: 'Seleccione un tipo de alimento',classes:'red darken-3'});
          $(document).scrollTop(0);
          event.preventDefault();
        }
      });
  </script>
@endsection
