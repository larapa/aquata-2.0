<!DOCTYPE html>
<html lang="en" dir="ltr">

  <head>
    <script type="text/javascript" src="/js/jquery-3.4.1.min.js"></script>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    {!! MaterializeCSS::include_all() !!}
    <meta name="viewport" charset="utf-8" content="width=device-width, initial-scale=1.0"/>
    <title>AQUATA @yield('head')</title>
  </head>

  <body>
    <header>
      <div  class="navbar-fixed">
        <nav>
          <div class="nav-wrapper white">
            <a href="{{route('index')}}" class="brand-logo center truncate black-text">EL TEPETATE</a>

            <ul class="left ">
              <li><a href="{{route('index')}}" class="grey-text"> <i class="material-icons">arrow_back</i> </a></li>

            </ul>
          </div>
        </nav>
      </div>

    </header>
    <div class="preloader-div" style="position:fixed;top:50%;left:45%;">
      <div class="preloader-wrapper big active">
        <div class="spinner-layer spinner-red-only">
          <div class="circle-clipper left">
            <div class="circle"></div>
          </div><div class="gap-patch">
            <div class="circle"></div>
          </div><div class="circle-clipper right">
            <div class="circle"></div>
          </div>
        </div>
      </div>
    </div>
    <main style="opacity:0">
      <br>
      <div class="container">
        <div class="row">
          <div class="carousel carousel-slider hide-on-small-only">
            @foreach ($recipes as $recipe)
              <a class="carousel-item"><img src="{{url("uploads/".$recipe->imagen)}}" height="500px" style="object-fit:cover" class="info-btn"></a>
            @endforeach
          </div>
          <div class="carousel carousel-slider white hide-on-med-and-up">
            @foreach ($recipes as $recipe)
              <div  class="carousel-item white white-text">
                <img src="{{url("uploads/".$recipe->imagen)}}" height="250px" style="object-fit:cover">
              </div>
            @endforeach
          </div>
        </div>
          @foreach ($recipes as $recipe)
            <div class="row info" id="info-{{$recipe->id}}">
              <div class="col s12 l6 m6">
                <div class="row">
                  <div class="col s12">
                    <h4>{{$recipe->nombre}}</h4>
                    <div class="divider"></div>
                    <br>
                  </div>
                  <div class="col s6">
                    <p class="black-text"><i class="material-icons left ">access_alarm</i>{{$recipe->tiempo}} minutos</p>
                  </div>
                  <div class="col s6">
                    <p class="black-text"><i class="material-icons left ">group</i>{{$recipe->porciones}} porcion(es)</p>
                  </div>
                  <div class="col s12">
                    <p align='justify'>
                      {{$recipe->descripcion}}
                    </p>
                  </div>
                </div>
                <div class="col s12">
                  <h4>Ingredientes</h4>
                  <div class="divider"></div>
                  <br>
                  <div class="col s12">
                    @foreach ($recipe->productos as $product)
                      <div class="card horizontal">
                      <div class="card-image-mini">
                        <img class=" materialboxed" data-caption="{{$product->nombre}}" src="{{url("uploads/".$product->imagen)}}" ></i>
                      </div>
                      <div class="card-stacked">
                        <div class="card-content">
                          <div class="valign-wrapper">
                            <h6>{{$product->nombre}}</h6>
                          </div>
                        </div>
                      </div>
                    </div>
                    @endforeach
                  </div>
                  <table>
                    <tbody>
                      @foreach ($recipe->ingredientes as $ingrediente)
                        @php
                          $value = json_decode($ingrediente,true);
                        @endphp
                        <tr>
                          <td>{{$value['cantidad']}}</td>
                          <td>{{$value['ingrediente']}}</td>
                        </tr>
                      @endforeach
                    </tbody>
                  </table>
                </div>
              </div>
              <div class="col s12 l6 m6">
                <h4>Instrucciones</h4>
                <div class="divider"></div>
                @php
                  $i = 0;
                @endphp
                @foreach ($recipe->instrucciones as $instruccion)
                  @php
                    $value = json_decode($instruccion,true);
                    $i++;
                  @endphp

                  <div class="row">
                    <br>
                    <div class="col s2">
                      <h5 class="hide-on-med-and-up " >{{$i}}</h5>
                      <h3 class="hide-on-small-only " >{{$i}}</h3>
                    </div>
                    <div class="col s10 " style="border-left: 5px solid grey;" >
                      {{$value['instruccion']}}
                    </div>
                    <div class="col s12">
                      <div class="divider"></div>
                    </div>
                  </div>
                @endforeach
              </div>
            </div>
          @endforeach

          <div class="row">
            <div class="row">
              <h2 class="header hide-on-med-and-down">Contáctanos</h2>
              <h4 class="header hide-on-large-only">Contáctanos</h4>
              <div class="row"> <div class="divider"></div> </div>
            </div>

            <div class="row center-align">
              <div class="col s12 m4 l4">
                <div class="container">
                  <h5>Facebook</h5>
                  <a href="https://www.facebook.com/Granja-de-Trucha-Tepetates-1125220357665723/"><img src="./images/Logos/facebook.png" style="width:30%;"></a>
                </div>
                <a href="https://www.facebook.com/Granja-de-Trucha-Tepetates-1125220357665723/"> <p class="grey-text">El tepetate</p> </a>
                <div class="divider hide-on-med-and-up"></div>
              </div>

              <div class="col s12 m4 l4">
                <div class="container">
                  <h5>Gmail</h5>
                  <a href="mailto:arturoalcocermolina@gmail.com"><img src="./images/Logos/gmail.png" style="width:30%;"></a>
                </div>
                <a href="mailto:arturoalcocermolina@gmail.com"> <p class="grey-text">arturoalcocermolina@gmail.com</p> </a>
                <div class="divider hide-on-med-and-up"></div>
                <br>
              </div>

              <div class="col s12 m4 l4">
                <div class="container">
                  <h5>Teléfono</h5>
                  <a href="tel:4432280290"><img src="./images/Logos/caller.png" style="width:30%;"></a>
                </div>
                <a href="tel:4432280290"> <p class="grey-text">+52 (443) 228-0290</p> </a>
                <div class="divider hide-on-med-and-up"></div>
                <br>
              </div>

            </div>
          </div>
      </div>

    </main>

  <script>
  $(document).ready(function() {
    $('.preloader-div').fadeOut("slow", function(){
            $('main').fadeTo( "slow", 1);
    });
  });
  $(document).ready(function(){
    $('.materialboxed').materialbox();
  });
  $(document).ready(function() {
     $('.info').hide();
  });

    M.AutoInit();
    $(document).ready(function() {
      $('.carousel').carousel({
        indicators: true,
        onCycleTo: function (ele) {
          $('.info').hide();
          $('#info-'+($(ele).index()+1)).show();
        }
      });

      $('.carousel.carousel-slider').carousel({
        fullWidth: true,
        indicators: true,
        onCycleTo: function (ele) {
          $('.info').hide();
          $('#info-'+($(ele).index()+1)).show();

        }
      });

    });

  </script>
  </body>
</html>
