<!DOCTYPE html>
<html lang="en" dir="ltr">

  <head>
    <script type="text/javascript" src="/js/jquery-3.4.1.min.js"></script>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    {!! MaterializeCSS::include_all() !!}
    <meta name="viewport" charset="utf-8" content="width=device-width, initial-scale=1.0"/>
    <title>AQUATA @yield('head')</title>
  </head>

  <body>
    <header>
      <div  class="navbar-fixed">
        <nav>
          <div class="nav-wrapper white">
            <a href="{{route('index')}}" class="brand-logo center truncate black-text">EL TEPETATE</a>

            <ul class="left ">
              <li><a href="{{route('index')}}" class="grey-text"> <i class="material-icons">arrow_back</i> </a></li>

            </ul>
          </div>
        </nav>
      </div>

    </header>
    <div class="preloader-div" style="position:fixed;top:50%;left:45%;">
      <div class="preloader-wrapper big active">
        <div class="spinner-layer spinner-red-only">
          <div class="circle-clipper left">
            <div class="circle"></div>
          </div><div class="gap-patch">
            <div class="circle"></div>
          </div><div class="circle-clipper right">
            <div class="circle"></div>
          </div>
        </div>
      </div>
    </div>
    <main style="opacity:0">
      <div class="container">
        <div class="row">
          <div class="col s12">
            <h4 >Nuestros productos</h4>
            <div class="divider"></div>
            <h6 align='justify' class="grey-text">
              La trucha arcoiris es un producto sano y rico, contiene aproximadamente 77.27 gramos de proteína por cada 100. Además, solo contiene 382 calorías y es un producto bajo en grasas (8.06 gramos) pero con un sabor exquisito. Contiene diversas vitaminas y minerales como vitamina B-12 (22.40 mg), vitamina A (213 UI), vitamina E (2.41 mg), fósforo (976 mg), selenio (105 µg), calcio (58 mg) y omega 3. <br>
              El salmón es un producto aclamado por su alta calidad y sabor. Sin embargo, no es un producto que podamos disfrutar fresco en México ya que este solo se comercia empaquetado. En cambio, la trucha salmonada es un producto más rico y barato de altísima calidad que podemos encontrar fresco debido a que su producción es local. Este producto mexicano es reconocido por sus cualidades nutrimentales y su excelente sabor.
            </h6>
          </div>
        </div>
        <div class="row">
          @foreach ($products as $product)
            <div class="col s12 m6 l4">
              <div class="card">
                <div class="card-image-boosted">
                  <img src="{{url("uploads/".$product->imagen)}}" class=" materialboxed" data-caption="{{$product->nombre}}">
                </div>
                <div class="card-content">
                  <h6 class=" tooltipped truncate" data-position="bottom" data-tooltip="{{$product->nombre}}">{{$product->nombre}}</h6>
                  <div class="divider"></div>
                  <h6 class="center-align">${{$product->precio}} Kg</h6>
                  <p class="grey-text" align='justify'>{{$product->descripcion}}</p>
                </div>
              </div>
            </div>
          @endforeach
        </div>

        <div class="row">
          <div class="row">
            <h2 class="header hide-on-med-and-down">Contáctanos</h2>
            <h4 class="header hide-on-large-only">Contáctanos</h4>
            <div class="row"> <div class="divider"></div> </div>
          </div>

          <div class="row center-align">
            <div class="col s12 m4 l4">
              <div class="container">
                <h5>Facebook</h5>
                <a href="https://www.facebook.com/Granja-de-Trucha-Tepetates-1125220357665723/"><img src="./images/Logos/facebook.png" style="width:30%;"></a>
              </div>
              <a href="https://www.facebook.com/Granja-de-Trucha-Tepetates-1125220357665723/"> <p class="grey-text">El tepetate</p> </a>
              <div class="divider hide-on-med-and-up"></div>
            </div>

            <div class="col s12 m4 l4">
              <div class="container">
                <h5>Gmail</h5>
                <a href="mailto:arturoalcocermolina@gmail.com"><img src="./images/Logos/gmail.png" style="width:30%;"></a>
              </div>
              <a href="mailto:arturoalcocermolina@gmail.com"> <p class="grey-text">arturoalcocermolina@gmail.com</p> </a>
              <div class="divider hide-on-med-and-up"></div>
              <br>
            </div>

            <div class="col s12 m4 l4">
              <div class="container">
                <h5>Teléfono</h5>
                <a href="tel:4432280290"><img src="./images/Logos/caller.png" style="width:30%;"></a>
              </div>
              <a href="tel:4432280290"> <p class="grey-text">+52 (443) 228-0290</p> </a>
              <div class="divider hide-on-med-and-up"></div>
              <br>
            </div>

          </div>
        </div>
      </div>
    </main>

  <script>
  $(document).ready(function() {
    $('.preloader-div').fadeOut("slow", function(){
            $('main').fadeTo( "slow", 1);
    });
  });
    M.AutoInit();
    $(document).ready(function(){
      $('.materialboxed').materialbox();
    });
    $(document).ready(function() {
      $('.carousel').carousel({
        indicators: true
      });
      $('.carousel.carousel-slider').carousel({
        fullWidth: true,
        indicators: true
      });

      $('.info-btn').click(function(event) {
        //$('#info').hide();
      });
    });

  </script>
  </body>
</html>
