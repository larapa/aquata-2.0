<!DOCTYPE html>
<html lang="en" dir="ltr">

  <head>
    <script type="text/javascript" src="/js/jquery-3.4.1.min.js"></script>

    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    {!! MaterializeCSS::include_all() !!}
    <meta name="viewport" charset="utf-8" content="width=device-width, initial-scale=1.0"/>
    <title>AQUATA @yield('head')</title>
  </head>

  <body>
    <header>
      <div  class="navbar-fixed">
        <nav>
          <div class="nav-wrapper white">
            <a href="{{route('index')}}" class="brand-logo center truncate black-text">EL TEPETATE</a>
            <a href="#" data-target="menu-responsive" class="sidenav-trigger black-text">
              <i class="material-icons">menu</i>
            </a>
            <ul class="right hide-on-med-and-down">
              <li><a href="{{route('Main.products')}}" class="grey-text">Productos</a></li>
              <li><a href="{{route('Main.recipes')}}" class="grey-text">Recetas</a></li>
            </ul>
          </div>
        </nav>
      </div>

      <nav class="sidenav" id="menu-responsive">
        <li><a href="{{route('Main.products')}}" class="grey-text">Productos</a></li>
        <li><a href="{{route('Main.recipes')}}" class="grey-text">Recetas</a></li>
      </nav>
    </header>
    <div class="preloader-div" style="position:fixed;top:50%;left:45%;">
      <div class="preloader-wrapper big active">
        <div class="spinner-layer spinner-red-only">
          <div class="circle-clipper left">
            <div class="circle"></div>
          </div><div class="gap-patch">
            <div class="circle"></div>
          </div><div class="circle-clipper right">
            <div class="circle"></div>
          </div>
        </div>
      </div>
    </div>
    <main style="opacity:0">
      <div class="parallax-container" >
        <div class="parallax">
          <img src="./images/Main/parallax3.jpg">
          <!--   <img src="./images/Muestras/img1.jpg"> -->
        </div>
      </div>
      <div class="section white">
        <div class="row container">

          <h2 class="header hide-on-med-and-down">¿Quiénes somos?</h2>
          <h4 class="header hide-on-large-only">¿Quiénes somos?</h4>
          <div class="row"> <div class="divider"></div> </div>
          <div class="row ">
            <div class="col s12 m6 l7 ">
              <p class="grey-text text-darken-3 lighten-3" align="justify">
                Somos una empresa Michoacana dedicada a la producción de trucha Arcoíris y  dispuestos a mejorar la alimentación sana al proveer a nuestros clientes de un producto que posee varios minerales importantes que mantienen nuestro cuerpo en funcionamiento y una buena concentración de vitaminas a, e y d, además de rica en vitaminas del complejo b, omega-3 de cadenas largas, que son importantes para prevenir enfermedades coronarias, minimizar accidentes vasculares cerebrales, mejora las respuestas inmunológicas y reduce el riesgo de enfermedades degenerativas.
              </p>
            </div>
            <div class="col s12 m6 l5">
              <div class="container">
                <img src="./images/Logos/Tepetates.png" style="width:100%;">
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col s12 m6 l6">
              <h5 class="header"> Misión</h5>
              <p align="justify">
                En Granja Tepetates nos esmeramos día a día para aplicar las buenas prácticas de producción de trucha arcoíris, con ello ofrecemos a nuestros clientes un producto saludable y nutritivo de  alta calidad e inocuidad.
              </p>
            </div>
            <div class="col s12 m6 l6">
              <h5 class="header"> Visión</h5>
              <p align="justify">
                La Granja Tepetates es reconocida por su producción de trucha arcoíris de alta calidad e inocuidad; sus métodos de producción están apegados a estándares que permiten mantener una responsabilidad y equilibrio del medio ambiente natural, económico y social.
              </p>
            </div>
          </div>
        </div>
        <div class="section teal lighten-1"></div>
      </div>
      <div class="parallax-container">
        <div class="parallax">
          <img src="./images/Main/parallax1.jpg">
        </div>
      </div>
      <div class="section white">
        <div class="row container">

          <h2 class="header hide-on-med-and-down right-align">Nuestros productos</h2>
          <h4 class="header hide-on-large-only">Nuestros productos</h4>
          <div class="row"> <div class="divider"></div> </div>
          <div class="row">
            <div class="col s12 m6 l6 hide-on-med-and-down">
              <div class="container">
                <img src="./images/Main/salmonada.png" style="width:100%;">
              </div>
            </div>
            <div class="col s12 m6 l6">
              <p class="grey-text text-darken-3 lighten-3" align="justify">
              La trucha arcoiris es un producto sano y rico, contiene aproximadamente 77.27 gramos de proteína
              por cada 100. Además, solo contiene 382 calorías y es un producto bajo en grasas (8.06 gramos)
              pero con un sabor
              exquisito.
              Contiene diversas vitaminas y minerales como vitamina B-12 (22.40 mg), vitamina A (213 UI), vitamina E (2.41 mg), fósforo (976 mg),
              selenio (105 µg), calcio (58 mg) y omega 3.
              </p>

              <h5 class="header"> Trucha salmonada</h5>

              <p align="justify">
                El salmón es un producto aclamado por su alta calidad y sabor. Sin embargo, no es un producto que
                podamos disfrutar fresco en México ya que este solo se comercia empaquetado. En cambio, la trucha salmonada
                es un producto más rico y barato de altísima calidad que podemos encontrar fresco debido a que su producción
                es local. Este producto mexicano es reconocido por sus cualidades nutrimentales y su excelente sabor.
              </p>
            </div>
            <div class="col s12 m6 l6 hide-on-large-only">
              <div class="container">
                <img src="./images/Main/salmonada.png" style="width:100%;">
              </div>
            </div>
          </div>
        </div>
        <div class="section salmon"></div>
      </div>
      <div class="parallax-container">
        <div class="parallax">
          <img src="./images/Main/parallax2.jpg">
        </div>
      </div>
      <div class="section white">
        <div class="row container">
          <div class="row">
            <h2 class="header hide-on-med-and-down">Contáctanos</h2>
            <h4 class="header hide-on-large-only">Contáctanos</h4>
            <div class="row"> <div class="divider"></div> </div>
          </div>

          <div class="row center-align">
            <div class="col s12 m4 l4">
              <div class="container">
                <h5>Facebook</h5>
                <a href="https://www.facebook.com/Granja-de-Trucha-Tepetates-1125220357665723/"><img src="./images/Logos/facebook.png" style="width:30%;"></a>
              </div>
              <a href="https://www.facebook.com/Granja-de-Trucha-Tepetates-1125220357665723/"> <p class="grey-text">El tepetate</p> </a>
              <div class="divider hide-on-med-and-up"></div>
            </div>

            <div class="col s12 m4 l4">
              <div class="container">
                <h5>Gmail</h5>
                <a href="mailto:arturoalcocermolina@gmail.com"><img src="./images/Logos/gmail.png" style="width:30%;"></a>
              </div>
              <a href="mailto:arturoalcocermolina@gmail.com"> <p class="grey-text">arturoalcocermolina@gmail.com</p> </a>
              <div class="divider hide-on-med-and-up"></div>
              <br>
            </div>

            <div class="col s12 m4 l4">
              <div class="container">
                <h5>Teléfono</h5>
                <a href="tel:4432280290"><img src="./images/Logos/caller.png" style="width:30%;"></a>
              </div>
              <a href="tel:4432280290"> <p class="grey-text">+52 (443) 228-0290</p> </a>
              <div class="divider hide-on-med-and-up"></div>
              <br>
            </div>

          </div>
        </div>
        <div class="section blue"></div>
      </div>
      <div class="section blue darken-3">
        <div class="row container white-text">

          <div class="row">
            <div class="col s12 m6 l6 hide-on-small-only">
              <img src="./images/Logos/DevT/Racoon_wide_white.svg">
            </div>
            <div class="s12 m6 l6 center-align ">
              <h4>Sistema administrativo AQUATA</h4>
              <br>

              <a class="white-text waves-effect waves-light btn-flat" href="{{ route('login') }}">Ingresar </a>
              <div class="col s12 hide-on-med-and-up">
                <img src="./images/Logos/DevT/Racoon_wide_white.svg">
              </div>
            </div>
          </div>
        </div>
      </div>
    </main>

  <script>
    M.AutoInit();
    $(document).ready(function() {
      $('.preloader-div').fadeOut("slow", function(){
              $('main').fadeTo( "slow", 1);
      });
    });
  </script>
  </body>
</html>
