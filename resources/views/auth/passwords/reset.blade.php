@extends('Layouts.MenuLogin')
@section('title','Cambiar contraseña')
@section('content')
  <main class="valign-wrapper">
      <div class="row div-no-overflow">
        <div class="container">
          <div class="container center-align">
            <br>
            <img src="/images/Logos/Tepetates.png" style="width:20%;">
          </div>

          <form method="POST" action="{{ route('password.request') }}" aria-label="{{ __('Recuperar contraseña') }}">
            @csrf
            <input type="hidden" name="token" value="{{$token}}">
            <div  class="col s12 m8 offset-m2">
              <div class="row">
                <div class="input-field">
                  <i class="material-icons prefix">account_circle</i>
                  <input id="email" type="text" name="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}">
                  @if ($errors->has('email'))
                      <span class="invalid-feedback" role="alert">
                          <strong class="red-text">{{ $errors->first('email') }}</strong>
                      </span>
                  @endif
                  <label for="email">Email</label>
                </div>
              </div>
              <div class="row">
                <div class="input-field">
                  <i class="material-icons prefix">lock</i>
                  <input id="password" type="password" name="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}">
                  @if ($errors->has('password'))
                      <span class="invalid-feedback" role="alert">
                          <strong>{{ $errors->first('password') }}</strong>
                      </span>
                  @endif
                  <label for="password">Nueva contraseña</label>
                </div>
              </div>
              <div class="row">
                <div class="input-field">
                  <i class="material-icons prefix">lock</i>
                  <input id="password-confirm" type="password" name="password_confirmation" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" required>
                  <label for="password-confirm">Confirmar contraseña</label>
                </div>
              </div>
              <div class="center-align">
                  <button type="submit" class="waves-effect waves-light btn blue darken-3 center-align">Cambiar contraseña</button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </main>
@endsection
@section('scripts')
  <script type="text/javascript">
    @if(session('status'))
       green_toast("{{session('status')}}");
    @endif
  </script>
@endsection
