@extends('Layouts.MenuLogin')
@section('title','Recuperar contraseña')
@section('content')

  <main class="valign-wrapper">
      <div class="row div-no-overflow">
        <div class="container">
          <div class="container center-align">
            <br>
            <img src="/images/Logos/Tepetates.png" style="width:20%;">
          </div>

          <form method="POST" action="{{ route('password.email') }}" aria-label="{{ __('Recuperar contraseña') }}">
            @csrf
            <div  class="col s12 m8 offset-m2">
              <div class="row">
                <div class="input-field">
                  <i class="material-icons prefix">account_circle</i>
                  <input id="email" type="text" name="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}">
                  @if ($errors->has('email'))
                      <span class="invalid-feedback" role="alert">
                          <strong class="red-text">{{ $errors->first('email') }}</strong>
                      </span>
                  @endif
                  <label for="email">Email</label>
                </div>
              </div>
              <div class="center-align">
                <button type="submit" class="waves-effect waves-light btn blue darken-3 center-align">Enviar email</button>
              </div>
            </div>
          </form>
        </div>
    </div>
  </main>

@endsection

@section('scripts')
  <script type="text/javascript">
    @if(session('status'))
       green_toast("{{session('status')}}");
    @endif
  </script>
@endsection
