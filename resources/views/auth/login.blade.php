@extends('Layouts.MenuLogin')
@section('title','Iniciar sesión')
@section('content')

    <main class="valign-wrapper">
        <div class="row div-no-overflow">
          <div class="container">
            <div class="container center-align">
              <br>
              <img src="/images/Logos/Tepetates.png" style="width:20%;">
            </div>
            <form method="POST" action="{{ route('login') }}" aria-label="{{ __('Login') }}">
              @csrf
              <div  class="col s12 m8 offset-m2">
                <div class="row">
                  <div class="input-field">
                    <i class="material-icons prefix">account_circle</i>
                    <input id="email" type="text" name="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}">
                    @if ($errors->has('email'))
                        <span class="invalid-feedback" role="alert">
                            <strong class="red-text">{{ $errors->first('email') }}s</strong>
                        </span>
                    @endif
                    <label for="email">Email</label>
                  </div>
                </div>
                <div class="row">
                  <div class="input-field">
                    <i class="material-icons prefix">lock</i>
                    <input id="password" type="password" name="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}">
                    @if ($errors->has('password'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                    @endif
                    <label for="password">Contraseña</label>
                  </div>
                </div>
                <div class="center-align">
                    <button type="submit" class="waves-effect waves-light btn blue darken-3 center-align">Iniciar Sesión</button>
                    <br>
                    <br>
                    <a href="{{ route('password.request') }}"class=" blue-text text-darken-3" style="opacity:0.5;">Olvidé mi contraseña</a></p>
                </div>
              </div>
            </form>
          </div>
        </div>
    </main>
@endsection
