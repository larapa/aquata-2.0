@extends('Layouts.Menu')
@section('title',' '.$user->nombre.' '.$user->aPaterno)
@section('content')
    <main>
      <!-- NOTE: titulo de la seccion -->
      <br>

        <div class="row div-no-overflow">
          <div class="container">
            <div class="col l6 m6 s12">
              <img src="{{url("uploads/".$user->imagen)}}" class="circle responsive-img">
            </div>
              <div class="col l6 m6 s12">

                <div class="container ">
                  <p class="truncate indigo-text text-lighten-4">Nombre</p>
                  <h5 class="truncate indigo-text text-darken-1">{{$user->nombre}} {{$user->aPaterno}} {{$user->aMaterno}}</h5>
                  <p class="truncate indigo-text text-lighten-4">Puesto</p>
                  <h5 class="truncate indigo-text text-darken-1">{{$job->nombre}}</h5>
                  <p class="truncate indigo-text text-lighten-4">Correo</p>
                  <h5 class="truncate indigo-text text-darken-1">{{$user->email}}</h5>
                  <p class="truncate indigo-text text-lighten-4">Telefono</p>
                  <h5 class="truncate indigo-text text-darken-1">{{$user->telefono}}</h5>

                  <br>
                  <a class="waves-effect waves-light btn pink accent-4" href="{{ route('logout') }}"
                     onclick="event.preventDefault();
                                   document.getElementById('logout-form').submit();">
                      Cerrar sesión
                  </a>

                  <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                      @csrf
                  </form>
                </div>
              </div>
          </div>
        </div>
    </main>


    <script>
      document.addEventListener('DOMContentLoaded', function() {
        var elems = document.querySelectorAll('.sidenav');
        var instances = M.Sidenav.init(elems);
      });
    </script>
    <script>
      document.addEventListener('DOMContentLoaded', function() {
        M.AutoInit();
      });
    </script>
    <script>
      document.addEventListener('DOMContentLoaded', function() {
        M.AutoInit();
      });
    </script>
@endsection
