@extends('Layouts.Menu')
@section('title','Retirar elementos')
@section('content')

  <form action="{{route('InventoryArticle.TakeOffValidation')}}" method="POST">
    <input type="hidden" name="id" value="{{$inventoryArticle->id}}">
    <input type="hidden" name="idInventario" value="{{$inventoryArticle->idInventario}}">
    <input type="hidden" name="idArticulo" value="{{$inventoryArticle->idArticulo}}">
    <input type="hidden" name="idUnidad" value="{{$info->idUnidad}}">
    <input type="hidden" name="idCategoria" value="{{$info->idCategoria}}">
    @csrf
    <h4 class="center-align">Retirar artículo: {{$info->articulo}}(s)</h4>
    <div class="row">
      <div class="container">
          <div class="row">
            <div class="col s12 m4 l4 center-align">
              <h4><i class="material-icons">help_outline</i> INFO</h4>

            </div>
            <div class="col s12 m8 l8">
              <p  ALIGN="justify">Al retirar elementos de un artículo significa que estos fueron utilizados,
                es importante que especifiques la cantidad de artículos consumidos y un motivo de su uso. Haremos un
                registro en tu<span class="blue-text"> historial de artículos</span>.
              </p>

            </div>
          </div>
          <div class="row">
            <div class="input-field col s8">
              <input disabled value="{{$info->articulo}} {{$info->cantidadUnidad}} {{$info->unidad}}" id="articulo" type="text">
              <label for="articulo">Nombre</label>
            </div>
            <div class="input-field col s4">
              <input disabled value="{{$inventory->cantidadElementos}}" id="cantidadElementos" type="text">
              <label for="cantidadElementos">Disponibles</label>
            </div>
          </div>
          <div class="row">
            <div class="input-field col s12">
              <input disabled value="{{$info->categoria}}" id="categoria" type="text">
              <label for="categoria">Categoria</label>
            </div>
          </div>
          <div class="row">
            <div class="input-field col s12" id='cantidad-div'>
                <input name="cantidad" id="cantidad" type="number" step="1" min="0" max="{{$inventory->cantidadElementos}}" class="validate" value="" required="" aria-required="true">
              <label for="cantidad">Cantidad a retirar</label>
              <span class="helper-text" id="helper-cantidad">Ingrese la cantidad de artículos a retirar</span>
            </div>
          </div>
          <div class="row">
            <div class="input-field col s12">
              <input value="" id="Motivo" name="motivo" type="text" data-length="20" maxlength="20" required="required" aria-required="true" class="validate">
              <label for="Motivo">Motivo de retiro</label>
              <span class="helper-text" id="helper-motivo">Ingrese el motivo del retiro de los artículos</span>
            </div>
          </div>
          <div class="row center-align">
            <button type="submit" class="waves-effect waves-light btn blue darken-3 center-align">Aceptar</button>

          </div>
      </div>
    </div>
  </form>
@endsection
@section('scripts')
  <script type="text/javascript">
    $(document).ready(function() {
        $('input').characterCounter();
    });

  </script>
@endsection
