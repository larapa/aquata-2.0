@extends('Layouts.Menu')
@section('head','usuario')
@section('title','Economia')
@section('content')

  <ul class="tabs tabs-fixed-width">

    <li class="tab col s4 m4 l4"><a target="_self" href="{{ route('Economy.Expense') }}">Gastos</a></li>
    <li class="tab col s4 m4 l4"><a class="active" href="#swipe-sales">Ventas</a></li>
    <li class="tab col s4 m4 l4"><a target="_self" href="{{ route('Economy.Statistic') }}">Estadisticas</a></li>

  </ul>

  <div id="swipe-sales">
    <!--Panel de control-->
    <div class="row">
      <div class="col s12 m12 l4">
        <div class="card white">
          <div class="card-content blue-text text-darken-2">
            <span class="card-title">Panel de control</span>
            <br>
            <ul class="collapsible popout">
              <!-- NOTE: Filtro de fechas -->
              <li>
                <div class="collapsible-header"><i class="material-icons">date_range</i>Selecionar periodo</div>
                <div class="collapsible-body">
                  <!-- NOTE: formulario para cambiar fechas -->
                  <form class="" id="form-fechas" action="{{route('Economy.SaleByDate')}}" method="post">
                    @csrf
                    <div class="row">
                      <!-- NOTE: datepicker 1 -->
                      <div class="col s12 m12 l12">
                        <h6 class="center-align black-text">Fecha inicial:</h6>
                        <input type="text" name="fecha1" class="datepicker center-align" id="fecha1">
                      </div>
                      <!-- NOTE: datepicker 2 -->
                      <div class="col s12 m12 l12">
                        <h6 class="center-align black-text">Fecha final:</h6>
                        <input type="text" name="fecha2" class="datepicker center-align" id="fecha2">
                      </div>
                    </div>
                    <div class="row">
                      <!-- NOTE: boton para actualizar los datos con las fechas de los pickers -->
                      <div class="col s12 m12 l12 center-align">
                        <br>
                        <a class="waves-effect waves-light btn teal" onclick="document.getElementById('form-fechas').submit();">Consultar</a>
                      </div>
                    </div>
                  </form>
              </li>
            </ul>
          </div>
          <div class="card-action center-align">
            <div class="row">
              <div class="col s6">
                <form id="Form-SaleAdd" action="{{ route('Sale.Add.View') }}" method="get">
                  @csrf
                  <a class="waves-effect waves-light btn teal" onclick="document.getElementById('Form-SaleAdd').submit();"><i class="material-icons right">add</i>Registrar venta</a>
                </form>

              </div>
              <div class="col s6">
                <a id="export_button" class="waves-effect waves-light btn teal" ><i class="material-icons right">file_download</i>Exportar</a>
              </div>
            </div>

          </div>
        </div>
      </div>
        <!-- NOTE: CONTENIDO -->
        <div class="col s12 m12 l8">
          <!-- NOTE: poner aqui los tabs -->
          <div class="card">
            <div class="card-content">
              <div id="descripcion-gastos">
                <!-- NOTE: aqui se pondra una descripcin diferente segun el tab que se muestre -->
              </div>
            </div>
            <!-- NOTE: aqui se definen los tabs -->
            <div class="card-tabs">
              <ul class="tabs">
                <li class="tab col s6"><a href="#tabla-gastos" class="active" id="tab-tabla">Tabla</a></li>
                <li class="tab col s6"><a href="#div-grafica-1" id="tab-grafica-1">Grafica Pastel</a></li>
              </ul>
            </div>
            <!-- NOTE: aqui va el contenido de los tabs -->
            <div class="card-content">
              <div id="tabla-gastos">
                <table class="centered responsive-table highlight">
                  @if (!$sales->isEmpty())
                    <thead>
                      <tr>
                        <th>ID</th>
                        <th>Vendedor</th>
                        <th>Fecha</th>
                        <th class="hide">Descripción</th>
                        <th class="noexport">Total</th>
                      </tr>
                    </thead>
                    <tbody>
                      @foreach($sales as $sale)
                        <tr class="renglon" id="{{$sale->id}}">
                          <td>{{$sale->id}}</td>
                          <td>{{$sale->nombre}} {{$sale->aPaterno}} {{$sale->aMaterno}}</td>
                          <td>{{$sale->fecha}}</td>
                          <td class="hide">{{$sale->descripcion}}</td>
                          @php
                            $total = 0;
                          @endphp
                          @foreach($salesInfo as $sInfo)
                            @if($sInfo->idVenta == $sale->id)
                              @php
                                $total = $total + ($sInfo->precioVenta)*(($sInfo->cantidad / 1000))-($sInfo->descuento);
                              @endphp
                            @endif
                          @endforeach
                          <td class="noexport">$<?php echo($total); ?></td>
                        </tr>
                        <tr class="noexport oculto hide-on-med-and-down" id="oculto-{{$sale->id}}">
                          <td class="noexport" colspan='4'>
                              <div class="row valign-wrapper">
                                <!-- NOTE: descripcion -->
                                <div class="col s8 m8 l8 offset-s1">
                                  <h6>Descripción</h6>
                                  <p align='justify'>
                                    {{$sale->descripcion}}
                                  </p>
                                </div>
                                <!-- NOTE: boton para cancelar -->
                                <div class="col s2 m2 l2">
                                  <form id="Form-SaleCancel-{{$sale->id}}" action="{{ route('Sale.Cancel') }}" method="post">
                                    @csrf
                                    <input type="hidden" name="id" value="{{$sale->id}}">
                                      <a id="CANCEL-SALE-{{$sale->id}}" class="waves-effect waves-light cancel-sale"><i class="material-icons">cancel</i></a>
                                  </form>
                                </div>
                              </div>
                              <table>
                                <thead>
                                  <tr class='blue-grey lighten-5'>
                                    <th>Producto</th>
                                    <th>Piezas</th>
                                    <th>Peso</th>
                                    <th>Precio</th>
                                    <th>Descuento</th>
                                    <th>Sub total</th>
                                  </tr>
                                </thead>
                                <tbody>
                                  @foreach($salesInfo as $sInfo)
                                    @if($sInfo->idVenta == $sale->id)
                                      <tr class='blue-grey lighten-5'>
                                        <td>{{$sInfo->producto}}</td>
                                        <td>{{$sInfo->piezas}}</td>
                                        <td>{{$sInfo->cantidad/1000}}kg</td>
                                        <td>${{$sInfo->precioVenta}}</td>
                                        <td>${{$sInfo->descuento}}</td>
                                        <td>$<?php echo(($sInfo->precioVenta)*(($sInfo->cantidad / 1000))-($sInfo->descuento)); ?></td>
                                      </tr>
                                    @endif
                                  @endforeach
                                  <tr class='blue-grey lighten-5'>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td>Total:</td>
                                    <td>$<?php echo($total); ?></td>
                                  </tr>
                                  <tr class="hide">
                                  </tr>
                                </tbody>
                              </table>
                          </td>
                        </tr>
                      @endforeach
                    </tbody>
                  @else
                    <tbody>
                      <h4>No hay informacion. ¡Vamos a registrar algunas ventas!</h3>
                    </tbody>
                  @endif
                </table>
              </div>

            <div id="div-grafica-1">
              <div class="chart" id="grafica-1">
                <!-- NOTE: AQUI VA LA GRAFICA 1 -->
              </div>
            </div>
          </div>
        </div>

      </div>
    </div>
  </div>

@endsection

@section('modals')
  <div id="modal-info" class="modal modal-fixed-footer">
    <div class="modal-content center-align">

    </div>
    <div class="modal-footer">
      <a class='waves-effect waves-light btn-flat grey-text text-darken-3 modal-close'>Cerrar</a>
    </div>
  </div>
@endsection

@section('modals')
  <div id="modal-info" class="modal modal-fixed-footer">
    <div class="modal-content center-align">

    </div>
    <div class="modal-footer">
      <a class='waves-effect waves-light btn-flat grey-text text-darken-3 modal-close'>Cerrar</a>
    </div>
  </div>
@endsection

@section('scripts')
  <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script src="//cdn.rawgit.com/rainabba/jquery-table2excel/1.1.0/dist/jquery.table2excel.min.js"></script>

  <script type="text/javascript" id="scripts-gastos">
    $(document).ready(function() {
      // NOTE: Validar que fecha 1 sea menor a fecha 2
      $('.oculto').hide();
      $( "#form-fechas" ).submit(function( event ) {
        alert($('#fecha1').datepicker('getDate'));
        if ($('#fecha1').datepicker('getDate') <= $('#fecha2').datepicker('getDate')) {
          return;
        }
        //$("#error-select").html("Seleccione una categoria");
        M.toast({html: 'Seleccione un periodo valido',classes:'red darken-3'});
        event.preventDefault();
      });

      $('#tab-tabla').click();

      //  NOTE: aqui se inicializan los picker 1 y 2
      var hoy = new Date();
      dia = hoy.getDate();
      mes = hoy.getMonth()-1;
      ano = hoy.getFullYear();

      @if(!empty($f1))
        //  NOTE: fehcas solicitadas por el usuario
        dia1 = {{ substr($f1,0,2) }};
        mes1 = {{ substr($f1,3,2) }};
        ano1 = {{ substr($f1,6,4) }};
        dia2 = {{ substr($f2,0,2) }};
        mes2 = {{ substr($f2,3,2) }};
        ano2 = {{ substr($f2,6,4) }};
        $('#fecha1').datepicker({
          format: 'dd/mm/yyyy',
          setDefaultDate: true,
          defaultDate: new Date(ano1,mes1-1,dia1),
          minDate: new Date(2020,03,23),
          maxDate: hoy,
          i18n: {
            months: ['Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'],
            monthsShort: ['Ene','Feb','Mar','Abr','May','Jun','Jul','Ago','Sep','Oct','Nov','Dic'],
            weekdays: ['Domingo','Lunes','Martes','Miércoles','Jueves','Viernes','Sábado'],
            weekdaysShort: ['Dom','Lun','Mar','Mier','Jue','Vier','Sab'],
            weekdaysAbbrev: ['D','L','M','M','J','V','S']
          },

        });

        $('#fecha2').datepicker({
          format: 'dd/mm/yyyy',
          setDefaultDate: true,
          defaultDate: new Date(ano2,mes2-1,dia2),
          minDate: new Date(2020,03,23),
          maxDate: hoy,
          i18n: {
            months: ['Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'],
            monthsShort: ['Ene','Feb','Mar','Abr','May','Jun','Jul','Ago','Sep','Oct','Nov','Dic'],
            weekdays: ['Domingo','Lunes','Martes','Miércoles','Jueves','Viernes','Sábado'],
            weekdaysShort: ['Dom','Lun','Mar','Mier','Jue','Vier','Sab'],
            weekdaysAbbrev: ['D','L','M','M','J','V','S']
          },

        });
      @else
        //  NOTE: fecha de hoy y de hace un mes
        $('#fecha1').datepicker({
          format: 'dd/mm/yyyy',
          setDefaultDate: true,
          defaultDate: new Date(ano,mes,dia),
          minDate: new Date(2020,03,23),
          maxDate: hoy,
          i18n: {
            months: ['Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'],
            monthsShort: ['Ene','Feb','Mar','Abr','May','Jun','Jul','Ago','Sep','Oct','Nov','Dic'],
            weekdays: ['Domingo','Lunes','Martes','Miércoles','Jueves','Viernes','Sábado'],
            weekdaysShort: ['Dom','Lun','Mar','Mier','Jue','Vier','Sab'],
            weekdaysAbbrev: ['D','L','M','M','J','V','S']
          },

        });

        $('#fecha2').datepicker({
          format: 'dd/mm/yyyy',
          setDefaultDate: true,
          defaultDate: hoy,
          minDate: new Date(2020,03,23),
          maxDate: hoy,
          i18n: {
            months: ['Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'],
            monthsShort: ['Ene','Feb','Mar','Abr','May','Jun','Jul','Ago','Sep','Oct','Nov','Dic'],
            weekdays: ['Domingo','Lunes','Martes','Miércoles','Jueves','Viernes','Sábado'],
            weekdaysShort: ['Dom','Lun','Mar','Mier','Jue','Vier','Sab'],
            weekdaysAbbrev: ['D','L','M','M','J','V','S']
          },

        });
      @endif

       @if(session('success'))
          M.toast({html: '{{session('success')}}',classes:'green darken-2'})
       @endif
       @if(session('error'))
          red_toast("{{session('error')}}");
       @endif

       var init_tabs = M.Tabs.init($('.tabs'), { onShow: drawCharts });
    });

    document.addEventListener('DOMContentLoaded', function() {
      var elems = document.querySelectorAll('.dropdown-trigger');
      var options =
        {
          constrainWidth:false,
          alignment:'right'
        };
      var instances = M.Dropdown.init(elems, options);
    });

    //  NOTE: mesajes para las Ventas
    $('#tab-tabla').click(function(){
      @if(!empty($f1))
        //  NOTE: fechas solicitadas por el usuario
        dia1 = {{ substr($f1,0,2) }};
        mes1 = {{ substr($f1,3,2) }};
        ano1 = {{ substr($f1,6,4) }};
        dia2 = {{ substr($f2,0,2) }};
        mes2 = {{ substr($f2,3,2) }};
        ano2 = {{ substr($f2,6,4) }};

        $('#descripcion-gastos').empty();
        $('#descripcion-gastos').append(
          "En la siguiente tabla se muestran las ventas registrados entre las fechas " + dia1 + "/" + mes1 + "/" + ano1 + " y " +
          dia2 + "/" + mes2 + "/" + ano2 + "."
        );
      @else
        var hoy = new Date();
        dia = hoy.getDate();
        mes = hoy.getMonth();
        ano = hoy.getFullYear();
        $('#descripcion-gastos').empty();
        $('#descripcion-gastos').append(
          "En la siguiente tabla se muestran las ventas registrados entre las fechas " + dia + "/" + mes + "/" + ano + " y " +
           + dia + "/" + (mes+1) + "/" + ano + "."
        );
      @endif

    });
    $('#tab-grafica-1').click(function(){
      @if(!empty($f1))
        //  NOTE: fehcas solicitadas por el usuario
        dia1 = {{ substr($f1,0,2) }};
        mes1 = {{ substr($f1,3,2) }};
        ano1 = {{ substr($f1,6,4) }};
        dia2 = {{ substr($f2,0,2) }};
        mes2 = {{ substr($f2,3,2) }};
        ano2 = {{ substr($f2,6,4) }};

        $('#descripcion-gastos').empty();
        $('#descripcion-gastos').append(
          "En la siguiente grafica se muestran las ventas registrados entre las fechas " + dia1 + "/" + mes1 + "/" + ano1 + " y " +
          dia2 + "/" + mes2 + "/" + ano2 + "."
        );
      @else
        var hoy = new Date();
        dia = hoy.getDate();
        mes = hoy.getMonth();
        ano = hoy.getFullYear();
        $('#descripcion-gastos').empty();
        $('#descripcion-gastos').append(
          "En la siguiente grafica se muestran las ventas registrados entre las fechas " + dia + "/" + mes + "/" + ano + " y " +
           + dia + "/" + (mes+1) + "/" + ano + "."
        );
      @endif
    });

    //  ventas

    $('.cancel-sale').click(function(event) {
      $id = $(this).attr('id');
      $id = $id.substring(12, $id.lenght);

      $('.modal-content').empty();
      $('.modal-content').append(
        "<h3>Cancelar  <i class='material-icons small teal-text'>cancel</i></h3>"+
        "<div class='row'>"+
          "<p>Esta a punto de cancelar una venta, debe considerar que esta accion no se puede deshacer</p>"+
          "<br>"+
          "<p>¿Esta seguro que desea continuar?</p>"+
        "</div>"
      );
      $('.modal-footer').empty();
      $('.modal-footer').append(
        "<a class='waves-effect waves-light btn-flat grey-text text-darken-3 modal-close'>Cerrar</a>"+
        "<a id='"+$id+"' class='waves-effect waves-light btn-flat red-text form-submit'>Eliminar</a>"
      );
      $('#modal-info').modal('open');

    });

    $('body').on('click', '.form-submit', function() {
      document.getElementById("Form-SaleCancel-"+$(this).attr('id')+"").submit();
    });

    //  NOTE: evento click para mostrar los detalles de la venta
    $(".renglon").click(function(){
      $id = $(this).attr('id');

      if ($(window).width() > 992) {
        //  NOTE: PANTALLA GRANDE
        if ($('#oculto-'+$id).is(":visible")) {
          $("#oculto-"+$id).fadeOut();
        }
        else {
          $('#oculto-'+$id).fadeIn();
        }
      }
      else {
        //  NOTE: pantalla chica
        modalInfoVenta($id);
      }
    });

    function modalInfoVenta($id){

      $('.modal-content').empty();

      @foreach($sales as $sale)

        if({{$sale->id}} == $id){
          //  descripcion
          $('.modal-content').append(
            "<div class='row valign-wrapper'>"+
              "<div class='col s8 m8 l8 offset-s1'>"+
                "<h6>Descripción</h6>"+
                "<p align='justify'>{{$sale->descripcion}}</p>"+
              "</div>"+
              //  boton para cancelar
              "<div class='col s2 m2 l2'>"+
                "<form id='Form-SaleCancel-{{$sale->id}}' action='{{ route('Sale.Cancel') }}' method='post'>"+
                  '@csrf'+
                  "<input type='hidden' name='id' value='{{$sale->id}}'>"+
                    "<a id='CANCEL-SALE-{{$sale->id}}' class='waves-effect waves-light cancel-sale'><i class='material-icons'>cancel</i></a>"+
                "</form>"+
              "</div>"+
            "</div>"
          );
          //  cuerpo de la tabla de detalles
          $('.modal-content').append(
            "<table class='centered'>"+
              "<thead>"+
                "<tr class='blue-grey lighten-5'>"+
                  "<th>Producto</th>"+
                  "<th>Piezas</th>"+
                  "<th>Peso</th>"+
                  "<th>Precio</th>"+
                  "<th>Descuento</th>"+
                  "<th>Sub total</th>"+
                "</tr>"+
              "</thead>"+
              "<tbody>"+
                @foreach($salesInfo as $sInfo)
                  @if($sInfo->idVenta == $sale->id)
                      "<tr class='noexport blue-grey lighten-5'>"+
                        "<td class='noexport'>{{$sInfo->producto}}</td>"+
                        "<td>{{$sInfo->piezas}}</td>"+
                        "<td>{{$sInfo->cantidad/1000}}</td>"+
                        "<td>${{$sInfo->precioVenta}}</td>"+
                        "<td>{{$sInfo->descuento}}</td>"+
                        "<td>$<?php echo(($sInfo->precioVenta)*(($sInfo->cantidad / 1000))-($sInfo->descuento)); ?></td>"+
                      "</tr>"+
                  @endif
                @endforeach
                "<tr class='noexport blue-grey lighten-5'>"+
                  "<td></td>"+
                  "<td></td>"+
                  "<td></td>"+
                  "<td></td>"+
                  "<td class='noexport'>Total:</td>"+
                  "<td>$"+
                    @php
                      $total_modal = 0;
                      foreach($salesInfo as $sInfo){
                        if($sInfo->idVenta == $sale->id){
                          $total_modal = $total_modal + ($sInfo->precioVenta)*(($sInfo->cantidad / 1000))-($sInfo->descuento);
                        }
                      }
                      echo $total_modal;
                    @endphp
                  +"</td>"+
                "</tr>"+
              "</tbody>"+
            "</table>"
          );
        }
      @endforeach

      $('.modal-footer').empty();
      $('.modal-footer').append(
        "<a class='waves-effect waves-light btn-flat grey-text text-darken-3 modal-close'>Cerrar</a>"
      );
      $('#modal-info').modal('open');
    }

    $("#export_button").click(function(){
      $("#tabla-gastos").table2excel({
          // exclude CSS class
          exclude:".noexport",
          name:"Worksheet Name",
          filename:"Ventas"//do not include extension
      });
    });

    //  NOTE: GRAFICAS
    google.load("visualization", "1", {packages:["corechart"]});
    google.charts.load('current', {'packages':['corechart']});

    google.setOnLoadCallback(drawCharts);

    //  NOTE: grafica de pastel
    function drawChart1() {
      var data = google.visualization.arrayToDataTable([
        ['Producto', 'Ventas totales'],

        @foreach($salesChart as $sChart)
          ['{{$sChart->producto}}', {{$sChart->total}}],
        @endforeach
      ]);

      var options = {
        title: '',
        is3D: true,
        colors: ['#536dfe','#e53935','#ffb74d','#43a047','#673ab7','#42a5f5','#ec407a','#8bc34a','#c51162','#0277bd','#6a1b9a','#4db6ac','#cddc39']
      };

        var chart = new google.visualization.PieChart(document.getElementById('grafica-1'));
        chart.draw(data, options);
    }

    function drawCharts(){
      drawChart1();
    }
    window.addEventListener("resize", drawCharts);

  </script>

@endsection
