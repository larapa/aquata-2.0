@extends('Layouts.Menu')
@section('title','Nuevo artículo')
@section('content')

  @if (session('mensaje'))
    <div class="alert"> {{session('mensaje')}} </div>
  @endif

  <form action="{{route('Article.AddUpdate')}}" method="POST">
    @csrf

    <h4 class="center-align">Agregar artículo</h4>
    <div class="row">
      <div class="container">
        <div class="col s12">
          <div class="row">
            <div class="col s12 m4 l4 center align">
              <h4><i class="material-icons">help_outline</i> INFO</h4>
            </div>
            <div class="col s12 m8 l8">
              <p  ALIGN="justify">
                En este apartado puedes agregar un <span class="blue-text">artículo</span>, este elemento
                necesita de una categoría, una unidad y su respectiva cantidad, esto nos permite organizar de
                mejor manera el <span class="blue-text">Inventario de artículos</span>. Asegurate de ingresar información lo más descriptiva y simple posible.
                <p>Podrás ver tus artículos en el panel de control del apartado <span class="blue-text">Inventario de artículos</span>.</p>
              </p>
            </div>
          </div>
          <!-- NOTE: Nombre -->
          <div class="row">
            <div class="input-field col s12">
              <input id="article_nombre" name="nombre" type="text" data-length="30" maxlength="30" required="" aria-required="true" class="validate" value="">
              <label for="article_nombre">Nombre</label>
              <span class="help-text" data-error="Incorrecto" data-success="Correcto">Ingrese el nombre del articulo</span>
            </div>
          </div>
          <!-- NOTE: Descripcion -->
          <div class="row">
            <div class="input-field col s12">
              <textarea id="article_descripcion" class="validate materialize-textarea" name="descripcion" type="text" data-length="120" maxlength="120" required="" aria-required="true"></textarea>
              <label for="article_descripcion">Descripción</label>
              <span class="help-text" data-error="Incorrecto" data-success="Correcto">Ingrese la descripción</span>
            </div>
          </div>
          <!-- NOTE: Cantidad Unidad -->
          <div class="row">
            <div class="input-field col s12">
              <input id="article_cantUnidad"  name="cantidadUnidad" type="number" step="1" min="0" data-length="11" maxlength="11" required="" aria-required="true" value="">
              <label for="article_cantUnidad">Agregar cantidad de la unidad</label>
              <span class="help-text" data-error="Incorrecto" data-success="Correcto">Ingrese la cantidad total de la unidad</span>
            </div>
          </div>
          <!-- NOTE: Unidad -->
          <div class="row">
            <div class="input-field col s12">
              <select name='idUnidad'>
                <option value="">Selecciona una unidad</option>
                @foreach ($units as $unit)
                  <option class="truncate" value="{{$unit->id}}">{{$unit->nombre}}</option>
                @endforeach
              </select>
              <label>Unidad</label>
            </div>
          </div>
          <div class="row">
            <div class="input-field col s12">
              <select name='idCategoriaArticulo'>
                <option value="">Selecciona una categoria</option>
                @foreach ($categories as $category)
                  <option class="truncate" value="{{$category->id}}">{{$category->nombre}}</option>
                @endforeach
              </select>
              <label>Categoria</label>
            </div>
          </div>
        <div class="center-align">
          <button type="submit" class="waves-effect waves-light btn blue darken-3 center-align">Aceptar</button>
        </div>
      </div>
    </div>
  </form>

@endsection

@section('scripts')
  <script type="text/javascript">

    $(document).ready(function() {
      $('input, textarea').characterCounter();
    });

  </script>
@endsection
