@extends('Layouts.Menu')
@section('title','Editar categoría')
@section('content')

  @if (session('mensaje'))
    <div class="alert"> {{session('mensaje')}} </div>
  @endif



  <form action="{{route('ArticleCategory.Update')}}" method="POST">
    <input type="hidden" name="id" value="{{$category->id}}">
    @csrf

    <h4 class="center-align">Categoría: {{ $category->nombre }}</h4>
    <div class="row">
      <div class="container">
        <div class="col s12">
          <div class="row">
            <div class="col s12 m4 l4 center align">
              <h4><i class="material-icons">help_outline</i> INFO</h4>

            </div>
            <div class="col s12 m8 l8">
              <p  ALIGN="justify">
                En este apartado puedes editar la información de <span class="blue-text">
                {{$category->nombre}}</span>, Asegurate de ingresar información lo más descriptiva
                y simple posible.
              </p>
            </div>
          </div>
          <!-- NOTE: NOMBRE -->
          <div class="row">
            <div class="input-field col s12">
              <input id="motive_nombre" name="nombre" type="text" data-length="30" maxlength="30" required="" aria-required="true" class="validate" value="{{ $category->nombre }}">
              <label for="motive_nombre">Nombre</label>
              <span class="help-text" data-error="Incorrecto" data-success="Correcto">Ingrese el nombre de la categoria del articulo</span>
            </div>
          </div>
          <!-- NOTE: DESCRIPCION -->
          <div class="row">
            <div class="input-field col s12">
              <textarea id="motive_descripcion" class="validate materialize-textarea" name="descripcion" type="text" data-length="120" maxlength="120" required="" aria-required="true">{{ $category->descripcion }}</textarea>
              <label for="motive_descripcion">Descripción</label>
              <span class="help-text" data-error="Incorrecto" data-success="Correcto">Ingrese la descripción</span>
            </div>
          </div>
        </div>
        <div class="center-align">
          <button type="submit" class="waves-effect waves-light btn blue darken-3 center-align">Aceptar</button>
        </div>
      </div>
    </div>
  </form>

@endsection

@section('scripts')
  <script type="text/javascript">

    $(document).ready(function() {
      $('input, textarea').characterCounter();
    });

  </script>
@endsection
