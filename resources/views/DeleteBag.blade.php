@extends('Layouts.Menu')
@section('title','Eliminar costal')
@section('content')

  @if (session('mensaje'))
    <div class="alert"> {{session('mensaje')}} </div>
  @endif

  <form action="{{route('Bag.UpdateDelete')}}" method="POST">
    <input type="hidden" name="id" value="{{$bag->id}}">
    @csrf

    <h4 class="center-align">Costal: {{ $bag->etiqueta }}</h4>
    <div class="row">
      <div class="container">
        <div class="col s12">
          <div class="row">
            <div class="col s12 m4 l4 center-align">
              <h4><i class="material-icons">help_outline</i> INFO</h4>

            </div>
            <div class="col s12 m8 l8">
              <p  ALIGN="justify">Cuando eliminas un costal con esta función, significa que estás
                 <span class="red-text text-darken-2">desechando este alimento</span>,
                es por esto que requerimos el motivo de la eliminación del costal. Esta acción será registrada en el
                <span class="blue-text"> historial de alimentación</span>.
              </p>
              <p  ALIGN="justify">
                Si te equivocaste al agregar un lote de costales y quieres deshacer este error, deberás ir a la seccion de
                <span class="blue-text">Gastos</span> en el apartado de <span class="blue-text">Economía</span>,
                ahí encontrarás todos los registros de compra que se han realizado.
                Busca el registro correspondiente a tu compra y realiza una <span class="blue-text">cancelación</span>, esto
                eliminará el lote de costales relacionado con ese gasto. Es importante no abusar de esta función, utilizala con
                precaución.
              </p>
            </div>
          </div>
          <!-- NOTE: Etiqueta -->
          <div class="row">
            <div class="input-field col s12">
              <input id="bag_etiqueta" name="" type="text" data-length="30" maxlength="30" required="" aria-required="true" class="validate" value="{{ $bag->etiqueta }}" disabled>
              <input type="hidden" name="etiqueta" value="{{ $bag->etiqueta }}">
              <label for="bag_etiqueta">Etiqueta</label>
            </div>
          </div>
          <!-- NOTE: Tamano -->
          <div class="row">
            <div class="input-field col s12">
              <input id="bag_tamano"  name="" type="number" step="0.01" min="0" data-length="11" maxlength="11" required="" aria-required="true" value="{{ $bag->tamano }}" disabled>
              <input type="hidden" name="tamano" value="{{ $bag->tamano }}">
              <label for="bag_tamano">Cantidad total en gramos del costal</label>
            </div>
          </div>
          <!-- NOTE: Alimento restante -->
          <div class="row">
            <div class="input-field col s12">
              <input id="bag_alimentoRestante"  name="" type="number" step="0.01" min="0" data-length="11" maxlength="11" required="" aria-required="true" value="{{ $bag->alimentoRestante }}" disabled>
              <input type="hidden" name="alimentoRestante" value="0">
              <label for="bag_alimentoRestante">Cantidad restante en gramos del costal</label>
            </div>
          </div>
          <!-- NOTE: Tipo alimento -->
          <div class="row">
            <div class="input-field col s12">
              <select name='h' disabled >
                <option value="">Tipo de alimento</option>
                @foreach ($types as $type)
                  <option class="truncate" value="{{$type->id}}"@if($type->id == $bag->idTipoAlimento) selected @endif>{{$type->nombre}}</option>
                @endforeach
              </select>
              <label>Tipo alimento</label>
              <input type="hidden" name="idTipoAlimento" value="{{$bag->idTipoAlimento}}">
            </div>
          </div>
          <!-- NOTE: Motivo -->
          <div class="row">
            <div class="input-field col s12">
              <select name='idMotivo'>
                <option value="">Selecciona un motivo por el cual elimina el costal</option>
                @foreach ($motives as $motive)
                  <option class="truncate" value="{{$motive->id}}">{{$motive->nombre}}</option>
                @endforeach
              </select>
              <label>Motivo de eliminación</label>
                <input type="hidden" name="idCostal" value="{{$bag->id}}">
            </div>
          </div>
        <div class="center-align">
          <button type="submit" class="waves-effect waves-light btn blue darken-3 center-align">Eliminar</button>
        </div>
      </div>
    </div>
  </form>

@endsection

@section('scripts')
  <script type="text/javascript">

    $(document).ready(function() {
    //  $('input').characterCounter();
    });

  </script>
@endsection
