@extends('Layouts.Menu')
@section('title','Nuevo Producto')
@section('content')

  <form action="{{route('Product.AddValidation')}}" method="POST" enctype="multipart/form-data">
    @csrf
    <h4 class="center-align">Agregar producto</h4>
    <div class="row">
      <div class="container">
        <div class="row">
          <div class="col s12 m4 l4 center align">
            <h4><i class="material-icons">help_outline</i> INFO</h4>

          </div>
          <div class="col s12 m8 l8">
            <p  ALIGN="justify">
              En este apartado puedes agregar un <span class="blue-text">producto</span>.
              Este elemento se mostrará publicamente en
              la página de presentación, asegurate de agregar una descripción e imagen muy atractivas.
              <p>Podrás ver tus motivos de uso en panel de control del apartado <span class="blue-text">Inventario de productos</span>.</p>
            </p>

          </div>
        </div>
        <div class="row">
          <div class="input-field col s12">
            <input id="nombre" name="nombre" type="text" data-length="45" class="validate" maxlength="45" required="required" >
            <label for="nombre">Nombre</label>
            <span class="helper-text" data-error="Incorrecto" data-success="Correcto">Ingresa el nombre del producto</span>
          </div>
        </div>
        <div class="row">
          <div class="input-field col s12">
            <textarea id="descripcion" name="descripcion" type="text" data-length="120" class="validate materialize-textarea" maxlength="120" required="required"></textarea>
            <label for="descripcion">Descripción</label>
            <span class="helper-text" data-error="Incorrecto" data-success="Correcto">Ingresa la descripcion del producto</span>
          </div>
        </div>
        <div class="row">
          <div class="file-field input-field col s12">
            <div class="btn">
              <span>Imagen</span>
              <input type="file" name="imagen">
            </div>
            <div class="file-path-wrapper">
              <input class="file-path validate" type="text" id="imagen" value="" required="required">
              <span class="helper-text" data-error="Incorrecto" data-success="Correcto">Selecciona la imagen del producto</span>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col s3 left-align switch">
            <br>
            <label>
              Activo
              <input type="hidden" name="T" value="0">
              <input name="T" type="checkbox" value="1" checked='checked'>
              <span class="lever"></span>
            </label>
          </div>
          <div class="input-field col s6">
            <input name="precio" id="precio" type="number" step=".01" min="0" class="validate" required="required">
            <label for="precio">Precio</label>
            <span class="helper-text">Ingrese el precio del producto</span>
          </div>
        </div>

        <div class="center-align">
          <button type="submit" class="waves-effect waves-light btn blue darken-3 center-align">Aceptar</button>
        </div>
      </div>
    </div>
  </form>

@endsection
@section('scripts')
  <script type="text/javascript">
    $(document).ready(function() {
      $('input#nombre, textarea#descripcion').characterCounter();
    });
  </script>
@endsection
