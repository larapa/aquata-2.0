@extends('Layouts.Menu')
@section('title','Nuevo alimento')
@section('content')

  @if (session('mensaje'))
    <div class="alert"> {{session('mensaje')}} </div>
  @endif



  <form action="{{route('Type.AddUpdate')}}" method="POST">
    @csrf

    <h4 class="center-align">Agregar tipo de alimento</h4>
    <div class="row">
      <div class="container">
        <div class="col s12">
          <div class="row">
            <div class="col s12 m4 l4 center align">
              <h4><i class="material-icons">help_outline</i> INFO</h4>
            </div>
            <div class="col s12 m8 l8">
              <p  ALIGN="justify">
                En este apartado puedes agregar un <span class="blue-text">tipo de alimento</span>, este elemento
                sirve para organizar de mejor manera tus costales y la alimentación de las truchas. Asegurate de ingresar información lo más descriptiva y simple posible.
                <p>Podrás ver tus tipos de alimento en el panel de control del apartado <span class="blue-text">Inventario de alimento</span>.</p>
              </p>
            </div>
          </div>
          <!-- NOTE: NOMBRE -->
          <div class="row">
            <div class="input-field col s12">
              <input id="type_nombre" name="nombre" type="text" data-length="30" maxlength="30" required="" aria-required="true" class="validate" value="">
              <label for="type_nombre">Nombre</label>
              <span class="help-text">Ingrese el nombre del tipo de alimento</span>
            </div>
          </div>
          <!-- NOTE: PESO -->
          <div class="row">
            <div class="input-field col s12">
              <textarea id="type_descripcion" name="descripcion" type="text" data-length="120" maxlength="120" required="" aria-required="true" class="validate materialize-textarea"></textarea>
              <label for="type_descripcion">Descripción</label>
              <span class="help-text">Ingrese la descripción</span>
            </div>
          </div>
        </div>
        <div class="center-align">
          <button type="submit" class="waves-effect waves-light btn blue darken-3 center-align">Aceptar</button>
        </div>
      </div>
    </div>
  </form>

@endsection

@section('scripts')
  <script type="text/javascript">

    $(document).ready(function() {
      $('input#type_nombre, textarea').characterCounter();
    });

  </script>
@endsection
