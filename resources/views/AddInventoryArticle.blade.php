@extends('Layouts.Menu')
@section('title','Inventario de artículos')
@section('content')

  @if (session('mensaje'))
    <div class="alert"> {{session('mensaje')}} </div>
  @endif

  <form action="{{route('InventoryArticle.AddUpdate')}}" method="POST">
    @csrf

    <h4 class="center-align">Inventario de artículos</h4>
    <div class="row">
      <div class="container">
        <div class="row">
          <div class="col s12 m4 l4 center align">
            <h4><i class="material-icons">help_outline</i> INFO</h4>
          </div>
          <div class="col s12 m8 l8">
            <p  ALIGN="justify">
              El <span class="blue-text">Sistema Administrativo AQUATA</span> cuenta con un inventario especial para la
              administración de tus artículos,
              en este apartado podrás seleccionar un <span class="blue-text">artículo</span> e ingresar la cantidad que deseas agregar de
              dicho artículo. Se creará automáticamente un <span class="blue-text">gasto</span> con el monto ingresado. Asegurate de
              ingresar la información correcta.
            </p>
          </div>
        </div>
        <div class="col s12">
          <!-- NOTE: Unidad -->
          <div class="row">
            <div class="input-field col s12">
              <select name='idArticulo'>
                <option value="" selected disabled>Selecciona un articulo</option>
                @foreach ($articles as $article)
                  <option class="truncate" value="{{$article->idArticulo}}">{{$article->nombre}} {{$article->cantidadUnidad}} {{$article->unidad}}</option>
                @endforeach
              </select>
              <label>Articulo</label>
              <span id="error-select" class="red-text darken-2"></span>

            </div>
          </div>
          <!-- NOTE: Cantidad -->
          <div class="row">
            <div class="input-field col s12">
              <input id="InventoyArticle_cantidadElementos"  name="cantidadElementos" type="number" step="1" min="0" data-length="11" maxlength="11" required="" aria-required="true" value="">
              <label for="InventoyArticle_cantidadElementos">Agregar cantidad de elementos</label>
              <span class="help-text" data-error="Incorrecto" data-success="Correcto">Ingrese la cantidad de elementos</span>
            </div>
          </div>
          <!-- NOTE: Costo -->
          <div class="row">
            <div class="input-field col s12">
              <input id="InventoyArticle_costo"  name="costo" type="number" step="0.01" min="0" data-length="11" maxlength="11" required="" aria-required="true" value="">
              <label for="InventoyArticle_costo">Agregar costo total del articulo</label>
              <span class="help-text" data-error="Incorrecto" data-success="Correcto">Ingrese el costo total del articulo</span>
            </div>
          </div>
          <div class="row">
            <div class="input-field col s12">
              <input value="" id="motivo" name="motivo" type="text" data-length="20" maxlength="20" required="required" aria-required="true" class="validate">
              <label for="motivo">Ingresar motivo de añadido de articulos</label>
            </div>
          </div>
          <div class="center-align">
            <button type="submit" class="waves-effect waves-light btn blue darken-3 center-align">Aceptar</button>
          </div>
      </div>
    </div>
  </form>

@endsection

@section('scripts')
  <script type="text/javascript">

    $(document).ready(function() {
      $('input, textarea').characterCounter();
    });

    $( "Form" ).submit(function( event ) {
      if ( $( "select" ).first().val() != null ) {
        return;
      }
      $("#error-select").html("Seleccione un articulo");
      M.toast({html: 'Seleccione un articulo',classes:'red darken-3'});
      //alert("Seleccione un tipo de trucha");
      $(document).scrollTop(0);
      event.preventDefault();
    });

  </script>
@endsection
