
@extends('Layouts.Menu')

@section('title','Trabajadores')

@section('content')

  <div class="row">
    <!-- NOTE: Panel de control -->
    <div class="col s12 m12 l4">
      <div class="card white">
        <div class="card-content blue-text text-darken-2">
          <span class="card-title">Panel de control</span>

          <!-- NOTE: switch para mostrar puestos inactivos -->
          <div class="row">
            <div class="col s8 m8 l8">
              ¿Mostrar empleados inactivos?
            </div>
            <div class="col s4 m4 l4">
              <div class="switch right-align">
               <label>
                 <input type="checkbox" id="employes-switch">
                 <span class="lever" id="employes-lever"></span>
               </label>
             </div>
            </div>
          </div>

          <ul class="collapsible popout expandable">
            <li>
              <div class="collapsible-header"><i class="material-icons">assignment_ind</i>Puestos</div>
              <div class="collapsible-body">
                <!-- NOTE: switch para mostrar puestos inactivos -->
                <div class="row">
                  <div class="col s8 m8 l8">
                    ¿Mostrar puesto inactivos?
                  </div>
                  <div class="col s4 m4 l4">
                    <div class="switch right-align">
                     <label>
                       <input type="checkbox" id="jobs-switch">
                       <span class="lever" id="jobs-lever"></span>
                     </label>
                   </div>
                  </div>
                </div>
                <!-- NOTE: Ver puestos -->
                <div>
                  <ul class="collection with-header">
                    @if(!$jobs->isEmpty())
                      @foreach($jobs as $job)
                        @if($job->T == '1' && $job->id != '1')
                          <li class="collection-item avatar usefull-job">
                            <div class="row">
                              <!-- NOTE: CONTENIDO -->
                              <div class="col s10 m10 l10">
                                <i class="material-icons circle blue">content_paste</i>
                                <span class="title">{{$job->nombre}}</span>
                                <p class="grey-text truncate">Puesto</p>
                              </div>
                              <!-- NOTE: BOTON DEL DROPDOWN -->
                              <div class="col s2 m2 l2">
                                <a href="#" class="secondary-content dropdown-trigger" data-target="job-dropdown-{{$job->id}}">
                                  <i class="material-icons right">arrow_drop_down</i>
                                </a>
                              </div>
                            </div>
                          </li>
                        @else
                          @if($job->id != '1')
                            <li class="collection-item avatar useless-job">
                              <div class="row">
                                <!-- NOTE: CONTENIDO -->
                                <div class="col s10 m10 l10">
                                  <i class="material-icons circle indigo lighten-4">content_paste</i>
                                  <span class="title grey-text">{{$job->nombre}}</span>
                                  <p class="grey-text truncate">Puesto</p>
                                </div>
                                <!-- NOTE: BOTON DEL DROPDOWN -->
                                <div class="col s2 m2 l2">
                                  <a href="#" class="secondary-content dropdown-trigger" data-target="job-dropdown-{{$job->id}}">
                                    <i class="material-icons right">arrow_drop_down</i>
                                  </a>
                                </div>
                              </div>
                            </li>
                          @endif
                        @endif
                      @endforeach
                    @else
                      <p>No hay elementos</p>
                    @endif
                  </ul>
                </div>
                <div class="row">
                  <div class="col s12 m12 l12 center-align">
                    <br>
                    <a href="{{ route('Job.Add.View') }}" class="waves-effect waves-light btn-small teal"><i class="material-icons right">add</i>Agregar puesto</a>
                  </div>
                </div>
              </div>
            </li>
          </ul>
        </div>
        <div class="card-action center-align">
          <a href="{{ route('Employee.Add.View') }}" class="waves-effect waves-light btn-small teal white-text"><i class="material-icons right">add</i>Agregar empleado</a>
        </div>
      </div>
    </div>

    <!-- NOTE: Collection de empleados ordenados por puesto -->
    <div class="col s12 m12 l8">
      <ul class="collection with-header">
        @if(!$jobs->isEmpty())
          @foreach($jobs as $job)
            @if($job->T == '0')
              <?php continue; ?>
            @endif
            @if(!$employes->isEmpty())
              <?php $bandera=0; ?>
              @foreach($employes as $employe)
                @if($job->id == $employe->idPuesto)
                  @if($job->id == '1')
                    <!-- NOTE: sin puesto -->
                    @if($bandera==0)
                      <li class="collection-header useless-employe"><h5>{{$job->nombre}}</h5></li>
                      <?php $bandera=1; ?>
                    @endif
                    <li class="collection-item avatar useless-employe">
                      <i class="material-icons circle indigo lighten-4"><img id="imagen-empleado-{{$employe->id}}" src= "{{url("uploads/".$employe->imagen)}}" width="100%" height="100%" style="opacity: 0.5;"></i>
                      <span class="title">{{$employe->nombre}} {{$employe->aPaterno}} {{$employe->aMaterno}}</span>
                      <p class="blue-grey-text">{{$employe->email}}</p>
                      <a href="#" class="secondary-content btn-flat teal-text dropdown-trigger" data-target="dropdown-{{$employe->id}}">
                        <i class="material-icons">settings</i>
                      </a>
                    </li>
                  @else
                    <!-- NOTE: con algun puesto -->
                    @if($bandera==0)
                      <li class="collection-header usefull-employe"><h5>{{$job->nombre}}</h5></li>
                      <?php $bandera=1; ?>
                    @endif
                    <li class="collection-item avatar usefull-employe">
                      <i class="material-icons circle blue"><img id="imagen-empleado-{{$employe->id}}" src="{{url("uploads/".$employe->imagen)}}" width="100%" height="100%"></i>
                      <span class="title">{{$employe->nombre}} {{$employe->aPaterno}} {{$employe->aMaterno}}</span>
                      <p class="blue-grey-text">{{$employe->email}}</p>
                      <div class="secondary-content">
                        @if($employe->idUsuario != null)
                          <a class="btn-flat teal-text tooltipped" data-tooltip="Usuario {{$employe->rol}}">
                            <i class="material-icons">person</i>
                          </a>
                        @endif
                        <a href="#" class="btn-flat teal-text dropdown-trigger" data-target="dropdown-{{$employe->id}}">
                          <i class="material-icons">settings</i>
                        </a>
                      </div>
                    </li>

                  @endif
                @else
                  <?php continue; ?>
                @endif
              @endforeach
            @else
              <p>No hay elementos</p>
            @endif
          @endforeach
        @else
          <p>No hay elementos</p>
        @endif
      </ul>
    </div>
  </div>

@endsection

@section('modals')
  <!-- NOTE: dropdown de cada puesto -->
  @foreach($jobs as $job)
    @if($job->T == '1')
      <ul id="job-dropdown-{{$job->id}}" class="dropdown-content">
        <li><a href="#" class="job black-text" id="job-{{$job->id}}"><i class="material-icons blue-text">info_outline</i>INFO</a></li>
        <li><a href="{{ route('Job.Edit',$job->id) }}" class="black-text"><i class="material-icons blue-text">create</i>Editar</a></li>
        <li><a href="{{ route('Job.Delete',$job->id) }}" class="black-text"><i class="material-icons blue-text">delete</i>Eliminar</a></li>
      </ul>
    @else
      <ul id="job-dropdown-{{$job->id}}" class="dropdown-content">
        <li><a href="#" class="job black-text" id="job-{{$job->id}}"><i class="material-icons blue-text">info_outline</i>INFO</a></li>
        <li><a href="{{ route('Job.Activate',$job->id) }}" class="black-text"><i class="material-icons left blue-text">cached</i>Activar</a></li>
      </ul>
    @endif
    <input type="hidden" name="" value="{{$job->id}}" id="job-id-{{$job->id}}">
    <input type="hidden" name="" value="{{$job->nombre}}" id="job-nombre-{{$job->id}}">
    <input type="hidden" name="" value="{{$job->T}}" id="job-T-{{$job->id}}">
  @endforeach

  <!-- NOTE: dropdown de cada empleado -->
  @foreach($jobs as $job)
    @foreach($employes as $employe)

      @if($job->id == $employe->idPuesto)
        @if($employe->idPuesto != '1' && $job->T == '1')
          <ul id="dropdown-{{$employe->id}}" class="dropdown-content">
            <li><a href="#" class="employe black-text" id="employe-{{$employe->id}}"><i class="material-icons blue-text">info_outline</i>INFO</a></li>
            <li><a href="{{ route('Employee.Edit',$employe->id) }}" class="black-text"><i class="material-icons blue-text">create</i>Editar</a></li>
            @if (auth()->user()->id != $employe->idUsuario )
              <li><a href="{{ route('Employee.Delete',$employe->id) }}" class="black-text"><i class="material-icons blue-text">delete</i>Eliminar</a></li>
            @endif
          </ul>
        @else
          <ul id="dropdown-{{$employe->id}}" class="dropdown-content">
            <li><a href="#" class="employe black-text" id="employe-{{$employe->id}}"><i class="material-icons blue-text">info_outline</i>INFO</a></li>
            <li><a href="{{ route('Employee.Activate',$employe->id) }}" class="black-text"><i class="material-icons left blue-text">cached</i>Activar</a></li>
          </ul>
        @endif
        <!-- NOTE: respaldo de cada valor de cada campo -->
        <input type="hidden" value="{{$employe->id}}" id="employe-id-{{$employe->id}}">
        <input type="hidden" value="{{$employe->nombre}}" id="employe-nombre-{{$employe->id}}">
        <input type="hidden" value="{{$employe->aPaterno}}" id="employe-aPaterno-{{$employe->id}}">
        <input type="hidden" value="{{$employe->aMaterno}}" id="employe-aMaterno-{{$employe->id}}">
        <input type="hidden" value="{{$employe->telefono}}" id="employe-telefono-{{$employe->id}}">
        <input type="hidden" value="{{$employe->email}}" id="employe-email-{{$employe->id}}">
        <input type="hidden" value="{{$employe->sueldo}}" id="employe-sueldo-{{$employe->id}}">
        <input type="hidden" value="{{$employe->imagen}}" id="employe-imagen-{{$employe->id}}">
        <input type="hidden" value="{{$job->id}}" id="employe-id-puesto-{{$employe->id}}">
        <input type="hidden" value="{{$job->nombre}}" id="employe-puesto-{{$employe->id}}">
        <input type="hidden" value="{{$job->T}}" id="employe-t-puesto-{{$employe->id}}">
      @endif
    @endforeach
  @endforeach

  <!-- NOTE: plantilla del modal para mostrar la INFO -->
  <div id="employe-info" class="modal modal-fixed-footer">
    <div class="modal-content center-align">

    </div>
    <div class="modal-footer">

    </div>

  </div>

@endsection

@section('scripts')
  <script>
  $(document).ready(function() {
    $('.useless-job').hide();
    $('.useless-employe').hide();

    //  NOTE: tostadas con los posibles mensajes al entrar a esta view
    //  PUESTOS
    @if(session('message-add-job'))
      green_toast("{{session('message-add-job')}}");
    @endif

    @if(session('message-update-job'))
      green_toast("{{session('message-update-job')}}");
    @endif

    @if(session('message-delete-job-1'))
      red_toast("{{session('message-delete-job-1')}}");
    @endif

    @if(session('message-delete-job-2'))
      green_toast("{{session('message-delete-job-2')}}");
    @endif

    @if(session('message-activate-job'))
      green_toast("{{session('message-activate-job')}}");
    @endif

    //  EMPLEADOS
    @if(session('message-add-employee'))
      green_toast("{{session('message-add-employee')}}");
    @endif

    @if(session('message-update-employee'))
      green_toast("{{session('message-update-employee')}}");
    @endif

    @if(session('message-delete-employee'))
      green_toast("{{session('message-delete-employee')}}");
    @endif

    @if(session('bd-error'))
      red_toast("{{session('bd-error')}}");
    @endif

  });

  //  NOTE: switch de puestos
  $('#jobs-lever').click(function(){
    if ( $('#jobs-switch').prop("checked") == true ){
      $('.useless-job').hide();
      $('.usefull-job').show();
    }
    else {
      $('.useless-job').show();
      $('.usefull-job').hide();
    }
  });

  //  NOTE: switch de empleados
  $('#employes-lever').click(function(){
    if ( $('#employes-switch').prop("checked") == true ){
      $('.useless-employe').hide();
      $('.usefull-employe').show();
    }
    else {
      $('.useless-employe').show();
      $('.usefull-employe').hide();
    }
  });

  //  NOTE: formato dropdowns
  document.addEventListener('DOMContentLoaded', function() {
    var elems = document.querySelectorAll('.dropdown-trigger');
    var options =
      {
        constrainWidth:false,
        alignment:'right'
      };
    var instances = M.Dropdown.init(elems, options);

  });

  //  NOTE: botones info de cada puesto
  $('.job').click(function(){
    $id = $(this).attr("id");
    $id = $id.substring(4,$id.lenght);
    $nombre = $('#job-nombre-'+$id).attr("value");
    $t = $('#job-T-'+$id).attr("value");
    if($t == '0'){
      $t = "Inactivo";
    }
    else {
      $t = "Activo";
    }

    jobInfo($id,$nombre,$t);
  });

  function jobInfo($id,$nombre,$t){
    $('.modal-content').empty();
    $('.modal-content').append(
      "<h3>INFO  <i class='material-icons small teal-text'>info_outline</i></h3>"+
      "<div class='divider'></div>"+
      "<p class='grey-text'>Puesto "+$t+"</p>"+
      "<div class='divider'></div>"+
      "<table class='striped'>"+
        "<tbody>"+
          "<tr>"+
            "<th class='center-align'>Nombre:</th>"+
            "<td>"+$nombre+"</td>"+
          "</tr>"+
        "</tbody>"+
      "</table>"
    );
    $('.modal-footer').empty();
    $('.modal-footer').append(
      "<a class='waves-effect waves-light btn-flat grey-text text-darken-3 modal-close'>Cerrar</a>"
    );
    $('#employe-info').modal('open');
  }

  //  NOTE: botones info de cada empleados
  $('.employe').click(function(){
    $id = $(this).attr("id");
    $id = $id.substring(8,$id.lenght);
    $nombre = $('#employe-nombre-'+$id).attr("value");
    $aPaterno = $('#employe-aPaterno-'+$id).attr("value");
    $aMaterno = $('#employe-aMaterno-'+$id).attr("value");
    $telefono = $('#employe-telefono-'+$id).attr("value");
    $email = $('#employe-email-'+$id).attr("value");
    $sueldo = $('#employe-sueldo-'+$id).attr("value");
    $imagen = $('#imagen-empleado-'+$id).attr("src");
    $id_puesto = $('#employe-id-puesto-'+$id).attr("value");
    $puesto = $('#employe-puesto-'+$id).attr("value");
    $t_puesto = $('#employe-t-puesto-'+$id).attr("value");

    if($t_puesto == '0' || $id_puesto == '1'){
      $t_puesto = "Inactivo";
    }
    else {
      $t_puesto = "Activo";
    }

    employeInfo($id,$nombre,$aPaterno,$aMaterno,$telefono,$email,$sueldo,$imagen,$t_puesto,$puesto);
  });

  function employeInfo($id,$nombre,$aPaterno,$aMaterno,$telefono,$email,$sueldo,$imagen,$t_puesto,$puesto){
    $('.modal-content').empty();
    $('.modal-content').append(
      "<h3>INFO  <i class='material-icons small teal-text'>info_outline</i></h3>"+
      "<div class='divider'></div>"+
      "<p class='grey-text'>Empleado "+$t_puesto+"</p>"+
      "<div class='divider'></div>"+
      "<table class='striped'>"+
        "<tbody>"+
          "<tr>"+
            "<th class='center-align'>Nombre:</th>"+
            "<td>"+$nombre+"</td>"+
          "</tr>"+
          "<tr>"+
            "<th class='center-align'>Apellido Paterno:</th>"+
            "<td>"+$aPaterno+"</td>"+
          "</tr>"+
          "<tr>"+
            "<th class='center-align'>Apellido Materno:</th>"+
            "<td>"+$aMaterno+"</td>"+
          "</tr>"+
          "<tr>"+
            "<th class='center-align'>Teléfono:</th>"+
            "<td>"+$telefono+"</td>"+
          "</tr>"+
          "<tr>"+
            "<th class='center-align'>Correo electrónico:</th>"+
            "<td>"+$email+"</td>"+
          "</tr>"+
          "<tr>"+
            "<th class='center-align'>Puesto:</th>"+
            "<td>"+$puesto+"</td>"+
          "</tr>"+
          "<tr>"+
            "<th class='center-align'>Sueldo:</th>"+
            "<td>"+$sueldo+"</td>"+
          "</tr>"+
          "<tr>"+
            "<th class='center-align' colspan='2'>Fotografia:</th>"+
          "</tr>"+
          "<tr>"+
            "<td class='center-align' colspan='2'><img src='"+$imagen+"' width='250px' height='250px' style='border-radius: 25px;' style=' objet-fit:cover; '></td>"+
          "</tr>"+
        "</tbody>"+
      "</table>"
    );
    $('.modal-footer').empty();
    $('.modal-footer').append(
      "<a class='waves-effect waves-light btn-flat grey-text text-darken-3 modal-close'>Cerrar</a>"
    );
    $('#employe-info').modal('open');
  }

  </script>

@endsection
