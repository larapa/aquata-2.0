@extends('Layouts.Menu')
@section('head','usuario')
@section('title','Historiales')
@section('content')
  <ul class="tabs tabs-fixed-width">

    <li class="tab col s4 m4 l4"><a class="active" href="#swipe-arcticlesHistory">Historial articulos</a></li>
    <li class="tab col s4 m4 l4"><a target="_self"  href="{{route('History.Food')}}">Historial alimento</a></li>
  </ul>

  <div id="swipe-arcticlesHistory">
    <!--Panel de control-->
    <div class="row">
      <div class="col s12 m12 l4">
        <div class="card white">
          <div class="card-content blue-text text-darken-2">
            <span class="card-title">Panel de control</span>
            <br>
            <ul class="collapsible popout">
              <!-- NOTE: Filtro de fechas -->
              <li>
                <div class="collapsible-header"><i class="material-icons">date_range</i>Selecionar periodo</div>
                <div class="collapsible-body">
                  <!-- NOTE: formulario para cambiar fechas -->
                  <form class="" id="form-fechas" action="{{route('History.ArticlesHistoryByDate')}}" method="post">
                    @csrf
                    <div class="row">
                      <!-- NOTE: datepicker 1 -->
                      <div class="col s12 m12 l12">
                        <h6 class="center-align black-text">Fecha inicial:</h6>
                        <input type="text" name="fecha1" class="datepicker center-align" id="fecha1">
                      </div>
                      <!-- NOTE: datepicker 2 -->
                      <div class="col s12 m12 l12">
                        <h6 class="center-align black-text">Fecha final:</h6>
                        <input type="text" name="fecha2" class="datepicker center-align" id="fecha2">
                      </div>
                    </div>
                    <div class="row">
                      <!-- NOTE: boton para actualizar los datos con las fechas de los pickers -->
                      <div class="col s12 m12 l12 center-align">
                        <br>
                        <a class="waves-effect waves-light btn teal" onclick="document.getElementById('form-fechas').submit();">Consultar</a>
                      </div>
                    </div>
                  </form>
              </li>
            </ul>
          </div>
          <div class="card-action center-align">
            <div class="row">
              <div class="col s12">
                <a id="export_button" class="waves-effect waves-light btn teal" ><i class="material-icons right">file_download</i>Exportar</a>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- NOTE: CONTENIDO -->
      <div class="col s12 m12 l8 center-align">
        <h4>Historial Articulos</h4>
            <div id="tabla-historialArticulos">
              <!-- NOTE: AQUI VA LA TABLA -->
              <table class="centered responsive-table highlight">
                <thead>
                  <tr>
                    <th>Motivo</th>
                    <th>Fecha</th>
                    <th>Cantidad</th>
                    <th>Articulo</th>
                    <th>Unidades</th>
                    <th></th>
                  </tr>
                </thead>
                <tbody>
                  @foreach($articlesHistory as $article)
                    <tr>
                      <td>{{$article->motivo}}</td>
                      <td>{{$article->fecha}}</td>
                      <td>{{$article->cantidadElementos}}</td>
                      <td>{{$article->articulo}}</td>
                      <td>{{$article->cantidad}} {{$article->unidad}}</td>
                      <td><a class="waves-effect waves-light"><i  id="INFO-{{$article->id}}" class="material-icons ARTICLE">info_outline</i></td>
                    </tr>
                    <input type="hidden" value="{{$article->nombre}} {{$article->aPaterno}} {{$article->aMaterno}}" id='history-nombre-{{$article->id}}'>
                    <input type="hidden" value="{{$article->motivo}}" id='history-motivo-{{$article->id}}'>
                    <input type="hidden" value="{{$article->fecha}}" id='history-fecha-{{$article->id}}'>
                    <input type="hidden" value="{{$article->articulo}}" id='history-articulo-{{$article->id}}'>
                    <input type="hidden" value="{{$article->descripcionArticulo}}" id='history-descripcion-{{$article->id}}'>
                    <input type="hidden" value="{{$article->cantidad}} {{$article->unidad}}" id='history-unidad-{{$article->id}}'>
                    <input type="hidden" value="{{$article->categoria}}" id='history-categoria-{{$article->id}}'>
                    <input type="hidden" value="{{$article->descripcionCategoria}}" id='history-descripcionCategoria-{{$article->id}}'>
                    <input type="hidden" value="{{$article->cantidadElementos}}" id='history-cantidadElementos-{{$article->id}}'>
                  @endforeach
                </tbody>
              </table>
            </div>
        </div>
      </div>
    </div>
  </div>

@endsection

@section('modals')
  <div id="test" class="modal modal-fixed-footer">
    <div class="modal-content center-align">

    </div>
    <div class="modal-footer">

    </div>
  </div>
@endsection

@section('scripts')
  <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script src="//cdn.rawgit.com/rainabba/jquery-table2excel/1.1.0/dist/jquery.table2excel.min.js"></script>

  <script type="text/javascript" id="scripts-gastos">
    $(document).ready(function() {
      // NOTE: Validar que fecha 1 sea menor a fecha 2
      $( "#form-fechas" ).submit(function( event ) {
        alert($('#fecha1').datepicker('getDate'));
        if ($('#fecha1').datepicker('getDate') <= $('#fecha2').datepicker('getDate')) {
          return;
        }
        M.toast({html: 'Seleccione un periodo valido',classes:'red darken-3'});
        event.preventDefault();
      });

      //  NOTE: aqui se inicializan los picker 1 y 2
      var hoy = new Date();
      dia = hoy.getDate();
      mes = hoy.getMonth()-1;
      ano = hoy.getFullYear();

      @if(!empty($f1))
        //  NOTE: fechas solicitadas por el usuario
        dia1 = {{ substr($f1,0,2) }};
        mes1 = {{ substr($f1,3,2) }};
        ano1 = {{ substr($f1,6,4) }};
        dia2 = {{ substr($f2,0,2) }};
        mes2 = {{ substr($f2,3,2) }};
        ano2 = {{ substr($f2,6,4) }};
        $('#fecha1').datepicker({
          format: 'dd/mm/yyyy',
          setDefaultDate: true,
          defaultDate: new Date(ano1,mes1-1,dia1),
          minDate: new Date(2020,03,23),
          maxDate: hoy,
          i18n: {
            months: ['Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'],
            monthsShort: ['Ene','Feb','Mar','Abr','May','Jun','Jul','Ago','Sep','Oct','Nov','Dic'],
            weekdays: ['Domingo','Lunes','Martes','Miércoles','Jueves','Viernes','Sábado'],
            weekdaysShort: ['Dom','Lun','Mar','Mier','Jue','Vier','Sab'],
            weekdaysAbbrev: ['D','L','M','M','J','V','S']
          },

        });

        $('#fecha2').datepicker({
          format: 'dd/mm/yyyy',
          setDefaultDate: true,
          defaultDate: new Date(ano2,mes2-1,dia2),
          minDate: new Date(2020,03,23),
          maxDate: hoy,
          i18n: {
            months: ['Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'],
            monthsShort: ['Ene','Feb','Mar','Abr','May','Jun','Jul','Ago','Sep','Oct','Nov','Dic'],
            weekdays: ['Domingo','Lunes','Martes','Miércoles','Jueves','Viernes','Sábado'],
            weekdaysShort: ['Dom','Lun','Mar','Mier','Jue','Vier','Sab'],
            weekdaysAbbrev: ['D','L','M','M','J','V','S']
          },

        });
      @else
        //  NOTE: fecha de hoy y de hace un mes
        $('#fecha1').datepicker({
          format: 'dd/mm/yyyy',
          setDefaultDate: true,
          defaultDate: new Date(ano,mes,dia),
          minDate: new Date(2020,03,23),
          maxDate: hoy,
          i18n: {
            months: ['Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'],
            monthsShort: ['Ene','Feb','Mar','Abr','May','Jun','Jul','Ago','Sep','Oct','Nov','Dic'],
            weekdays: ['Domingo','Lunes','Martes','Miércoles','Jueves','Viernes','Sábado'],
            weekdaysShort: ['Dom','Lun','Mar','Mier','Jue','Vier','Sab'],
            weekdaysAbbrev: ['D','L','M','M','J','V','S']
          },

        });

        $('#fecha2').datepicker({
          format: 'dd/mm/yyyy',
          setDefaultDate: true,
          defaultDate: hoy,
          minDate: new Date(2020,03,23),
          maxDate: hoy,
          i18n: {
            months: ['Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'],
            monthsShort: ['Ene','Feb','Mar','Abr','May','Jun','Jul','Ago','Sep','Oct','Nov','Dic'],
            weekdays: ['Domingo','Lunes','Martes','Miércoles','Jueves','Viernes','Sábado'],
            weekdaysShort: ['Dom','Lun','Mar','Mier','Jue','Vier','Sab'],
            weekdaysAbbrev: ['D','L','M','M','J','V','S']
          },

        });
      @endif

       @if(session('error'))
          red_toast("{{session('error')}}");
       @endif

    });

    document.addEventListener('DOMContentLoaded', function() {
      var elems = document.querySelectorAll('.dropdown-trigger');
      var options =
        {
          constrainWidth:false,
          alignment:'right'
        };
      var instances = M.Dropdown.init(elems, options);
    });

    //  NOTE: modals con la informacion
    $('.ARTICLE').click(function(){
      $id = $(this).attr("id");
      $id = $id.substr(5,$id.lenght);
      $nombre = $('#history-nombre-'+$id).attr("value");
      $motivo = $('#history-motivo-'+$id).attr("value");
      $fecha = $('#history-fecha-'+$id).attr("value");
      $articulo = $('#history-articulo-'+$id).attr("value");
      $descripcion = $('#history-descripcion-'+$id).attr("value");
      $unidad = $('#history-unidad-'+$id).attr("value");
      $categoria = $('#history-categoria-'+$id).attr("value");
      $descripcionCategoria = $('#history-descripcionCategoria-'+$id).attr("value");
      $cantidadElementos = $('#history-cantidadElementos-'+$id).attr("value");
      historyArticlesInfo($id,$nombre,$descripcion,$motivo,$fecha,$articulo,$unidad,$categoria,$descripcionCategoria,$cantidadElementos);
    });

    function historyArticlesInfo($id,$nombre,$descripcion,$motivo,$fecha,$articulo,$unidad,$categoria,$descripcionCategoria,$cantidadElementos){
      $('.modal-content').empty();
      $('.modal-content').append(
        "<h3>INFO  <i class='material-icons small teal-text'>info_outline</i></h3>"+
        "<div class='divider'></div>"+
        "<table class='striped'>"+
          "<tbody>"+
          "<tr>"+
            "<th class='center-align'>Usuario:</th>"+
            "<td>"+$nombre+"</td>"+
          "</tr>"+
          "<tr>"+
            "<th class='center-align'>Motivo:</th>"+
            "<td>"+$motivo+"</td>"+
          "</tr>"+
          "<tr>"+
            "<th class='center-align'>Fecha:</th>"+
            "<td>"+$fecha+"</td>"+
          "</tr>"+
          "<tr>"+
            "<th class='center-align'>Cantidad de elementos:</th>"+
            "<td>"+$cantidadElementos+"</td>"+
          "</tr>"+
          "<tr>"+
            "<th class='center-align'>Articulo:</th>"+
            "<td>"+$articulo+"</td>"+
          "</tr>"+
          "<tr>"+
            "<th class='center-align'>Descripción:</th>"+
            "<td>"+$descripcion+"</td>"+
          "</tr>"+
          "<tr>"+
            "<th class='center-align'>Unidades:</th>"+
            "<td>"+$unidad+"</td>"+
          "</tr>"+
          "<tr>"+
            "<th class='center-align'>Categoría:</th>"+
            "<td>"+$categoria+"</td>"+
          "</tr>"+
          "<tr>"+
            "<th class='center-align'>Descripción:</th>"+
            "<td>"+$descripcionCategoria+"</td>"+
          "</tr>"+
          "</tbody>"+
        "</table>"
      );
      $('.modal-footer').empty();
      $('.modal-footer').append(
        "<a class='waves-effect waves-light btn-flat grey-text text-darken-3 modal-close'>Cerrar</a>"
      );
      $('#test').modal('open');
    }


    $("#export_button").click(function(){
      $("#tabla-historialArticulos").table2excel({
          // exclude CSS class
          exclude:".noexport",
          name:"Worksheet Name",
          filename:"Historial_Inventario_Articulos"//do not include extension
      });
    });

  </script>

@endsection
