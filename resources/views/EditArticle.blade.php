@extends('Layouts.Menu')
@section('title','Editar articulo')
@section('content')

  @if (session('mensaje'))
    <div class="alert"> {{session('mensaje')}} </div>
  @endif

  <form action="{{route('Article.Update')}}" method="POST">
    @csrf
    <input type="hidden" name="id" value="{{$article->id}}">
    <h4 class="center-align">Articulo: {{$article->nombre}}</h4>
    <div class="row">
      <div class="container">
        <div class="col s12">
          <div class="row">
            <div class="col s12 m4 l4 center align">
              <h4><i class="material-icons">help_outline</i> INFO</h4>

            </div>
            <div class="col s12 m8 l8">
              <p  ALIGN="justify">
                En este apartado puedes editar la información de <span class="blue-text">
                {{$article->nombre}}</span>, Asegurate de ingresar información lo más descriptiva
                y simple posible.
              </p>
            </div>
          </div>
          <!-- NOTE: Nombre -->
          <div class="row">
            <div class="input-field col s12">
              <input id="article_nombre" name="nombre" type="text" data-length="30" maxlength="30" required="" aria-required="true" class="validate" value="{{$article->nombre}}">
              <label for="bag_etiqueta">Nombre</label>
              <span class="help-text" data-error="Incorrecto" data-success="Correcto">Ingrese el nombre del articulo</span>
            </div>
          </div>
          <!-- NOTE: Descripcion -->
          <div class="row">
            <div class="input-field col s12">
              <textarea id="article_descripcion" class="validate materialize-textarea" name="descripcion" type="text" data-length="120" maxlength="120" required="" aria-required="true">{{$article->descripcion}}</textarea>
              <label for="motive_descripcion">Descripción</label>
              <span class="help-text" data-error="Incorrecto" data-success="Correcto">Ingrese la descripción</span>
            </div>
          </div>
          <!-- NOTE: Cantidad Unidad -->
          <div class="row">
            <div class="input-field col s12 m6 l6">
              <input id="article_cantUnidad"  name="cantidadUnidad" type="number" step="1" min="0" data-length="11" maxlength="11" required="" aria-required="true" value="{{$article->cantidadUnidad}}">
              <label for="bag_tamano">Agregar cantidad de la unidad</label>
              <span class="help-text" data-error="Incorrecto" data-success="Correcto">Ingrese la cantidad total de la unidad</span>
            </div>

            <div class="input-field col s12 m6 l6">
              <select name='idUnidad'>
                <option value="">Selecciona una unidad</option>
                @foreach ($units as $unit)
                  <option class="truncate" value="{{$unit->id}}"@if ($unit->id == $article->idUnidad) selected @endif >{{$unit->nombre}}</option>
                @endforeach
              </select>
              <label>Unidad</label>
            </div>
          </div>
          <div class="row">
            <div class="input-field col s12">
              <select name='idCategoriaArticulo'>
                <option value="">Selecciona una categoria</option>
                @foreach ($categories as $category)
                  <option class="truncate" value="{{$category->id}}" @if ($category->id == $article->idCategoriaArticulo) selected @endif >{{$category->nombre}}</option>
                @endforeach
              </select>
              <label>Categoria</label>
            </div>
          </div>
        <div class="center-align">
          <button type="submit" class="waves-effect waves-light btn blue darken-3 center-align">Aceptar</button>
        </div>
      </div>
    </div>
  </form>

@endsection

@section('scripts')
  <script type="text/javascript">

    $(document).ready(function() {
      $('input, textarea').characterCounter();
    });

  </script>
@endsection
