@extends('Layouts.Menu')

@section('title','Trabajadores')

@section('content')

  <div class="row">
    <!-- NOTE: Panel de control -->
    <div class="col s12 m4 l4">
      <div class="card white">
        <div class="card-content blue-text text-darken-2">
          <span class="card-title">Panel de control</span>

          <ul class="collapsible popout expandable">
            <li>
              <div class="collapsible-header"><i class="material-icons">assignment_ind</i>Puestos</div>
              <div class="collapsible-body">
                <div class="row">
                  <div class="col s8 m8 l8">
                    ¿Mostrar puesto inactivos?
                  </div>
                  <div class="col s4 m4 l4">
                    <div class="switch right-align">
                     <label>
                       <input type="checkbox">
                       <span class="lever"></span>
                     </label>
                   </div>
                  </div>
                </div>
                <!-- NOTE: Ver puestos -->
                <div class="collection">
                  @if(!$jobs->isEmpty())
                    @foreach($jobs as $job)
                      @if($job->T == '1')
                        <a href="#!" class="collection-item">{{$job->nombre}}</a>
                      @else
                        <a href="#!" class="collection-item black-text">{{$job->nombre}}</a>
                      @endif
                    @endforeach
                  @else
                    <p>No hay elementos</p>
                  @endif
                </div>
                <div class="row">
                  <div class="col s12 m12 l12 center-align">
                    <br>
                    <a class="waves-effect waves-light btn-small teal"><i class="material-icons right">add</i>Agregar puesto</a>
                  </div>
                </div>
              </div>
            </li>
          </ul>
        </div>
        <div class="card-action center-align">
          <a class="waves-effect waves-light btn-small teal white-text"><i class="material-icons right">add</i>Agregar empleado</a>
        </div>
      </div>
    </div>
  </div>

  <!-- NOTE: dropdown del empleado -->
  <ul id="employe-dropdown" class="dropdown-content" >
    <li><a href="#!" class="teal-text">Ver</a></li>
    <li><a href="#!" class="teal-text">Editar</a></li>
    <li><a href="#modal_delete_employe" class="red-text text-accent-2 modal-trigger">Eliminar</a></li>
  </ul>

  <!-- NOTE: eliminar empleado -->
  <section>
    <div id="modal_delete_employe" class="modal">
        <div class="modal-content center-align">
          <h4>Advertencia</h4>
          <p>Esta acción no se puede deshacer.</p>
          <p>¿Desdea continuar?</p>
        </div>
        <div class="modal-footer">
          <a class="waves-effect waves-light btn-flat grey-text text-darken-3 modal-close">Cancelar</a>
          <a class="waves-effect waves-light btn-flat red-text text-darken-3 modal-close" onclick="delete_employe()">Eliminar</a>
        </div>
      </div>
  </section>



@endsection

@section('scripts')
  <script>
  function delete_employe(){
    M.toast({html: 'El empleado se ha eliminado'})
  }
  </script>
@endsection
