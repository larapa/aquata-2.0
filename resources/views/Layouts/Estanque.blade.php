@extends('Layouts.Menu')
@section('title',$user->nombre)
@section('content')
@auth

  <div class="row">
    <div class="col s12 m12 l4">
      <div class="card white">
        <div class="card-content blue-text text-darken-2">
          <span class="card-title">Panel de control</span>
          <div class="row">
            <div class="col s8 m8 l8">
              ¿Mostrar estanques inactivos?
            </div>
            <div class="col s4 m4 l4">
              <div class="switch right-align">
               <label>
                 <input type="checkbox" id="ponds-switch">
                 <span class="lever" id="ponds-lever"></span>
               </label>
             </div>
            </div>
          </div>
          <ul class="collapsible popout expandable">
            <li>
              <div class="collapsible-header"><i class="material-icons">archive</i>Etapas</div>
              <div class="collapsible-body">
                <div class="row">
                  <div class="col s8 m8 l8">
                    ¿Mostrar etapas inexistentes?
                  </div>
                  <div class="col s4 m4 l4">
                    <div class="switch right-align">
                     <label>
                       <input type="checkbox" id="trouts-switch">
                       <span class="lever" id="trouts-lever"></span>
                     </label>
                   </div>
                  </div>
                </div>

                <!-- ver los tipos de alimento -->
                <ul class="collection" id="trouts-collection">
                  @if(!$trouts->isEmpty())
                    @foreach($trouts as $trout)
                      @if($trout->T == '1')
                        <li class="collection-item avatar useful-trout">
                          <div class="row">
                            <div class="col s10">
                              <i class="material-icons circle blue">restaurant</i>
                              <span id="{{$trout->id}}"class="trucha title">{{$trout->tipo}}</span>
                              <p class="grey-text truncate">{{ $trout->descripcion }}</p>
                            </div>
                            <div class="col s2">
                              <a class="secondary-content dropdown-trigger btn-flat teal-text" data-target="TYPE-{{$trout->id}}"><i class="material-icons right">arrow_drop_down</i></a>
                            </div>
                          </div>
                        </li>
                      @if(auth()->user()->idRol == 1)
                        <ul id='TYPE-{{$trout->id}}' class='dropdown-content'>
                          <li><a id="tro-{{$trout->id}}" class="trout black-text"><i class="material-icons left blue-text">error_outline</i>INFO</a></li>
                          <li><a class="black-text"  href="{{ route('Trucha.Find', $trout->id ) }}"><i class="material-icons left blue-text" >edit</i>Editar</a></li>
                            <form action="{{ route('Delete.Trout')}}" method="POST" id="Form-Delete-{{$trout->id}}">
                              @csrf
                              <input type="hidden" name="id" value="{{$trout->id}}">
                              <li><a id="{{$trout->id}}" onclick="document.getElementById('Form-Delete-{{$trout->id}}').submit();" class="black-text"><i class="material-icons left blue-text">delete</i>Eliminar</a></li>
                            </form>
                        </ul>
                      @endif
                      @else
                        <li class="collection-item avatar useless-trout">
                          <div class="row">
                            <div class="col s10">
                              <i class="material-icons circle indigo lighten-4">restaurant</i>
                              <span id="{{$trout->id}}"class="trucha title grey-text">{{$trout->tipo}}</span>
                              <p class="grey-text">{{ $trout->descripcion }}</p>
                            </div>
                            <div class="col s2">
                              <a class="secondary-content dropdown-trigger btn-flat teal-text" data-target="TYPE-{{$trout->id}}"><i class="material-icons right">arrow_drop_down</i></a>
                            </div>
                          </div>
                        </li>
                      @if(auth()->user()->idRol == 1)
                        <ul id='TYPE-{{$trout->id}}' class='dropdown-content'>
                          <form action="{{ route('Trout.Activate') }}" method="POST" id="Form-Act-{{$trout->id}}">
                            @csrf
                            <input type="hidden" name="id" value="{{$trout->id}}">
                            <li><a  id="{{$trout->id}}" onclick="document.getElementById('Form-Act-{{$trout->id}}').submit();"><i class="material-icons left blue-text">error_outline</i>Activar</a></li>
                          </form>
                        </ul>
                      @endif
                      @endif
                      <input type="hidden" name="" value="{{$trout->id}}" id="tro-id-{{$trout->id}}">
                      <input type="hidden" name="" value="{{$trout->tipo}}" id="tro-tipo-{{$trout->id}}">
                      <input type="hidden" name="" value="{{$trout->descripcion}}" id="tro-desc-{{$trout->id}}">
                      <input type="hidden" name="" value="{{$trout->T}}" id="tro-T-{{$trout->id}}">
                    @endforeach
                  @else
                    <p>No hay elementos</p>
                  @endif
                </ul>
                @if(auth()->user()->idRol == 1)
                  <div class="row">
                    <div class="col s12 m12 l12 center-align">
                      <br>
                      <a class="waves-effect waves-light btn-small teal white-text" href="{{ route('Trout.AddForm') }}"><i class="material-icons right">add</i>Agregar etapa</a>
                    </div>
                  </div>
                @endif
              </div>
            </li>
          </ul>
        </div>
        @if(auth()->user()->idRol == 1)
          <div class="card-action center-align">
            <a class="waves-effect waves-light btn-small teal white-text" href="{{ route('Estanque.AddForm') }}"><i class="material-icons right">add</i>Agregar estanque</a>
          </div>
        @endif
      </div>
    </div>
    <!-- NOTE: ESTANQUE 1 -->
    @if(!empty($ponds))
    <div class="col s12 m12 l8 " id="ponds-collection">
      <h4 class="center-align">Estanques</h4>
      <div class="row">
      @foreach($ponds as $pond)
        @if($pond->T == '1')
          <div class="col s12 m6 l6 useful-pond">
            <div class="card">
              <div class="card-image waves-effect waves-block waves-light">
                <img class="activator" src="{{url("uploads/".$pond->imagen)}}" width="auto" height="280px" style="object-fit:cover;">
              </div>
              <div class="card-content">
                <span class="card-title activator grey-text text-darken-4">{{$pond -> nombre}}<i class="material-icons right">more_vert</i></span>
                <p class="blue-text">{{$pond -> tipo}}</p>
              </div>
              <div class="card-reveal">
                <span class="card-title grey-text text-darken-4">{{$pond -> nombre}}<i class="material-icons right">close</i></span>
                <div class=" center-align">

                  <p class="truncate teal-text">Fecha de siembra</p>
                    <p class="center-align">{{$pond -> fechaIngreso}}</p>
                  <div class="divider"></div>

                  <p class="truncate teal-text">Etapa</p>
                    <p class="truncate center-align">{{$pond -> tipo}}</p>
                    <p class="center-align grey-text">{{$pond -> descripcion}}</p>
                  <div class="divider"></div>

                  <p class="truncate teal-text">Temperatura</p>
                    <p class="truncate center-align">{{$pond -> temperatura}} grados</p>
                  <div class="divider"></div>

                  <p class="truncate teal-text">Cantidad de peces</p>
                    <p class="truncate center-align">{{$pond -> cantidad}} Peces</p>
                  <div class="divider"></div>

                  <p class="truncate teal-text">Peso promedio</p>
                    <p class="truncate center-align">{{$pond -> peso}} gramos</p>
                  <div class="divider"></div>

                  <p class="truncate teal-text">Tamaño promedio</p>
                    <p class="truncate center-align">{{$pond -> tamano}} centimetros</p>
                  <div class="divider"></div>

                  <p class="truncate teal-text">Volumen de agua</p>
                    <p class="truncate center-align">{{$pond -> volumenAgua}} litros</p>
                  <div class="divider"></div>

                  <p class="truncate teal-text">Biomasa</p>
                    <p class="truncate center-align">{{$pond -> cantidad * $pond-> peso}} kilos</p>
                  <div class="divider"></div>

                  <p class="truncate teal-text">Oxigeno</p>
                    <p class="truncate center-align">{{$pond -> oxigeno}} unidades de oxigeno</p>
                  <div class="divider"></div>

                  <p class="truncate teal-text">PH</p>
                    <p class="truncate center-align">{{$pond -> ph}} unidades de ph</p>
                  <div class="divider"></div>
                  @if(auth()->user()->idRol == 1)
                  <br>
                    <div class="row">
                      <div class="col s6 m6 l6">
                        <form action="{{ route('Estanque.Delete') }}" method="POST">
                          @csrf
                          <input type="hidden" name="id" value="{{$pond->id}}">
                          <button type="submit" class="waves-effect waves-light btn-flat red-text text-darken-3 center-align">
                            <i class="material-icons right">delete</i>Eliminar
                          </button>
                        </form>
                      </div>
                      <div class="col s6 m6 l6">
                        <form action="{{ route('Estanque.Find') }}" method="POST">
                          @csrf
                          <input type="hidden" name="id" value="{{$pond->id}}">
                          <button type="submit" class="waves-effect waves-light btn-flat yellow-text text-darken-3 center-align">
                            <i class="material-icons right">edit</i>Editar
                          </button>
                        </form>
                      </div>
                    </div>
                  @endif
                </div>
              </div>
              <div class="card-action">
                <div class="row">
                  <div class="col s6 m6 l6 center-align">
                  @if(auth()->user()->idRol == 1 || auth()->user()->idRol == 3)
                    <button class="pond_edit waves-effect waves-light btn-flat teal-text center-align modal-trigger" href="#feed_pond{{$pond->id}}">
                      <i class="material-icons right">restaurant_menu</i>
                      Alimentar
                    </button>
                  @endif
                  </div>
                  <div class="col s6 m6 l6 center-align">
                    <form action="{{route('Ponds.History')}}" method="POST">
                      @csrf
                      <input type="hidden" name="id" value="{{$pond->id}}">
                      @if(auth()->user()->idRol == 1)
                      <button type="submit" class="waves-effect waves-light btn-flat purple-text text-darken-3 center-align">
                        <i class="material-icons right">today</i>Historial
                      </button>
                      @endif
                    </form>
                  </div>
                </div>
              </div>
            </div>
            <div id="feed_pond{{$pond->id}}" class="modal bottom-sheet">
              <div class="modal-content center-align">

                  <div class="row">
                    <div class="container ">
                      <form class="calculate" action="#" method="post">
                        <input type="hidden" name="id" value="{{$pond->id}}">
                        <div class="col s12">
                          <div class="row">
                            <div class="col s12">
                              <h5>Calcular alimentación de {{$pond->nombre}}</h5>
                              <div class="divider"></div>
                              <br>
                            </div>
                            <div class="col s12 m12 l6">
                              <div class="input-field col s12">
                                <input id="pond_size-{{$pond->id}}" name="tamano" type="number" step="0.01" min="0.01" max="9999999.99" class="validate" required=required"" value="{{ $pond->tamano }}">
                                <label for="pond_size-{{$pond->id}}">Tamaño</label>
                                <span class="helper-text" data-error="Incorrecto" data-success="Correcto">Ingrese el tamaño promedio en centímetros</span>
                              </div>
                            </div>
                            <div class="col s12 m12 l6">
                              <div class="input-field col s12">
                                <input id="pond_weight-{{$pond->id}}" name="peso" type="number" step="0.01" min="0.01" max="9999999.99" class="validate" required="required" value="{{ $pond->peso }}">
                                <label for="pond_weight-{{$pond->id}}">Peso</label>
                                <span class="helper-text" data-error="Incorrecto" data-success="Correcto">Ingrese el peso en gramos</span>
                              </div>
                            </div>
                            <div class="col s12 m12 l6">
                              <div class="input-field col s12">
                                <input id="pond_quantity-{{$pond->id}}" name="cantidad" type="number" step="1" min="1" max="99999999999" class="validate" required="required" value="{{ $pond->cantidad }}">
                                <label for="pond_quantity-{{$pond->id}}">Cantidad</label>
                                <span class="helper-text" data-error="Incorrecto" data-success="Correcto">Ingrese la cantidad de truchas</span>
                              </div>
                            </div>
                            <div class="col s12 m12 l6">
                              <div class="input-field col s12">
                                <input id="pond_temperature-{{$pond->id}}" name="temperatura" type="number" step="0.01" min="0.01" max="999999.99" class="validate" required="required" value="{{ $pond->temperatura }}">
                                <label for="pond_temperature-{{$pond->id}}">Temperatura</label>
                                <span class="helper-text" data-error="Incorrecto" data-success="Correcto">Ingrese la temperatura en °C</span>
                              </div>
                            </div>
                            <div class="col s12 m12 l6">
                              <div class="input-field col s12">
                                <input id="pond_inc-{{$pond->id}}" name="incremento" type="number" step="0.01" min="0.01" max="999999999999.99" class="validate" required="required" value="1.27">
                                <label for="pond_inc-{{$pond->id}}">Incremento diario (15°C)</label>
                                <span class="helper-text" data-error="Incorrecto" data-success="Correcto">Ingrese el incremento diario de la especie</span>
                              </div>
                            </div>
                            <div class="col s12 m12 l6">
                              <div class="input-field col s12">
                                <input id="pond_conv-{{$pond->id}}" name="conversio" type="number" step="0.01" min="0.01" max="999999999999.99" class="validate" required="required" value="1.1">
                                <label for="pond_conv-{{$pond->id}}">Conversión alimenticia</label>
                                <span class="helper-text" data-error="Incorrecto" data-success="Correcto">Ingrese la conversión alimenticia</span>
                              </div>
                            </div>

                          </div>

                          <br>

                          <div class="row">
                            <div class="col s12">
                              <h5>Información general</h5>
                              <br>
                              <div class="divider"></div>
                              <br>
                            </div>

                            <div class="col s12 m12 l6 offset-l3">
                              <div class="col s8 m8 l8 left-align">
                                ¿Mostrar información?
                              </div>
                              <div class="col s4 m4 l4">
                                <div class="switch right-align">
                                 <label>
                                   <input type="checkbox" id="switch-aditional-info-{{$pond->id}}">
                                   <span class="lever info-lever" id="aditional-info-{{$pond->id}}"></span>
                                 </label>
                               </div>
                              </div>
                            </div>

                            <div class="col s12">
                              <div class="row hidden-info" id="row-aditional-info-{{$pond->id}}">
                                <br>
                                <div class="divider"></div>
                                <br>
                                <div class="col s12 m12 l6">
                                  <div class="input-field col s12">
                                    <input id="pond_date-{{$pond->id}}" name="fechaIngreso" type="text" class="datepicker"  required="required" value="{{ $pond->fechaIngreso }}">
                                    <label for="pond_date-{{$pond->id}}">Fecha</label>
                                    <span class="helper-text" data-error="Incorrecto" data-success="Correcto">Seleccione la fecha de ingreso</span>
                                  </div>
                                </div>
                                <div class="col s12 m12 l6">
                                  <div class="input-field col s12">
                                    <input id="pond_name-{{$pond->id}}" value="{{ $pond->nombre }}" name="nombre" type="text" class="validate" data-length="30" maxlength="30" required="required">
                                    <label for="pond_name-{{$pond->id}}">Nombre</label>
                                    <span class="helper-text" data-error="Incorrecto" data-success="Correcto">Ingrese el nombre del estanque</span>
                                  </div>
                                </div>
                                <div class="col s12 m12 l6">
                                  <div class="input-field col s12">
                                    <input id="pond_type-{{$pond->id}}" name="tipo" type="text" class="validate" required="required" value="{{ $pond->tipo }} " disabled>
                                    <label for="pond_type-{{$pond->id}}">Etapa</label>
                                    <span class="helper-text" data-error="Incorrecto" data-success="Correcto">Seleccione la etapa</span>
                                  </div>
                                </div>
                                <div class="col s12 m12 l6">
                                  <div class="input-field col s12">
                                    <input id="pond_water_lvl-{{$pond->id}}" name="volumenAgua" type="number" step="0.01" min="0.01" max="999999999999.99" class="validate" required="required" value="{{ $pond->volumenAgua }}">
                                    <label for="pond_water_lvl-{{$pond->id}}">Volumen de agua</label>
                                    <span class="helper-text" data-error="Incorrecto" data-success="Correcto">Ingrese el volumen en litros</span>
                                  </div>
                                </div>

                                <div class="col s12 m12 l6">
                                  <div class="input-field col s12">
                                    <input id="pond_oxigen-{{$pond->id}}" name="oxigeno" type="number" step="0.01" min="0.01" max="999999999.99" class="validate" required="required" value="{{ $pond->oxigeno }}">
                                    <label for="pond_oxigen-{{$pond->id}}">Oxígeno</label>
                                    <span class="helper-text" data-error="Incorrecto" data-success="Correcto">Ingrese el porcentaje de oxígeno</span>
                                  </div>
                                </div>
                                <!-- NOTE: PH -->
                                <div class="col s12 m12 l6">
                                  <div class="input-field col s12">
                                    <input id="pond_ph-{{$pond->id}}" name="ph" type="number" step="0.01" min="0.01" max="99999.99" class="validate" required="required" value="{{ $pond->ph }}">
                                    <label for="pond_ph-{{$pond->id}}">PH</label>
                                    <span class="helper-text" data-error="Incorrecto" data-success="Correcto">Ingrese el nivel de PH</span>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col s12 m12 l12 center-align">
                            <button type="submit" class="waves-effect waves-light btn-flat blue-text text-darken-1 center-align">Calcular alimento</button>
                          </div>
                        </div>
                      </form>
                    </div>
                  </div>
              </div>
            </div>
          </div>
        @else
          <div class="col s12 m6 l6 useless-pond">
            <div class="card grey lighten-2">
              <div class="card-image waves-effect waves-block waves-light">
                <img class="activator" src="{{url("uploads/".$pond->imagen)}}" width="auto" height="280px" style="opacity:0.5; object-fit:cover;">
              </div>
              <div class="card-content">
                <!-- NOTE: NOMBRE -->
                <span class="card-title activator grey-text text-darken-4">{{$pond -> nombre}}<i class="material-icons right">more_vert</i></span>
                <!-- NOTE: DESCRIPCIÓN -->
                <p class="blue-text">{{$pond -> tipo}}</p>
              </div>
              <div class="card-reveal">
                <!-- NOTE: NOMBRE -->
                <span class="card-title grey-text text-darken-4">{{$pond -> nombre}}<i class="material-icons right">close</i></span>
                <div class=" center-align">

                  <p class="truncate teal-text">Fecha de siembra</p>
                    <p class="center-align">{{$pond -> fechaIngreso}}</p>
                  <div class="divider"></div>

                  <p class="truncate teal-text">Etapa</p>
                    <p class="truncate center-align">{{$pond -> tipo}}</p>
                    <p class="center-align grey-text">{{$pond -> descripcion}}</p>
                  <div class="divider"></div>

                  <p class="truncate teal-text">Temperatura</p>
                    <p class="truncate center-align">{{$pond -> temperatura}} grados</p>
                  <div class="divider"></div>

                  <p class="truncate teal-text">Cantidad de peces</p>
                    <p class="truncate center-align">{{$pond -> cantidad}} Peces</p>
                  <div class="divider"></div>

                  <p class="truncate teal-text">Peso promedio</p>
                    <p class="truncate center-align">{{$pond -> peso}} gramos</p>
                  <div class="divider"></div>

                  <p class="truncate teal-text">Tamaño promedio</p>
                    <p class="truncate center-align">{{$pond -> tamano}} centimetros</p>
                  <div class="divider"></div>

                  <p class="truncate teal-text">Volumen de agua</p>
                    <p class="truncate center-align">{{$pond -> volumenAgua}} litros</p>
                  <div class="divider"></div>

                  <p class="truncate teal-text">Biomasa</p>
                    <p class="truncate center-align">{{$pond -> cantidad * $pond-> peso }} kilos</p>
                  <div class="divider"></div>

                  <p class="truncate teal-text">Oxigeno</p>
                    <p class="truncate center-align">{{$pond -> oxigeno}} unidades de oxigeno</p>
                  <div class="divider"></div>

                  <p class="truncate teal-text">PH</p>
                    <p class="truncate center-align">{{$pond -> ph}} unidades de ph</p>
                  <div class="divider"></div>
                </div>
              </div>
              <div class="card-action center-align">
                @if(auth()->user()->idRol == 1)
                  <div class="row">
                    <div class="col s12 m12 l12 center-align">
                      <!-- NOTE: Se tienen que mostrar los datos del tipo en el formato y cambiar el texto del MODAL -->
                      <form action="{{ route('Estanque.Activate') }}" method="POST">
                        @csrf
                        <input type="hidden" name="id" value="{{$pond->id}}">
                        <button id="{{$pond->id}}" type="submit" class="waves-effect waves-light btn-flat blue-text text-darken-3 center-align">Activar<i class="material-icons right">cached</i></button>
                      </form>
                    </div>
                  </div>
                @endif
              </div>
            </div>
          </div>
        @endif
      @endforeach
      </div>
    </div>
  </div>
  @else
    <p>Nada</p>
  @endif



@endsection

@section('modals')

@foreach ($ponds as $pond)
  <div id="info-modal-{{$pond->id}}" class="modal modal-fixed-footer">
    <div class="modal-content center-align">
      <h5>Alimento recomendado:</h5>
      <h2 id='recomendado-{{$pond->id}}'></h2>
      <div class="container">

        <table class="striped centered">
          <thead>
            <tr>
              <th>Día</th>
              <th>Alimento</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td id="negative-day-3-{{$pond->id}}">-3</td>
              <td id="value-negative-day-3-{{$pond->id}}">{{$pond->minusThreeDay/1000}} Kg</td>
            </tr>
            <tr>
              <td id="negative-day-2-{{$pond->id}}">-2</td>
              <td id="value-negative-day-2-{{$pond->id}}">{{$pond->minusTwoDay/1000}} Kg</td>
            </tr>
            <tr>
              <td id="negative-day-1-{{$pond->id}}">-1</td>
              <td id="value-negative-day-1-{{$pond->id}}">{{$pond->minusOneDay/1000}} Kg</td>
            </tr>

            <tr>
              <th class="center-align" id="day-zero-{{$pond->id}}">HOY</th>
              <th class="center-align" id="value-day-zero-{{$pond->id}}">@if ($pond->today != null){{$pond->today/1000}} Kg @else --- @endif</th>
            </tr>

            <tr>
              <td id="positive-day-1-{{$pond->id}}">1</td>
              <td id="value-positive-day-1-{{$pond->id}}">XXXXX</td>
            </tr>
            <tr>
              <td id="positive-day-2-{{$pond->id}}">2</td>
              <td id="value-positive-day-2-{{$pond->id}}">XXXXX</td>
            </tr>
            <tr>
              <td id="positive-day-3-{{$pond->id}}">3</td>
              <td id="value-positive-day-3-{{$pond->id}}">XXXXX</td>
            </tr>
          </tbody>
        </table>
        <br>
      </div>

      <form action="{{route('Ponds.Feed')}}" method="post" id="pond-feed-{{$pond->id}}" class="feed-form">
        @csrf
        <input id='id-confirm-{{$pond->id}}' type="hidden" name="id" value="{{$pond->id}}">
        <input id='tamano-confirm-{{$pond->id}}' type="hidden" name="tamano" value="">
        <input id='recomendacion-confirm-{{$pond->id}}' type="hidden" name="recomendacion" value="">
        <input id='peso-confirm-{{$pond->id}}' type="hidden" name="peso" value="">
        <input id='cantidad-confirm-{{$pond->id}}' type="hidden" name="cantidad" value="">
        <input id='temperatura-confirm-{{$pond->id}}' type="hidden" name="temperatura" value="">
        <input id='incremento-confirm-{{$pond->id}}' type="hidden" name="incremento" value="">
        <input id='conversio-confirm-{{$pond->id}}' type="hidden" name="conversio" value="">
        <input id='fechaIngreso-confirm-{{$pond->id}}' type="hidden" name="fechaIngreso" value="">
        <input id='nombre-confirm-{{$pond->id}}' type="hidden" name="nombre" value="">
        <input id='volumenAgua-confirm-{{$pond->id}}' type="hidden" name="volumenAgua" value="">
        <input id='oxigeno-confirm-{{$pond->id}}' type="hidden" name="oxigeno" value="">
        <input id='ph-confirm-{{$pond->id}}' type="hidden" name="ph" value="">
        <div class="row">
          <div class="input-field col s12">
            <select class="select-pond" id='used-{{$pond->id}}' name='idCostal'>
              <option value="" disabled selected>Seleccione el costal utilizado</option>
              @foreach ($bags as $bag)
                <option value="{{$bag->id}}" cantidad='{{$bag->alimentoRestante}}'>{{$bag->etiqueta}} ({{$bag->alimentoRestante/1000}}/{{$bag->tamano/1000}} kg {{$bag->nombre}})</option>
              @endforeach
            </select>
            <label>Costal</label>
            <span class="red-text text-darken-3 error-select"></span>

          </div>
        </div>
        <div class="row">
          <div class="input-field col s12">
            <input id="quantity-used-{{$pond->id}}" name="quantity" type="number" step="0.01" min="0" max='0' class="validate" required="required">
            <label for="quantity-used-{{$pond->id}}">Cantidad suministrada</label>
            <span class="helper-text" data-error="No tiene suficiente comida en este costal" data-success="Correcto">Ingrese la cantidad que suministró en kg</span>
          </div>
        </div>
        <button type="submit" id="btn-feed-{{$pond->id}}" class="hide"></button>
      </form>
    </div>
    <div class="modal-footer">
      <a href="#!" id="feed-{{$pond->id}}" class="feed-launcher waves-effect btn-flat">Alimentar</a>
      <a href="#!" class="modal-close waves-effect  btn-flat">Cerrar</a>
    </div>
  </div>
@endforeach

<div id="modal-info" class="modal modal-fixed-footer">
  <div class="modal-content center-align" id='info-m'>

  </div>
  <div class="modal-footer" id='footer-m'>
    <a class='waves-effect waves-light btn-flat grey-text text-darken-3 modal-close'>Cerrar</a>
  </div>
</div>

@endsection

@section('scripts')
  <script type="text/javascript">
    $(document).ready(function() {
      $('.useless-pond').hide();
      $('.useless-trout').hide();
      $('.hidden-info').hide();

      @if(session('success'))
        M.toast({html: '{{session('success')}}',classes:'green darken-2'})
      @endif
      @if(session('error'))
        M.toast({html: '{{session('error')}}',classes:'red darken-2'})
      @endif
      @if (session('status'))
        M.toast({html: '{{session('status')}}',classes:'green darken-2'})
      @endif
      $('.feed-launcher').click(function(event) {
        $('#btn-'+$(this).attr('id')).click();
      });
      $("select.select-pond").change(function(){
        var x = $(this).children("option:selected").attr('cantidad');
        x/=1000;
        $('#quantity-'+$(this).attr('id')).val('');
        $('#quantity-'+$(this).attr('id')).attr('max',x);
      });

    $( "Form.feed-form" ).submit(function( event ) {
      //event.preventDefault();

        if ( $(this).find('.select-pond').val() != null ) {
          $(this).find('.error-select').empty()
          //event.preventDefault();
        }
        else{
          $(this).find('.error-select').html("Seleccione un producto");
          //alert($(this).closest('select.select-pond"').val())
          M.toast({html: 'Seleccione un costal',classes:'red darken-3'});
          event.preventDefault();
        }
      });

      $('Form.calculate').on('submit',function(){
         event.preventDefault();
         var x = $(this).serializeArray();
         var data = {};
         $.each(x, function(i, field){
          data[field.name] = field.value;
         });
         $(this).closest('.modal').modal('close');
         $('#info-modal-'+data.id).modal('open');

         $('#tamano-confirm-'+data.id).val(data.tamano);
         $('#peso-confirm-'+data.id).val(data.peso);
         $('#cantidad-confirm-'+data.id).val(data.cantidad);
         $('#temperatura-confirm-'+data.id).val(data.temperatura);
         $('#incremento-confirm-'+data.id).val(data.incremento);
         $('#conversio-confirm-'+data.id).val(data.conversio);
         $('#fechaIngreso-confirm-'+data.id).val(data.fechaIngreso);
         $('#nombre-confirm-'+data.id).val(data.nombre);
         $('#volumenAgua-confirm-'+data.id).val(data.volumenAgua);
         $('#oxigeno-confirm-'+data.id).val(data.oxigeno);
         $('#ph-confirm-'+data.id).val(data.ph);
         $('#recomendado-'+data.id).empty();
         $("#value-positive-day-1-"+data.id).empty();

         $("#day-zero-"+data.id).empty();
        // $("#value-day-zero-"+data.id).empty();
         $("#positive-day-1-"+data.id).empty();
         $("#positive-day-2-"+data.id).empty();
         $("#positive-day-3-"+data.id).empty();
         $("#negative-day-1-"+data.id).empty();
         $("#negative-day-2-"+data.id).empty();
         $("#negative-day-3-"+data.id).empty();
         $("#value-positive-day-1-"+data.id).empty();
         $("#value-positive-day-2-"+data.id).empty();
         $("#value-positive-day-3-"+data.id).empty();

         var alpha =0;
         var B = data.cantidad * data.peso/1000;
         var L = data.tamano * 10;
         if(data.temperatura >14)
          alpha = data.incremento * (1 + (15-data.temperatura)*0.0825);
         else
          alpha = data.incremento * (1 - (15-data.temperatura)*0.0825);
        //today
         var X = B* data.conversio * (((B/(1-(((L+alpha)*(L+alpha)*(L+alpha)-L*L*L)/(L*L*L))))-B)/B);
         $('#recomendado-'+data.id).append((X).toFixed(2)+' Kg')
        // $("#value-day-zero-"+data.id).append('---');
         $('#recomendacion-confirm-'+data.id).val((X*1000).toFixed(2));
         //values
        //tomorrow
          L+=alpha;
          s= ((L+alpha)*(L+alpha)*(L+alpha) - L*L*L)/(L*L*L);
          B=B/(1-s);
         var Y = B* data.conversio * (((B/(1-(((L+alpha)*(L+alpha)*(L+alpha)-L*L*L)/(L*L*L))))-B)/B);
         $("#value-positive-day-1-"+data.id).append((Y).toFixed(2)+' Kg');
         L+=alpha;
         s= ((L+alpha)*(L+alpha)*(L+alpha) - L*L*L)/(L*L*L);
         B=B/(1-s);
         var Y = B* data.conversio * (((B/(1-(((L+alpha)*(L+alpha)*(L+alpha)-L*L*L)/(L*L*L))))-B)/B);
         $("#value-positive-day-2-"+data.id).append((Y).toFixed(2)+' Kg');
         L+=alpha;
         s= ((L+alpha)*(L+alpha)*(L+alpha) - L*L*L)/(L*L*L);
         B=B/(1-s);
         var Y = B* data.conversio * (((B/(1-(((L+alpha)*(L+alpha)*(L+alpha)-L*L*L)/(L*L*L))))-B)/B);
         $("#value-positive-day-3-"+data.id).append((Y).toFixed(2)+' Kg');

         //dates
         $("#day-zero-"+data.id).append('{{$days['today']['day']}}/{{$days['today']['month']}}/{{$days['today']['year']}}');
         $("#positive-day-1-"+data.id).append('{{$days['day-one']['day']}}/{{$days['day-one']['month']}}/{{$days['day-one']['year']}}');
         $("#positive-day-2-"+data.id).append('{{$days['day-two']['day']}}/{{$days['day-two']['month']}}/{{$days['day-two']['year']}}');
         $("#positive-day-3-"+data.id).append('{{$days['day-three']['day']}}/{{$days['day-three']['month']}}/{{$days['day-three']['year']}}');
         $("#negative-day-1-"+data.id).append('{{$days['negative-day-one']['day']}}/{{$days['negative-day-one']['month']}}/{{$days['negative-day-one']['year']}}');
         $("#negative-day-2-"+data.id).append('{{$days['negative-day-two']['day']}}/{{$days['negative-day-two']['month']}}/{{$days['negative-day-two']['year']}}');
         $("#negative-day-3-"+data.id).append('{{$days['negative-day-three']['day']}}/{{$days['negative-day-three']['month']}}/{{$days['negative-day-three']['year']}}');
        // tomorrow
       });

      $('.info-lever').click(function(){
         var id = $(this).attr('id');
        if ($("#switch-"+id).prop("checked") == true)
          $("#row-"+id).hide();
        else
          $("#row-"+id).show();
       });

      $('#ponds-lever').click(function(){
         if ($("#ponds-switch").prop("checked") == true) {
           $('.useless-pond').hide();
           $('.useful-pond').show();
         }
         else{
           $('.useful-pond').hide();
           $('.useless-pond').show();
         }
       });

       $('#trouts-lever').click(function(){
          if ($("#trouts-switch").prop("checked") == true) {
            $('.useless-trout').hide();
            $('.useful-trout').show();
          }
          else{
            $('.useful-trout').hide();
            $('.useless-trout').show();
          }
        });
    });

    document.addEventListener('DOMContentLoaded', function() {
      var elems = document.querySelectorAll('.dropdown-trigger');
      var options =
        {
          constrainWidth:false,
          alignment:'right'
        };
      var instances = M.Dropdown.init(elems, options);
    });

    //boton info trucha
      $('.trout').click(function(){
       $id = $(this).attr("id");
       $id = $id.substring(4,$id.lenght);

       //M.toast({html: $id , classes: 'red'})

       $tipo = $('#tro-tipo-'+$id).attr("value");
       $desc = $('#tro-desc-'+$id).attr("value");
       $t = $('#tro-T-'+$id).attr("value");

       if($t == '0'){
         $t = "Inactiva";
       }
       else {
         $t = "Activa";
       }

       troInfo($id,$tipo,$desc,$t);
      });

   function troInfo($id,$tipo,$desc,$t){
   $('#info-m').empty();
   $('#info-m').append(
     "<h3>INFO  <i class='material-icons small teal-text'>info_outline</i></h3>"+
     "<div class='divider'></div>"+
     "<p class='grey-text'>Trucha "+$t+"</p>"+
     "<div class='divider'></div>"+
     "<table>"+
       "<tbody>"+
         "<tr>"+
           "<th class='center-align'>Etapa:</th>"+
           "<td>"+$tipo+"</td>"+
         "</tr>"+
         "<tr>"+
           "<th class='center-align'>Descripción:</th>"+
           "<td>"+$desc+"</td>"+
         "</tr>"+
       "</tbody>"+
     "</table>"
   );
   $('#footer-m').empty();
   $('#footer-m').append(
     "<a class='waves-effect waves-light btn-flat grey-text text-darken-3 modal-close'>Cerrar</a>"
   );
   $('#modal-info').modal('open');
 }
  </script>
@endauth
@endsection
