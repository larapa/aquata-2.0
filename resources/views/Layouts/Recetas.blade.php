@extends('Layouts.Menu')
@section('title','Recetas')
@section('content')

      <!-- NOTE: titulo de la seccion -->

<div class="row">
  <div class="col s12 m12 l4">
        <div class="card white">
          <div class="card-content blue-text text-darken-2">
            <span class="card-title">Panel de control</span>
            <div class="row">
              <div class="col s8 m8 l8">
                ¿Mostrar recetas inactivas?
              </div>
              <div class="col s4 m4 l4">
                <div class="switch right-align">
                 <label>
                   <input type="checkbox" id="recipes-switch">
                   <span class="lever" id="recipes-lever"></span>
                 </label>
               </div>
              </div>
            </div>
                <div ><i class="material-icons">error_outline</i> En este apartado se muestran las recetas que se podrán visualizar en la página de promoción de la granja</div>

                </div>
            <div class="card-action center-align">
              <a class="waves-effect waves-light btn-small teal white-text" href="{{ route('Receta.AddForm') }}"><i class="material-icons right">add</i>Agregar receta</a>
            </div>
          </div>
        </div>

        <!-- NOTE: Receta 1 -->
    @if(!empty($recipes))
    <div class="col s12 m12 l8 center-align" id="recipes-collection">
        <h4>Recetas</h4>
        <div class="row">
        @foreach($recipes as $recipe)
        @if($recipe->T == '1')
        <div class="col s12 m12 l6 useful-recipe" id="rec-{{$recipe->id}}">
        <input type="hidden" name="" value="{{json_encode($recipe->ingredientes)}}" id="rec-ingr-{{$recipe->id}}">
        <input type="hidden" name="" value="{{json_encode($recipe->instrucciones)}}" id="rec-inst-{{$recipe->id}}">
          <div class="card">
            <div class="card-image waves-effect waves-block waves-light">
              <img class="activator" src="{{url("uploads/".$recipe->imagen)}}" width="auto" height="280px" style="object-fit:cover;">
            </div>
            <div class="card-content">
              <span class="card-title activator grey-text text-darken-4">{{$recipe -> nombre}}<i class="material-icons right">more_vert</i></span>
              <p class="truncate"><a href="#">{{$recipe -> descripcion}}</a></p>
            </div>

            <div class="card-reveal">
              <!-- NOTE: NOMBRE -->
              <span class="card-title blue-text text-darken-4 left-align">{{$recipe -> nombre}}<i class="material-icons right">close</i></span>
              <div class="divider"></div>
              <div class=" center-align">

                <p class="truncate teal-text">Descripción</p>
                <p class="blue-teal-text text-lighten-1 center-align">{{$recipe -> descripcion}}</p>
                <div class="divider"></div>

                {{-- tiempo --}}
                <p class="truncate teal-text">Tiempo estimado:</p>
                <p class="truncate">{{$recipe -> tiempo}} minutos</p>
                <div class="divider"></div>


                {{-- Producto --}}
                <p class="truncate teal-text">Producto a utilizar:</p>
                  @foreach ($recipe->productos as $product)
                      <p class="truncate teal-text"></p>
                      <p class="truncate">🐟 {{$product->nombre}} </p>
                    @endforeach
                    <p class="truncate">{{$recipe -> pnombre}} </p>
                  <div class="divider"></div>

                <div class="row">
                  <p class="truncate teal-text">Ingredientes</p>
                    <div class=" center-align" id="div_ingredientes">
                    </div>
                </div>
                <div class="divider"></div>

                <div class="row">
                    <p class="truncate teal-text">Instrucciones</p>
                    <div class=" center-align" id="div_instrucciones">
                    </div>
                </div>
              </div>

            </div>

            <div class="card-action">
              <div class="row">
                <div class="col s6 m6 l6 center-align">
                      <form action="{{ route('Receta.Delete') }}" method="POST">
                        @csrf
                        <input type="hidden" name="id" value="{{$recipe->id}}">
                        <button id="{{$recipe->id}}" type="submit" class="waves-effect waves-light btn-flat red-text text-darken-3 center-align">Eliminar<i class="material-icons right">delete</i></button>
                      </form>
                </div>
                <div class="col s6 m6 l6 center-align">
                  <!-- NOTE: Se tienen que mostrar los datos del tipo en el formato y cambiar el texto del MODAL -->
                  <form action="{{ route('Recipe.Find') }}" method="POST">
                    @csrf
                    <input type="hidden" name="id" value="{{$recipe->id}}">
                    <input type="hidden" name="instrucciones" value="{{json_encode($recipe->instrucciones)}}">
                    <input type="hidden" name="ingredientes" value="{{json_encode($recipe->ingredientes)}}">
                    <button id="{{$recipe->id}}" type="submit" class="waves-effect waves-light btn-flat yellow-text text-darken-3 center-align">Editar<i class="material-icons right">edit</i></button>
                  </form>
                </div>
              </div>
            </div>

          </div>
        </div>
          @else
            <div class="col s12 m8 l6 useless-recipe" id="rec-{{$recipe->id}}">
              <input type="hidden" name="" value="{{json_encode($recipe->ingredientes)}}" id="rec-ingr-{{$recipe->id}}">
              <input type="hidden" name="" value="{{json_encode($recipe->instrucciones)}}" id="rec-inst-{{$recipe->id}}">
              <div class="card grey lighten-2">
                <div class="card-image waves-effect waves-block waves-light">
                  <img class="activator" src="{{url("uploads/".$recipe->imagen)}}" width="auto" height="280px" style="opacity:0.5; object-fit:cover;">
                </div>
                <div class="card-content">
                  <span class="card-title activator grey-text text-darken-4">{{$recipe -> nombre}}<i class="material-icons right">more_vert</i></span>
                  <p class="truncate"><a>{{$recipe -> descripcion}}</a></p>
                </div>
                <div class="card-reveal">
                  <!-- NOTE: NOMBRE -->
                  <span class="card-title blue-text text-darken-4 left-align">{{$recipe -> nombre}}<i class="material-icons right">close</i></span>
                  <div class="divider"></div>
                  <div class=" center-align">

                    <p class="truncate teal-text">Descripción</p>
                    <p class="blue-teal-text text-lighten-1 center-align">{{$recipe -> descripcion}}</p>
                    <div class="divider"></div>

                    {{-- tiempo --}}
                    <p class="truncate teal-text">Tiempo estimado:</p>
                    <p class="truncate">{{$recipe -> tiempo}} minutos</p>
                    <div class="divider"></div>


                    {{-- Producto --}}
                    <p class="truncate teal-text">Producto a utilizar:</p>
                      @foreach ($recipe->productos as $product)
                          <p class="truncate teal-text"></p>
                          <p class="truncate">🐟 {{$product->nombre}} </p>
                        @endforeach
                        <p class="truncate">{{$recipe -> pnombre}} </p>
                      <div class="divider"></div>

                    <div class="row">
                      <p class="truncate teal-text">Ingredientes</p>
                        <div class=" center-align" id="div_ingredientes">
                        </div>
                    </div>
                    <div class="divider"></div>

                    <div class="row">
                        <p class="truncate teal-text">Instrucciones</p>
                        <div class=" center-align" id="div_instrucciones">
                        </div>
                    </div>
                  </div>

                </div>

                <div class="card-action center-align">
                  <div class="row">
                    <div class="col s12 m12 l12 center-align">
                      <!-- NOTE: Se tienen que mostrar los datos del tipo en el formato y cambiar el texto del MODAL -->
                      <form action="{{ route('Receta.Activate') }}" method="POST">
                        @csrf
                        <input type="hidden" name="id" value="{{$recipe->id}}">
                        <button id="{{$recipe->id}}" type="submit" class="waves-effect waves-light btn-flat blue-text text-darken-3 center-align">Activar<i class="material-icons right">edit</i></button>
                      </form>
                    </div>
                  </div>
                </div>

              </div>
            </div>
          @endif
        @endforeach

      </div>
    </div>

  </div>
 </div>
@else
  <p>Nada</p>
@endif



  <script type="text/javascript">
    $(document).ready(function() {
      $('.useless-recipe').hide();


      @if(session('success'))
         M.toast({html: '{{session('success')}}',classes:'green darken-2'})
      @endif
      @if(session('error'))
         M.toast({html: '{{session('error')}}',classes:'red darken-2'})
      @endif

      $('#recipes-lever').click(function(){
         if ($("#recipes-switch").prop("checked") == true) {
           $('.useless-recipe').hide();
           $('.useful-recipe').show(250);
         }
         else{
           $('.useful-recipe').hide();
           $('.useless-recipe').show(250);
         }
       });

    });

      document.addEventListener('DOMContentLoaded', function() {
        var elems = document.querySelectorAll('.sidenav');
        var instances = M.Sidenav.init(elems);
      });
    </script>
    <script>
      document.addEventListener('DOMContentLoaded', function() {
        M.AutoInit();
      });
      document.addEventListener('DOMContentLoaded', function() {
        var elems = document.querySelectorAll('.horizontal-fab');
        var instances = M.FloatingActionButton.init(elems, {
          direction: 'left'
        });
      });


// MOSTAR EL JSON DE LOS INGREDIENTES Y PASOS de recetas activas
      $('.useful-recipe').click(function(){
       $id = $(this).attr("id");
       $id = $id.substring(4,$id.lenght);
       tipo = $('#rec-ingr-'+$id).attr("value");
       var tipo1 = JSON.parse(tipo);
        ingredientes(tipo1,tipo);

      });

      $('.useful-recipe').click(function(){
       $id = $(this).attr("id");
       $id = $id.substring(4,$id.lenght);
       tipo = $('#rec-inst-'+$id).attr("value");
       var tipo1 = JSON.parse(tipo);
        instrucciones(tipo1,tipo);

      });

      $('.useless-recipe').click(function(){
        $id = $(this).attr("id");
        $id = $id.substring(4,$id.lenght);
        tipo = $('#rec-ingr-'+$id).attr("value");
        var tipo1 = JSON.parse(tipo);
         ingredientes(tipo1,tipo);
      });

      $('.useless-recipe').click(function(){
        $id = $(this).attr("id");
        $id = $id.substring(4,$id.lenght);
        tipo = $('#rec-inst-'+$id).attr("value");
        var tipo1 = JSON.parse(tipo);
         instrucciones(tipo1,tipo);
      });


      function ingredientes(tipo1,tipo){
      $('div#div_ingredientes').empty();

      var bueno= JSON.parse('['+tipo1+']');

          $.each(bueno  , function( index, elemento){
              var div =
              '<div class="row">'+
              '<div class="col s4 m4 l4">'+
              '<p class="blue-text text-darken-3 left-align">⦿  '+elemento.cantidad+'</p>'+
              '</div>'+
              '<div class="col s8 m8 l8">'+
             '<p class="dark-text text-darken-2 left-align ">'+elemento.ingrediente+'</p>'+
              '</div>'+
              '</div>';
              $('div#div_ingredientes').append(div);

          });
      };

      function instrucciones(tipo1,tipo){
      $('div#div_instrucciones').empty();


      var bueno= JSON.parse('['+tipo1+']');

          $.each(bueno  , function( index, elemento){
              var div =
              '<div class="row">'+
              '<div class="col s2 m2 l2">'+
              '<p class="blue-text text-darken-3 right-align">⦿  '+elemento.id_paso+'</p>'+
              '</div>'+
              '<div class="col s10 m10 l10">'+
             '<p class="dark-text text-darken-2 left-align ">'+elemento.instruccion+'</p>'+
              '</div>'+
              '</div>';
              $('div#div_instrucciones').append(div);

          });
      };
    </script>
@endsection
