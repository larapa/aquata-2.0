<!DOCTYPE html>
<html lang="en" dir="ltr">

  <head>
    <script type="text/javascript" src="/js/jquery-3.4.1.min.js"></script>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    {!! MaterializeCSS::include_all() !!}
    <meta name="viewport" charset="utf-8" content="width=device-width, initial-scale=1.0"/>
    <title>AQUATA @yield('head')</title>
  </head>
@auth
  <body>
    <header>
      <div  class="navbar-fixed">
        <nav>
          <div class="nav-wrapper blue darken-3">
            <a href="#" class="brand-logo truncate">@yield('title')</a>
            <a href="#" data-target="menu-responsive" class="sidenav-trigger">
              <i class="material-icons">menu</i>
            </a>
            <ul class="right hide-on-med-and-down">
              @if(auth()->user()->idRol == 1)
                <li><a href="{{route('ponds')}}"><i class="material-icons tooltipped" data-position="bottom" data-tooltip="Inicio">home</i></a></li>
                <li><a href="{{route('Inventory')}}"><i class="material-icons tooltipped" data-position="bottom" data-tooltip="Inventario">storage</i></a></li>
                <li><a href="{{route('Employee')}}"><i class="material-icons tooltipped" data-position="bottom" data-tooltip="Trabajadores">people</i></a></li>
                <li><a href="{{route('Economy')}}"><i class="material-icons tooltipped" data-position="bottom" data-tooltip="Economía">monetization_on</i></a></li>
                <li><a href="{{route('History.Articles')}}"><i class="material-icons tooltipped" data-position="bottom" data-tooltip="Historiales">today</i></a></li>
                <li><a href="{{route('recipes')}}"><i class="material-icons tooltipped" data-position="bottom" data-tooltip="Recetas">web</i></a></li>
                <li><a href="{{route('Profile')}}"><i class="material-icons tooltipped" data-position="bottom" data-tooltip="Perfil">face</i></a></li>
              @endif
              @if(auth()->user()->idRol == 2)
                <li><a href="{{route('ponds')}}"><i class="material-icons tooltipped" data-position="bottom" data-tooltip="Inicio">home</i></a></li>
                <li><a href="{{route('Employee')}}"><i class="material-icons tooltipped" data-position="bottom" data-tooltip="Trabajadores">people</i></a></li>
                <li><a href="{{route('Economy')}}"><i class="material-icons tooltipped" data-position="bottom" data-tooltip="Economía">monetization_on</i></a></li>
                <li><a href="{{route('Profile')}}"><i class="material-icons tooltipped" data-position="bottom" data-tooltip="Perfil">face</i></a></li>
              @endif
              @if(auth()->user()->idRol == 3)
                <li><a href="{{route('ponds')}}"><i class="material-icons tooltipped" data-position="bottom" data-tooltip="Inicio">home</i></a></li>
                <li><a href="{{route('Inventory')}}"><i class="material-icons tooltipped" data-position="bottom" data-tooltip="Inventario">storage</i></a></li>
                <li><a href="{{route('Profile')}}"><i class="material-icons tooltipped" data-position="bottom" data-tooltip="Perfil">face</i></a></li>
              @endif
            </ul>
          </div>
        </nav>
      </div>

      <nav class="sidenav" id="menu-responsive">
          @if(auth()->user()->idRol == 1)
            <li><a href="{{route('ponds')}}"><i class="material-icons">home</i>Inicio </a></li>
            <li><a href="{{route('Inventory')}}"><i class="material-icons">storage</i>Inventario</a></li>
            <li><a href="{{route('Employee')}}"><i class="material-icons">people</i>Trabajadores</a></li>
            <li><a href="{{route('Economy')}}"><i class="material-icons">monetization_on</i>Economía</a></li>
            <li><a href="{{route('History.Articles')}}"><i class="material-icons">today</i>Historiales</a></li>
            <li><a href="{{route('recipes')}}"><i class="material-icons">web</i>Recetas</a></li>
            <li><a href="{{route('Profile')}}"><i class="material-icons">face</i>Perfil</a></li>
          @endif
          @if(auth()->user()->idRol == 2)
            <li><a href="{{route('ponds')}}"><i class="material-icons">home</i>Inicio </a></li>
            <li><a href="{{route('Employee')}}"><i class="material-icons">people</i>Trabajadores</a></li>
            <li><a href="{{route('Economy')}}"><i class="material-icons">monetization_on</i>Economía</a></li>
            <li><a href="{{route('Profile')}}"><i class="material-icons">face</i>Perfil</a></li>
          @endif
          @if(auth()->user()->idRol == 3)
            <li><a href="{{route('ponds')}}"><i class="material-icons">home</i>Inicio </a></li>
            <li><a href="{{route('Inventory')}}"><i class="material-icons">storage</i>Inventario</a></li>
            <li><a href="{{route('Profile')}}"><i class="material-icons">face</i>Perfil</a></li>
          @endif
      </nav>
    </header>
    <div class="preloader-div" style="position:fixed;top:50%;left:45%;">
      <div class="preloader-wrapper big active">
        <div class="spinner-layer spinner-blue">
          <div class="circle-clipper left">
            <div class="circle"></div>
          </div><div class="gap-patch">
            <div class="circle"></div>
          </div><div class="circle-clipper right">
            <div class="circle"></div>
          </div>
        </div>

        <div class="spinner-layer spinner-red">
          <div class="circle-clipper left">
            <div class="circle"></div>
          </div><div class="gap-patch">
            <div class="circle"></div>
          </div><div class="circle-clipper right">
            <div class="circle"></div>
          </div>
        </div>

        <div class="spinner-layer spinner-yellow">
          <div class="circle-clipper left">
            <div class="circle"></div>
          </div><div class="gap-patch">
            <div class="circle"></div>
          </div><div class="circle-clipper right">
            <div class="circle"></div>
          </div>
        </div>

        <div class="spinner-layer spinner-green">
          <div class="circle-clipper left">
            <div class="circle"></div>
          </div><div class="gap-patch">
            <div class="circle"></div>
          </div><div class="circle-clipper right">
            <div class="circle"></div>
          </div>
        </div>
      </div>
    </div>
    <main style="opacity:0">

      @yield('content')

    </main>

    <section>

      @yield('modals')

    </section>

    <footer class="page-footer blue darken-4">
      <div class="container">
        <div class="row">
          <div class="col s12  m6 l6 center-align offset-l3 offset-m3">
            <img src="/images/Logos/DevT/Racoon_wide_white.svg" width="60%">
          </div>
        </div>
      </div>
    </footer>

    <script>
    $(document).ready(function() {
      $('.preloader-div').fadeOut("slow", function(){
              $('main').fadeTo( "slow", 1);
      });
      //$('main').css('opacity','1');

    });

      document.addEventListener('DOMContentLoaded', function() {
        var elems = document.querySelectorAll('.sidenav');
        var instances = M.Sidenav.init(elems);
      });
      document.addEventListener('DOMContentLoaded', function() {
        M.AutoInit();
      });

      //  NOTE: tostadas para mostrar mensajes
      function green_toast($info){
        M.toast({html: $info , classes: 'green darken-2'})
      }
        function red_toast($info){
        M.toast({html: $info , classes: 'red darken-2'})
      }
      function yellow_toast($info){
        M.toast({html: $info , classes: 'yellow darken-2'})
      }
    </script>
    @yield('scripts')
  </body>
@endauth
</html>
