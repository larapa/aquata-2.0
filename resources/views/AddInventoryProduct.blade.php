@extends('Layouts.Menu')
@section('title','Intentario de productos')
@section('content')

  <form action="{{route('InventoryProduct.AddValidation')}}" method="POST">
    @csrf
    <h4 class="center-align">Inventario de productos</h4>
    <div class="row">
      <div class="container">
        <div class="col s12">
          <div class="row">
            <div class="col s12 m4 l4 center-align">
              <h4><i class="material-icons">help_outline</i> INFO</h4>

            </div>
            <div class="col s12 m8 l8">
              <p  ALIGN="justify">
                El <span class="blue-text">Sistema Administrativo AQUATA</span> cuenta con un inventario especial para la
                administración de tus productos, aquí podrás llevar el control de la cantidad de piezas que tienes almacenadas.
                Al ingresar un producto al inventario también estas <span class="red-text text-darken-2">retirando truchas de un estanque</span>.
                Asegurate de que la información que ingreses sea la correcta ya que esto afectará directamente a otras fucionalidades del sistema.
              </p>
              <p  ALIGN="justify">
                Haremos un registro de este suceso en el <span class="blue-text">historial de estanque</span> del estanque que selecciones. Podrás ver tus piezas
                disponibles en el <span class="blue-text">inventario de productos</span>.
              </p>

            </div>
          </div>
          <div class="row">
            <div class="input-field col s12">
              <select name = 'producto' id='product'>
                <option value="" disabled selected>Selecciona un producto</option>
                @foreach ($products as $product)
                  <option value="{{$product->id}}">{{$product->nombre}}</option>
                @endforeach
              </select>

              <label>Producto</label>
              <span id="error-product" class="red-text text-darken-3"></span>
            </div>
          </div>
          <div class="row">
            <div class="input-field col s12 m12 l6 ">
                <input name="cantidadProductos" id="cantidadp" type="number" step="1" min="0" max="99999999" class="validate" value="" required="" aria-required="true">
              <label for="cantidadp">Productos generados</label>
              <span class="helper-text" id="helper-cantidad">Ingrese la cantidad de productos creados</span>
            </div>
            <div class="input-field col s12 m12 l6 ">
                <input name="cantidadTruchas" id="cantidad" type="number" step="1" min="0" max="100" class="validate" value="" required="" aria-required="true">
              <label for="cantidad">Truchas utilizadas</label>
              <span class="helper-text" id="helper-cantidad">Ingrese la cantidad de truchas utilizadas</span>
            </div>
          </div>
          <div class="row">
            <div class="input-field col s12">
              <select name='estanque' id='estanque'>
                <option value="" disabled selected>Selecciona un estanque</option>
                @foreach ($ponds as $pond)
                  <option class="truncate" value="{{$pond->id}}">{{$pond->nombre}}</option>
                @endforeach

              </select>
              <label>Estanque</label>
              <span id="error-pond" class="red-text text-darken-3"></span>
            </div>
          </div>
          @foreach ($ponds as $pond)
            <div class="row hidden-info" id='info-{{$pond->id}}'>
              <div class="row">
                <div class="input-field col s12">
                  <input disabled value="{{$pond->cantidad}}" id="quantity-{{$pond->id}}" type="text" class="validate">
                  <label for="quantity-{{$pond->id}}">Cantidad disponible</label>
                </div>
              </div>
              <div class="row">
                <div class="input-field col s12">
                  <input disabled value="{{$pond->tipo}}" id="disabled" type="text" class="validate">
                  <label for="disabled">Tipo de trucha</label>
                </div>
              </div>
            </div>
          @endforeach

        </div>
        <div class="center-align">
          <button type="submit" class="waves-effect waves-light btn blue darken-3 center-align">Aceptar</button>
        </div>
      </div>
    </div>
  </form>
@endsection
@section('scripts')
  <script type="text/javascript">
  $(document).ready(function() {
    M.updateTextFields();
    $('.hidden-info').hide();
  });

  $('select#estanque').change(function(event) {
    $('.hidden-info').hide();
    $('#info-'+$(this).val()).show('slow');
    $('#cantidad').val('');
    $('#cantidad').attr('max',$('#quantity-'+$(this).val()).val())
  });

  $( "Form" ).submit(function( event ) {
      if ( $( "select#product" ).first().val() != null ) {
        $("#error-product").empty();

      }
      else{
        $("#error-product").html("Seleccione un producto");
        M.toast({html: 'Seleccione un producto',classes:'red darken-3'});
        //alert("Seleccione un tipo de trucha");
        $(document).scrollTop(0);
        event.preventDefault();
      }
      if ( $( "select#estanque" ).first().val() != null ) {
        $("#error-pond").empty();
        return;
      }
      else{
        $("#error-pond").html("Seleccione un estanque");
        M.toast({html: 'Seleccione un estanque',classes:'red darken-3'});
        //alert("Seleccione un tipo de trucha");
        $(document).scrollTop(0);
        event.preventDefault();
      }
    });
  </script>
@endsection
