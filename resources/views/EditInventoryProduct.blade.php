@extends('Layouts.Menu')
@section('title','Editar producto de inventario')
@section('content')

  <form action="{{route('InventoryProduct.Update')}}" method="POST">
    <input type="hidden" name="id" value="{{$inventoryProduct->id}}">
    @csrf
    <h4 class="center-align">Editar producto de inventario </h4>
    <div class="row">
      <div class="container">
        <div class="col s12">
          <div class="row">
            <div class="col s4">
              <h4><i class="material-icons">help_outline</i> INFO</h4>

            </div>
            <div class="col s8">
              <p  ALIGN="justify">La modificación de un elemento del inventario de productos consiste en
                seleccionar el producto que será elaborado con una trucha del estanque seleccionado.
              </p>
              <p  ALIGN="justify">
                Considera que al añadir elementos de este producto, serán restados a la cantidad
                de truchas presentes en el estanque seleccionado.
              </p>
            </div>
          </div>
          <div class="row">
            <div class="input-field col s12">
              <select name='estanque'>
                <option value="" disabled>Selecciona un estanque</option>
                @foreach ($ponds as $pond)
                  <option class="truncate" value="{{$pond->id}}"@if($pond->id == $inventoryProduct->idEstanque) selected @endif>{{$pond->nombre}} - {{$pond->tipo}} </option>
                @endforeach

              </select>
              <label>Estanque</label>
            </div>
          </div>
          <div class="row">
            <div class="input-field col s12">
              <select name = 'producto'>
                <option value="" disabled >Selecciona un producto</option>
                @foreach ($products as $product)
                  <option value="{{$product->id}}"@if($product->id == $inventoryProduct->idProducto) selected @endif>{{$product->nombre}}</option>
                @endforeach
              </select>
              <label>Producto</label>
            </div>
          </div>
        </div>
        <div class="center-align">
          <button type="submit" class="waves-effect waves-light btn blue darken-3 center-align">Aceptar</button>
        </div>
      </div>
    </div>
  </form>
@endsection
