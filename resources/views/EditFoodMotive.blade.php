@extends('Layouts.Menu')
@section('title','Editar motivo de uso')
@section('content')

  @if (session('mensaje'))
    <div class="alert"> {{session('mensaje')}} </div>
  @endif



  <form action="{{route('Motive.Update')}}" method="POST">
    <input type="hidden" name="id" value="{{$motive->id}}">
    @csrf
    <input type="hidden" name="direccion" value="0">

    <h4 class="center-align">Motivo de uso: {{ $motive->nombre }}</h4>
    <div class="row">
      <div class="container">
        <div class="col s12">
          <div class="row">
            <div class="col s12 m4 l4 center align">
              <h4><i class="material-icons">help_outline</i> INFO</h4>

            </div>
            <div class="col s12 m8 l8">
              <p  ALIGN="justify">
                En este apartado puedes editar la información de <span class="blue-text">
                {{$motive->nombre}}</span>, Asegurate de ingresar información lo más descriptiva
                y simple posible.
              </p>
              <p  ALIGN="justify">
                Los motivos de uso <span class="blue-text">Alimentación</span>,
                <span class="blue-text">Compra</span> y <span class="blue-text">Desastre</span>
                no son editables.
              </p>
            </div>
          </div>
          <!-- NOTE: NOMBRE -->
          <div class="row">
            <div class="input-field col s12">
              <input id="motive_nombre" name="nombre" type="text" data-length="30" maxlength="30" required="" aria-required="true" class="validate" value="{{ $motive->nombre }}">
              <label for="motive_nombre">Nombre</label>
              <span class="help-text" data-error="Incorrecto" data-success="Correcto">Ingrese el nombre del motivo de uso</span>
            </div>
          </div>
          <!-- NOTE: DESCRIPCION -->
          <div class="row">
            <div class="input-field col s12">
              <textarea id="motive_descripcion" class="validate materialize-textarea" name="descripcion" type="text" data-length="120" maxlength="120" required="" aria-required="true">{{ $motive->descripcion }}</textarea>
              <label for="motive_descripcion">Descripción</label>
              <span class="help-text" data-error="Incorrecto" data-success="Correcto">Ingrese la descripción</span>
            </div>
          </div>

        </div>
        <div class="center-align">
          <button type="submit" class="waves-effect waves-light btn blue darken-3 center-align">Aceptar</button>
        </div>
      </div>
    </div>
  </form>

@endsection

@section('scripts')
  <script type="text/javascript">

    $(document).ready(function() {
      $('input#motive_nombre, textarea').characterCounter();
    });

  </script>
@endsection
