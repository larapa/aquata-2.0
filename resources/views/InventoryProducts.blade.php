<?php // TODO: Falta definir las rutas y modals?>
@extends('Layouts.Menu')
@section('head','usuario')
@section('title','Inventario')
@section('content')
    <ul class="tabs tabs-fixed-width">

      <li class="tab col s4 m4 l4"><a target="_self" href="{{route('Inventory.Food')}}">Alimento</a></li>
      <li class="tab col s4 m4 l4"><a target="_self" href="{{route('Inventory.Articles')}}">Articulos</a></li>

      <li class="tab col s4 m4 l4"><a class="active" href="#swipe-products">Productos</a></li>

    </ul>

    <div id="swipe-food">
    </div>

    <div id="swipe-products">
      <!--Panel de control-->
      <div class="row">
        <div class="col s12 m12 l4">
          <div class="card white">
            <div class="card-content blue-text text-darken-2">
              <span class="card-title">Panel de control</span>
              <div class="row">
                <div class="col s8 m8 l8">
                  ¿Mostrar productos agotados?
                </div>
                <div class="col s4 m4 l4">
                  <div class="switch right-align">
                   <label>
                     <input type="checkbox" id="inventProducts-switch">
                     <span class="lever" id="inventProducts-lever"></span>
                   </label>
                 </div>
                </div>
              </div>
              <ul class="collapsible popout">
                <li>
                  <div class="collapsible-header"><i class="material-icons">kitchen</i>Productos</div>
                  <div class="collapsible-body">
                    <div class="row">
                      <div class="col s8 m8 l8">
                        ¿Mostrar productos inactivos?
                      </div>
                      <div class="col s4 m4 l4">
                        <div class="switch right-align">
                         <label>
                           <input type="checkbox" id="products-switch" >
                           <span class="lever" id="products-lever"></span>
                         </label>
                       </div>
                      </div>
                    </div>
                    <!-- ver los productos -->
                    <ul class="collection" id="products-collection">
                      @foreach($products as $product)
                        @if($product->T == '1')
                          <li class="collection-item avatar usefull-product">
                            <div class="row">
                              <div class="col s10">
                                <i class="material-icons circle blue"><img id="imagen-producto-{{$product->id}}" src="{{url("uploads/".$product->imagen)}}" width="100%" height="100%"></i>
                                <span class="title" id="NAME-PRODUCT-{{$product->id}}">{{$product->nombre}}</span>
                                <p class="grey-text truncate" id="DESCRIPTION-PRODUCT-{{$product->id}}" price="{{$product->precio}}" T="{{$product->T}}" imagen="{{$product->imagen}}">{{$product->descripcion}}</p>
                              </div>
                              <div class="col s2">
                                <a class="secondary-content dropdown-trigger btn-flat teal-text" data-target="PRODUCT-{{$product->id}}"><i class="material-icons right">arrow_drop_down</i></a>
                              </div>
                            </div>
                          </li>
                        @else
                          <li class="collection-item avatar useless-product">
                            <div class="row">
                              <div class="col s10">
                                <i class="material-icons circle indigo lighten-4"><img id="imagen-producto-{{$product->id}}" src="{{url("uploads/".$product->imagen)}}" width="100%" height="100%" style="opacity: 0.5;"></i>
                                <span class="title" id="NAME-PRODUCT-{{$product->id}}">{{$product->nombre}}</span>
                                <p class="grey-text truncate" id="DESCRIPTION-PRODUCT-{{$product->id}}" price="{{$product->precio}}" T="{{$product->T}}"  imagen="{{$product->imagen}}">{{$product->descripcion}}</p>
                              </div>
                              <div class="col s2">
                                <a class="secondary-content dropdown-trigger btn-flat teal-text" data-target="PRODUCT-{{$product->id}}"><i class="material-icons right">arrow_drop_down</i></a>
                              </div>
                            </div>
                          </li>
                        @endif
                      @endforeach
                    </ul>
                    <div class="card-action center-align">
                      <a class="waves-effect waves-light btn-small teal white-text" href="{{route('Product.Add')}}"><i class="material-icons right">add</i>Agregar producto</a>
                    </div>
                  </div>
                </li>
              </ul>
            </div>
            <div class="card-action center-align">
              <div class="row">
                <div class="col s6">
                  <a class="waves-effect waves-light btn-small teal white-text" href="{{route('InventoryProduct.Add')}}"><i class="material-icons right">add</i>Agregar</a>
                </div>
                <div class="col s6">
                  <a class="waves-effect waves-light btn-small teal white-text" href="{{route('InventoryProduct.Convert')}}"><i class="material-icons right">compare_arrows</i>Convertir</a>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div class="col s12 m12 l8 center-align">
          <h4>Inventario Productos</h4>
          <table class=" highlight centered responsive-table">
            <thead>
              <tr>
                <th>Producto</th>
                <th>Existencia</th>
                <th>Precio</th>

                <th class="hide-on-med-and-down"> </th>
                <th class="hide-on-med-and-down"> </th>
                <th class="hide-on-large-only">Añadir</th>
                <th class="hide-on-large-only">Desastre</th>
              </tr>
            </thead>
            <tbody id="inventProducts-table">
              @foreach($inventProducts as $inventProduct)
                @if($inventProduct->cantidadElementos != 0)
                  <tr id="INVENTPRODUCT-{{$inventProduct->id}}" class="inventProduct">
                    <td id="NAME-INVENTPRODUCT-{{$inventProduct->id}}">{{$inventProduct->nombre}}</td>
                    <td id="QUANTITY-INVENTPRODUCT-{{$inventProduct->id}}">{{$inventProduct->cantidadElementos}}</td>
                    <td id="PRICE-INVENTPRODUCT-{{$inventProduct->id}}">${{$inventProduct->precio}} /Kg</td>


                    <td id="EDIT-QUANTITY-INVENTPRODUCT-{{$inventProduct->id}}"><a class="waves-effect waves-light" href="{{route('InventoryProduct.Upgrade',$inventProduct->id)}}"><i class="material-icons">add_circle</i></a></td>
                    <td id="DISASTER-INVENTPRODUCT-{{$inventProduct->id}}"><a class="waves-effect waves-light" href="{{route('InventoryProduct.Disaster',$inventProduct->id)}}"><i class="material-icons">delete_sweep</i></a></td>
                  </tr>
                @else
                  <tr id="INVENTPRODUCT-{{$inventProduct->id}}" class="inventProduct grey-text text-lighten-2 useless-inventProduct">
                    <td id="NAME-INVENTPRODUCT-{{$inventProduct->id}}">{{$inventProduct->nombre}}</td>
                    <td id="QUANTITY-INVENTPRODUCT-{{$inventProduct->id}}">{{$inventProduct->cantidadElementos}}</td>
                    <td id="PRICE-INVENTPRODUCT-{{$inventProduct->id}}">${{$inventProduct->precio}} /Kg</td>


                    <td id="EDIT-QUANTITY-INVENTPRODUCT-{{$inventProduct->id}}"><a class="waves-effect waves-light" href="{{route('InventoryProduct.Upgrade',$inventProduct->id)}}"><i class="material-icons">add_circle</i></a></td>
                    <td id="DISASTER-INVENTPRODUCT-{{$inventProduct->id}}"><i class="material-icons">delete_sweep</i></td>
                  </tr>
                @endif
              @endforeach
            </tbody>

          </table>
        </div>
      </div>
    </div>

    <div class="pages center-align">
      <p>{{$inventProducts->links()}}</p>
    </div>

@endsection

@section('modals')
  <div class="modal modal-fixed-footer">
    <div class="modal-content center-align">

    </div>
    <div class="modal-footer">
      <a class='waves-effect waves-light btn-flat grey-text text-darken-3 modal-close'>Cerrar</a>
    </div>
  </div>
  <!-- Definir las rutas -->
  @foreach($products as $product)
    @if($product->T == '1')
      <ul id='PRODUCT-{{$product->id}}' class='dropdown-content'>
          <li><a href="#!" id='INFO-PRODUCT-{{$product->id}}' class="black-text info-product"><i class="material-icons left blue-text">error_outline</i>INFO</a></li>
          <li class="divider" tabindex="-1"></li>
          <li><a href="{{route('Product.Edit',$product->id)}}" class="black-text"><i class="material-icons left blue-text">edit</i>Editar</a></li>
          <li><a href="{{ route('Product.Delete', $product->id ) }}" class="black-text"><i class="material-icons left blue-text">delete</i>Eliminar</a></li>
      </ul>
    @else
      <ul id='PRODUCT-{{$product->id}}' class='dropdown-content'>
          <li><a href="#!"  id='INFO-PRODUCT-{{$product->id}}' class="black-text info-product"><i class="material-icons left blue-text">error_outline</i>INFO</a></li>
          <li class="divider" tabindex="-1"></li>
          <li><a href="{{ route('Product.Active', $product->id ) }}" class="black-text"><i class="material-icons left blue-text">cached</i>Activar</a></li>
      </ul>
    @endif
  @endforeach
@endsection

@section('scripts')
  <script type="text/javascript" id="scripts-alimento">
    $(document).ready(function() {
      @if(session('success'))
         M.toast({html: '{{session('success')}}',classes:'green darken-2'})
      @endif
      @if(session('non-success'))
         M.toast({html: '{{session('non-success')}}',classes:'red darken-2'})
      @endif
      @if (session('error'))
       M.toast(html: "{{session('error')}}", classes:'red darken-2');
      @endif

      //Agregue estos
      $('.useless-product').hide();
      $('.useless-inventProduct').hide();

      //Agregue estos 2
       $('#products-lever').click(function(){
            if ($("#products-switch").prop("checked") == true) {
              $('.useless-product').hide();
              $('.usefull-product').show();
            }
            else{
             $('.useless-product').show();
             $('.usefull-product').hide();
            }
        });

        $('#inventProducts-lever').click(function(){
             if ($("#inventProducts-switch").prop("checked") == true) {
               $('.useless-inventProduct').hide();
             }
             else{
              $('.useless-inventProduct').show();
             }
         });

        $('#inventProducts-lever').click();

        $('.info-product').click(function(event) {
          $id =  $(this).attr('id').substring('13',$(this).attr('id').lenght);
          $nombre = $('#NAME-PRODUCT-'+$id).text();
          $descripcion = $('#DESCRIPTION-PRODUCT-'+$id).text();
          $precio = $('#DESCRIPTION-PRODUCT-'+$id).attr('price');
          $t = $('#DESCRIPTION-PRODUCT-'+$id).attr('T');
          $img = $('#imagen-producto-'+$id).attr('src');
          if($t == '1')
            $t = 'Activo';
          else
            $t = 'Inactivo';
          productInfo($nombre,$descripcion,$precio,$t,$img);
        });

     });




    //no tocar, es para que funcionen los dropdown
    document.addEventListener('DOMContentLoaded', function() {
      var elems = document.querySelectorAll('.dropdown-trigger');
      var options =
        {
          constrainWidth:false,
          alignment:'right',
          container:'#deltest'
        };
      var instances = M.Dropdown.init(elems, options);
    });

    function productInfo($name,$description,$price,$t,$img){
        $('.modal-content').empty();
        $('.modal-content').append(
          "<h3>INFO  <i class='material-icons small teal-text'>info_outline</i></h3>"+
          "<div class='divider'></div>"+
          "<p class='grey-text'>Producto "+$t+"</p>"+
          "<div class='divider'></div>"+
          "<table class='striped'>"+
            "<tbody>"+
              "<tr>"+
                "<th class='center-align'>Nombre:</th>"+
                "<td>"+$nombre+"</td>"+
              "</tr>"+
              "<tr>"+
                "<th class='center-align'>Descripción:</th>"+
                "<td>"+$descripcion+"</td>"+
              "</tr>"+
              "<tr>"+
                "<th class='center-align'>Precio:</th>"+
                "<td>"+$precio+"</td>"+
              "</tr>"+
              "<tr>"+
                "<th class='center-align' colspan='2'>Imagen:</th>"+
              "</tr>"+
              "<tr>"+
                "<td class='center-align' colspan='2'><img src='"+$img+"' width='100px' height='100px' style='border-radius: 25px;'></td>"+
              "</tr>"+
            "</tbody>"+
          "</table>"
      );
      // TODO: sacar la imagen del icono del elemento
      $('.modal').modal('open');
    }
  </script>
@endsection
<?php // TODO: HACER LOGO DE mapache ?>
