@extends('Layouts.Menu')
@section('title','Editar etapa')
@section('content')


  @if (session('mensaje'))
    <div class="alert"> {{session('mensaje')}} </div>
  @endif

  <form action="{{ route('Trucha.Update', $trout->id) }}" method="POST">
    @csrf
<input type="hidden" name="id" value="{{ $trout->id }}">
    <h4 class="center-align">Etapa: {{ $trout->tipo }}</h4>
    <div class="row">
      <div class="container">
        <div class="col s12">
          <div class="row">
            <div class="col s12 m4 l4 center align">
              <h4><i class="material-icons">help_outline</i> INFO</h4>

            </div>
            <div class="col s12 m8 l8">
              <p  ALIGN="justify">
                En este apartado puedes editar la información de <span class="blue-text">
                {{$trout->tipo}}</span>. Asegurate de ingresar información lo más descriptiva
                y simple posible.
              </p>
            </div>
          </div>
          <!-- NOTE: tipo -->
          <div class="row">
            <div class="input-field col s12">
              <input id="trout_name" value="{{ $trout->tipo }}" name="tipo" type="text" class="validate" data-length="30" maxlength="30" required="required">
              <label for="trout_name">Nombre</label>
              <span class="help-text" data-error="Incorrecto" data-success="Correcto">Ingrese el nombre de la etapa</span>
            </div>
          </div>
          <br></br>
          <!-- NOTE: desc -->
          <div class="row">
            <div class="input-field col s12">
              <input id="trout_name" name="descripcion" type="text" class="validate" required="required"  data-length="120" maxlength="120"  value="{{ $trout->descripcion }}">
              <label for="trout_name">Descripcion</label>
              <span class="help-text">Ingrese una descripcion</span>
            </div>
          </div>
          <br></br>
        <div class="row">
          <div class="col s12 m12 l12 center-align">
            <button type="submit" class="waves-effect waves-light btn white-text text-darken-1 center-align">Editar</button>
          </div>
        </div>
      </div>
    </div>
  </div>

@endsection

@section('scripts')
  <script type="text/javascript">
    $(document).ready(function() {
      $('input#trout_name').characterCounter();
    });
  </script>
@endsection
