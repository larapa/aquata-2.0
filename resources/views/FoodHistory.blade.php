@extends('Layouts.Menu')
@section('head','usuario')
@section('title','Historiales')
@section('content')
  <ul class="tabs tabs-fixed-width">
    <li class="tab col s4 m4 l4"><a target="_self"  href="{{route('History.Articles')}}">Historial articulos</a></li>
    <li class="tab col s4 m4 l4"><a class="active" href="#swipe-foodHistory">Historial alimento</a></li>
  </ul>

  <div id="swipe-foodHistory">
    <!--Panel de control-->
    <div class="row">
      <div class="col s12 m12 l4">
        <div class="card white">
          <div class="card-content blue-text text-darken-2">
            <span class="card-title">Panel de control</span>
            <br>
            <ul class="collapsible popout">
              <!-- NOTE: Filtro de fechas -->
              <li>
                <div class="collapsible-header"><i class="material-icons">date_range</i>Selecionar periodo</div>
                <div class="collapsible-body">
                  <!-- NOTE: formulario para cambiar fechas -->
                  <form class="" id="form-fechas" action="{{route('History.FoodHistoryByDate')}}" method="post">
                    @csrf
                    <div class="row">
                      <!-- NOTE: datepicker 1 -->
                      <div class="col s12 m12 l12">
                        <h6 class="center-align black-text">Fecha inicial:</h6>
                        <input type="text" name="fecha1" class="datepicker center-align" id="fecha1">
                      </div>
                      <!-- NOTE: datepicker 2 -->
                      <div class="col s12 m12 l12">
                        <h6 class="center-align black-text">Fecha final:</h6>
                        <input type="text" name="fecha2" class="datepicker center-align" id="fecha2">
                      </div>
                    </div>
                    <div class="row">
                      <!-- NOTE: boton para actualizar los datos con las fechas de los pickers -->
                      <div class="col s12 m12 l12 center-align">
                        <br>
                        <a class="waves-effect waves-light btn teal" onclick="document.getElementById('form-fechas').submit();">Consultar</a>
                      </div>
                    </div>
                  </form>
              </li>
            </ul>
          </div>
          <div class="card-action center-align">
            <div class="row">
              <div class="col s12">
                <a id="export_button" class="waves-effect waves-light btn teal" ><i class="material-icons right">file_download</i>Exportar</a>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- NOTE: CONTENIDO -->
      <div class="col s12 m12 l8 center-align">
        <h4>Historial Alimento</h4>
            <div id="tabla-historialArticulos">
              <!-- NOTE: AQUI VA LA TABLA -->
              <table class="centered responsive-table highlight">
                <thead>
                  <tr>
                    <th>Motivo</th>
                    <th>Fecha</th>
                    <th>Etiqueta</th>
                    <th>Tamaño</th>
                    <th>Alimento restante</th>
                    <th></th>
                  </tr>
                </thead>
                <tbody>
                  @foreach($foodHistory as $food)
                    <tr>
                      <td>{{$food->motivo}}</td>
                      <td>{{$food->fecha}}</td>
                      <td>{{$food->etiqueta}}</td>
                      <td>{{$food->tamano}}</td>
                      <td>{{$food->alimentoRestante}}</td>
                      <td><a class="waves-effect waves-light"><i  id="INFO-{{$food->id}}" class="material-icons FOOD">info_outline</i></td>
                    </tr>
                    <input type="hidden" value="{{$food->tipo}}" id='history-tipo-{{$food->id}}'>
                    <input type="hidden" value="{{$food->tipoAlimentoDescripcion}}" id='history-tipoAlimentoDescripcion-{{$food->id}}'>
                    <input type="hidden" value="{{$food->motivo}}" id='history-motivo-{{$food->id}}'>
                    <input type="hidden" value="{{$food->motivoDescripcion}}" id='history-motivoDescripcion-{{$food->id}}'>
                    <input type="hidden" value="{{$food->direccion}}" id='history-direccion-{{$food->id}}'>
                    <input type="hidden" value="{{$food->id}}" id='history-id-{{$food->id}}'>
                    <input type="hidden" value="{{$food->fecha}}" id='history-fecha-{{$food->id}}'>
                    <input type="hidden" value="{{$food->alimentoRestante}}" id='history-alimentoRestante-{{$food->id}}'>
                    <input type="hidden" value="{{$food->tamano}}" id='history-tamano-{{$food->id}}'>
                    <input type="hidden" value="{{$food->etiqueta}}" id='history-etiqueta-{{$food->id}}'>
                  @endforeach
                </tbody>
              </table>
            </div>
        </div>
      </div>
    </div>
  </div>

@endsection

@section('modals')
  <div id="test" class="modal modal-fixed-footer">
    <div class="modal-content center-align">

    </div>
    <div class="modal-footer">

    </div>
  </div>
@endsection

@section('scripts')
  <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script src="//cdn.rawgit.com/rainabba/jquery-table2excel/1.1.0/dist/jquery.table2excel.min.js"></script>

  <script type="text/javascript" id="scripts-gastos">
    $(document).ready(function() {
      // NOTE: Validar que fecha 1 sea menor a fecha 2
      $( "#form-fechas" ).submit(function( event ) {
        alert($('#fecha1').datepicker('getDate'));
        if ($('#fecha1').datepicker('getDate') <= $('#fecha2').datepicker('getDate')) {
          return;
        }
        M.toast({html: 'Seleccione un periodo valido',classes:'red darken-3'});
        event.preventDefault();
      });

      //  NOTE: aqui se inicializan los picker 1 y 2
      var hoy = new Date();
      dia = hoy.getDate();
      mes = hoy.getMonth()-1;
      ano = hoy.getFullYear();

      @if(!empty($f1))
        //  NOTE: fechas solicitadas por el usuario
        dia1 = {{ substr($f1,0,2) }};
        mes1 = {{ substr($f1,3,2) }};
        ano1 = {{ substr($f1,6,4) }};
        dia2 = {{ substr($f2,0,2) }};
        mes2 = {{ substr($f2,3,2) }};
        ano2 = {{ substr($f2,6,4) }};
        $('#fecha1').datepicker({
          format: 'dd/mm/yyyy',
          setDefaultDate: true,
          defaultDate: new Date(ano1,mes1-1,dia1),
          minDate: new Date(2020,03,23),
          maxDate: hoy,
          i18n: {
            months: ['Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'],
            monthsShort: ['Ene','Feb','Mar','Abr','May','Jun','Jul','Ago','Sep','Oct','Nov','Dic'],
            weekdays: ['Domingo','Lunes','Martes','Miércoles','Jueves','Viernes','Sábado'],
            weekdaysShort: ['Dom','Lun','Mar','Mier','Jue','Vier','Sab'],
            weekdaysAbbrev: ['D','L','M','M','J','V','S']
          },

        });

        $('#fecha2').datepicker({
          format: 'dd/mm/yyyy',
          setDefaultDate: true,
          defaultDate: new Date(ano2,mes2-1,dia2),
          minDate: new Date(2020,03,23),
          maxDate: hoy,
          i18n: {
            months: ['Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'],
            monthsShort: ['Ene','Feb','Mar','Abr','May','Jun','Jul','Ago','Sep','Oct','Nov','Dic'],
            weekdays: ['Domingo','Lunes','Martes','Miércoles','Jueves','Viernes','Sábado'],
            weekdaysShort: ['Dom','Lun','Mar','Mier','Jue','Vier','Sab'],
            weekdaysAbbrev: ['D','L','M','M','J','V','S']
          },

        });
      @else
        //  NOTE: fecha de hoy y de hace un mes
        $('#fecha1').datepicker({
          format: 'dd/mm/yyyy',
          setDefaultDate: true,
          defaultDate: new Date(ano,mes,dia),
          minDate: new Date(2020,03,23),
          maxDate: hoy,
          i18n: {
            months: ['Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'],
            monthsShort: ['Ene','Feb','Mar','Abr','May','Jun','Jul','Ago','Sep','Oct','Nov','Dic'],
            weekdays: ['Domingo','Lunes','Martes','Miércoles','Jueves','Viernes','Sábado'],
            weekdaysShort: ['Dom','Lun','Mar','Mier','Jue','Vier','Sab'],
            weekdaysAbbrev: ['D','L','M','M','J','V','S']
          },

        });

        $('#fecha2').datepicker({
          format: 'dd/mm/yyyy',
          setDefaultDate: true,
          defaultDate: hoy,
          minDate: new Date(2020,03,23),
          maxDate: hoy,
          i18n: {
            months: ['Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'],
            monthsShort: ['Ene','Feb','Mar','Abr','May','Jun','Jul','Ago','Sep','Oct','Nov','Dic'],
            weekdays: ['Domingo','Lunes','Martes','Miércoles','Jueves','Viernes','Sábado'],
            weekdaysShort: ['Dom','Lun','Mar','Mier','Jue','Vier','Sab'],
            weekdaysAbbrev: ['D','L','M','M','J','V','S']
          },

        });
      @endif

       @if(session('error'))
          red_toast("{{session('error')}}");
       @endif

    });

    document.addEventListener('DOMContentLoaded', function() {
      var elems = document.querySelectorAll('.dropdown-trigger');
      var options =
        {
          constrainWidth:false,
          alignment:'right'
        };
      var instances = M.Dropdown.init(elems, options);
    });

    //  NOTE: modals con la informacion
    $('.FOOD').click(function(){
      $id = $(this).attr("id");
      $id = $id.substr(5,$id.lenght);
      $tipo = $('#history-tipo-'+$id).attr("value");
      $tipoAlimentoDescripcion = $('#history-tipoAlimentoDescripcion-'+$id).attr("value");
      $fecha = $('#history-fecha-'+$id).attr("value");
      $motivo = $('#history-motivo-'+$id).attr("value");
      $motivoDescripcion = $('#history-motivoDescripcion-'+$id).attr("value");
      $direccion = $('#history-direccion-'+$id).attr("value");
      if($direccion == 1)
      {
        $direccion="Entrada";
      }
      else {
        $direccion = "Salida";
      }
      $alimentoRestante = $('#history-alimentoRestante-'+$id).attr("value");
      $tamano = $('#history-tamano-'+$id).attr("value");
      $etiqueta = $('#history-etiqueta-'+$id).attr("value");
      historyFoodInfo($etiqueta, $tamano, $alimentoRestante, $direccion,$motivoDescripcion, $motivo, $fecha, $tipoAlimentoDescripcion, $tipo, $id);
    });

    function historyFoodInfo($etiqueta, $tamano, $alimentoRestante, $direccion,$motivoDescripcion, $motivo, $fecha, $tipoAlimentoDescripcion, $tipo, $id){
      $('.modal-content').empty();
      $('.modal-content').append(
        "<h3>INFO  <i class='material-icons small teal-text'>info_outline</i></h3>"+
        "<div class='divider'></div>"+
        "<table class='striped'>"+
          "<tbody>"+
          "<tr>"+
            "<th class='center-align'>Motivo:</th>"+
            "<td>"+$motivo+"</td>"+
          "</tr>"+
          "<tr>"+
            "<th class='center-align'>Descripción:</th>"+
            "<td>"+$motivoDescripcion+"</td>"+
          "</tr>"+
          "<tr>"+
            "<th class='center-align'>Fecha:</th>"+
            "<td>"+$fecha+"</td>"+
          "</tr>"+
          "<tr>"+
            "<th class='center-align'>Etiqueta:</th>"+
            "<td>"+$etiqueta+"</td>"+
          "</tr>"+
          "<tr>"+
            "<th class='center-align'>Tamaño:</th>"+
            "<td>"+$tamano+"</td>"+
          "</tr>"+
          "<tr>"+
            "<th class='center-align'>Alimento restante:</th>"+
            "<td>"+$alimentoRestante+"</td>"+
          "</tr>"+
          "<tr>"+
            "<th class='center-align'>Tipo alimento:</th>"+
            "<td>"+$tipo+"</td>"+
          "</tr>"+
          "<tr>"+
            "<th class='center-align'>Descripción:</th>"+
            "<td>"+$tipoAlimentoDescripcion+"</td>"+
          "</tr>"+
          "<tr>"+
            "<th class='center-align'>Dirección:</th>"+
            "<td>"+$direccion+"</td>"+
          "</tr>"+
          "</tbody>"+
        "</table>"
      );
      $('.modal-footer').empty();
      $('.modal-footer').append(
        "<a class='waves-effect waves-light btn-flat grey-text text-darken-3 modal-close'>Cerrar</a>"
      );
      $('#test').modal('open');
    }


    $("#export_button").click(function(){
      $("#tabla-historialArticulos").table2excel({
          // exclude CSS class
          exclude:".noexport",
          name:"Worksheet Name",
          filename:"Historial_Inventario_Alimento"//do not include extension
      });
    });

  </script>

@endsection
