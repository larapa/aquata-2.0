<?php // TODO: Falta definir las rutas y modals?>
@extends('Layouts.Menu')
@section('title','Inventario')
@section('content')
    <ul class="tabs tabs-fixed-width">

      <li class="tab col s4 m4 l4"><a href="#swipe-food">Alimento</a></li>
      <li class="tab col s4 m4 l4"><a href="#swipe-products">Productos</a></li>
      <li class="tab col s4 m4 l4"><a href="#swipe-articles">Articulos</a></li>

    </ul>

    <div id="swipe-food">
      <!-- Aqui es el panel de control -->
      <div class="row">
        <div class="col s12 m4 l4">
          <div class="card white">
            <div class="card-content blue-text text-darken-2">
              <span class="card-title">Panel de control</span>
              <div class="row">
                <div class="col s8 m8 l8">
                  ¿Mostrar costales vacios?
                </div>
                <div class="col s4 m4 l4">
                  <div class="switch right-align">
                   <label>
                     <input type="checkbox" id="bags-switch">
                     <span class="lever" id="bags-lever"></span>
                   </label>
                 </div>
                </div>
              </div>
              <ul class="collapsible popout">
                <li>
                  <div class="collapsible-header"><i class="material-icons">restaurant</i>Tipos de alimento</div>
                  <div class="collapsible-body">
                    <div class="row">
                      <div class="col s8 m8 l8">
                        ¿Mostrar tipos inactivos?
                      </div>
                      <div class="col s4 m4 l4">
                        <div class="switch right-align">
                         <label>
                           <input type="checkbox" id="types-switch" >
                           <span class="lever" id="types-lever"></span>
                         </label>
                       </div>
                      </div>
                    </div>
                    <!-- ver los tipos de alimento -->
                      <ul class="collection" id="types-collection">
                        @foreach($types as $type)
                          @if($type->T == '1')
                            <li class="collection-item avatar">
                              <i class="material-icons circle blue">restaurant</i>
                              <span class="title">{{$type->nombre}}</span>
                              <p class="grey-text truncate">{{$type->descripcion}}</p>
                              <a class="secondary-content dropdown-trigger btn-flat teal-text" data-target="TYPE-{{$type->id}}"><i class="material-icons right">arrow_drop_down</i></a>
                            </li>
                          @else
                            <li class="collection-item avatar useless-type">
                              <i class="material-icons circle indigo lighten-4">restaurant</i>
                              <span class="title">{{$type->nombre}}</span>
                              <p class="grey-text truncate">{{$type->descripcion}}</p>
                              <a class="secondary-content dropdown-trigger btn-flat teal-text" data-target="TYPE-{{$type->id}}"><i class="material-icons right">arrow_drop_down</i></a>
                            </li>
                          @endif
                        @endforeach
                      </ul>
                    <div class="row">
                      <div class="col s12 m12 l12 center-align">
                        <br>
                        <a class="waves-effect waves-light btn-small teal"><i class="material-icons right">add</i>Agregar tipo</a>
                      </div>
                    </div>
                  </div>
                </li>
                <li>
                  <div class="collapsible-header"><i class="material-icons">archive</i>Motivos de uso</div>
                  <div class="collapsible-body">
                    <div class="row">
                      <div class="col s8 m8 l8">
                        ¿Mostrar motivos inactivos?
                      </div>
                      <div class="col s4 m4 l4">
                        <div class="switch right-align">
                         <label>
                           <input type="checkbox" id="motives-switch">
                           <span class="lever" id="motives-lever"></span>
                         </label>
                       </div>
                      </div>
                    </div>
                    <!-- ver los tipos de alimento -->
                      <ul class="collection">
                        @foreach($motives as $motive)
                          @if($motive->T == '1')
                            <li class="collection-item avatar">
                              <i class="material-icons circle blue">archive</i>
                              <span class="title">{{$motive->nombre}}</span>
                              <p class="grey-text truncate">{{$motive->descripcion}}</p><br>
                              <a class="secondary-content dropdown-trigger btn-flat teal-text" data-target="MOTIVE-{{$motive->id}}"><i class="material-icons right">arrow_drop_down</i></a>
                            </li>
                          @else
                            <li class="collection-item avatar useless-motive">
                              <i class="material-icons circle indigo lighten-4">archive</i>
                              <span class="title">{{$motive->nombre}}</span>
                              <p class="grey-text truncate">{{$motive->descripcion}}</p><br>
                              <a class="secondary-content dropdown-trigger btn-flat teal-text" data-target="MOTIVE-{{$motive->id}}"><i class="material-icons right">arrow_drop_down</i></a>
                            </li>
                          @endif
                        @endforeach
                      </ul>
                    <div class="row">
                      <div class="col s12 m12 l12 center-align">
                        <br>
                        <a class="waves-effect waves-light btn-small teal white-text"><i class="material-icons right">add</i>Agregar motivo</a>
                      </div>
                    </div>
                  </div>
                </li>
              </ul>
            </div>
            <div class="card-action center-align">
              <a class="waves-effect waves-light btn-small teal white-text"><i class="material-icons right">add</i>Agregar costal</a>
            </div>
          </div>
        </div>
        <div class="col s12 m8 l8 center-align">
          <h4>Costales</h4>
          <table class=" highlight centered responsive-table">
            <thead>
              <tr>
                <th>Etiqueta</th>
                <th>Total</th>
                <th>Restante</th>
                <th>Tipo</th>
                <th class="hide-on-med-and-down"> </th>
                <th class="hide-on-med-and-down"> </th>
                <th class="hide-on-large-only">Editar</th>
                <th class="hide-on-large-only">Eliminar</th>
              </tr>
            </thead>
            <tbody id="bags-table">
              @foreach($bags as $bag)
                @if($bag->T == '1')
                  <tr id="BAG-{{$bag->id}}" class="bag">
                    <td id="TAG-BAG-{{$bag->id}}">{{$bag->etiqueta}}</td>
                    <td id="SIZE-BAG-{{$bag->id}}">{{$bag->tamano}}</td>
                    <td id="REMAINING-BAG-{{$bag->id}}">{{$bag->alimentoRestante}}</td>
                    <td id="TYPE-BAG-{{$bag->id}}">{{$bag->nombre}}</td>
                    <td id="EDIT-BAG-{{$bag->id}}"><i class="material-icons">edit</i></td>
                    <td id="DELETE-BAG-{{$bag->id}}"><i class="material-icons">delete</i></td>
                  </tr>
                @else
                  <tr id="BAG-{{$bag->id}}" class="bag grey-text text-lighten-2 useless-bag">
                    <td id="TAG-BAG-{{$bag->id}}">{{$bag->etiqueta}}</td>
                    <td id="SIZE-BAG-{{$bag->id}}">{{$bag->tamano}}</td>
                    <td id="REMAINING-BAG-{{$bag->id}}">{{$bag->alimentoRestante}}</td>
                    <td id="TYPE-BAG-{{$bag->id}}">{{$bag->nombre}}</td>
                    <td id="EDIT-BAG-{{$bag->id}}"><i class="material-icons">edit</i></td>
                    <td id="DELETE-BAG-{{$bag->id}}"><i class="material-icons">delete</i></td>
                  </tr>
                @endif
              @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>

    <div id="swipe-products">

    </div>

    <div id="swipe-articles">
      <!-- Aqui es el panel de control -->
      <div class="row">
        <div class="col s12 m4 l4">
          <div class="card white">
            <div class="card-content blue-text text-darken-2">
              <span class="card-title">Panel de control</span>
              <div class="row">
                <div class="col s8 m8 l8">
                  ¿Mostrar los articulos sin existencia?
                </div>
                <div class="col s4 m4 l4">
                  <div class="switch right-align">
                   <label>
                     <input type="checkbox" id="bags-switch">
                     <span class="lever" id="bags-lever"></span>
                   </label>
                 </div>
                </div>
              </div>
              <ul class="collapsible popout">
                <li>
                  <div class="collapsible-header"><i class="material-icons">layers</i>Artículos</div>
                  <div class="collapsible-body">
                    <div class="row">
                      <div class="col s8 m8 l8">
                        ¿Mostrar artículos inactivos?
                      </div>
                      <div class="col s4 m4 l4">
                        <div class="switch right-align">
                         <label>
                           <input type="checkbox" id="articles-switch" >
                           <span class="lever" id="articles-lever"></span>
                         </label>
                       </div>
                      </div>
                    </div>
                    <!-- ver las categorias de articulos -->
                      <ul class="collection" id="articles-collection">
                        @foreach($articles as $article)
                          @if($article->T == '1')
                            <li class="collection-item avatar">
                              <i class="material-icons circle blue">layers</i>
                              <span class="title">{{$article->nombre}}</span>
                              <p class="grey-text truncate">{{$article->descripcion}}</p>
                              <a class="secondary-content dropdown-trigger btn-flat teal-text" data-target="ARTICLE-{{$article->id}}"><i class="material-icons right">arrow_drop_down</i></a>
                            </li>
                          @else
                            <li class="collection-item avatar useless-article">
                              <i class="material-icons circle indigo lighten-4">layers</i>
                              <span class="title">{{$article->nombre}}</span>
                              <p class="grey-text truncate">{{$article->descripcion}}</p>
                              <a class="secondary-content dropdown-trigger btn-flat teal-text" data-target="ARTICLE-{{$article->id}}"><i class="material-icons right">arrow_drop_down</i></a>
                            </li>
                          @endif
                        @endforeach
                      </ul>
                    <div class="row">
                      <div class="col s12 m12 l12 center-align">
                        <br>
                        <a class="waves-effect waves-light btn-small teal"><i class="material-icons right">add</i>Agregar tipo</a>
                      </div>
                    </div>
                  </div>
                </li>
                <li>
                  <div class="collapsible-header"><i class="material-icons">linear_scale</i>Unidades</div>
                  <div class="collapsible-body">
                    <div class="row">
                      <div class="col s8 m8 l8">
                        ¿Mostrar unidades inactivas?
                      </div>
                      <div class="col s4 m4 l4">
                        <div class="switch right-align">
                         <label>
                           <input type="checkbox" id="units-switch" >
                           <span class="lever" id="units-lever"></span>
                         </label>
                       </div>
                      </div>
                    </div>
                    <!-- ver las unidades -->
                      <ul class="collection" id="units-collection">
                        @foreach($units as $unit)
                          @if($unit->T == '1')
                            <li class="collection-item avatar">
                              <i class="material-icons circle blue">linear_scale</i>
                              <span class="title">{{$unit->nombre}}</span>
                              <p class="grey-text truncate">unidad</p>
                              <a class="secondary-content dropdown-trigger btn-flat teal-text" data-target="UNIT-{{$unit->id}}"><i class="material-icons right">arrow_drop_down</i></a>
                            </li>
                          @else
                            <li class="collection-item avatar useless-unit">
                              <i class="material-icons circle indigo lighten-4">linear_scale</i>
                              <span class="title">{{$unit->nombre}}</span>
                              <p class="grey-text truncate">unidad</p>
                              <a class="secondary-content dropdown-trigger btn-flat teal-text" data-target="UNIT-{{$unit->id}}"><i class="material-icons right">arrow_drop_down</i></a>
                            </li>
                          @endif
                        @endforeach
                      </ul>
                    <div class="row">
                      <div class="col s12 m12 l12 center-align">
                        <br>
                        <a class="waves-effect waves-light btn-small teal"><i class="material-icons right">add</i>Agregar tipo</a>
                      </div>
                    </div>
                  </div>
                </li>
                <li>
                  <div class="collapsible-header"><i class="material-icons">local_offer</i>Categorias</div>
                  <div class="collapsible-body">
                    <div class="row">
                      <div class="col s8 m8 l8">
                        ¿Mostrar categorias inactivas?
                      </div>
                      <div class="col s4 m4 l4">
                        <div class="switch right-align">
                         <label>
                           <input type="checkbox" id="articleCategories-switch" >
                           <span class="lever" id="articleCategories-lever"></span>
                         </label>
                       </div>
                      </div>
                    </div>
                    <!-- ver las categorias de articulos -->
                      <ul class="collection" id="articleCategories-collection">
                        @foreach($articleCategories as $category)
                          @if($category->T == '1')
                            <li class="collection-item avatar">
                              <i class="material-icons circle blue">local_offer</i>
                              <span class="title">{{$category->nombre}}</span>
                              <p class="grey-text truncate">{{$category->descripcion}}</p>
                              <a class="secondary-content dropdown-trigger btn-flat teal-text" data-target="ARTICLECATEGORY-{{$category->id}}"><i class="material-icons right">arrow_drop_down</i></a>
                            </li>
                          @else
                            <li class="collection-item avatar useless-articleCategory">
                              <i class="material-icons circle indigo lighten-4">local_offer</i>
                              <span class="title">{{$category->nombre}}</span>
                              <p class="grey-text truncate">{{$category->descripcion}}</p>
                              <a class="secondary-content dropdown-trigger btn-flat teal-text" data-target="ARTICLECATEGORY-{{$category->id}}"><i class="material-icons right">arrow_drop_down</i></a>
                            </li>
                          @endif
                        @endforeach
                      </ul>
                    <div class="row">
                      <div class="col s12 m12 l12 center-align">
                        <br>
                        <a class="waves-effect waves-light btn-small teal"><i class="material-icons right">add</i>Agregar tipo</a>
                      </div>
                    </div>
                  </div>
                </li>
              </ul>
            </div>
            <div class="card-action center-align">
              <a class="waves-effect waves-light btn-small teal white-text"><i class="material-icons right">add</i>Agregar costal</a>
            </div>
          </div>
        </div>
        <div class="col s12 m8 l8 center-align">
          <h4>Articulos</h4>
          <table class=" highlight centered responsive-table">
            <thead>
              <tr>
                <th>Articulo</th>
                <th>Existencia</th>
                <th>Tamaño</th>
                <th>Categoria</th>
                <th class="hide-on-med-and-down"> </th>
                <th class="hide-on-large-only">Editar</th>

              </tr>
            </thead>
            <tbody id="articlesInventory-table">
              @foreach($articlesInventory as $article)
                @if($article->elementos != '0')
                  <tr id="INVENTORYARTICLE-{{$article->id}}" class="bag">
                    <td id="ARTICLE-INVENTORYARTICLE-{{$article->id}}">{{$article->articulo}}</td>
                    <td id="EXISTENCE-INVENTORYARTICLE-{{$article->id}}">{{$article->elementos}}</td>
                    <td id="SIZE-INVENTORYARTICLE-{{$article->id}}">{{$article->unidades}} {{$article->unidad}}</td>
                    <td id="CATEGORY-INVENTORYARTICLE-{{$article->id}}">{{$article->categoria}}</td>
                    <td id="EDIT-INVENTORYARTICLE-{{$article->id}}"><i class="material-icons">import_export</i></td>
                  </tr>
                @else
                  <tr id="ARTICLESINVENTORY-{{$article->id}}" class="bag grey-text text-lighten-2 useless-inventoyArticle">

                  </tr>
                @endif
              @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>

    <div class="pages center-align">
      {{ $bags->links() }}
    </div>


@endsection

@section('modals')
  <div id="test" class="modal modal-fixed-footer">
    <div class="modal-content center-align">

    </div>
    <div class="modal-footer">

    </div>
  </div>
<!-- Definir las rutas -->
  @foreach($types as $type)
    @if($type->T == '1')
      <ul id='TYPE-{{$type->id}}' class='dropdown-content'>
          <li><a href="#!" class="black-text"><i class="material-icons left blue-text">error_outline</i>INFO</a></li>
          <li><a href="#!" class="black-text"><i class="material-icons left blue-text">edit</i>Editar</a></li>
          <li><a href="{{ route('Type.Delete', $type->id ) }}" class="black-text"><i class="material-icons left blue-text">delete</i>Eliminar</a></li>
      </ul>
    @else
      <ul id='TYPE-{{$type->id}}' class='dropdown-content'>
        <li><a href="{{ route('Type.Active', $type->id ) }}" class="black-text"><i class="material-icons left blue-text">cached</i>Activar</a></li>
      </ul>
    @endif
  @endforeach
  @foreach($articleCategories as $category)
    @if($category->T == '1')
      <ul id='ARTICLECATEGORY-{{$category->id}}' class='dropdown-content'>
          <li><a href="#!" class="black-text"><i class="material-icons left blue-text">error_outline</i>INFO</a></li>
          <li><a href="#!" class="black-text"><i class="material-icons left blue-text">edit</i>Editar</a></li>
          <li><a href="{{ route('ArticleCategory.Delete', $category->id ) }}" class="black-text"><i class="material-icons left blue-text">delete</i>Eliminar</a></li>
      </ul>
    @else
      <ul id='ARTICLECATEGORY-{{$category->id}}' class='dropdown-content'>
        <li><a href="{{ route('ArticleCategory.Active', $category->id ) }}" class="black-text"><i class="material-icons left blue-text">cached</i>Activar</a></li>
      </ul>
    @endif
  @endforeach
  @foreach($articles as $article)
    @if($article->T == '1')
      <ul id='ARTICLE-{{$article->id}}' class='dropdown-content'>
          <li><a href="#!" class="black-text"><i class="material-icons left blue-text">error_outline</i>INFO</a></li>
          <li><a href="#!" class="black-text"><i class="material-icons left blue-text">edit</i>Editar</a></li>
          <li><a href="" class="black-text"><i class="material-icons left blue-text">delete</i>Eliminar</a></li>
      </ul>
    @else
      <ul id='ARTICLE-{{$article->id}}' class='dropdown-content'>
        <li><a href="" class="black-text"><i class="material-icons left blue-text">cached</i>Activar</a></li>
      </ul>
    @endif
  @endforeach
  @foreach($motives as $motive)
    @if($motive->T == '1')
    <ul id='MOTIVE-{{$motive->id}}' class='dropdown-content'>
        <li><a href="#!" class="black-text"><i class="material-icons left blue-text">error_outline</i>INFO</a></li>
        <li><a href="#!" class="black-text"><i class="material-icons left blue-text">edit</i>Editar</a></li>
        <li><a href="{{ route('Motive.Delete', $motive->id ) }}" class="black-text"><i class="material-icons left blue-text">delete</i>Eliminar</a></li>
    </ul>
    @else
      <ul id='MOTIVE-{{$motive->id}}' class='dropdown-content'>
        <li><a href="{{ route('Motive.Active', $motive->id ) }}" class="black-text"><i class="material-icons left blue-text">cached</i>Activar</a></li>
      </ul>
    @endif
  @endforeach
  @foreach($units as $unit)
    @if($unit->T == '1')
      <ul id='UNIT-{{$unit->id}}' class='dropdown-content'>
          <li><a href="#!" class="black-text"><i class="material-icons left blue-text">edit</i>Editar</a></li>
          <li><a href="{{ route('Unit.Delete', $unit->id ) }}" class="black-text"><i class="material-icons left blue-text">delete</i>Eliminar</a></li>
      </ul>
    @else
      <ul id='UNIT-{{$unit->id}}' class='dropdown-content'>
        <li><a href="{{ route('Unit.Active', $unit->id ) }}" class="black-text"><i class="material-icons left blue-text">cached</i>Activar</a></li>
      </ul>
    @endif
  @endforeach
@endsection

@section('scripts')
  <script type="text/javascript" id="scripts-alimento">
    $(document).ready(function() {
      //ocultar los inútiles
      $('.useless-type').hide();
      $('.useless-motive').hide();
      $('.useless-bag').hide();
      $('.useless-unit').hide();
      $('.useless-articleCategory').hide();

      //los métodos para mostrar o no a los inútiles
      $('#types-lever').click(function(){
         if ($("#types-switch").prop("checked") == true) {
           $('.useless-type').hide();
         }
         else{
           $('.useless-type').show();
         }
       });

       $('#motives-lever').click(function(){
          if ($("#motives-switch").prop("checked") == true) {
            $('.useless-motive').hide();
          }
          else{
            $('.useless-motive').show();
          }
        });

        $('#units-lever').click(function(){
           if ($("#units-switch").prop("checked") == true) {
             $('.useless-unit').hide();
           }
           else{
             $('.useless-unit').show();
           }
         });

         $('#articleCategories-lever').click(function(){
            if ($("#articleCategories-switch").prop("checked") == true) {
              $('.useless-articleCategory').hide();
            }
            else{
              $('.useless-articleCategory').show();
            }
          });

      $('#bags-lever').click(function(){
           if ($("#bags-switch").prop("checked") == true) {
             $('.useless-bag').hide();
           }
           else{
            $('.useless-bag').show();
           }
       });

       //poner la palanca de los costales en ON
       $('#bags-lever').click();
     });


    function bagInfo($id,$tag,$size,$remaining,$type,$t){
      $id = $id.substring(4,$id.lenght);
      $('.modal-content').empty();
      $('.modal-content').append(
        "<h3>INFO  <i class='material-icons small'>info_outline</i></h3>"+
        "<div class='row'>"+
          "<div class='col s4 m4 l4'>"+
            "<p class='truncate blue-text text-darken-3'>Etiqueta</p>"+
          "</div>"+
          "<div class='col m8 l8 s8'>"+
            "<p>"+$tag+"</p>"+
          "</div>"+
        "</div>"+
        "<div class='row'>"+
          "<div class='col s4 m4 l4'>"+
            "<p class='truncate blue-text text-darken-3'>Total</p>"+
          "</div>"+
          "<div class='col m8 l8 s8'>"+
            "<p>"+$size+"</p>"+
          "</div>"+
        "</div>"+
        "<div class='row'>"+
          "<div class='col s4 m4 l4'>"+
            "<p class='truncate blue-text text-darken-3'>Restante</p>"+
          "</div>"+
          "<div class='col m8 l8 s8'>"+
            "<p>"+$remaining+"</p>"+
          "</div>"+
        "</div>"+
        "<div class='row'>"+
          "<div class='col s4 m4 l4'>"+
            "<p class='truncate blue-text text-darken-3'>Tipo de alimento</p>"+
          "</div>"+
          "<div class='col m8 l8 s8'>"+
            "<p>"+$type+"</p>"+
          "</div>"+
        "</div>"
      );
      if($t.substring(0,7)=='useless')
        $('.modal-content').append("<p class='grey-text'>Este costal ya no está disponible</p>");
        $('.modal-footer').empty();
      if($t.substring(0,7)!='useless'){
        $('.modal-footer').append(
          "<a class='waves-effect waves-light btn-flat grey-text text-darken-3 modal-close'>Cancelar</a>"+
          "<a class='waves-effect waves-light btn-flat red-text text-darken-4' onclick=''>Eliminar</a>"+
          "<a class='waves-effect waves-light btn-flat amber-text text-accent-3' onclick=''>Editar</a>"
        );
      }
      else {
        $('.modal-footer').append(
          "<a class='waves-effect waves-light btn-flat grey-text text-darken-3 modal-close'>Cancelar</a>"
        );
      }
      $('#test').modal('open');
    }

    function typeInfo($id,$nombre,$description,$t){
      $id = $id.substring(5,$id.lenght);
      $('.modal-content').empty();
      $('.modal-content').append(
        "<h3>INFO  <i class='material-icons small'>info_outline</i></h3>"+
        "<div class='row'>"+
          "<div class='col s4 m4 l4'>"+
            "<p class='truncate blue-text text-darken-3'>Nombre</p>"+
          "</div>"+
          "<div class='col m8 l8 s8'>"+
            "<p>"+$nombre+"</p>"+
          "</div>"+
        "</div>"+
        "<div class='row'>"+
          "<div class='col s4 m4 l4'>"+
            "<p class='truncate blue-text text-darken-3'>Descripción</p>"+
          "</div>"+
          "<div class='col m8 l8 s8'>"+
            "<p>"+$description+"</p>"+
          "</div>"+
        "</div>"
      );
      if($t.substring(0,7)=='useless')
        $('.modal-content').append("<p class='grey-text'>Este tipo de alimento no está disponible</p>");
        $('.modal-footer').empty();
      if($t.substring(0,7)!='useless'){

        $('.modal-footer').append(route);
      }
      else {
        $('.modal-footer').append(
          "<a class='waves-effect waves-light btn-flat grey-text text-darken-3 modal-close'>Cancelar</a>"
        );
      }
      $('#test').modal('open');
    }

    function motiveInfo($id,$nombre,$description,$t){
      $id = $id.substring(7,$id.lenght);
      $('.modal-content').empty();
      $('.modal-content').append(
        "<h3>INFO  <i class='material-icons small'>info_outline</i></h3>"+
        "<div class='row'>"+
          "<div class='col s4 m4 l4'>"+
            "<p class='truncate blue-text text-darken-3'>Nombre</p>"+
          "</div>"+
          "<div class='col m8 l8 s8'>"+
            "<p>"+$nombre+"</p>"+
          "</div>"+
        "</div>"+
        "<div class='row'>"+
          "<div class='col s4 m4 l4'>"+
            "<p class='truncate blue-text text-darken-3'>Descripción</p>"+
          "</div>"+
          "<div class='col m8 l8 s8'>"+
            "<p>"+$description+"</p>"+
          "</div>"+
        "</div>"
      );
      if($t.substring(0,7)=='useless')
        $('.modal-content').append("<p class='grey-text'>Este tipo de alimento no está disponible</p>");
        $('.modal-footer').empty();
      if($t.substring(0,7)!='useless'){
        $('.modal-footer').append(
          "<a class='waves-effect waves-light btn-flat grey-text text-darken-3 modal-close'>Cancelar</a>"+
          "<a class='waves-effect waves-light btn-flat red-text text-darken-4' onclick=''>Eliminar</a>"+
          "<a class='waves-effect waves-light btn-flat amber-text text-accent-3' onclick=''>Editar</a>"
        );
      }
      else {
        $('.modal-footer').append(
          "<a class='waves-effect waves-light btn-flat grey-text text-darken-3 modal-close'>Cancelar</a>"
        );
      }
      $('#test').modal('open');
    }

    //no tocar, es para que funcionen los dropdown
    document.addEventListener('DOMContentLoaded', function() {
      var elems = document.querySelectorAll('.dropdown-trigger');
      var options =
        {
          constrainWidth:false,
          alignment:'right'
        };
      var instances = M.Dropdown.init(elems, options);
    });

  </script>
@endsection
<?php // TODO: HACER LOGO DE mapache ?>
