@extends('Layouts.Menu')
@section('title','Añadir elementos')
@section('content')

  <form action="{{route('InventoryArticle.UpgradeValidation')}}" method="POST">
    <input type="hidden" name="id" value="{{$inventoryArticle->id}}">
    <input type="hidden" name="idInventario" value="{{$inventoryArticle->idInventario}}">
    <input type="hidden" name="idArticulo" value="{{$inventoryArticle->idArticulo}}">
    <input type="hidden" name="idUnidad" value="{{$info->idUnidad}}">
    <input type="hidden" name="idCategoria" value="{{$info->idCategoria}}">
    @csrf
    <h4 class="center-align">Añadir artículo: {{$info->articulo}}(s)</h4>

    <div class="row">
      <div class="container">
          <div class="row">
            <div class="col s12 m4 l4 center-align">
              <h4><i class="material-icons">help_outline</i> INFO</h4>

            </div>
            <div class="col s12 m8 l8">
              <p  ALIGN="justify">Al agregar elementos de un artículo es necesario que especifiques
                la cantidad a agregar y el <span class="blue-text">COSTO TOTAL</span> que pagaste por estos artículos.
                Este suceso será registrado en el <span class="blue-text">historial de artículos</span> y como un <span class="blue-text">gasto</span>, puedes
                administrar tus gastos en el apartado <span class="blue-text">Economía</span>. Añade una pequeña
                justificación por la que se realizó el gasto para poder identificarlo con mayor facilidad.
              </p>

            </div>
          </div>
          <div class="row">
            <div class="input-field col s8">
              <input disabled value="{{$info->articulo}} {{$info->cantidadUnidad}} {{$info->unidad}}" id="articulo" type="text">
              <label for="articulo">Nombre</label>
            </div>
            <div class="input-field col s4">
              <input disabled value="{{$inventory->cantidadElementos}}" id="cantidadElementos" type="text">
              <label for="cantidadElementos">Disponibles</label>
            </div>
          </div>
          <div class="row">
            <div class="input-field col s12">
              <input disabled value="{{$info->categoria}}" id="categoria" type="text">
              <label for="categoria">Categoria</label>
            </div>
          </div>
          <div class="row">
            <div class="input-field col s12 m6 l6" id='cantidad-div'>
                <input name="cantidad" id="cantidad" type="number" step="1" min="0" maxlength="11" class="validate" value="" required="" aria-required="true">
              <label for="cantidad">Cantidad a agregar</label>
              <span class="helper-text" id="helper-cantidad">Ingrese la cantidad de nuevos artículos</span>
            </div>

            <div class="input-field col s12 m6 l6" id='cantidad-div'>
                <input name="costo" id="costo" type="number" step="1" min="0" maxlength="11" class="validate" value="" required="" aria-required="true">
              <label for="costo">Costo total</label>
              <span class="helper-text" id="helper-cantidad">Ingrese el costo total de los nuevos artículos</span>
            </div>
          </div>
          <div class="row">
            <div class="input-field col s12">
              <input value="" id="motivo" name="motivo" type="text" data-length="20" maxlength="20" required="required" aria-required="true" class="validate">
              <label for="motivo">Ingresar motivo de añadido de articulos</label>
            </div>
          </div>
          <div class="row center-align">
            <button type="submit" class="waves-effect waves-light btn blue darken-3 center-align">Aceptar</button>
          </div>
      </div>
    </div>
  </form>
@endsection
@section('scripts')
  <script type="text/javascript">
    $(document).ready(function() {


        $('input').characterCounter();

    });

  </script>
@endsection
