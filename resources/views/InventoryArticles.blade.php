<?php // TODO: Falta definir las rutas y modals?>
@extends('Layouts.Menu')
@section('head','usuario')
@section('title','Inventario')
@section('content')

  <ul class="tabs tabs-fixed-width">

    <li class="tab col s4 m4 l4"><a target="_self" href="{{route('Inventory.Food')}}">Alimento</a></li>
    <li class="tab col s4 m4 l4"><a class="active"  href="#swipe-articles">Articulos</a></li>
    <li class="tab col s4 m4 l4"><a target="_self" href="{{route('Inventory.Products')}}">Productos</a></li>


  </ul>

    <div id="swipe-articles">
      <!-- Aqui es el panel de control -->
      <div class="row">
        <div class="col s12 m12 l4">
          <div class="card white">
            <div class="card-content blue-text text-darken-2">
              <span class="card-title">Panel de control</span>
              <div class="row">
                <div class="col s8 m8 l8">
                  ¿Mostrar los articulos sin existencia?
                </div>
                <div class="col s4 m4 l4">
                  <div class="switch right-align">
                   <label>
                     <input type="checkbox" id="inventoryArticles-switch">
                     <span class="lever" id="inventoryArticles-lever"></span>
                   </label>
                 </div>
                </div>
              </div>
              <ul class="collapsible popout">
                <li>
                  <div class="collapsible-header"><i class="material-icons">layers</i>Artículos</div>
                  <div class="collapsible-body">
                    <div class="row">
                      <div class="col s8 m8 l8">
                        ¿Mostrar artículos inactivos?
                      </div>
                      <div class="col s4 m4 l4">
                        <div class="switch right-align">
                         <label>
                           <input type="checkbox" id="articles-switch" >
                           <span class="lever" id="articles-lever"></span>
                         </label>
                       </div>
                      </div>
                    </div>
                    <!-- ver las categorias de articulos -->
                      <ul class="collection" id="articles-collection">
                        @foreach($articles as $article)
                          @if($article->T == '1')
                            <li class="collection-item avatar usefull-article">
                              <div class="row">
                                <div class="col s10">
                                  <i class="material-icons circle blue">layers</i>
                                  <span class="title">{{$article->nombre}}</span>
                                  <p class="grey-text truncate">{{$article->cantidadUnidad}} {{$article->unidad}}</p>
                                </div>
                                <div class="col s2">
                                  <a class="secondary-content dropdown-trigger btn-flat teal-text" data-target="ARTICLE-{{$article->id}}"><i class="material-icons right">arrow_drop_down</i></a>
                                </div>
                              </div>
                            </li>
                          @else
                            <li class="collection-item avatar useless-article">
                              <div class="row">
                                <div class="col s10">
                                  <i class="material-icons circle indigo lighten-4">layers</i>
                                  <span class="title">{{$article->nombre}}</span>
                                  <p class="grey-text truncate">{{$article->descripcion}}</p>
                                </div>
                                <div class="col s2">
                                  <a class="secondary-content dropdown-trigger btn-flat teal-text" data-target="ARTICLE-{{$article->id}}"><i class="material-icons right">arrow_drop_down</i></a>
                                </div>
                              </div>
                            </li>
                          @endif
                        @endforeach
                      </ul>
                    <div class="row">
                      <div class="col s12 m12 l12 center-align">
                        <br>
                        <a class="waves-effect waves-light btn-small teal"  href="{{ route('Article.Add')}}"><i class="material-icons right">add</i>Agregar articulo</a>
                      </div>
                    </div>
                  </div>
                </li>
                <li>
                  <div class="collapsible-header"><i class="material-icons">linear_scale</i>Unidades</div>
                  <div class="collapsible-body">
                    <div class="row">
                      <div class="col s8 m8 l8">
                        ¿Mostrar unidades inactivas?
                      </div>
                      <div class="col s4 m4 l4">
                        <div class="switch right-align">
                         <label>
                           <input type="checkbox" id="units-switch" >
                           <span class="lever" id="units-lever"></span>
                         </label>
                       </div>
                      </div>
                    </div>
                    <!-- ver las unidades -->
                      <ul class="collection" id="units-collection">
                        @foreach($units as $unit)
                          @if($unit->T == '1')
                            <li class="collection-item avatar usefull-unit">
                              <div class="row">
                                <div class="col s10">
                                  <i class="material-icons circle blue">linear_scale</i>
                                  <span class="title">{{$unit->nombre}}</span>
                                  <p class="grey-text truncate">Unidad</p>
                                </div>
                                <div class="col s2">
                                  <a class="secondary-content dropdown-trigger btn-flat teal-text" data-target="UNIT-{{$unit->id}}"><i class="material-icons right">arrow_drop_down</i></a>
                                </div>
                              </div>
                            </li>
                          @else
                            <li class="collection-item avatar useless-unit">
                              <div class="row">
                                <div class="col s10">
                                  <i class="material-icons circle indigo lighten-4">linear_scale</i>
                                  <span class="title">{{$unit->nombre}}</span>
                                  <p class="grey-text truncate">Unidad</p>
                                </div>
                                <div class="col s2">
                                  <a class="secondary-content dropdown-trigger btn-flat teal-text" data-target="UNIT-{{$unit->id}}"><i class="material-icons right">arrow_drop_down</i></a>
                                </div>
                              </div>
                            </li>
                          @endif
                        @endforeach
                      </ul>
                    <div class="row">
                      <div class="col s12 m12 l12 center-align">
                        <br>
                        <a class="waves-effect waves-light btn-small teal" href="{{ route('Unit.Add')}}"><i class="material-icons right">add</i>Agregar unidad</a>
                      </div>
                    </div>
                  </div>
                </li>
                <li>
                  <div class="collapsible-header"><i class="material-icons">local_offer</i>Categorias</div>
                  <div class="collapsible-body">
                    <div class="row">
                      <div class="col s8 m8 l8">
                        ¿Mostrar categorias inactivas?
                      </div>
                      <div class="col s4 m4 l4">
                        <div class="switch right-align">
                         <label>
                           <input type="checkbox" id="articleCategories-switch" >
                           <span class="lever" id="articleCategories-lever"></span>
                         </label>
                       </div>
                      </div>
                    </div>
                    <!-- ver las categorias de articulos -->
                      <ul class="collection" id="articleCategories-collection">
                        @foreach($articleCategories as $category)
                          @if($category->T == '1')
                            <li class="collection-item avatar usefull-articleCategory">
                              <div class="row">
                                <div class="col s10">
                                  <i class="material-icons circle blue">local_offer</i>
                                  <span class="title">{{$category->nombre}}</span>
                                  <p class="grey-text truncate">{{$category->descripcion}}</p>
                                </div>
                                <div class="col s2">
                                  <a class="secondary-content dropdown-trigger btn-flat teal-text" data-target="ARTICLECATEGORY-{{$category->id}}"><i class="material-icons right">arrow_drop_down</i></a>
                                </div>
                              </div>
                            </li>
                          @else
                            <li class="collection-item avatar useless-articleCategory">
                              <div class="row">
                                <div class="col s10">
                                  <i class="material-icons circle indigo lighten-4">local_offer</i>
                                  <span class="title">{{$category->nombre}}</span>
                                  <p class="grey-text truncate">{{$category->descripcion}}</p>
                                </div>
                                <div class="col s2">
                                  <a class="secondary-content dropdown-trigger btn-flat teal-text" data-target="ARTICLECATEGORY-{{$category->id}}"><i class="material-icons right">arrow_drop_down</i></a>
                                </div>
                              </div>
                            </li>
                          @endif
                        @endforeach
                      </ul>
                    <div class="row">
                      <div class="col s12 m12 l12 center-align">
                        <br>
                        <a class="waves-effect waves-light btn-small teal" href="{{ route('ArticleCategory.Add')}}"><i class="material-icons right">add</i>Agregar categoria</a>
                      </div>
                    </div>
                  </div>
                </li>
              </ul>
            </div>
            <div class="card-action center-align">
              <a class="waves-effect waves-light btn-small teal white-text"  href="{{ route('InventoryArticle.Add')}}"><i class="material-icons right">add</i>Agregar</a>
            </div>
          </div>
        </div>

        <div class="col s12 m12 l8 center-align">
          <h4>Inventario Articulos</h4>
          <table class=" highlight centered responsive-table">
            <thead>
              <tr>
                <th>Articulo</th>
                <th>Existencia</th>
                <th>Tamaño</th>
                <th>Categoria</th>
                <th class="hide-on-med-and-down"> </th>
                <th class="hide-on-med-and-down"> </th>

                <th class="hide-on-large-only">Agregar</th>
                <th class="hide-on-large-only">Retirar</th>

              </tr>
            </thead>
            <tbody id="articlesInventory-table">
              @foreach($articlesInventory as $article)
                @if($article->elementos != '0')
                  <tr id="INVENTORYARTICLE-{{$article->id}}">
                    <td id="ARTICLE-INVENTORYARTICLE-{{$article->id}}">{{$article->articulo}}</td>
                    <td id="EXISTENCE-INVENTORYARTICLE-{{$article->id}}">{{$article->elementos}}</td>
                    <td id="SIZE-INVENTORYARTICLE-{{$article->id}}">{{$article->unidades}} {{$article->unidad}}</td>
                    <td id="CATEGORY-INVENTORYARTICLE-{{$article->id}}">{{$article->categoria}}</td>
                    <td id="EDIT-INVENTORYARTICLE-{{$article->id}}"><a id="EDIT-INVENTORYARTICLE-{{$article->id}}" class="waves-effect waves-light" href="{{route('InventoryArticle.Upgrade',$article->id)}}"><i class="material-icons">add_circle</i></td>
                    <td id="DELETE-INVENTORYARTICLE-{{$article->id}}"><a id="DELETE-INVENTORYARTICLE-{{$article->id}}" class="waves-effect waves-light" href="{{route('InventoryArticle.TakeOff',$article->id)}}"><i class="material-icons">remove_circle</i></td>
                  </tr>
                @else
                  <tr id="INVENTORYARTICLES-{{$article->id}}" class="grey-text text-lighten-2 useless-inventoryArticle">
                    <td id="ARTICLE-INVENTORYARTICLE-{{$article->id}}">{{$article->articulo}}</td>
                    <td id="EXISTENCE-INVENTORYARTICLE-{{$article->id}}">{{$article->elementos}}</td>
                    <td id="SIZE-INVENTORYARTICLE-{{$article->id}}">{{$article->unidades}} {{$article->unidad}}</td>
                    <td id="CATEGORY-INVENTORYARTICLE-{{$article->id}}">{{$article->categoria}}</td>
                    <td id="EDIT-INVENTORYARTICLE-{{$article->id}}"><a id="EDIT-INVENTORYARTICLE-{{$article->id}}" class="waves-effect waves-light" href="{{route('InventoryArticle.Upgrade',$article->id)}}" ><i class="material-icons">add_circle</i></td>
                    <td id="DELETE-INVENTORYARTICLE-{{$article->id}}"><a id="DELETE-INVENTORYARTICLE-{{$article->id}}" class="grey-text" ><i class="material-icons">remove_circle</i></td>
                  </tr>
                @endif
              @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>

    <div class="pages center-align">
      <p>{{$articlesInventory->links()}}</p>
    </div>


@endsection

@section('modals')
  <div id="test" class="modal modal-fixed-footer">
    <div class="modal-content center-align">

    </div>
    <div class="modal-footer">

    </div>
  </div>
<!-- Definir las rutas -->
  @foreach($articleCategories as $category)
    @if($category->T == '1')
      <ul id='ARTICLECATEGORY-{{$category->id}}' class='dropdown-content'>
          <li><a id='CATEGORY-{{$category->id}}' class="CATEGORY black-text" href="#!" ><i class="material-icons left blue-text">error_outline</i>INFO</a></li>
          <li><a href="{{ route('ArticleCategory.Edit', $category->id ) }}" class="black-text"><i class="material-icons left blue-text">edit</i>Editar</a></li>
          <li><a href="{{ route('ArticleCategory.Delete', $category->id ) }}" class="black-text"><i class="material-icons left blue-text">delete</i>Eliminar</a></li>
      </ul>
    @else
      <ul id='ARTICLECATEGORY-{{$category->id}}' class='dropdown-content'>
        <li><a id='CATEGORY-{{$category->id}}' class="CATEGORY black-text" href="#!" ><i class="material-icons left blue-text">error_outline</i>INFO</a></li>
        <li><a href="{{ route('ArticleCategory.Active', $category->id ) }}" class="black-text"><i class="material-icons left blue-text">cached</i>Activar</a></li>
      </ul>
    @endif
    <input type="hidden" name="" value="{{$category->id}}" id="category-id-{{$category->id}}">
    <input type="hidden" name="" value="{{$category->nombre}}" id="category-nombre-{{$category->id}}">
    <input type="hidden" name="" value="{{$category->descripcion}}" id="category-descripcion-{{$category->id}}">
    <input type="hidden" name="" value="{{$category->T}}" id="category-T-{{$category->id}}">
  @endforeach

  @foreach($articles as $article)
    @if($article->T == '1')
      <ul id='ARTICLE-{{$article->id}}' class='dropdown-content'>
          <li><a id='ARTICLE-{{$article->id}}' class="ARTICLE black-text" href="#!" ><i class="material-icons left blue-text">error_outline</i>INFO</a></li>
          <li><a href="{{ route('Article.Edit', $article->id )}}" class="black-text"><i class="material-icons left blue-text">edit</i>Editar</a></li>
          <li><a href="{{ route('Article.Delete', $article->id )}}" class="black-text"><i class="material-icons left blue-text">delete</i>Eliminar</a></li>
      </ul>
    @else
      <ul id='ARTICLE-{{$article->id}}' class='dropdown-content'>
          <li><a id='ARTICLE-{{$article->id}}' class="ARTICLE black-text" href="#!"><i class="material-icons left blue-text">error_outline</i>INFO</a></li>
        <li><a href="{{ route('Article.Activate', $article->id )}}" class="black-text"><i class="material-icons left blue-text">cached</i>Activar</a></li>
      </ul>
    @endif
    <input type="hidden" name="" value="{{$article->id}}" id="article-id-{{$article->id}}">
    <input type="hidden" name="" value="{{$article->nombre}}" id="article-nombre-{{$article->id}}">
    <input type="hidden" name="" value="{{$article->descripcion}}" id="article-descripcion-{{$article->id}}">
    <input type="hidden" name="" value="{{$article->cantidadUnidad}}" id="article-cantidadUnidad-{{$article->id}}">
    <input type="hidden" name="" value="{{$article->unidad}}" id="article-unidad-{{$article->id}}">
    <input type="hidden" name="" value="{{$article->categoria}}" id="article-categoria-{{$article->id}}">
    <input type="hidden" name="" value="{{$article->T}}" id="article-T-{{$article->id}}">
  @endforeach

  @foreach($units as $unit)
    @if($unit->T == '1')
      <ul id='UNIT-{{$unit->id}}' class='dropdown-content'>
        <li><a id='UNIT-{{$unit->id}}' class="UNIT black-text" href="#!" ><i class="material-icons left blue-text">error_outline</i>INFO</a></li>
        <li><a href="{{ route('Unit.Edit', $unit->id ) }}" class="black-text"><i class="material-icons left blue-text">edit</i>Editar</a></li>
        <li><a href="{{ route('Unit.Delete', $unit->id ) }}" class="black-text"><i class="material-icons left blue-text">delete</i>Eliminar</a></li>
      </ul>
    @else
      <ul id='UNIT-{{$unit->id}}' class='dropdown-content'>
        <li><a id='UNIT-{{$unit->id}}' class="UNIT black-text" href="#!" ><i class="material-icons left blue-text">error_outline</i>INFO</a></li>
        <li><a href="{{ route('Unit.Active', $unit->id ) }}" class="black-text"><i class="material-icons left blue-text">cached</i>Activar</a></li>
      </ul>
    @endif
    <input type="hidden" name="" value="{{$unit->id}}" id="unit-id-{{$unit->id}}">
    <input type="hidden" name="" value="{{$unit->nombre}}" id="unit-nombre-{{$unit->id}}">
    <input type="hidden" name="" value="{{$unit->T}}" id="unit-T-{{$unit->id}}">
  @endforeach
@endsection

@section('scripts')
  <script type="text/javascript">

    $(document).ready(function() {
      @if(session('success'))
         M.toast({html: '{{session('success')}}',classes:'green darken-2'})
      @endif
      //ocultar los inútiles
      $('.useless-unit').hide();
      $('.useless-articleCategory').hide();
      $('.useless-article').hide();
      $('.useless-inventoryArticle').hide();

      //los métodos para mostrar o no a los inútiles

        $('#units-lever').click(function(){
           if ($("#units-switch").prop("checked") == true) {
             $('.useless-unit').hide();
             $('.usefull-unit').show();
           }
           else{
             $('.useless-unit').show();
             $('.usefull-unit').hide();
           }
         });

         $('#articles-lever').click(function(){
            if ($("#articles-switch").prop("checked") == true) {
              $('.useless-article').hide();
              $('.usefull-article').show();
            }
            else{
              $('.useless-article').show();
              $('.usefull-article').hide();
            }
          });

         $('#articleCategories-lever').click(function(){
            if ($("#articleCategories-switch").prop("checked") == true) {
              $('.useless-articleCategory').hide();
              $('.usefull-articleCategory').show();
            }
            else{
              $('.useless-articleCategory').show();
              $('.usefull-articleCategory').hide();
            }
          });

          $('#inventoryArticles-lever').click(function(){
             if ($("#inventoryArticles-switch").prop("checked") == true) {
               $('.useless-inventoryArticle').hide();
             }
             else{
               $('.useless-inventoryArticle').show();
             }
           });
          //poner la palanca de los costales en ON
          $('#inventoryArticles-lever').click();

          $('.ARTICLE').click(function(){
            $id = $(this).attr("id");
            $id = $id.substring(8,$id.lenght);
            $nombre = $('#article-nombre-'+$id).attr("value");
            $descripcion = $('#article-descripcion-'+$id).attr('value');
            $cantidadUnidad = $('#article-cantidadUnidad-'+$id).attr('value');
            $unidad = $('#article-unidad-'+$id).attr('value');
            $categoria = $('#article-categoria-'+$id).attr('value');
            $t = $('#article-T-'+$id).attr("value");
            if($t == '0'){
              $t = "Inactivo";
            }
            else {
              $t = "Activo";
            }

            articleInfo($id,$nombre,$descripcion,$cantidadUnidad,$unidad,$categoria,$t);
        });

        $('.UNIT').click(function(){
          $id = $(this).attr("id");
          $id = $id.substring(5,$id.lenght);
          $nombre = $('#unit-nombre-'+$id).attr("value");
          $t = $('#unit-T-'+$id).attr("value");
          if($t == '0'){
            $t = "Inactivo";
          }
          else {
            $t = "Activo";
          }

          unitInfo($id,$nombre,$t);
      });

      $('.CATEGORY').click(function(){
        $id = $(this).attr("id");
        $id = $id.substring(9,$id.lenght);
        $nombre = $('#category-nombre-'+$id).attr("value");
        $descripcion = $('#category-descripcion-'+$id).attr("value");
        $t = $('#category-T-'+$id).attr("value");
        if($t == '0'){
          $t = "Inactiva";
        }
        else {
          $t = "Activa";
        }

        categoryInfo($id,$nombre,$descripcion,$t);
    });

    @if (session('message-delete-article'))
     test("{{session('message-delete-article')}}");
    @endif

    @if (session('message-delete-unit'))
     test("{{session('message-delete-unit')}}");
    @endif

    @if (session('message-delete-category'))
     test("{{session('message-delete-category')}}");
    @endif

    @if (session('message-active-article'))
     test("{{session('message-active-article')}}");
    @endif

    @if (session('message-active-unit'))
     test("{{session('message-active-unit')}}");
    @endif

    @if (session('message-active-category'))
     test("{{session('message-active-category')}}");
    @endif

    @if (session('message-edit-unit'))
     test("{{session('message-edit-unit')}}");
    @endif

    @if (session('message-edit-category'))
     test("{{session('message-edit-category')}}");
    @endif

    @if (session('message-edit-article'))
     test("{{session('message-edit-article')}}");
    @endif

    @if (session('message-add-unit'))
     test("{{session('message-add-unit')}}");
    @endif

    @if (session('message-add-category'))
     test("{{session('message-add-category')}}");
    @endif

    @if (session('message-add-article'))
     test("{{session('message-add-article')}}");
    @endif
    @if (session('error'))
     M.toast(html: "{{session('error')}}", classes:'red darken-2');
    @endif

    @if (session('message-add-inventaryArticle'))
     test("{{session('message-add-invetaryArticle')}}");
    @endif

     });

     function test($info){
       M.toast({html: $info, classes: 'green darken-2'})
     }


     function unitInfo($id,$nombre,$t){
        $('.modal-content').empty();
        $('.modal-content').append(
          "<h3>INFO  <i class='material-icons small teal-text'>info_outline</i></h3>"+
          "<div class='divider'></div>"+
          "<p class='grey-text'>Unidad "+$t+"</p>"+
          "<div class='divider'></div>"+
          "<table class='striped'>"+
            "<tbody>"+
              "<tr>"+
                "<th class='center-align'>Nombre:</th>"+
                "<td>"+$nombre+"</td>"+
              "</tr>"+
            "</tbody>"+
          "</table>"
        );
        $('.modal-footer').empty();
        $('.modal-footer').append(
          "<a class='waves-effect waves-light btn-flat grey-text text-darken-3 modal-close'>Cerrar</a>"
        );
        $('#test').modal('open');
    }

     function articleInfo($id,$nombre,$descripcion,$cantidadUnidad,$unidad,$categoria,$t){
        $('.modal-content').empty();
        $('.modal-content').append(
          "<h3>INFO  <i class='material-icons small teal-text'>info_outline</i></h3>"+
          "<div class='divider'></div>"+
          "<p class='grey-text'>Artículo "+$t+"</p>"+
          "<div class='divider'></div>"+
          "<table class='striped'>"+
            "<tbody>"+
              "<tr>"+
                "<th class='center-align'>Nombre:</th>"+
                "<td>"+$nombre+"</td>"+
              "</tr>"+
              "<tr>"+
                "<th class='center-align'>Descripción:</th>"+
                "<td>"+$descripcion+"</td>"+
              "</tr>"+
              "<tr>"+
                "<th class='center-align'>Cantidad:</th>"+
                "<td>"+$cantidadUnidad+"</td>"+
              "</tr>"+
              "<tr>"+
                "<th class='center-align'>Unidad de medida:</th>"+
                "<td>"+$unidad+"</td>"+
              "</tr>"+
              "<tr>"+
                "<th class='center-align'>Categoria:</th>"+
                "<td>"+$categoria+"</td>"+
              "</tr>"+
            "</tbody>"+
          "</table>"
        );
        $('.modal-footer').empty();
        $('.modal-footer').append(
          "<a class='waves-effect waves-light btn-flat grey-text text-darken-3 modal-close'>Cerrar</a>"
        );
        $('#test').modal('open');
    }

    function categoryInfo($id,$nombre,$descripcion,$t){
       $('.modal-content').empty();
       $('.modal-content').append(
         "<h3>INFO  <i class='material-icons small teal-text'>info_outline</i></h3>"+
         "<div class='divider'></div>"+
         "<p class='grey-text'>Categoria "+$t+"</p>"+
         "<div class='divider'></div>"+
         "<table class='striped'>"+
           "<tbody>"+
             "<tr>"+
               "<th class='center-align'>Nombre:</th>"+
               "<td>"+$nombre+"</td>"+
             "</tr>"+
             "<tr>"+
               "<th class='center-align'>Descripción:</th>"+
               "<td>"+$descripcion+"</td>"+
             "</tr>"+
           "</tbody>"+
         "</table>"
       );
       $('.modal-footer').empty();
       $('.modal-footer').append(
         "<a class='waves-effect waves-light btn-flat grey-text text-darken-3 modal-close'>Cerrar</a>"
       );
       $('#test').modal('open');
   }

    //no tocar, es para que funcionen los dropdown
    document.addEventListener('DOMContentLoaded', function() {
      var elems = document.querySelectorAll('.dropdown-trigger');
      var options =
        {
          constrainWidth:false,
          alignment:'right'
        };
      var instances = M.Dropdown.init(elems, options);
    });

  </script>
@endsection
<?php // TODO: HACER LOGO DE mapache ?>
