@extends('Layouts.Menu')
@section('title','Nuevo estanque')
@section('content')


  <form id="addForm" action="{{ route('Estanque.Add') }}" method="POST" enctype="multipart/form-data">
    @csrf

    <h4 class="center-align">Agregar estanque</h4>
    <div class="row">
      <div class="container">
        <div class="col s12">
          <div class="row">
            <div class="col s12 m4 l4 center align">
              <h4><i class="material-icons">help_outline</i> INFO</h4>

            </div>
            <div class="col s12 m8 l8">
              <p  ALIGN="justify">
                En este apartado puedes agregar un <span class="blue-text">estanque</span> para la crianza de
                las truchas. Es necesario que especifiques la <span class="blue-text">información ACTUAL</span> del estanque ya que crearemos un
                <span class="blue-text">historial de estanque</span>.
              </p>
              <p>Podrás ver tus estanques en el apartado <span class="blue-text">Inicio</span>.</p>
            </div>
          </div>
          <!-- NOTE: NOMBRE -->
          <div class="row">
            <div class="input-field col s12">
              <input id="pond_name" name="nombre" type="text" class="validate" data-length="30" maxlength="30" required="required">
              <label for="pond_name">Nombre</label>
              <span class="help-text" data-error="Incorrecto" data-success="Correcto">Ingrese el nombre del estanque</span>
            </div>
          </div>
          <!-- NOTE: TIPO -->
          <div class="row">
            <div class="input-field col s12">
              <select id="select_trout" name="idTrucha">
                <option id="first_select" value="" selected disabled>Seleccione una etapa de trucha</option>
                  @if(!empty($trouts))
                    @foreach($trouts as $trout)
                      @if($trout->T == '1')
                          <option value="{{ $trout->id }}">{{ $trout->tipo }}</option>
                      @endif
                    @endforeach
                  @endif
                </select>
                <label data-error="Escoje una opcion">Etapa</label>
                <span id="error-select" class="red-text darken-2"></span>
            </div>
          </div>

          <!-- NOTE: CANTIDAD -->
          <div class="row">
            <div class="input-field col s12">
              <input id="pond_quantity" name="cantidad" type="number" step="1" min="1" max="99999999999" class="validate" required="required">
              <label for="pond_quantity">Cantidad</label>
              <span class="helper-text" data-error="Incorrecto" data-success="Correcto">Ingrese la cantidad de truchas</span>
            </div>
          </div>
          <!-- NOTE: PESO -->
          <div class="row">
            <div class="input-field col s12">
              <input id="pond_weight" name="peso" type="number" step="0.01" min="0.01" max="9999999.99" class="validate" required="required">
              <label for="pond_weight">Peso</label>
              <span class="helper-text" data-error="Incorrecto" data-success="Correcto">Ingrese el peso en gramos</span>
            </div>
          </div>
          <!-- NOTE: TAMAÑO -->
          <div class="row">
            <div class="input-field col s12">
              <input id="pond_size" name="tamano" type="number" step="0.01" min="0.01" max="9999999.99" class="validate" required=required"">
              <label for="pond_size">Tamaño</label>
              <span class="helper-text" data-error="Incorrecto" data-success="Correcto">Ingrese el tamaño promedio en centímetros</span>
            </div>
          </div>
          <!-- NOTE: NIVEL DE AGUA -->
          <div class="row">
            <div class="input-field col s12">
              <input id="pond_water_lvl" name="volumenAgua" type="number" step="0.01" min="0.01" max="999999999999.99" class="validate" required="required">
              <label for="pond_water_lvl">Volumen de agua</label>
              <span class="helper-text" data-error="Incorrecto" data-success="Correcto">Ingrese el peso en litros</span>
            </div>
          </div>
          <!-- NOTE: TEMPERATURA -->
          <div class="row">
            <div class="input-field col s12">
              <input id="pond_temperature" name="temperatura" type="number" step="0.01" min="0.01" max="999999.99" class="validate" required="required">
              <label for="pond_temperature">Temperatura</label>
              <span class="helper-text" data-error="Incorrecto" data-success="Correcto">Ingrese la temperatura en °C</span>
            </div>
          </div>
          <!-- NOTE: OXÍGENO -->
          <div class="row">
            <div class="input-field col s12">
              <input id="pond_oxigen" name="oxigeno" type="number" step="0.01" min="0.01" max="999999999.99" class="validate" required="required">
              <label for="pond_oxigen">Oxígeno</label>
              <span class="helper-text" data-error="Incorrecto" data-success="Correcto">Ingrese el porcentaje de oxígeno</span>
            </div>
          </div>
          <!-- NOTE: PH -->
          <div class="row">
            <div class="input-field col s12">
              <input id="pond_ph" name="ph" type="number" step="0.01" min="0.01" max="99999.99" class="validate" required="required">
              <label for="pond_ph">PH</label>
              <span class="helper-text" data-error="Incorrecto" data-success="Correcto">Ingrese el nivel de PH</span>
            </div>
          </div>
          <div class="row">
            <div class="file-field input-field">
              <div class="btn teal">
                <span>Subir imagen</span>
                <input type="file" name="imagen">
              </div>
              <div class="file-path-wrapper">
                <input type="text" class="file-path validate" required="required" aria-required="true">
                <span class="helper-text" data-error="Incorrecto" data-success="Correcto">Selecciona la imagen del estanque</span>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col s12 m12 l12 center-align">
              <button type="submit" class=" waves-effect waves-light btn blue darken-3 center-align">Agregar</button>
            </div>
          </div>
        </div>
      </div>

    </div>
  </form>

@endsection

@section('scripts')
  <script type="text/javascript">
    $(document).ready(function() {
      $('input#pond_name').characterCounter();
      $( "Form" ).submit(function( event ) {
        if ( $( "select" ).first().val() != null ) {
          return;
        }
        $("#error-select").html("Seleccione etapa de trucha");
        M.toast({html: 'Seleccione una etapa de trucha',classes:'red darken-3'});
        //alert("Seleccione un tipo de trucha");
        $(document).scrollTop(0);
        event.preventDefault();
      });
    });
  </script>
@endsection
