@extends('Layouts.Menu')

@section('title','Editar puesto')

@section('content')
  <form action="{{route('Job.Update')}}" method="POST">
    <input type="hidden" name="id" value="{{$job->id}}">
    @csrf

    <h4 class="center-align">Editar: {{ $job->nombre }}</h4>
    <div class="row">
      <div class="container">
        <div class="col s12">
          <div class="row">
            <div class="col s12 m4 l4 center align">
              <h4><i class="material-icons">help_outline</i> INFO</h4>

            </div>
            <div class="col s12 m8 l8">
              <p  ALIGN="justify">
                En este apartado puedes editar la información de <span class="blue-text">
                {{$job->nombre}}</span>, Asegurate de ingresar información lo más descriptiva
                y simple posible.
              </p>

            </div>
          </div>
          <!-- NOTE: NOMBRE -->
          <div class="row">
            <div class="input-field col s12">
              <input id="job_nombre" name="nombre" type="text" data-length="30" maxlength="30" required="required" aria-required="true" class="validate" value="{{ $job->nombre }}">
              <label for="type_nombre">Nombre</label>
              <span class="help-text" data-error="Incorrecto" data-success="Correcto">Ingrese el nombre del puesto</span>
            </div>
          </div>
        </div>
        <div class="center-align">
          <button type="submit" class="waves-effect waves-light btn blue darken-3 center-align">Aceptar</button>
        </div>
      </div>
    </div>
  </form>
@endsection

@section('scripts')
  <script>
    $(document).ready(function() {
      $('#job_nombre').characterCounter();
    });
  </script>
@endsection
