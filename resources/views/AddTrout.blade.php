@extends('Layouts.Menu')
@section('title','Nueva etapa')
@section('content')


  <form id="addForm" action="{{ route('Trout.Add') }}" method="POST">
    @csrf

    <h4 class="center-align">Agregar etapa</h4>
    <div class="row">
      <div class="container">
        <div class="col s12">
          <div class="row">
            <div class="col s12 m4 l4 center align">
              <h4><i class="material-icons">help_outline</i> INFO</h4>

            </div>
            <div class="col s12 m8 l8">
              <p  ALIGN="justify">
                En este apartado puedes agregar una <span class="blue-text">etapa</span>, las etapas sirven
                para organizar de una mejor manera tus estanques. Asegurate de ingresar información lo más descriptiva
                y simple posible.
              </p>
              <p>Podrás ver tus etapas en el panel de control del apartado <span class="blue-text">Inicio</span>.</p>
            </div>
          </div>
          <!-- NOTE: tipo -->
          <div class="row">
            <div class="input-field col s12">
              <input id="trout_name" name="tipo" type="text" class="validate" data-length="30" maxlength="30" required="required">
              <label for="trout_name">Tipo</label>
              <span class="help-text" data-error="Incorrecto" data-success="Correcto">Ingrese la etapa</span>
            </div>
          </div>
          <br></br>
          <!-- NOTE: descripcion, se cambio por un textarea -->
          <div class="row">
            <div class="input-field col s12">
              <input id="trout_description" name="descripcion" type="text" class="validate" data-length="120" maxlength="120" required="required">
              <label for="trout_description">Descripcion</label>
              <span class="help-text">Ingrese una descripcion</span>
            </div>
          </div>

          <div class="row">
            <div class="col s12 m12 l12 center-align">
              <button type="submit" class=" waves-effect waves-light btn white-text text-darken-1 center-align">Agregar</button>
            </div>
          </div>
        </div>
      </div>

    </div>
  </form>

@endsection


@section('scripts')
  <script type="text/javascript">
  $(document).ready(function() {
    $('input#trout_name').characterCounter();
  });
  </script>
@endsection
