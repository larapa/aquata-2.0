@extends('Layouts.Menu')
@section('title','Añadir elementos')
@section('content')

  <form action="{{route('InventoryProduct.UpgradeValidation')}}" method="POST">
    <input type="hidden" name="id" value="{{$inventoryProduct->id}}">
    <input type="hidden" name="idInventario" value="{{$inventoryProduct->idInventario}}">

    @csrf
    <h4 class="center-align">Añadir producto: {{$info->producto}}</h4>
    <div class="row">
      <div class="container">
        <div class="col s12">
          <!-- NOTE: Estanque -->
          <div class="row">
            <div class="col s12 m4 l4 center-align">
              <h4><i class="material-icons">help_outline</i> INFO</h4>


            </div>
            <div class="col s12 m8 l8">
              <p  ALIGN="justify">La modificación de la cantidad de elementos del inventario de productos consiste en
                 ingresar la cantidad a añadir.
                 Cuando se añaden productos significa que <span class="red-text text-darken-2">se han sacado del estanque</span> relacionado a este
                 producto, por lo que la cantidad en este <span class="blue-text">también será modificada</span>.
              </p>
              <p  ALIGN="justify">
                La única forma de restar elementos del inventario de productos es haciendo una <span class="blue-text">venta</span>.
                Puedes administrar tus ventas desde la seccion <span class="blue-text">Ventas</span>
                en el apartado <span class="blue-text">Economía</span>.
              </p>
              <p  ALIGN="justify">
                Si la casilla de <span class="red-text text-darken-2">cancelación</span> está seleccionada significa que cometiste un error anteriormente y
                la cantidad de peces en los estanques fue modificada erroneamente. Esta opción hará que la cantidad que
                indiques sea restada de la cantidad de productos y añadida a la cantidad de truchas del estanque relacionado.
                Utilizala con cuidado.
              </p>
            </div>
          </div>
        </div>
        <div class="center-align">
          <div class="row">
            <div class="input-field col s8">
              <input disabled value="{{$info->producto}}" id="estanque" type="text">
              <label for="estanque">Producto</label>
            </div>
            <div class="input-field col s4">
              <input disabled value="{{$inventory->cantidadElementos}}" id="producto-max" type="text">
              <label for="producto-max">Disponibles</label>
            </div>
          </div>
          <div class="row">
            <div class="input-field col s12 m8 l8">
              <select name='estanque' id='estanque'>
                <option value="" disabled selected>Selecciona un estanque</option>
                @foreach ($ponds as $pond)
                  <option class="truncate" value="{{$pond->id}}">{{$pond->nombre}}</option>
                @endforeach

              </select>
              <label>Estanque</label>
              <span id="error-pond" class="red-text text-darken-3"></span>
            </div>
            <div class="col s12 m4 l4 center-align switch">
              <br>
              <label>
                Cancelación
                <input type="hidden" name="cancelacion" value="0">
                <input name="cancelacion" type="checkbox" value="1" id="cancelacion">
                <span class="lever" id="cancelation-lever"></span>
              </label>
            </div>
          </div>
          @foreach ($ponds as $pond)
            <div class="row hidden-info" id='info-{{$pond->id}}'>
              <div class="row">
                <div class="input-field col s12">
                  <input disabled value="{{$pond->cantidad}}" id="quantity-{{$pond->id}}" type="text" class="validate">
                  <label for="quantity-{{$pond->id}}">Cantidad disponible</label>
                </div>
              </div>
              <div class="row">
                <div class="input-field col s12">
                  <input disabled value="{{$pond->tipo}}" id="disabled" type="text" class="validate">
                  <label for="disabled">Tipo de trucha</label>
                </div>
              </div>
            </div>
          @endforeach

          <div class="row">

            <div class="input-field col s12 m6 l6" id='cantidad-div'>
                <input name="cantidadTruchas" id="cantidad" type="number" step="1" min="0" class="validate" value="" required="" aria-required="true">
              <label for="cantidad">Cantidad truchas</label>
              <span class="helper-text" id="helper-cantidad">Ingrese la cantidad de a utilizar</span>
            </div>
            <div class="input-field col s12 m6 l6" id='cantidad-div'>
                <input name="cantidadProductos" id="cantidadp" type="number" step="1" min="0" class="validate" value="" required="" aria-required="true">
              <label for="cantidadp">Cantidad productos</label>
              <span class="helper-text" id="helper-cantidad-p">Ingrese la cantidad de a agregar</span>
            </div>
          </div>
          <button type="submit" class="waves-effect waves-light btn blue darken-3 center-align">Aceptar</button>
        </div>
      </div>
    </div>
  </form>
@endsection
<?php // TODO: poener max cuando el switch de cancelación esté encendido ?>
@section('scripts')
  <script type="text/javascript">

  $(document).ready(function() {
    M.updateTextFields();
    $('.hidden-info').hide();
  });

  $('select#estanque').change(function(event) {
    $('.hidden-info').hide();
    $('#info-'+$(this).val()).show('slow');
    $('#cantidad').val('');
    $('#cantidadp').val('');
    $('#cantidad').attr('max',$('#quantity-'+$(this).val()).val())
  });
    $(document).ready(function() {

      $('#cancelation-lever').click(function(event) {
        if($('#cancelacion').prop("checked") == false){
          //cantidades y maximos
          $('#cantidadp').attr({'max':'{{$inventory->cantidadElementos}}'});
          $('#cantidad').attr('max','');
          $('#cantidad').val('');
          $('#cantidadp').val('');
          //mensajes
          $('#helper-cantidad').empty();
          $('#helper-cantidad-p').empty();
          $('#helper-cantidad').append('Ingrese la cantidad a regresar');
          $('#helper-cantidad-p').append('Ingrese la cantidad a eliminar');
        }
        else {
          //cantidades y maximos
          //$('select#estanque')
          //alert($('#quantity-'+$('select#estanque').val()).val());
          $('#cantidad').attr('max',$('#quantity-'+$('select#estanque').val()).val());
          $('#cantidadp').attr('max','');
          $('#cantidad').val('');
          $('#cantidadp').val('');
          //mensajes
          $('#helper-cantidad').empty();
          $('#helper-cantidad-p').empty();
          $('#helper-cantidad').append('Ingrese la cantidad a utilizar');
          $('#helper-cantidad-p').append('Ingrese la cantidad a agregar');
        }
      });

    });

  </script>
@endsection
