<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(puestoSeeder::class);
        $this->call(empleadoSeeder::class);
        $this->call(rolesSeeder::class);
        $this->call(usuarioSeeder::class);
        $this->call(truchaSeeder::class);
        $this->call(estanqueSeeder::class);
        $this->call(categoria_gastoSeeder::class);
        $this->call(gastoSeeder::class);
        $this->call(tipo_alimentoSeeder::class);
        $this->call(costalSeeder::class);
        $this->call(motivoSeeder::class);
        $this->call(historial_inventario_alimentoSeeder::class);
        $this->call(inventarioSeeder::class);
        $this->call(unidadSeeder::class);
        $this->call(categoria_articuloSeeder::class);
        $this->call(recetaSeeder::class);
        $this->call(articuloSeeder::class);
        $this->call(inventario_articuloSeeder::class);
        $this->call(historial_inventario_articuloSeeder::class);
        $this->call(productoSeeder::class);
        $this->call(ventaSeeder::class);
        $this->call(receta_productoSeeder::class);
        $this->call(inventario_productoSeeder::class);
        $this->call(venta_productoSeeder::class);
        $this->call(historial_estanqueSeeder::class);
        $this->call(registro_alimentacionSeeder::class);
    }
}
