<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class puestoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('puestos')->insert([
        'id' => null,
        'nombre' => 'Inactivo'
      ]);
      DB::table('puestos')->insert([
          'id' => null,
          'nombre' => 'Socio'
      ]);
      DB::table('puestos')->insert([
          'id' => null,
          'nombre' => 'Encargado'
      ]);
      DB::table('puestos')->insert([
          'id' => null,
          'nombre' => 'Técnico'
      ]);
    }
}
