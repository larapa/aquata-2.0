<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class motivoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('motivos')->insert([
          'id' => null,
          'nombre' => 'Alimentacion',
          'descripcion' => 'Alimentacion de truchas',
          'direccion' => '0'
      ]);

      DB::table('motivos')->insert([
          'id' => null,
          'nombre' => 'Desastre',
          'descripcion' => 'Accidente en el inventario',
          'direccion' => '0'
      ]);

      DB::table('motivos')->insert([
          'id' => null,
          'nombre' => 'Compra',
          'descripcion' => 'Compre alimento',
          'direccion' => '1'
      ]);

      DB::table('motivos')->insert([
          'id' => null,
          'nombre' => 'Edicion',
          'descripcion' => 'Edite alimento',
          'direccion' => '1'
      ]);
    }
}
