<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class empleadoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('empleados')->insert([
          'id' => null,
          'nombre' => 'Arturo',
          'aPaterno' => 'Alcocer',
          'aMaterno' => 'Molina',
          'Telefono' => '4432280290',
          'email' => 'arturoalcocermolina@gmail.com',
          'sueldo' => '0.00',
          'imagen' => 'Empleado7.jpg',
          'idPuesto' => '2'
      ]);

    }
}
