<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class usuarioSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('usuarios')->insert([
          'id' => null,
          'name' => 'Arturo',
          'email' => 'arturoalcocermolina@gmail.com',
          'password' => Hash::make('0123456789'),
          'T' => '1',
          'remember_token' => null,
          'idEmpleado' => '1',
          'idRol'=> '1'
      ]);

    }
}
