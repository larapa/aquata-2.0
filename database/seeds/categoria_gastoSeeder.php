<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class categoria_gastoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('categoria_gastos')->insert([
          'id' => null,
          'nombre' => 'Compra de alimento',
          'descripcion' => 'Son mis gastos de compra de alimentos'
      ]);

      DB::table('categoria_gastos')->insert([
          'id' => null,
          'nombre' => 'Compra de articulo',
          'descripcion' => 'Son mis gastos de compra de articulos'
      ]);

      DB::table('categoria_gastos')->insert([
          'id' => null,
          'nombre' => 'Cancelacion',
          'descripcion' => 'Categoria para cancelar un gasto'
      ]);

    }
}
