<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class rolesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

      DB::table('roles')->insert([
          'id' => null,
          'nombre' => 'Socio'
      ]);

      DB::table('roles')->insert([
          'id' => null,
          'nombre' => 'Dueño'
      ]);

      DB::table('roles')->insert([
          'id' => null,
          'nombre' => 'Empleado'
      ]);

    }
}
