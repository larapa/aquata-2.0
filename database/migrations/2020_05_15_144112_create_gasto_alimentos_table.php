<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGastoAlimentosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gasto_alimentos', function (Blueprint $table) {
          $table->bigIncrements('id');
          $table->unsignedBigInteger('idGasto');
          $table->foreign('idGasto')->references('id')->on('gastos');
          $table->unsignedBigInteger('idCostal');
          $table->foreign('idCostal')->references('id')->on('costals');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('gasto_alimentos');
    }
}
