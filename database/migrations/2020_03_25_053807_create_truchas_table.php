<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTruchasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('truchas', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('tipo',30);
            $table->string('descripcion',120)->default('Sin descripcion');
            $table->tinyInteger('T')->default('1');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('truchas');
    }
}
