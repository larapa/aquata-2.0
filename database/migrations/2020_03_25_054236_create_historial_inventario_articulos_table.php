<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHistorialInventarioArticulosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('historial_inventario_articulos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamp('fecha');
            $table->string('articulo',30);
            $table->string('descripcionArticulo',120);
            $table->string('unidad',30);
            // TODO: cambiar HistorialInventarioArticulo y la cantidad a INT en el diagrama relacional
            $table->integer('cantidad');
            $table->string('categoria',30);
            $table->string('descripcionCategoria',120);
            $table->integer('cantidadElementos');
            $table->unsignedBigInteger('idArticulo');
            $table->unsignedBigInteger('idUsuario');
            $table->foreign('idArticulo')->references('id')->on('articulos');
            $table->foreign('idUsuario')->references('id')->on('usuarios');
            $table->string('motivo',20);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('historial_inventario_articulos');
    }
}
