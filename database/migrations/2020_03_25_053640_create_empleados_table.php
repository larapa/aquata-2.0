<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmpleadosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('empleados', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nombre', 30);
            $table->string('aPaterno',20);
            $table->string('aMaterno',20)->nullable($value = true);
            $table->char('telefono',10);
            $table->string('email',50);
            $table->decimal('sueldo',9,2);
            $table->unsignedBigInteger('idPuesto');
            $table->foreign('idPuesto')->references('id')->on('puestos');
            // NOTE: La baja logica se hará con el idPuesto = 1
        });

        DB::statement("ALTER TABLE empleados ADD imagen MEDIUMBLOB NOT NULL");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('empleados');
    }
}
