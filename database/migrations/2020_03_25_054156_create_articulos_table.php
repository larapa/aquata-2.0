<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateArticulosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('articulos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nombre',30);
            $table->string('descripcion',120)->default('Sin descripcion');
            // TODO: cambiar cantidadUnidad en diagrama relacional
            $table->integer('cantidadUnidad');
            $table->tinyInteger('T')->default('1');
            $table->unsignedBigInteger('idUnidad');
            $table->unsignedBigInteger('idCategoriaArticulo');
            $table->foreign('idUnidad')->references('id')->on('unidads');
            $table->foreign('idCategoriaArticulo')->references('id')->on('categoria_articulos');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('articulos');
    }
}
