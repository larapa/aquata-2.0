<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRegistroAlimentacionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('registro_alimentacions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamp('fecha');
            $table->float('recomendado',7,2);
            $table->float('suministrado',7,2);
            $table->unsignedBigInteger('idEstanque');
            $table->unsignedBigInteger('idUsuario');
            $table->unsignedBigInteger('idHistorialInventarioAlimento');
            $table->foreign('idEstanque')->references('id')->on('estanques');
            $table->foreign('idUsuario')->references('id')->on('usuarios');
            $table->foreign('idHistorialInventarioAlimento')->references('id')->on('historial_inventario_alimentos');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('registro_alimentacions');
    }
}
