<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEstanquesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('estanques', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nombre',30);
            $table->integer('cantidad');
            $table->float('tamano',7,2);
            $table->float('peso',7,2);
            $table->float('temperatura',6,2);
            $table->float('oxigeno',9,2);
            $table->float('volumenAgua',12,2);
            $table->float('ph',5,2);
            $table->timestamp('fechaIngreso');
            $table->tinyInteger('T')->default('1');
            $table->unsignedBigInteger('idTrucha');
            $table->foreign('idTrucha')->references('id')->on('truchas');
            // TODO: Mover al final el idTrucha en estanque en el diagrama relacional
        });
        DB::statement("ALTER TABLE estanques ADD imagen MEDIUMBLOB NOT NULL");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('estanques');
    }
}
