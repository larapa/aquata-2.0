<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHistorialEstanquesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('historial_estanques', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamp('fecha');
            $table->integer('cantidad');
            $table->string('nombre',35);
            $table->string('tipo',30);
            $table->float('tamano',7,2);
            $table->float('peso',7,2);
            $table->float('temperatura',6,2);
            $table->float('oxigeno',9,2);
            $table->float('nivelAgua',12,2);
            $table->float('ph',5,2);
            $table->timestamp('fechaIngreso');
            $table->integer('motivo');
            $table->unsignedBigInteger('idEstanque');
            $table->unsignedBigInteger('idUsuario');
            $table->unsignedBigInteger('idTrucha');
            $table->foreign('idEstanque')->references('id')->on('estanques');
            $table->foreign('idUsuario')->references('id')->on('usuarios');
            $table->foreign('idTrucha')->references('id')->on('truchas');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('historial_estanques');
    }
}
