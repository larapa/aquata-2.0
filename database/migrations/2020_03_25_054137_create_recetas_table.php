<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRecetasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('recetas', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nombre', 30);
            $table->string('descripcion', 120)->default('Sin descripcion');
            $table->integer('tiempo');
            $table->json('instrucciones');
            $table->json('ingredientes');
            $table->string('porciones', 45);
            $table->tinyInteger('T')->default('1');
        });
        DB::statement("ALTER TABLE recetas ADD imagen MEDIUMBLOB NOT NULL");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('recetas');
    }
}
