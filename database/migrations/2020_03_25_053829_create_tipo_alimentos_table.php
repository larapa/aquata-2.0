<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTipoAlimentosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tipo_alimentos', function (Blueprint $table) {
          $table->bigIncrements('id');
          $table->string('nombre', 30);
          $table->string('descripcion', 120)->default('Sin descripcion');
          $table->tinyInteger('T')->default('1');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tipo_alimentos');
    }
}
