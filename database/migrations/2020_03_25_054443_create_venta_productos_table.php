<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVentaProductosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('venta_productos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->float('cantidad', 10, 2);
            $table->integer('piezas');
            $table->decimal('descuento', 9, 2);
            $table->decimal('precioVenta', 8, 2);
            $table->unsignedBigInteger('idProducto');
            $table->foreign('idProducto')->references('id')->on('productos');
            $table->unsignedBigInteger('idVenta');
            $table->foreign('idVenta')->references('id')->on('ventas');
            $table->unsignedBigInteger('idInventarioProducto');
            $table->foreign('idInventarioProducto')->references('id')->on('inventario_productos');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        Schema::dropIfExists('venta_productos');

    }
}
