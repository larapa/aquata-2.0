<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('productos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->decimal('precio',8,2);
            $table->string('nombre',45);
            $table->string('descripcion',120)->default('Sin descripcion');
            $table->tinyInteger('T')->default('1');
        });
        // TODO: Agregar imagen en los diagramas
        DB::statement("ALTER TABLE productos ADD imagen MEDIUMBLOB NOT NULL");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('productos');
    }
}
