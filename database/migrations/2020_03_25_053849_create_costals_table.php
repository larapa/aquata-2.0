<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCostalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('costals', function (Blueprint $table) {
            $table->bigIncrements('id');
            // TODO: quitar 'tipo' del diagrama relacional
            $table->string('etiqueta',30)->default('Sin etiqueta');
            $table->float('tamano',8,2);
            $table->float('alimentoRestante',8,2);
            $table->tinyInteger('T')->default('1');
            $table->unsignedBigInteger('idTipoAlimento');
            $table->foreign('idTipoAlimento')->references('id')->on('tipo_alimentos');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('costals');
    }
}
