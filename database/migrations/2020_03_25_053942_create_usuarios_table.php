<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsuariosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('usuarios', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('email')->unique();
            $table->string('password');
            $table->tinyInteger('T')->default('1');
            $table->rememberToken();
            $table->unsignedBigInteger('idEmpleado');
            $table->unsignedBigInteger('idRol');
            $table->foreign('idEmpleado')->references('id')->on('empleados');
            $table->foreign('idRol')->references('id')->on('roles');
            // NOTE: Se confirma con el puesto del empleado
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('usuarios');
    }
}
