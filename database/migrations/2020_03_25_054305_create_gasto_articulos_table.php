<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGastoArticulosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gasto_articulos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('idGasto');
            $table->foreign('idGasto')->references('id')->on('gastos');
            $table->unsignedBigInteger('idInventarioArticulo');
            $table->foreign('idInventarioArticulo')->references('id')->on('inventario_articulos');
            $table->integer('cantidadElementos');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('gasto_articulos');
    }
}
