<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHistorialInventarioAlimentosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('historial_inventario_alimentos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamp('fecha');
            $table->float('alimentoRestante',8,2);
            $table->float('tamano',8,2);
            $table->string('etiqueta',30);
            $table->unsignedBigInteger('idMotivo');
            $table->unsignedBigInteger('idCostal');
            $table->foreign('idMotivo')->references('id')->on('motivos');
            $table->foreign('idCostal')->references('id')->on('costals');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('historial_inventario_alimentos');
    }
}
