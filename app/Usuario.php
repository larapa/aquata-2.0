<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Auth\Authenticatable;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Notifications\Notifiable;

class Usuario extends Model implements AuthenticatableContract, CanResetPasswordContract
{
    use Authenticatable;
    use Notifiable;
    use CanResetPassword;
    protected $fillable = ['email','password','T','idEmpleado','idRol'];
    public $timestamps = false;

    public function role(){

      return $this->belongsTo('App\Role');
    }

    public function socio(){

      if($this->idRol == '1'){
        return true;
      }


        return false;


    }
}
