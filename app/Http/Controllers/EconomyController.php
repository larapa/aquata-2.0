<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App;
use App\HistorialInventarioAlimento;
use App\Inventario;
use App\InventarioArticulo;
use App\InventarioProducto;
use App\VentaProducto;
use App\Gasto;
use App\Venta;
use Auth;
use DB;
use App\CategoriaGasto;
use Carbon\Carbon;

class EconomyController extends Controller
{

  public function showStatistics(){
    if(Auth::check()&&auth()->user()->T == 1){
      if(auth()->user()->idRol == 1||auth()->user()->idRol == 2){
        try {
          $statsExpenses = App\Gasto::select('nombre', 'fecha', 'concepto', 'monto', 'gastos.id')
                                    ->join('categoria_gastos', "categoria_gastos.id" , "=" , "gastos.idCategoriaGasto")
                                    ->where('categoria_gastos.id', "!=", '3')
                                    ->where('fecha', '>=', Carbon::now()->subMonth());

          $stats = App\Venta::selectRaw("'Venta' as nombre, fecha, descripcion,
                                          (precioVenta * (cantidad)/1000 - descuento) as monto, ventas.id")
                            ->join('venta_productos', 'venta_productos.idVenta', '=', 'ventas.id')
                            ->where('ventas.T', '=', '1')
                            ->union($statsExpenses)
                            ->where('fecha', '>=', Carbon::now()->subMonth())
                            ->orderBy('fecha', 'desc')
                            ->get();

          $statsExpensesChart = App\Gasto::selectRaw("'Gasto' as nombre, sum(monto) as total")
                                    ->join('categoria_gastos', "categoria_gastos.id" , "=" , "gastos.idCategoriaGasto")
                                    ->where('categoria_gastos.id', "!=", '3')
                                    ->where('fecha', '>=', Carbon::now()->subMonth());

          $statsChart = App\Venta::selectRaw("'Venta' as nombre,
                                          sum(precioVenta * (cantidad)/1000 - descuento) as total")
                            ->join('venta_productos', 'venta_productos.idVenta', '=', 'ventas.id')
                            ->where('ventas.T', '=', '1')
                            ->where('fecha', '>=', Carbon::now()->subMonth())
                            ->union($statsExpensesChart)
                            ->get();

          return view('EconomyStatistics',compact('stats', 'statsChart'));

        } catch (\Exception $e) {
          return redirect('AQUATA/inicio')->with('error', 'Algo salió mal, inténtalo nuevamente');
        } catch (\Throwable $e) {
          return redirect('AQUATA/inicio')->with('error', 'Algo salió mal, inténtalo nuevamente');
        }
      }
      else{
        return redirect('AQUATA/inicio')->with('error', '¡Oops! No tienes permiso para hacer eso');
      }
    }
    else{
      return redirect('/login');
    }
  }

  public function showStatisticsByDate(Request $request){
    if(Auth::check()&&auth()->user()->T == 1){
      if(auth()->user()->idRol == 1||auth()->user()->idRol == 2){
        try {
          if(Carbon::createFromFormat('d/m/Y', $request -> fecha1) >= Carbon::createFromFormat('d/m/Y', $request -> fecha2)){
            return redirect('AQUATA/Economia/Estadisticas')->with('error', 'La fecha inicial no puede ser mayor que la final');
          }
          else{
            $statsExpenses = App\Gasto::select('nombre', 'fecha', 'concepto', 'monto', 'gastos.id')
                                      ->join('categoria_gastos', "categoria_gastos.id" , "=" , "gastos.idCategoriaGasto")
                                      ->where('categoria_gastos.id', "!=", '3')
                                      ->where('fecha', '>=', Carbon::createFromFormat('d/m/Y', $request -> fecha1))
                                      ->where('fecha', '<=', Carbon::createFromFormat('d/m/Y', $request -> fecha2));

            $stats = App\Venta::selectRaw("'Venta' as nombre, fecha, descripcion,
                                            (precioVenta * (cantidad)/1000 - descuento) as monto, ventas.id")
                              ->join('venta_productos', 'venta_productos.idVenta', '=', 'ventas.id')
                              ->where('ventas.T', '=', '1')
                              ->union($statsExpenses)
                              ->where('fecha', '>=', Carbon::createFromFormat('d/m/Y', $request -> fecha1))
                              ->where('fecha', '<=', Carbon::createFromFormat('d/m/Y', $request -> fecha2))
                              ->orderBy('fecha', 'desc')
                              ->get();

            $statsExpensesChart = App\Gasto::selectRaw("'Gasto' as nombre, sum(monto) as total")
                                      ->join('categoria_gastos', "categoria_gastos.id" , "=" , "gastos.idCategoriaGasto")
                                      ->where('categoria_gastos.id', "!=", '3')
                                      ->where('fecha', '>=', Carbon::createFromFormat('d/m/Y', $request -> fecha1))
                                      ->where('fecha', '<=', Carbon::createFromFormat('d/m/Y', $request -> fecha2));

            $statsChart = App\Venta::selectRaw("'Venta' as nombre,
                                            sum(precioVenta * (cantidad)/1000 - descuento) as total")
                              ->join('venta_productos', 'venta_productos.idVenta', '=', 'ventas.id')
                              ->where('ventas.T', '=', '1')
                              ->where('fecha', '>=', Carbon::createFromFormat('d/m/Y', $request -> fecha1))
                              ->where('fecha', '<=', Carbon::createFromFormat('d/m/Y', $request -> fecha2))
                              ->union($statsExpensesChart)
                              ->get();

            $f1 = $request -> fecha1;
            $f2 = $request -> fecha2;
            return view('EconomyStatistics',compact('stats', 'statsChart', 'f1','f2'));
          }
        } catch (\Exception $e) {
          return redirect('AQUATA/inicio')->with('error', 'Algo salió mal, inténtalo nuevamente');
        } catch (\Throwable $e) {
          return redirect('AQUATA/inicio')->with('error', 'Algo salió mal, inténtalo nuevamente');
        }
      }
      else{
        return redirect('AQUATA/inicio')->with('error', '¡Oops! No tienes permiso para hacer eso');
      }
    }
    else{
      return redirect('/login');
    }
  }

  public function showSales(){
    if(Auth::check()&&auth()->user()->T == 1){
      if(auth()->user()->idRol == 1 || auth()->user()->idRol == 2){
        try {
          $sales = App\Venta::select(
                                'ventas.descripcion',
                                'ventas.fecha',
                                'ventas.id',
                                'empleados.nombre',
                                'empleados.aPaterno',
                                'empleados.aMaterno')
                              ->join("usuarios", "ventas.idUsuario", '=', 'usuarios.id')
                              ->join("empleados", "empleados.id", '=', 'usuarios.idEmpleado')
                              ->where('ventas.fecha', '>=', Carbon::now()->subMonth())
                              ->where('ventas.T', '=' ,'1')
                              ->orderBy('ventas.id','desc')
                              ->get();

          $salesInfo = App\Producto::select(
                                      'venta_productos.idVenta',
                                      'productos.nombre as producto',
                                      'venta_productos.cantidad as cantidad',
                                      'venta_productos.piezas',
                                      'venta_productos.descuento',
                                      'venta_productos.precioVenta',
                                      'ventas.descripcion')
                                      ->join('venta_productos','venta_productos.idProducto','=','productos.id')
                                      ->join('ventas','venta_productos.idVenta','=','ventas.id')
                                      ->where('ventas.fecha', '>=', Carbon::now()->subMonth())
                                      ->where('ventas.T', '=' ,'1')
                                      ->orderBy('productos.id')
                                      ->get();

          $salesChart = App\VentaProducto::groupBy('idProducto')
                                          ->join('productos', 'productos.id', '=', 'venta_productos.idProducto')
                                          ->join('ventas','venta_productos.idVenta','=','ventas.id')
                                          ->where('ventas.fecha', '>=', Carbon::now()->subMonth())
                                          ->where('ventas.t','=','1')
                                          ->selectRaw('sum(((cantidad/1000)*precioVenta)-descuento) as total, productos.nombre as producto')
                                          ->get();

          return view('EconomySale',compact('sales','salesInfo','salesChart'));
        } catch (\Exception $e) {
          return redirect('AQUATA/inicio')->with('error', 'Algo salió mal, inténtalo nuevamente');
        } catch (\Throwable $ex){
          return redirect('AQUATA/inicio')->with('error', 'Algo salió mal, inténtalo nuevamente');
        }
      }
      else {
        return redirect('AQUATA/inicio')->with('error', '¡Oops! No tienes permiso para hacer eso');
      }
    }
    else{
      return redirect('/login');
    }
  }

  public function showSalesByDate(Request $request){
    if(Auth::check()&&auth()->user()->T == 1){
      if(auth()->user()->idRol == 1 || auth()->user()->idRol == 2){
        DB::beginTransaction();
        try {
          if(Carbon::createFromFormat('d/m/Y', $request -> fecha1) >= Carbon::createFromFormat('d/m/Y', $request -> fecha2)){
            return redirect('AQUATA/Economia/Ventas')->with('error', 'La fecha inicial no puede ser mayor que la final');
          }
          else {
            $sales = App\Venta::select(
                                  'ventas.descripcion',
                                  'ventas.fecha',
                                  'ventas.id',
                                  'empleados.nombre',
                                  'empleados.aPaterno',
                                  'empleados.aMaterno')
                                ->join("usuarios", "ventas.idUsuario", '=', 'usuarios.id')
                                ->join("empleados", "empleados.id", '=', 'usuarios.idEmpleado')
                                ->where('ventas.fecha', '>=', Carbon::createFromFormat('d/m/Y', $request -> fecha1))
                                ->where('ventas.fecha', '<=', Carbon::createFromFormat('d/m/Y', $request -> fecha2))
                                ->where('ventas.T', '=' ,'1')
                                ->orderBy('ventas.id','desc')
                                ->get();

            $salesInfo = App\Producto::select(
                                        'venta_productos.idVenta',
                                        'productos.nombre as producto',
                                        'venta_productos.cantidad as cantidad',
                                        'venta_productos.piezas',
                                        'venta_productos.descuento',
                                        'venta_productos.precioVenta',
                                        'productos.precio as precio')
                                        ->join('venta_productos','venta_productos.idProducto','=','productos.id')
                                        ->join('ventas','venta_productos.idVenta','=','ventas.id')
                                        ->where('ventas.fecha', '>=', Carbon::createFromFormat('d/m/Y', $request -> fecha1))
                                        ->where('ventas.fecha', '<=', Carbon::createFromFormat('d/m/Y', $request -> fecha2))
                                        ->where('ventas.T', '=' ,'1')
                                        ->orderBy('productos.id')
                                        ->get();

            $salesChart = App\VentaProducto::groupBy('idProducto')
                                            ->join('productos', 'productos.id', '=', 'venta_productos.idProducto')
                                            ->join('ventas','venta_productos.idVenta','=','ventas.id')
                                            ->where('ventas.fecha', '>=', Carbon::createFromFormat('d/m/Y', $request -> fecha1))
                                            ->where('ventas.fecha', '<=', Carbon::createFromFormat('d/m/Y', $request -> fecha2))
                                            ->where('ventas.T', '=' ,'1')
                                            ->selectRaw('sum((cantidad*precioVenta)-descuento) as total, productos.nombre as producto')
                                            ->get();

            $f1 = $request -> fecha1;
            $f2 = $request -> fecha2;

            DB::commit();
            return view('EconomySale',compact('sales','salesInfo','salesChart','f1','f2'));
          }
        } catch (\Exception $e) {
          DB::rollBack();
          return redirect('AQUATA/inicio')->with('error', 'Algo salió mal, inténtalo nuevamente');
        } catch (\Throwable $ex) {
          DB::rollBack();
          return redirect('AQUATA/inicio')->with('error', 'Algo salió mal, inténtalo nuevamente');
        }
      }
      else {
        return redirect('AQUATA/inicio')->with('error', '¡Oops! No tienes permiso para hacer eso');
      }
    }
    else{
      return redirect('/login');
    }
  }

  public function addSaleView(){
    if(Auth::check()&&auth()->user()->T == 1){
      $products = App\Producto::select(
                                'productos.id as idProducto',
                                'productos.nombre',
                                'productos.precio',
                                'inventarios.cantidadElementos',
                                'inventario_productos.id as idInventarioProducto')
                                ->join('inventario_productos','inventario_productos.idProducto','=','productos.id')
                                ->join('inventarios','inventario_productos.idInventario','=','inventarios.id')
                                ->where('productos.T','=','1')
                                ->where('inventarios.cantidadElementos','>','0')
                                ->orderBy('productos.nombre','asc')
                                ->get();

      return view('AddSale',compact('products'));
    }
    else{
      return redirect('/login');
    }
  }

  public function addSale(Request $request){
    if(Auth::check()&&auth()->user()->T == 1){
      if(auth()->user()->idRol == 1){
        DB::beginTransaction();
        try {
          $products = App\Producto::select(
                                    //'inventarios.id as idInventario',
                                    'productos.id as idProducto',
                                    'productos.nombre',
                                    'productos.precio',
                                    'inventarios.cantidadElementos',
                                    'inventario_productos.id as idInventarioProducto')
                                    ->join('inventario_productos','inventario_productos.idProducto','=','productos.id')
                                    ->join('inventarios','inventario_productos.idInventario','=','inventarios.id')
                                    ->where('productos.T','=','1')
                                    ->orderBy('productos.nombre','asc')
                                    ->get();
          $idUser = Auth::id();
          $u=App\Usuario::findOrFail($idUser);
          $user = App\Empleado::findOrFail($u->idEmpleado);

          $venta = new App\Venta;
          //  descripcion
          $venta->descripcion = $request->descripcion;
          //  fecha
          $venta->fecha = Carbon::now();
          //  usuario


          $venta->idUsuario = $user->id;
          $venta->save();
          $idVenta = $venta->id;

          //  bandera para solo crear una venta
          $bandera = false;

          foreach ($products as $product) {
            $auxBandera = "bandera-".$product->idProducto;

            if ($request->$auxBandera == 'visible') {
              //  NOTE: Solo se agregaran los renglones que sean visibles
              $ventaProducto = new \App\VentaProducto;

              //  cantidad
              $auxPeso = "peso-".$product->idProducto;
              $ventaProducto->cantidad = ($request->$auxPeso)*1000;
              //  piezas
              $auxCantidad = "cantidad-".$product->idProducto;
              $ventaProducto->piezas = $request->$auxCantidad;
              //  descuento
              $auxDescuento = "descuento-".$product->idProducto;
              $ventaProducto->descuento = $request->$auxDescuento;
              //  precioVenta
              $ventaProducto->precioVenta = $product->precio;
              //  idProducto
              $auxIdProducto = "idProducto-".$product->idProducto;
              $ventaProducto->idProducto = $request->$auxIdProducto;
              //  idVenta
              $ventaProducto->idVenta = $idVenta;
              //  idInventarioProducto
              $auxIdInventario = "idInventarioProducto-".$product->idProducto;
              $ventaProducto->idInventarioProducto = $request->$auxIdInventario;
              //  se guarda el registro
              $ventaProducto->save();

              //  NOTE: se busca el registro en inventario_productos
              $inventarioProductos = App\InventarioProducto::findOrFail($request->$auxIdInventario);

              $inventario = App\Inventario::findOrFail($inventarioProductos->idInventario);
              //  cantidad elementos
              $inventario->cantidadElementos = $inventario->cantidadElementos - $request->$auxCantidad;
              //  se guarda el registro
              $inventario->save();
            }
          }

          DB::commit();
          return redirect('AQUATA/Economia/Ventas')->with('success','Venta registrada exitosamente');
        } catch (\Exception $e) {
          DB::rollBack();
          return redirect('AQUATA/Economia/Ventas')->with('error','Algo salió mal, inténtalo nuevamente');
        } catch (\Throwable $ex) {
          DB::rollBack();
          return redirect('AQUATA/Economia/Ventas')->with('error','Algo salió mal, inténtalo nuevamente');
        }
      }
      else{
        return redirect('AQUATA/inicio')->with('error', '¡Oops! No tienes permiso para hacer eso');
      }
    }
    else{
      return redirect('/login');
    }
  }

  public function showExpenses()
  {
    if(Auth::check()&&auth()->user()->T == 1){
      if(auth()->user()->idRol == 1 || auth()->user()->idRol == 2){
        DB::beginTransaction();
        try {
          $categorys =  App\CategoriaGasto::all()
                                          ->where('id', '!=' , '3');

          $expenseschart = App\Gasto::groupBy('idCategoriaGasto')
                                    ->join('categoria_gastos', 'gastos.idCategoriaGasto', '=', 'categoria_gastos.id')
                                    ->where('gastos.fecha', '>=', Carbon::now()->subMonth())
                                    ->where('categoria_gastos.id', '!=', '3')
                                    ->selectRaw('sum(monto) as monto, categoria_gastos.nombre as nombre')
                                    ->get();

          $expenses = App\Gasto::select(
                      'gastos.id',
                      'gastos.fecha',
                      'gastos.concepto',
                      'gastos.monto',
                      'categoria_gastos.nombre as categoria',
                      'empleados.nombre',
                      'empleados.aPaterno',
                      'empleados.aMaterno')
                      ->join("categoria_gastos", "gastos.idCategoriaGasto", '=', 'categoria_gastos.id')
                      ->join("usuarios", "gastos.idUsuario", '=', 'usuarios.id')
                      ->join("empleados", "empleados.id", '=', 'usuarios.idEmpleado')
                      ->where('gastos.fecha', '>=', Carbon::now()->subMonth())
                      ->where('categoria_gastos.id', '!=', '3')
                      ->orderBy('gastos.fecha','desc')
                      ->get();

          DB::commit();
          return view('EconomyExpense',compact('categorys','expenses','expenseschart'));
        }
        catch (\Exception $e) {
          DB::rollBack();
          return redirect('AQUATA/inicio')->with('error','Algo salió mal, inténtalo nuevamente');
        }
        catch(\Throwable $ex){
          DB::rollBack();
          return redirect('AQUATA/inicio')->with('error','Algo salió mal, inténtalo nuevamente');
        }
      }
      else {
        return redirect('AQUATA/inicio')->with('error', '¡Oops! No tienes permiso para hacer eso');
      }
    }
    else{
      return redirect('/login');
    }
  }

  public function showExpensesByDate(Request $request){
    if(Auth::check()&&auth()->user()->T == 1){
      if(auth()->user()->idRol == 1 || auth()->user()->idRol == 2){
        DB::beginTransaction();
        try {
          $categorys =  App\CategoriaGasto::all()
                                          ->where('id', '!=' , '3');

          if(Carbon::createFromFormat('d/m/Y', $request -> fecha1) >= Carbon::createFromFormat('d/m/Y', $request -> fecha2)){
            return redirect('AQUATA/Economia/Gastos')->with('error', 'La fecha inicial no puede ser mayor que la final');
          }
          else{
            $expenseschart = App\Gasto::groupBy('idCategoriaGasto')
                                      ->join('categoria_gastos', 'gastos.idCategoriaGasto', '=', 'categoria_gastos.id')
                                      ->where('gastos.fecha', '>', Carbon::createFromFormat('d/m/Y', $request -> fecha1))
                                      ->where('gastos.fecha', '<', Carbon::createFromFormat('d/m/Y', $request -> fecha2))
                                      ->where('categoria_gastos.id', '!=', '3')
                                      ->selectRaw('sum(monto) as monto, categoria_gastos.nombre as nombre')
                                      ->get();

            $expenses = App\Gasto::select(
                        'gastos.id',
                        'gastos.fecha',
                        'gastos.concepto',
                        'gastos.monto',
                        'categoria_gastos.nombre as categoria',
                        'empleados.nombre',
                        'empleados.aPaterno',
                        'empleados.aMaterno')
                        ->join("categoria_gastos", "gastos.idCategoriaGasto", '=', 'categoria_gastos.id')
                        ->join("usuarios", "gastos.idUsuario", '=', 'usuarios.id')
                        ->join("empleados", "empleados.id", '=', 'usuarios.idEmpleado')
                        ->where('gastos.fecha', '>=', Carbon::createFromFormat('d/m/Y', $request -> fecha1))
                        ->where('gastos.fecha', '<=', Carbon::createFromFormat('d/m/Y', $request -> fecha2))
                        ->where('categoria_gastos.id', '!=', '3')
                        ->orderBy('gastos.fecha','desc')
                        ->get();

            $f1 = $request -> fecha1;
            $f2 = $request -> fecha2;
            DB::commit();
            return view('EconomyExpense',compact('categorys','expenses','expenseschart','f1','f2'));
          }
        }
        catch (\Exception $e) {
          DB::rollBack();
          return redirect('AQUATA/inicio')->with('error','Algo salió mal, inténtalo nuevamente');
        }
        catch (\Throwable $e){
          DB::rollBack();
          return redirect('AQUATA/inicio')->with('error','Algo salió mal, inténtalo nuevamente');
        }
      }
      else {
        return redirect('AQUATA/inicio')->with('error', '¡Oops! No tienes permiso para hacer eso');
      }
    }
    else{
      return redirect('/login');
    }
  }

  public function addExpenseView()
  {
    if(Auth::check()&&auth()->user()->T == 1){
      $categorys = App\CategoriaGasto::all()
                                      ->where('id', '>' , '3');;
      return view('AddExpense', compact('categorys'));
    }
    else{
      return redirect('/login');
    }
  }

  public function addExpense(Request $request)
  {
    if(Auth::check()&&auth()->user()->T == 1){
      if(auth()->user()->idRol == 1){
        DB::beginTransaction();
        try {
          $request->validate([
            'concepto'=>'required',
            'monto'=>'required',
            'idCategoriaGasto'=>'required'
          ]);
          $idUser = Auth::id();
          $u=App\Usuario::findOrFail($idUser);
          $user = App\Empleado::findOrFail($u->idEmpleado);

          $newExpense = new App\Gasto;
          $newExpense->concepto = $request -> concepto;
          $newExpense->monto = $request -> monto;
          $newExpense->fecha = Carbon::now();
          $newExpense->idCategoriaGasto = $request -> idCategoriaGasto;
          $newExpense->idUsuario = $user->id;

          $newExpense->save();

          DB::commit();
          return redirect('AQUATA/Economia/Gastos')->with('success','Gasto registrado con exito');
        }
        catch (\Exception $e) {
          DB::rollBack();
          return redirect('AQUATA/Economia/Gastos')->with('error','Algo salió mal, inténtalo nuevamente');
        }
        catch (\Throwable $ex){
          DB::rollBack();
          return redirect('AQUATA/Economia/Gastos')->with('error','Algo salió mal, inténtalo nuevamente');
        }
      }
      else{
        return redirect('AQUATA/inicio')->with('error', '¡Oops! No tienes permiso para hacer eso');
      }
    }
    else{
      return redirect('/login');
    }
  }

  public function cancelExpense(Request $request)
  {
    if(Auth::check()&&auth()->user()->T == 1){
      if(auth()->user()->idRol == 1){
        DB::beginTransaction();
        try {
          $request->validate([
            'id'=>'required'
          ]);

          $updateExpense = App\Gasto::findOrFail($request->id);

          if($updateExpense->idCategoriaGasto == "2"){
            //Encontrar el registro de gasto_Articulos
            $expenseArticle = App\GastoArticulo::select(
                                                    'idGasto',
                                                    'idInventarioArticulo',
                                                    'cantidadElementos')
                                                  ->where('idGasto', '=', $updateExpense->id)
                                                  ->get();

            //Encontrar el registro de Inventario_articulos
            $inventoryArticle = App\InventarioArticulo::findOrFail($expenseArticle[0]->idInventarioArticulo);

            $updateInventory = App\Inventario::findOrFail($inventoryArticle->idInventario);

            $updateInventory->cantidadElementos = ($updateInventory->cantidadElementos - $expenseArticle[0]->cantidadElementos);
            $updateInventory->save();
            //
          }
          else if($updateExpense->idCategoriaGasto == "1"){
            //Encontrar el registro de gasto_Articulos
            $expensesFood = App\GastoAlimento::select('idGasto',
                                                    'idCostal')
                                            ->where('idGasto', '=', $updateExpense->id)
                                            ->get();

            //Encontrar los registros de costal
            foreach ($expensesFood as $expenseFood) {
              $bag = App\Costal::findOrFail($expenseFood->idCostal);
              $bag->etiqueta = 'Costal devuelto';
              $bag->T = '0';
              $bag->save();

              $history = new App\HistorialInventarioAlimento;
              $history->alimentoRestante = $bag->alimentoRestante;
              $history->tamano = $bag->tamano;
              $history->etiqueta = $bag->etiqueta;
              $history->idCostal = $bag->id;
              $history->idMotivo = '4'; //el id 3 corresponde al motivo de agregar un costal
              $history->save();
            }
          }
          $updateExpense->idCategoriaGasto = "3";

          $updateExpense->save();

          DB::commit();
          return redirect('AQUATA/Economia/Gastos')->with('success','Gasto cancelado con exito');

        }catch (\Exception $e) {
          DB::rollBack();
          return redirect('AQUATA/Economia/Gastos')->with('error','Algo salió mal, inténtalo nuevamente');
        }
        catch (\Throwable $e) {
          DB::rollBack();
          return redirect('AQUATA/Economia/Gastos')->with('error','Algo salió mal, inténtalo nuevamente');
        }
      }
      else{
        return redirect('AQUATA/inicio')->with('error', '¡Oops! No tienes permiso para hacer eso');
      }
    }
    else{
      return redirect('/login');
    }
  }

  public function cancelSale(Request $request)
  {
    if(Auth::check()&&auth()->user()->T == 1){
      if(auth()->user()->idRol == 1){
        DB::beginTransaction();
        try {
          $request->validate([
            'id'=>'required'
          ]);

          $updateSale = App\Venta::findOrFail($request->id);

            //Encontrar los registros de venta_productos
          $saleProducts = App\VentaProducto::select('idVenta',
                                                    'idInventarioProducto',
                                                    'piezas')
                                                  ->where('idVenta', '=', $updateSale->id)
                                                  ->get();

          foreach ($saleProducts as $saleProduct) {
            //Encontrar el registro de Inventario_productos
            $inventoryProduct = App\InventarioProducto::findOrFail($saleProduct->idInventarioProducto);

            $updateInventory = App\Inventario::findOrFail($inventoryProduct->idInventario);
            $updateInventory->cantidadElementos = ($updateInventory->cantidadElementos + $saleProduct->piezas);
            $updateInventory->save();
          }

          $updateSale->T = "0";

          $updateSale->save();

          DB::commit();
          return redirect('AQUATA/Economia/Ventas')->with('success','Venta cancelada con exito');
        }
        catch (\Exception $e) {
          DB::rollBack();
          return redirect('AQUATA/Economia/Ventas')->with('error','Algo salió mal, inténtalo nuevamente');
        }
        catch (\Throwable $e) {
          DB::rollBack();
          return redirect('AQUATA/Economia/Ventas')->with('error','Algo salió mal, inténtalo nuevamente');
        }
      }
      else{
        return redirect('AQUATA/inicio')->with('error', '¡Oops! No tienes permiso para hacer eso');
      }
    }
    else{
      return redirect('/login');
    }
  }

  public function addExpenseCategoryView()
  {
    if(Auth::check()&&auth()->user()->T == 1){
      return view('AddExpenseCategory');
    }
    else{
      return redirect('/login');
    }
  }

  public function addExpenseCategory(Request $request){
    if(Auth::check()&&auth()->user()->T == 1){
      if(auth()->user()->idRol == 1){
        DB::beginTransaction();
        try {
          $request->validate([
            'nombre'=>'required',
            'descripcion'=>'required'
          ]);
          $newCategory = new App\CategoriaGasto;
          $newCategory->nombre = $request->nombre;
          $newCategory->descripcion = $request->descripcion;
          $newCategory->T = "1";
          $newCategory->save();

          DB::commit();
          return redirect('AQUATA/Economia/Gastos')->with('success','Categoria agregada exitosamente');
        } catch (\Exception $e) {
          DB::rollBack();
          return redirect('AQUATA/Economia/Gastos')->with('error','Algo salió mal, inténtalo nuevamente');
        } catch (\Throwable $ex) {
          DB::rollBack();
          return redirect('AQUATA/Economia/Gastos')->with('error','Algo salió mal, inténtalo nuevamente');
        }
      }
      else{
        return redirect('AQUATA/inicio')->with('error', '¡Oops! No tienes permiso para hacer eso');
      }
    }
    else{
      return redirect('/login');
    }
  }

  public function editExpenseCategory(Request $request){
    if(Auth::check()&&auth()->user()->T == 1){
      $category = App\CategoriaGasto::findOrFail($request -> id);
      return view('EditExpenseCategory',compact('category'));
    }
    else{
      return redirect('/login');
    }
  }

  public function updateExpenseCategory(Request $request){
    if(Auth::check()&&auth()->user()->T == 1){
      if(auth()->user()->idRol == 1){
        DB::beginTransaction();
        try {
          $request->validate([
            'nombre'=>'required',
            'descripcion'=>'required'
          ]);
          $category = App\CategoriaGasto::findOrFail($request->id);
          $category->timestamps = false;
          $category->nombre = $request->nombre;
          $category->descripcion = $request->descripcion;
          $category->save();

          DB::commit();
          return redirect('AQUATA/Economia/Gastos')->with('success','Edicion de categoria exitosa');
        } catch (\Exception $e) {
          DB::rollBack();
          return redirect('AQUATA/Economia/Gastos')->with('error','Algo salió mal, inténtalo nuevamente');
        } catch (\Throwable $ex) {
          DB::rollBack();
          return redirect('AQUATA/Economia/Gastos')->with('error','Algo salió mal, inténtalo nuevamente');
        }
      }
      else{
        return redirect('AQUATA/inicio')->with('error', '¡Oops! No tienes permiso para hacer eso');
      }
    }
    else{
      return redirect('/login');
    }

  }

  public function deleteExpenseCategory(Request $request){
    if(Auth::check()&&auth()->user()->T == 1){
      if(auth()->user()->idRol == 1){
        DB::beginTransaction();
        try {
          $nuevo = App\CategoriaGasto::findOrFail($request -> id);
          $nuevo->T="0";
          $nuevo->save();
          DB::commit();
          return back()->with('success','Eliminacion de categoria exitosa');
        } catch (\Exception $e) {
          DB::rollBack();
          return redirect('AQUATA/Economia/Gastos')->with('error','Algo salió mal, inténtalo nuevamente');
        } catch(\Throwable $ex) {
          DB::rollBack();
          return redirect('AQUATA/Economia/Gastos')->with('error','Algo salió mal, inténtalo nuevamente');
        }
      }
      else{
        return redirect('AQUATA/inicio')->with('error', '¡Oops! No tienes permiso para hacer eso');
      }
    }
    else{
      return redirect('/login');
    }
  }

  public function activateExpenseCategory(Request $request){
    if(Auth::check()&&auth()->user()->T == 1){
      if(auth()->user()->idRol == 1){
        DB::beginTransaction();
        try {
          $nuevo = App\CategoriaGasto::findOrFail($request -> id);
          $nuevo->T="1";
          $nuevo->save();

          DB::commit();
          return back()->with('success','Activacion de categoria exitosa');
        } catch (\Exception $e) {
          DB::rollBack();
          return redirect('AQUATA/Economia/Gastos')->with('error','Algo salió mal, inténtalo nuevamente');
        } catch (\Throwable $ex) {
          DB::rollBack();
          return redirect('AQUATA/Economia/Gastos')->with('error','Algo salió mal, inténtalo nuevamente');
        }
      }
      else{
        return redirect('AQUATA/inicio')->with('error', '¡Oops! No tienes permiso para hacer eso');
      }
    }
    else{
      return redirect('/login');
    }
  }
}
