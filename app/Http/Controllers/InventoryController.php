<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App;
use DB;
use Auth;
use App\GastoArticulo;
use Carbon\Carbon;

class InventoryController extends Controller
{
    public function showArticlesInventory( $units = null, $articleCategories = null, $articlesInventory = null, $articles = null){
      if(Auth::check()&&auth()->user()->T == 1){
        $articlesInventory = App\Inventario::select('inventario_articulos.id','inventarios.id as inventario','articulos.nombre as articulo','inventarios.cantidadElementos as elementos','articulos.cantidadUnidad as unidades','unidads.nombre as unidad','categoria_articulos.nombre as categoria')
                    ->join("inventario_articulos","inventario_articulos.idInventario",'=','inventarios.id')
                    ->join("articulos","inventario_articulos.idArticulo",'=','articulos.id')
                    ->join("unidads","articulos.idUnidad",'=','unidads.id')
                    ->join("categoria_articulos","articulos.idCategoriaArticulo",'=','categoria_articulos.id')->paginate(20);

        $articles = App\Articulo::select('articulos.id','articulos.nombre','articulos.descripcion','articulos.cantidadUnidad','articulos.T','unidads.nombre as unidad','categoria_articulos.nombre as categoria')
                    ->join("unidads","articulos.idUnidad",'=','unidads.id')
                    ->join("categoria_articulos","articulos.idCategoriaArticulo",'=','categoria_articulos.id')->get();

        $units = App\Unidad::all();
        $articleCategories = App\CategoriaArticulo::all();

        $idUser = Auth::id();
        $u=App\Usuario::findOrFail($idUser);
        $user = App\Empleado::findOrFail($u->idEmpleado);

        if(auth()->user()->idRol == 1)
        {
          return view('InventoryArticles',compact('units','articleCategories','articlesInventory','articles','user'));
        }
        else
        {
          if(auth()->user()->idRol == 3)
          {
            return view('InventoryArticles',compact('units','articleCategories','articlesInventory','articles','user'));
          }
          else
          {
            return redirect('AQUATA/inicio')->with('error', '¡Oops! no tienes permiso para hacer eso');
          }
       }
      }
      else
      {
        return redirect('/login');
      }
    }

    public function showFoodInventory( $types = null, $motives = null, $bags = null ){
      if(Auth::check()&&auth()->user()->T == 1)
      {
        $types=App\TipoAlimento::all();
        $motives = App\Motivo::all();
        $bags = App\TipoAlimento::select('costals.id','costals.etiqueta', 'costals.tamano', 'costals.alimentoRestante','tipo_alimentos.nombre','tipo_alimentos.descripcion','costals.T')
                    ->join("costals", "costals.idTipoAlimento", '=', 'tipo_alimentos.id')->where('costals.T', '=' ,'1')->orderBy('costals.T','desc')->orderBy('costals.etiqueta','desc')->paginate(20);
        $idUser = Auth::id();
        $u=App\Usuario::findOrFail($idUser);
        $user = App\Empleado::findOrFail($u->idEmpleado);


        if(auth()->user()->idRol == 1)
        {
          return view('InventoryFood',compact('types','motives','bags','user'));
        }
        else
        {
          if(auth()->user()->idRol == 3)
          {
            return view('InventoryFood',compact('types','motives','bags','user'));
          }
          else
          {
            return redirect('AQUATA/inicio')->with('error', '¡Oops! no tienes permiso para hacer eso');
          }
        }
      }
      else
      {
      return redirect('/login');
      }
    }

    public function showProductsInventory($products = null, $inventProducts = null){
      if(Auth::check()&&auth()->user()->T == 1)
      {
        $products=App\Producto::select('id','precio','nombre','descripcion','T','imagen')->orderBy("T","desc")->get();

        $inventProducts=App\InventarioProducto::select('inventario_productos.id','productos.nombre','productos.precio','inventarios.cantidadElementos')
                      ->join('inventarios','inventarios.id','=','inventario_productos.idInventario')
                      ->join('productos','productos.id','=','inventario_productos.idProducto')
                      ->orderBy('productos.nombre')->paginate(20);
        $idUser = Auth::id();
        $u=App\Usuario::findOrFail($idUser);
        $user = App\Empleado::findOrFail($u->idEmpleado);
        if(auth()->user()->idRol == 1)
        {
          return view('InventoryProducts',compact('products', 'inventProducts', 'user'));
        }
        else
        {
          if(auth()->user()->idRol == 3)
          {
            return view('InventoryProducts',compact('products', 'inventProducts', 'user'));
          }
          else
          {
            return redirect('AQUATA/inicio')->with('error', '¡Oops! no tienes permiso para hacer eso');
          }
        }
      }
      else
      {
      return redirect('/login');
      }
     }

    public function addInventoryProduct(){
      if(Auth::check()&&auth()->user()->T == 1)
      {
       $products = App\Producto::select('id','nombre','descripcion','T')->where('T','1')->get();
       $ponds = App\Estanque::select('estanques.id','estanques.nombre','truchas.tipo','estanques.cantidad')
                 ->join('truchas','truchas.id','=','estanques.idTrucha')->where('estanques.T','1')->get();
       $idUser = Auth::id();
       $u=App\Usuario::findOrFail($idUser);
       $user = App\Empleado::findOrFail($u->idEmpleado);
       if(auth()->user()->idRol == 1)
       {
         return view('AddInventoryProduct',compact('products','ponds','user'));
       }
       else
       {
         if(auth()->user()->idRol == 3)
         {
           return view('AddInventoryProduct',compact('products','ponds','user'));
         }
         else
         {
           return redirect('AQUATA/inicio')->with('error', '¡Oops! no tienes permiso para hacer eso');
         }
       }
     }
     else
     {
     return redirect('/login');
     }
    }

    public function addArticle(){
      if(Auth::check()&&auth()->user()->T == 1)
      {
       $units = App\Unidad::select('id','nombre','T')->where('T','1')->get();
       $categories = App\CategoriaArticulo::select('id','nombre', 'descripcion','T')->where('T','1')->get();
       $idUser = Auth::id();
       $u=App\Usuario::findOrFail($idUser);
       $user = App\Empleado::findOrFail($u->idEmpleado);
       if(auth()->user()->idRol == 1)
       {
         return view('AddArticle',compact('units','categories','user'));
       }
       else
       {
         if(auth()->user()->idRol == 3)
         {
           return view('AddArticle',compact('units','categories','user'));
         }
         else
         {
           return redirect('AQUATA/inicio')->with('error', '¡Oops! no tienes permiso para hacer eso');
         }
       }
     }
     else
     {
       return redirect('/login');
     }
    }

    public function editArticle($id){
      if(Auth::check()&&auth()->user()->T == 1)
      {
        $article = App\Articulo::findOrFail($id);
        $units = App\Unidad::select('id','nombre','T')->where('T','1')->get();
        $categories = App\CategoriaArticulo::select('id','nombre', 'descripcion','T')->where('T','1')->get();
        $idUser = Auth::id();
        $u=App\Usuario::findOrFail($idUser);
        $user = App\Empleado::findOrFail($u->idEmpleado);
        if(auth()->user()->idRol == 1)
        {
          return view('EditArticle',compact('article','units','categories', 'user'));
        }
        else
        {
          if(auth()->user()->idRol == 3)
          {
            return view('EditArticle',compact('article','units','categories', 'user'));
          }
          else
          {
            return redirect('AQUATA/inicio')->with('error', '¡Oops! no tienes permiso para hacer eso');
          }
        }
      }
      else
      {
      return redirect('/login');
      }

    }

    public function updateArticle(Request $request){
      if(Auth::check()&&auth()->user()->T == 1)
      {
        $idUser = Auth::id();
        $u=App\Usuario::findOrFail($idUser);
         if(auth()->user()->idRol == 1)
         {
           DB::beginTransaction();
            try {
              $article = App\Articulo::findOrFail($request->id);
              $article->nombre = $request->nombre;
              $article->descripcion = $request->descripcion;
              $article->cantidadUnidad = $request->cantidadUnidad;
              $article->idUnidad = $request->idUnidad;
              $article->idCategoriaArticulo = $request->idCategoriaArticulo;
              $article->save();

                DB::commit();

                return redirect('AQUATA/Inventario/Articulos')->with('message-edit-article','Articulo editado exitosamente');

            }
            catch (\Exception $e) {
              DB::rollBack();
              return redirect('AQUATA/Inventario/Articulos')->with('error', 'No se puedo completar la acción, intentelo de nuevo');
            }
            catch (\Throwable $e) {
              DB::rollBack();
              return redirect('AQUATA/Inventario/Articulos')->with('error', 'No se puedo completar la acción, intentelo de nuevo');
            }
         }
         else
         {
           if(auth()->user()->idRol == 3)
           {
             DB::beginTransaction();
              try {
                $article = App\Articulo::findOrFail($request->id);
                $article->nombre = $request->nombre;
                $article->descripcion = $request->descripcion;
                $article->cantidadUnidad = $request->cantidadUnidad;
                $article->idUnidad = $request->idUnidad;
                $article->idCategoriaArticulo = $request->idCategoriaArticulo;
                $article->save();

                  DB::commit();

                  return redirect('AQUATA/Inventario/Articulos')->with('message-edit-article','Articulo editado exitosamente');

              }
              catch (\Exception $e) {
                DB::rollBack();
                return redirect('AQUATA/Inventario/Articulos')->with('error', 'No se puedo completar la acción, intentelo de nuevo');
              }
              catch (\Throwable $e) {
                DB::rollBack();
                return redirect('AQUATA/Inventario/Articulos')->with('error', 'No se puedo completar la acción, intentelo de nuevo');
              }
           }
           else
           {
             return redirect('AQUATA/inicio')->with('error', '¡Oops! no tienes permiso para hacer eso');
           }
         }
       }
       else
       {
       return redirect('/login');
       }
    }

    public function addUpdateArticle(Request $request){
      if(Auth::check()&&auth()->user()->T == 1)
      {
      $idUser = Auth::id();
      $u=App\Usuario::findOrFail($idUser);
         if(auth()->user()->idRol == 1)
         {
           DB::beginTransaction();
            try {
              $article = new App\Articulo;
              $article->nombre = $request->nombre;
              $article->descripcion = $request->descripcion;
              $article->cantidadUnidad = $request->cantidadUnidad;
              $article->idUnidad = $request->idUnidad;
              $article->idCategoriaArticulo = $request->idCategoriaArticulo;
              $article->save();

              DB::commit();
              return redirect('AQUATA/Inventario/Articulos')->with('message-add-article','Articulo agregado exitosamente');
            }
            catch (\Exception $e) {
              DB::rollBack();
              return redirect('AQUATA/Inventario/Articulos')->with('error', 'No se puedo completar la acción, intentelo de nuevo');
            }
            catch (\Throwable $e) {
              DB::rollBack();
              return redirect('AQUATA/Inventario/Articulos')->with('error', 'No se puedo completar la acción, intentelo de nuevo');
            }
         }
         else
         {
           if(auth()->user()->idRol == 3)
           {
             DB::beginTransaction();
              try {
                $article = new App\Articulo;
                $article->nombre = $request->nombre;
                $article->descripcion = $request->descripcion;
                $article->cantidadUnidad = $request->cantidadUnidad;
                $article->idUnidad = $request->idUnidad;
                $article->idCategoriaArticulo = $request->idCategoriaArticulo;
                $article->save();

                DB::commit();
                return redirect('AQUATA/Inventario/Articulos')->with('message-add-article','Articulo agregado exitosamente');
              }
              catch (\Exception $e) {
                DB::rollBack();
                return redirect('AQUATA/Inventario/Articulos')->with('error', 'No se puedo completar la acción, intentelo de nuevo');
              }
              catch (\Throwable $e) {
                DB::rollBack();
                return redirect('AQUATA/Inventario/Articulos')->with('error', 'No se puedo completar la acción, intentelo de nuevo');
              }
           }
           else
           {
             return redirect('AQUATA/inicio')->with('error', '¡Oops! no tienes permiso para hacer eso');
           }
         }
       }
       else
       {
       return redirect('/login');
       }
    }

    public function convertInventoryProduct(){
      if(Auth::check()&&auth()->user()->T == 1)
      {
       $inventoryProducts = App\InventarioProducto::select('inventario_productos.id as id','inventario_productos.idInventario as idInventario','inventario_productos.idProducto as idProducto','productos.nombre as producto','inventarios.cantidadElementos as cantidadMaxima')
                     ->join('inventarios','inventarios.id','=','inventario_productos.idInventario')
                     ->join('productos','productos.id','=','inventario_productos.idProducto')
                     ->where('inventarios.cantidadElementos','>','0')->orderBy('productos.nombre')->get();
       $products = App\Producto::select('id','nombre')
                     ->where('T','=','1')->get();
       $idUser = Auth::id();
       $u=App\Usuario::findOrFail($idUser);
       $user = App\Empleado::findOrFail($u->idEmpleado);
       if(auth()->user()->idRol == 1)
       {
         return view('ConvertInventoryProduct',compact('inventoryProducts','products','user'));
       }
       else
       {
         if(auth()->user()->idRol == 3)
         {
           return view('ConvertInventoryProduct',compact('inventoryProducts','products','user'));
         }
         else
         {
           return redirect('AQUATA/inicio')->with('error', '¡Oops! no tienes permiso para hacer eso');
         }
       }
     }
     else
     {
     return redirect('/login');
     }
    }

    public function convertValidationInventoryProduct(Request $request){
      $idUser = Auth::id();
      $u=App\Usuario::findOrFail($idUser);
      $user = App\Empleado::findOrFail($u->idEmpleado);
       if(Auth::check()&&auth()->user()->T == 1)
       {
         if(auth()->user()->idRol == 1)
         {
           DB::beginTransaction();
            try {
              //return $request;
              $origin = App\InventarioProducto::findOrFail($request->origin);
              $validation = App\InventarioProducto::where('idProducto','=',$request->destination)->count();

              if($validation > 0){
                //agregar a inventario alimento
                $destination =App\InventarioProducto::where('idProducto','=',$request->destination)->first();
                $inventoryOrigin = App\Inventario::findOrFail($origin->idInventario);
                $inventoryDestination = App\Inventario::findOrFail($destination->idInventario);
                if ($inventoryDestination->id == $inventoryOrigin->id) {
                  return redirect('AQUATA/Inventario/Productos')->with('non-success','El producto es el mismo');
                }
                else {
                  $inventoryOrigin->cantidadElementos -= $request->quantityOrigin;
                  $inventoryDestination->cantidadElementos += $request->quantityDestination;
                  $inventoryOrigin->save();
                  $inventoryDestination->save();
                }
              }else{
                $inventory = new App\Inventario;
                $inventory->cantidadElementos = $request->quantityDestination;
                $inventory->save();

                $inventoryProduct = new App\InventarioProducto;
                $inventoryProduct->idInventario = $inventory->id;
                $inventoryProduct->idProducto = $request->destination;
                $inventoryProduct->save();
                $inventoryOrigin = App\Inventario::findOrFail($origin->idInventario);
                $inventoryOrigin->cantidadElementos -= $request->quantityOrigin;
                $inventoryOrigin->save();
                return redirect('AQUATA/Inventario/Productos')->with('success','El inventario ha sido actualizado correctamente');
              }
              DB::commit();
             return redirect('AQUATA/Inventario/Productos')->with('success','El inventario ha sido actualizado correctamente');

            }
            catch (\Exception $e) {
              DB::rollBack();
              return redirect('AQUATA/Inventario/Productos')->with('error', 'No se puedo completar la acción, intentelo de nuevo');
            }
            catch (\Throwable $e) {
              DB::rollBack();
              return redirect('AQUATA/Inventario/Productos')->with('error', 'No se puedo completar la acción, intentelo de nuevo');
            }
         }
         else
         {
           if(auth()->user()->idRol == 3)
           {
             DB::beginTransaction();
              try {
                //return $request;
                $origin = App\InventarioProducto::findOrFail($request->origin);
                $validation = App\InventarioProducto::where('idProducto','=',$request->destination)->count();

                if($validation > 0){
                  //agregar a inventario alimento
                  $destination =App\InventarioProducto::where('idProducto','=',$request->destination)->first();
                  $inventoryOrigin = App\Inventario::findOrFail($origin->idInventario);
                  $inventoryDestination = App\Inventario::findOrFail($destination->idInventario);
                  if ($inventoryDestination->id == $inventoryOrigin->id) {
                    return redirect('AQUATA/Inventario/Productos')->with('non-success','El producto es el mismo');
                  }
                  else {
                    $inventoryOrigin->cantidadElementos -= $request->quantityOrigin;
                    $inventoryDestination->cantidadElementos += $request->quantityDestination;
                    $inventoryOrigin->save();
                    $inventoryDestination->save();
                  }
                }else{
                  $inventory = new App\Inventario;
                  $inventory->cantidadElementos = $request->quantityDestination;
                  $inventory->save();

                  $inventoryProduct = new App\InventarioProducto;
                  $inventoryProduct->idInventario = $inventory->id;
                  $inventoryProduct->idProducto = $request->destination;
                  $inventoryProduct->save();
                  $inventoryOrigin = App\Inventario::findOrFail($origin->idInventario);
                  $inventoryOrigin->cantidadElementos -= $request->quantityOrigin;
                  $inventoryOrigin->save();
                  return redirect('AQUATA/Inventario/Productos')->with('success','El inventario ha sido actualizado correctamente');
                }
                DB::commit();
               return redirect('AQUATA/Inventario/Productos')->with('success','El inventario ha sido actualizado correctamente');

              }
              catch (\Exception $e) {
                DB::rollBack();
                return redirect('AQUATA/Inventario/Productos')->with('error', 'No se puedo completar la acción, intentelo de nuevo');
              }
              catch (\Throwable $e) {
                DB::rollBack();
                return redirect('AQUATA/Inventario/Productos')->with('error', 'No se puedo completar la acción, intentelo de nuevo');
              }
           }
           else
           {
             return redirect('AQUATA/inicio')->with('error', '¡Oops! no tienes permiso para hacer eso');
           }
         }
       }
       else
       {
       return redirect('/login');
       }
    }

    public function addValidationInventoryProduct(Request $request){
      $idUser = Auth::id();
      $u=App\Usuario::findOrFail($idUser);
      $user = App\Empleado::findOrFail($u->idEmpleado);
      if(Auth::check()&&auth()->user()->T == 1)
      {
        if(auth()->user()->idRol == 1)
        {
          DB::beginTransaction();
          try {
              //return $request;
              $validation = App\InventarioProducto::where('idProducto',$request->producto)->count();
              if($validation == 0){
                $inventory = new App\Inventario;
                $inventory->cantidadElementos = $request->cantidadProductos;
                $inventory->save();

                $inventoryProduct = new App\InventarioProducto;
                $inventoryProduct->idInventario = $inventory->id;
                $inventoryProduct->idProducto = $request->producto;
                $inventoryProduct->save();

               //return redirect('AQUATA//Inventario/Productos')->with('success','El inventario ha sido actualizado correctamente');
             }
             else {
               $inventoryProduct = App\InventarioProducto::where('idProducto',$request->producto)->first();
               $inventory = App\Inventario::findOrFail($inventoryProduct->idInventario);
               $inventory->cantidadElementos += $request->cantidadProductos;
               $inventory->save();
               //return redirect('AQUATA//Inventario/Productos')->with('non-success','El producto ya existe');
             }
             $pond = App\Estanque::findOrFail($request->estanque);
             $pond->cantidad -= $request->cantidadTruchas;
             $pond->save();

             $trout = App\Trucha::findOrFail($pond->idTrucha);
             $history = new App\HistorialEstanque();
             $history->timestamps = false;
             $history->fecha = Carbon::now();
             $history->cantidad = $pond->cantidad;
             $history->nombre = $pond->nombre;
             $history->tipo = $trout->tipo;
             $history->tamano = $pond->tamano;
             $history->peso = $pond->peso;
             $history->temperatura = $pond->temperatura;
             $history->oxigeno = $pond->oxigeno;
             $history->ph = $pond->ph;
             $history->nivelAgua = $pond->volumenAgua;
             $history->fechaIngreso = $pond->fechaIngreso;
             $history->motivo = 6;
             $history->idEstanque = $pond->id;
             $history->idUsuario = $u->id;
             $history->idTrucha = $pond->idTrucha;
             $history->save();
             DB::commit();
             return redirect('AQUATA/Inventario/Productos')->with('success','El inventario ha sido actualizado correctamente');
          }
          catch (\Exception $e) {
            DB::rollBack();
            return redirect('AQUATA/Inventario/Productos')->with('error', 'No se puedo completar la acción, intentelo de nuevo');
          }
          catch (\Throwable $e) {
            DB::rollBack();
            return redirect('AQUATA/Inventario/Productos')->with('error', 'No se puedo completar la acción, intentelo de nuevo');
          }
        }
        else
        {
          if(auth()->user()->idRol == 3)
          {
            DB::beginTransaction();
            try {
                //return $request;
                $validation = App\InventarioProducto::where('idProducto',$request->producto)->count();
                if($validation == 0){
                  $inventory = new App\Inventario;
                  $inventory->cantidadElementos = $request->cantidadProductos;
                  $inventory->save();

                  $inventoryProduct = new App\InventarioProducto;
                  $inventoryProduct->idInventario = $inventory->id;
                  $inventoryProduct->idProducto = $request->producto;
                  $inventoryProduct->save();

                 //return redirect('AQUATA//Inventario/Productos')->with('success','El inventario ha sido actualizado correctamente');
               }
               else {
                 $inventoryProduct = App\InventarioProducto::where('idProducto',$request->producto)->first();
                 $inventory = App\Inventario::findOrFail($inventoryProduct->idInventario);
                 $inventory->cantidadElementos += $request->cantidadProductos;
                 $inventory->save();
                 //return redirect('AQUATA//Inventario/Productos')->with('non-success','El producto ya existe');
               }
               $pond = App\Estanque::findOrFail($request->estanque);
               $pond->cantidad -= $request->cantidadTruchas;
               $pond->save();

               $trout = App\Trucha::findOrFail($pond->idTrucha);
               $history = new App\HistorialEstanque();
               $history->timestamps = false;
               $history->fecha = Carbon::now();
               $history->cantidad = $pond->cantidad;
               $history->nombre = $pond->nombre;
               $history->tipo = $trout->tipo;
               $history->tamano = $pond->tamano;
               $history->peso = $pond->peso;
               $history->temperatura = $pond->temperatura;
               $history->oxigeno = $pond->oxigeno;
               $history->ph = $pond->ph;
               $history->nivelAgua = $pond->volumenAgua;
               $history->fechaIngreso = $pond->fechaIngreso;
               $history->motivo = 6;
               $history->idEstanque = $pond->id;
               $history->idUsuario = $u->id;
               $history->idTrucha = $pond->idTrucha;
               $history->save();
               DB::commit();
               return redirect('AQUATA/Inventario/Productos')->with('success','El inventario ha sido actualizado correctamente');
            }
            catch (\Exception $e) {
              DB::rollBack();
              return redirect('AQUATA/Inventario/Productos')->with('error', 'No se puedo completar la acción, intentelo de nuevo');
            }
            catch (\Throwable $e) {
              DB::rollBack();
              return redirect('AQUATA/Inventario/Productos')->with('error', 'No se puedo completar la acción, intentelo de nuevo');
            }
          }
          else
          {
            return redirect('AQUATA/inicio')->with('error', '¡Oops! no tienes permiso para hacer eso');
          }
        }
      }
      else
      {
      return redirect('/login');
      }
    }

    public function addInventoryArticle()
    {
      if(Auth::check()&&auth()->user()->T == 1)
      {
        $articles = App\Articulo::select('articulos.id as idArticulo','articulos.nombre','articulos.cantidadUnidad', 'unidads.nombre as unidad')
                    ->join('unidads', "unidads.id", '=', 'articulos.idUnidad')->where('articulos.T','1')->get();
        $idUser = Auth::id();
        $u=App\Usuario::findOrFail($idUser);
        $user = App\Empleado::findOrFail($u->idEmpleado);
        if(auth()->user()->idRol == 1)
        {
          return view('AddInventoryArticle',compact('articles','user'));
        }
        else
        {
          if(auth()->user()->idRol == 3)
          {
            return view('AddInventoryArticle',compact('articles','user'));
          }
          else
          {
            return redirect('AQUATA/inicio')->with('error', '¡Oops! no tienes permiso para hacer eso');
          }
        }
      }
      else
      {
      return redirect('/login');
      }
    }

    public function addUpdateInventoryArticle(Request $request){
      $idUser = Auth::id();
      $u=App\Usuario::findOrFail($idUser);
      if(Auth::check()&&auth()->user()->T == 1)
      {
        if(auth()->user()->idRol == 1)
        {
          DB::beginTransaction();
          try {
                $validation = App\InventarioArticulo::where('idArticulo',$request->idArticulo)->count();
                if($validation == 0){
                  $inventory = new App\Inventario;
                  $inventory->cantidadElementos = $request->cantidadElementos;
                  $inventory->save();

                  $inventoryArticle = new App\InventarioArticulo;
                  $inventoryArticle->idInventario = $inventory->id;
                  $inventoryArticle->idArticulo = $request->idArticulo;
                  $inventoryArticle->save();

                  $article = App\Articulo::findOrFail($request->idArticulo);
                  $unidad = App\Unidad::findOrFail($article->idUnidad);
                  $categoria = App\CategoriaArticulo::findOrFail($article->idCategoriaArticulo);

                  $historyInventaryArticle = new App\HistorialInventarioArticulo;
                  $historyInventaryArticle->articulo = $article->nombre;
                  $historyInventaryArticle->descripcionArticulo = $article->descripcion;
                  $historyInventaryArticle->fecha = Carbon::now();
                  $historyInventaryArticle->unidad = $unidad->nombre;
                  $historyInventaryArticle->cantidad = $article->cantidadUnidad;
                  $historyInventaryArticle->categoria = $categoria->nombre;
                  $historyInventaryArticle->descripcionCategoria = $categoria->descripcion;
                  $historyInventaryArticle->cantidadElementos = $request->cantidadElementos;
                  $historyInventaryArticle->idArticulo = $request->idArticulo;
                  $historyInventaryArticle->idUsuario = $u->id;
                  $historyInventaryArticle->motivo=$request->motivo;
                  $historyInventaryArticle->save();

                  $gasto = new App\Gasto;
                  $gasto->concepto = "Se compraron ".$request->cantidadElementos." ".$article->nombre." de ".$article->cantidadUnidad." ".$unidad->nombre;
                  $gasto->monto = $request->costo;
                  $gasto->fecha = Carbon::now();
                  $gasto->idCategoriaGasto = 2;
                  $gasto->idUsuario = $u->id;
                  $gasto->save();

                  $expenseArticle = new App\GastoArticulo;
                  $expenseArticle->idGasto = $gasto->id;
                  $expenseArticle->idInventarioArticulo = $inventoryArticle->id;
                  $expenseArticle->cantidadElementos = $request->cantidadElementos;
                  $expenseArticle->save();
                  DB::commit();

                 return redirect('AQUATA/Inventario/Articulos')->with('success','El inventario ha sido actualizado correctamente');
               }
               else {
                 $inventoryArticle = App\InventarioArticulo::where('idArticulo',$request->idArticulo)->first();
                 $inventory = App\Inventario::findOrFail($inventoryArticle->idInventario);
                 $inventory->cantidadElementos = $inventory->cantidadElementos + $request->cantidadElementos;
                 $inventory->save();

                 $article = App\Articulo::findOrFail($request->idArticulo);
                 $unidad = App\Unidad::findOrFail($article->idUnidad);
                 $categoria = App\CategoriaArticulo::findOrFail($article->idCategoriaArticulo);

                 $historyInventaryArticle = new App\HistorialInventarioArticulo;
                 $historyInventaryArticle->articulo = $article->nombre;
                 $historyInventaryArticle->descripcionArticulo = $article->descripcion;
                 $historyInventaryArticle->unidad = $unidad->nombre;
                 $historyInventaryArticle->fecha = Carbon::now();
                 $historyInventaryArticle->cantidad = $article->cantidadUnidad;
                 $historyInventaryArticle->categoria = $categoria->nombre;
                 $historyInventaryArticle->descripcionCategoria = $categoria->descripcion;
                 $historyInventaryArticle->cantidadElementos = $inventory->cantidadElementos;
                 $historyInventaryArticle->idArticulo = $request->idArticulo;
                 $historyInventaryArticle->idUsuario = $u->id;
                 $historyInventaryArticle->motivo=$request->motivo;
                 $historyInventaryArticle->save();

                 $gasto = new App\Gasto;
                 $gasto->concepto = "Se compraron ".$request->cantidadElementos." ".$article->nombre." de ".$article->cantidadUnidad." ".$unidad->nombre;
                 $gasto->monto = $request->costo;
                 $gasto->fecha = Carbon::now();
                 $gasto->idCategoriaGasto = 2;
                 $gasto->idUsuario = $u->id;
                 $gasto->save();

                 $expenseArticle = new App\GastoArticulo;
                 $expenseArticle->idGasto = $gasto->id;
                 $expenseArticle->idInventarioArticulo = $inventoryArticle->id;
                 $expenseArticle->cantidadElementos = $request->cantidadElementos;
                 $expenseArticle->save();
                 DB::commit();

                 return redirect('AQUATA/Inventario/Articulos')->with('success','El inventario ha sido actualizado correctamente');
               }
          }
          catch (\Exception $e) {
            DB::rollBack();
            return redirect('AQUATA/Inventario/Articulos')->with('error', 'No se puedo completar la acción, intentelo de nuevo');
          }
          catch (\Throwable $e) {
            DB::rollBack();
            return redirect('AQUATA/Inventario/Articulos')->with('error', 'No se puedo completar la acción, intentelo de nuevo');
          }
        }
        else
        {
          if(auth()->user()->idRol == 3)
          {
            DB::beginTransaction();
            try {
                  $validation = App\InventarioArticulo::where('idArticulo',$request->idArticulo)->count();
                  if($validation == 0){
                    $inventory = new App\Inventario;
                    $inventory->cantidadElementos = $request->cantidadElementos;
                    $inventory->save();

                    $inventoryArticle = new App\InventarioArticulo;
                    $inventoryArticle->idInventario = $inventory->id;
                    $inventoryArticle->idArticulo = $request->idArticulo;
                    $inventoryArticle->save();

                    $article = App\Articulo::findOrFail($request->idArticulo);
                    $unidad = App\Unidad::findOrFail($article->idUnidad);
                    $categoria = App\CategoriaArticulo::findOrFail($article->idCategoriaArticulo);

                    $historyInventaryArticle = new App\HistorialInventarioArticulo;
                    $historyInventaryArticle->articulo = $article->nombre;
                    $historyInventaryArticle->descripcionArticulo = $article->descripcion;
                    $historyInventaryArticle->unidad = $unidad->nombre;
                    $historyInventaryArticle->fecha = Carbon::now();
                    $historyInventaryArticle->cantidad = $article->cantidadUnidad;
                    $historyInventaryArticle->categoria = $categoria->nombre;
                    $historyInventaryArticle->descripcionCategoria = $categoria->descripcion;
                    $historyInventaryArticle->cantidadElementos = $request->cantidadElementos;
                    $historyInventaryArticle->idArticulo = $request->idArticulo;
                    $historyInventaryArticle->idUsuario = $u->id;
                    $historyInventaryArticle->motivo=$request->motivo;
                    $historyInventaryArticle->save();

                    $gasto = new App\Gasto;
                    $gasto->concepto = "Se compraron ".$request->cantidadElementos." ".$article->nombre." de ".$article->cantidadUnidad." ".$unidad->nombre;
                    $gasto->monto = $request->costo;
                    $gasto->fecha = Carbon::now();
                    $gasto->idCategoriaGasto = 2;
                    $gasto->idUsuario = $u->id;
                    $gasto->save();

                    $expenseArticle = new App\GastoArticulo;
                    $expenseArticle->idGasto = $gasto->id;
                    $expenseArticle->idInventarioArticulo = $inventoryArticle->id;
                    $expenseArticle->cantidadElementos = $request->cantidadElementos;
                    $expenseArticle->save();
                    DB::commit();

                   return redirect('AQUATA/Inventario/Articulos')->with('success','El inventario ha sido actualizado correctamente');
                 }
                 else {
                   $inventoryArticle = App\InventarioArticulo::where('idArticulo',$request->idArticulo)->first();
                   $inventory = App\Inventario::findOrFail($inventoryArticle->idInventario);
                   $inventory->cantidadElementos = $inventory->cantidadElementos + $request->cantidadElementos;
                   $inventory->save();

                   $article = App\Articulo::findOrFail($request->idArticulo);
                   $unidad = App\Unidad::findOrFail($article->idUnidad);
                   $categoria = App\CategoriaArticulo::findOrFail($article->idCategoriaArticulo);

                   $historyInventaryArticle = new App\HistorialInventarioArticulo;
                   $historyInventaryArticle->articulo = $article->nombre;
                   $historyInventaryArticle->descripcionArticulo = $article->descripcion;
                   $historyInventaryArticle->unidad = $unidad->nombre;
                   $historyInventaryArticle->fecha = Carbon::now();
                   $historyInventaryArticle->cantidad = $article->cantidadUnidad;
                   $historyInventaryArticle->categoria = $categoria->nombre;
                   $historyInventaryArticle->descripcionCategoria = $categoria->descripcion;
                   $historyInventaryArticle->cantidadElementos = $inventory->cantidadElementos;
                   $historyInventaryArticle->idArticulo = $request->idArticulo;
                   $historyInventaryArticle->idUsuario = $u->id;
                   $historyInventaryArticle->motivo=$request->motivo;
                   $historyInventaryArticle->save();

                   $gasto = new App\Gasto;
                   $gasto->concepto = "Se compraron ".$request->cantidadElementos." ".$article->nombre." de ".$article->cantidadUnidad." ".$unidad->nombre;
                   $gasto->monto = $request->costo;
                   $gasto->fecha = Carbon::now();
                   $gasto->idCategoriaGasto = 2;
                   $gasto->idUsuario = $u->id;
                   $gasto->save();

                   $expenseArticle = new App\GastoArticulo;
                   $expenseArticle->idGasto = $gasto->id;
                   $expenseArticle->idInventarioArticulo = $inventoryArticle->id;
                   $expenseArticle->cantidadElementos = $request->cantidadElementos;
                   $expenseArticle->save();
                   DB::commit();

                   return redirect('AQUATA/Inventario/Articulos')->with('success','El inventario ha sido actualizado correctamente');
                 }
            }
            catch (\Exception $e) {
              DB::rollBack();
              return redirect('AQUATA/Inventario/Articulos')->with('error', 'No se puedo completar la acción, intentelo de nuevo');
            }
            catch (\Throwable $e) {
              DB::rollBack();
              return redirect('AQUATA/Inventario/Articulos')->with('error', 'No se puedo completar la acción, intentelo de nuevo');
            }
          }
          else
          {
            return redirect('AQUATA/inicio')->with('error', '¡Oops! no tienes permiso para hacer eso');
          }
        }
      }
      else
      {
      return redirect('/login');
      }

    }


    public function editInventoryProduct($id){
      if(Auth::check()&&auth()->user()->T == 1)
      {
        $inventoryProduct = App\InventarioProducto::findOrFail($id);
        $products = App\Producto::select('id','nombre','descripcion','T')->where('T','1')->get();
        $ponds = App\Estanque::select('estanques.id','estanques.nombre','truchas.tipo')
                  ->join('truchas','truchas.id','=','estanques.idTrucha')->where('estanques.T','1')->get();
        $idUser = Auth::id();
        $u=App\Usuario::findOrFail($idUser);
        $user = App\Empleado::findOrFail($u->idEmpleado);
        if(auth()->user()->idRol == 1)
        {
          return view('EditInventoryProduct',compact('inventoryProduct','products','ponds','user'));
        }
        else
        {
          if(auth()->user()->idRol == 3)
          {
            return view('EditInventoryProduct',compact('inventoryProduct','products','ponds','user'));
          }
          else
          {
            return redirect('AQUATA/inicio')->with('error', '¡Oops! no tienes permiso para hacer eso');
          }
        }
      }
      else
      {
      return redirect('/login');
      }

    }


    public function updateInventoryProduct(Request $request){
      $idUser = Auth::id();
      $u=App\Usuario::findOrFail($idUser);
      $user = App\Empleado::findOrFail($u->idEmpleado);
       if(Auth::check()&&auth()->user()->T == 1)
       {
         if(auth()->user()->idRol == 1)
         {
           DB::beginTransaction();
            try {
              $validation = App\InventarioProducto::where('idProducto',$request->producto)->where('idEstanque',$request->estanque)->count();
              if($validation == 0){
                $inventoryProduct = App\InventarioProducto::findOrFail($request->id);
                $inventoryProduct->idEstanque = $request->estanque;
                $inventoryProduct->idProducto = $request->producto;
                $inventoryProduct->save();
                DB::commit();
                return redirect('AQUATA/Inventario/Productos')->with('success','El inventario ha sido actualizado correctamente');
              }
              else {
                DB::commit();
                return redirect('AQUATA/Inventario/Productos')->with('non-success','El producto ya existe');
              }
            }
            catch (\Exception $e) {
              DB::rollBack();
              return redirect('AQUATA/Inventario/Productos')->with('error', 'No se puedo completar la acción, intentelo de nuevo');
            }
            catch (\Throwable $e) {
              DB::rollBack();
              return redirect('AQUATA/Inventario/Productos')->with('error', 'No se puedo completar la acción, intentelo de nuevo');
            }
         }
         else
         {
           if(auth()->user()->idRol == 3)
           {
             DB::beginTransaction();
              try {
                $validation = App\InventarioProducto::where('idProducto',$request->producto)->where('idEstanque',$request->estanque)->count();
                if($validation == 0){
                  $inventoryProduct = App\InventarioProducto::findOrFail($request->id);
                  $inventoryProduct->idEstanque = $request->estanque;
                  $inventoryProduct->idProducto = $request->producto;
                  $inventoryProduct->save();
                  DB::commit();
                  return redirect('AQUATA/Inventario/Productos')->with('success','El inventario ha sido actualizado correctamente');
                }
                else {
                  DB::commit();
                  return redirect('AQUATA/Inventario/Productos')->with('non-success','El producto ya existe');
                }
              }
              catch (\Exception $e) {
                DB::rollBack();
                return redirect('AQUATA/Inventario/Productos')->with('error', 'No se puedo completar la acción, intentelo de nuevo');
              }
              catch (\Throwable $e) {
                DB::rollBack();
                return redirect('AQUATA/Inventario/Productos')->with('error', 'No se puedo completar la acción, intentelo de nuevo');
              }
           }
           else
           {
             return redirect('AQUATA/inicio')->with('error', '¡Oops! no tienes permiso para hacer eso');
           }
         }
       }
       else
       {
       return redirect('/login');
       }
    }

    public function upgradeInventoryProduct($id){
      if(Auth::check()&&auth()->user()->T == 1)
      {
        $inventoryProduct = App\InventarioProducto::findOrFail($id);
        $inventory = App\Inventario::findOrFail($inventoryProduct->idInventario);
        $info = App\InventarioProducto::select('productos.nombre as producto')
                      ->join('productos','productos.id','=','inventario_productos.idProducto')
                      ->where('inventario_productos.id','=',$id)->first();
        $ponds = App\Estanque::select('estanques.id','estanques.nombre','truchas.tipo','estanques.cantidad')
                        ->join('truchas','truchas.id','=','estanques.idTrucha')->where('estanques.T','1')->get();
        $idUser = Auth::id();
        $u=App\Usuario::findOrFail($idUser);
        $user = App\Empleado::findOrFail($u->idEmpleado);
        if(auth()->user()->idRol == 1)
        {
          return view('UpgradeInventoryProduct',compact('inventoryProduct','inventory','info','ponds','user'));
        }
        else
        {
          if(auth()->user()->idRol == 3)
          {
            return view('UpgradeInventoryProduct',compact('inventoryProduct','inventory','info','ponds','user'));
          }
          else
          {
            return redirect('AQUATA/inicio')->with('error', '¡Oops! no tienes permiso para hacer eso');
          }
        }
      }
      else
      {
      return redirect('/login');
      }
    }

    public function upgradeInventoryArticle($id){
      if(Auth::check()&&auth()->user()->T == 1)
      {
        $inventoryArticle = App\InventarioArticulo::findOrFail($id);
        $inventory = App\Inventario::findOrFail($inventoryArticle->idInventario);
        $info = App\InventarioArticulo::select('articulos.nombre as articulo','articulos.cantidadUnidad as cantidadUnidad','unidads.id as idUnidad', 'categoria_articulos.id as idCategoria', 'unidads.nombre as unidad','categoria_articulos.nombre as categoria')
                      ->join('articulos','articulos.id','=','inventario_articulos.idArticulo')
                      ->join('unidads','unidads.id','=','articulos.idUnidad')
                      ->join('categoria_articulos','categoria_articulos.id','=','articulos.idCategoriaArticulo')
                      ->where('inventario_articulos.id','=',$id)->first();
        $idUser = Auth::id();
        $u=App\Usuario::findOrFail($idUser);
        $user = App\Empleado::findOrFail($u->idEmpleado);
        if(auth()->user()->idRol == 1)
        {
          return view('UpgradeInventoryArticle',compact('inventoryArticle','inventory','info','user'));
        }
        else
        {
          if(auth()->user()->idRol == 3)
          {
            return view('UpgradeInventoryArticle',compact('inventoryArticle','inventory','info','user'));
          }
          else
          {
            return redirect('AQUATA/inicio')->with('error', '¡Oops! no tienes permiso para hacer eso');
          }
        }
      }
      else
      {
      return redirect('/login');
      }
    }

    public function takeOffInventoryArticle($id){
      if(Auth::check()&&auth()->user()->T == 1)
      {
        $inventoryArticle = App\InventarioArticulo::findOrFail($id);
        $inventory = App\Inventario::findOrFail($inventoryArticle->idInventario);
        $info = App\InventarioArticulo::select('articulos.nombre as articulo','articulos.cantidadUnidad as cantidadUnidad','unidads.id as idUnidad', 'categoria_articulos.id as idCategoria', 'unidads.nombre as unidad','categoria_articulos.nombre as categoria')
                      ->join('articulos','articulos.id','=','inventario_articulos.idArticulo')
                      ->join('unidads','unidads.id','=','articulos.idUnidad')
                      ->join('categoria_articulos','categoria_articulos.id','=','articulos.idCategoriaArticulo')
                      ->where('inventario_articulos.id','=',$id)->first();
        $idUser = Auth::id();
        $u=App\Usuario::findOrFail($idUser);
        $user = App\Empleado::findOrFail($u->idEmpleado);
        if(auth()->user()->idRol == 1)
        {
          return view('TakeOffInventoryArticle',compact('inventoryArticle','inventory','info','user'));
        }
        else
        {
          if(auth()->user()->idRol == 3)
          {
            return view('TakeOffInventoryArticle',compact('inventoryArticle','inventory','info','user'));
          }
          else
          {
            return redirect('AQUATA/inicio')->with('error', '¡Oops! no tienes permiso para hacer eso');
          }
        }
      }
      else
      {
      return redirect('/login');
      }
    }

    public function takeOffValidationInventoryArticle(Request $request){
      $idUser = Auth::id();
      $u=App\Usuario::findOrFail($idUser);
      $user = App\Empleado::findOrFail($u->idEmpleado);
       if(Auth::check()&&auth()->user()->T == 1)
       {
         if(auth()->user()->idRol == 1)
         {
           DB::beginTransaction();

            try {

             $request->validate([
               'id'=>'required',
               'idInventario'=>'required',
               'idArticulo'=>'required',
               'cantidad'=>'required',
               'idUnidad'=>'required',
               'idCategoria'=>'required',
               'motivo'=>'required'
             ]);

             $inventoryArticle = App\InventarioArticulo::where('idArticulo',$request->idArticulo)->first();
             $inventory = App\Inventario::findOrFail($inventoryArticle->idInventario);
             $inventory->cantidadElementos = $inventory->cantidadElementos - $request->cantidad;
             $inventory->save();

             $article = App\Articulo::findOrFail($request->idArticulo);
             $unidad = App\Unidad::findOrFail($article->idUnidad);
             $categoria = App\CategoriaArticulo::findOrFail($article->idCategoriaArticulo);

             $historyInventaryArticle = new App\HistorialInventarioArticulo;
             $historyInventaryArticle->articulo = $article->nombre;
             $historyInventaryArticle->descripcionArticulo = $article->descripcion;
             $historyInventaryArticle->unidad = $unidad->nombre;
             $historyInventaryArticle->fecha = Carbon::now();
             $historyInventaryArticle->cantidad = $article->cantidadUnidad;
             $historyInventaryArticle->categoria = $categoria->nombre;
             $historyInventaryArticle->descripcionCategoria = $categoria->descripcion;
             $historyInventaryArticle->cantidadElementos = $inventory->cantidadElementos;
             $historyInventaryArticle->idArticulo = $request->idArticulo;
             $historyInventaryArticle->idUsuario = $u->id;
             $historyInventaryArticle->motivo=$request->motivo;
             $historyInventaryArticle->save();
             DB::commit();
             return redirect('AQUATA/Inventario/Articulos')->with('success','El inventario ha sido actualizado correctamente');
            }
            catch (\Exception $e) {
              DB::rollBack();
              return redirect('AQUATA/Inventario/Articulos')->with('error', 'No se puedo completar la acción, intentelo de nuevo');
            }
            catch (\Throwable $e) {
              DB::rollBack();
              return redirect('AQUATA/Inventario/Articulos')->with('error', 'No se puedo completar la acción, intentelo de nuevo');
            }
         }
         else
         {
           if(auth()->user()->idRol == 3)
           {
             DB::beginTransaction();

              try {

               $request->validate([
                 'id'=>'required',
                 'idInventario'=>'required',
                 'idArticulo'=>'required',
                 'cantidad'=>'required',
                 'idUnidad'=>'required',
                 'idCategoria'=>'required',
                 'motivo'=>'required'
               ]);

               $inventoryArticle = App\InventarioArticulo::where('idArticulo',$request->idArticulo)->first();
               $inventory = App\Inventario::findOrFail($inventoryArticle->idInventario);
               $inventory->cantidadElementos = $inventory->cantidadElementos - $request->cantidad;
               $inventory->save();

               $article = App\Articulo::findOrFail($request->idArticulo);
               $unidad = App\Unidad::findOrFail($article->idUnidad);
               $categoria = App\CategoriaArticulo::findOrFail($article->idCategoriaArticulo);

               $historyInventaryArticle = new App\HistorialInventarioArticulo;
               $historyInventaryArticle->articulo = $article->nombre;
               $historyInventaryArticle->descripcionArticulo = $article->descripcion;
               $historyInventaryArticle->unidad = $unidad->nombre;
               $historyInventaryArticle->fecha = Carbon::now();
               $historyInventaryArticle->cantidad = $article->cantidadUnidad;
               $historyInventaryArticle->categoria = $categoria->nombre;
               $historyInventaryArticle->descripcionCategoria = $categoria->descripcion;
               $historyInventaryArticle->cantidadElementos = $inventory->cantidadElementos;
               $historyInventaryArticle->idArticulo = $request->idArticulo;
               $historyInventaryArticle->idUsuario = $u->id;
               $historyInventaryArticle->motivo=$request->motivo;
               $historyInventaryArticle->save();
               DB::commit();
               return redirect('AQUATA/Inventario/Articulos')->with('success','El inventario ha sido actualizado correctamente');
              }
              catch (\Exception $e) {
                DB::rollBack();
                return redirect('AQUATA/Inventario/Articulos')->with('error', 'No se puedo completar la acción, intentelo de nuevo');
              }
              catch (\Throwable $e) {
                DB::rollBack();
                return redirect('AQUATA/Inventario/Articulos')->with('error', 'No se puedo completar la acción, intentelo de nuevo');
              }
           }
           else
           {
             return redirect('AQUATA/inicio')->with('error', '¡Oops! no tienes permiso para hacer eso');
           }
         }
       }
       else
       {
       return redirect('/login');
       }
    }

    public function upgradeValidationInventoryArticle(Request $request){
      $idUser = Auth::id();
      $u=App\Usuario::findOrFail($idUser);
      $user = App\Empleado::findOrFail($u->idEmpleado);
       if(Auth::check()&&auth()->user()->T == 1)
       {
         if(auth()->user()->idRol == 1)
         {
           DB::beginTransaction();
            try {
                $request->validate([
                  'id'=>'required',
                  'idInventario'=>'required',
                  'idArticulo'=>'required',
                  'cantidad'=>'required',
                  'costo'=>'required',
                  'idUnidad'=>'required',
                  'idCategoria'=>'required',
                  'motivo'=>'required'
                ]);

                $inventoryArticle = App\InventarioArticulo::where('idArticulo',$request->idArticulo)->first();
                $inventory = App\Inventario::findOrFail($inventoryArticle->idInventario);
                $inventory->cantidadElementos = $inventory->cantidadElementos + $request->cantidad;
                $inventory->save();

                $article = App\Articulo::findOrFail($request->idArticulo);
                $unidad = App\Unidad::findOrFail($article->idUnidad);
                $categoria = App\CategoriaArticulo::findOrFail($article->idCategoriaArticulo);

                $historyInventaryArticle = new App\HistorialInventarioArticulo;
                $historyInventaryArticle->articulo = $article->nombre;
                $historyInventaryArticle->descripcionArticulo = $article->descripcion;
                $historyInventaryArticle->unidad = $unidad->nombre;
                $historyInventaryArticle->fecha = Carbon::now();
                $historyInventaryArticle->cantidad = $article->cantidadUnidad;
                $historyInventaryArticle->categoria = $categoria->nombre;
                $historyInventaryArticle->descripcionCategoria = $categoria->descripcion;
                $historyInventaryArticle->cantidadElementos = $inventory->cantidadElementos;
                $historyInventaryArticle->idArticulo = $request->idArticulo;
                $historyInventaryArticle->idUsuario = $u->id;
                $historyInventaryArticle->motivo=$request->motivo;
                $historyInventaryArticle->save();

                $gasto = new App\Gasto;
                $gasto->concepto = "Se compraron ".$request->cantidad." ".$article->nombre." de ".$article->cantidadUnidad." ".$unidad->nombre;
                $gasto->monto = $request->costo;
                $gasto->fecha = Carbon::now();
                $gasto->idCategoriaGasto = 2;
                $gasto->idUsuario = $u->id;
                $gasto->save();

                $expenseArticle = new App\GastoArticulo;
                $expenseArticle->idGasto = $gasto->id;
                $expenseArticle->idInventarioArticulo = $inventoryArticle->id;
                $expenseArticle->cantidadElementos = $request->cantidad;
                $expenseArticle->save();
                DB::commit();
                return redirect('AQUATA/Inventario/Articulos')->with('success','El inventario ha sido actualizado correctamente');
            }
            catch (\Exception $e) {
              DB::rollBack();
              return redirect('AQUATA/Inventario/Articulos')->with('error', 'No se puedo completar la acción, intentelo de nuevo');
            }
            catch (\Throwable $e) {
              DB::rollBack();
              return redirect('AQUATA/Inventario/Articulos')->with('error', 'No se puedo completar la acción, intentelo de nuevo');
            }
         }
         else
         {
           if(auth()->user()->idRol == 3)
           {
             DB::beginTransaction();
              try {
                  $request->validate([
                    'id'=>'required',
                    'idInventario'=>'required',
                    'idArticulo'=>'required',
                    'cantidad'=>'required',
                    'costo'=>'required',
                    'idUnidad'=>'required',
                    'idCategoria'=>'required',
                    'motivo'=>'required'
                  ]);

                  $inventoryArticle = App\InventarioArticulo::where('idArticulo',$request->idArticulo)->first();
                  $inventory = App\Inventario::findOrFail($inventoryArticle->idInventario);
                  $inventory->cantidadElementos = $inventory->cantidadElementos + $request->cantidad;
                  $inventory->save();

                  $article = App\Articulo::findOrFail($request->idArticulo);
                  $unidad = App\Unidad::findOrFail($article->idUnidad);
                  $categoria = App\CategoriaArticulo::findOrFail($article->idCategoriaArticulo);

                  $historyInventaryArticle = new App\HistorialInventarioArticulo;
                  $historyInventaryArticle->articulo = $article->nombre;
                  $historyInventaryArticle->fecha = Carbon::now();
                  $historyInventaryArticle->descripcionArticulo = $article->descripcion;
                  $historyInventaryArticle->unidad = $unidad->nombre;
                  $historyInventaryArticle->cantidad = $article->cantidadUnidad;
                  $historyInventaryArticle->categoria = $categoria->nombre;
                  $historyInventaryArticle->descripcionCategoria = $categoria->descripcion;
                  $historyInventaryArticle->cantidadElementos = $inventory->cantidadElementos;
                  $historyInventaryArticle->idArticulo = $request->idArticulo;
                  $historyInventaryArticle->idUsuario = $u->id;
                  $historyInventaryArticle->motivo=$request->motivo;
                  $historyInventaryArticle->save();

                  $gasto = new App\Gasto;
                  $gasto->concepto = "Se compraron ".$request->cantidad." ".$article->nombre." de ".$article->cantidadUnidad." ".$unidad->nombre;
                  $gasto->monto = $request->costo;
                  $gasto->fecha = Carbon::now();
                  $gasto->idCategoriaGasto = 2;
                  $gasto->idUsuario = $u->id;
                  $gasto->save();

                  $expenseArticle = new App\GastoArticulo;
                  $expenseArticle->idGasto = $gasto->id;
                  $expenseArticle->idInventarioArticulo = $inventoryArticle->id;
                  $expenseArticle->cantidadElementos = $request->cantidad;
                  $expenseArticle->save();
                  DB::commit();
                  return redirect('AQUATA/Inventario/Articulos')->with('success','El inventario ha sido actualizado correctamente');
              }
              catch (\Exception $e) {
                DB::rollBack();
                return redirect('AQUATA/Inventario/Articulos')->with('error', 'No se puedo completar la acción, intentelo de nuevo');
              }
              catch (\Throwable $e) {
                DB::rollBack();
                return redirect('AQUATA/Inventario/Articulos')->with('error', 'No se puedo completar la acción, intentelo de nuevo');
              }
           }
           else
           {
             return redirect('AQUATA/inicio')->with('error', '¡Oops! no tienes permiso para hacer eso');
           }
         }
       }
       else
       {
       return redirect('/login');
       }
    }

    public function upgradeValidationInventoryProduct(Request $request){
      $idUser = Auth::id();
      $u=App\Usuario::findOrFail($idUser);
      $user = App\Empleado::findOrFail($u->idEmpleado);
       if(Auth::check()&&auth()->user()->T == 1)
       {
         if(auth()->user()->idRol == 1)
         {
           DB::beginTransaction();
            try {
              $inventory = App\Inventario::findOrFail($request->idInventario);
              $pond = App\Estanque::findOrFail($request->estanque);
              $motive=0;
              if ($request->cancelacion == '0') {
                $inventory->cantidadElementos += $request->cantidadProductos;
                $pond->cantidad -= $request->cantidadTruchas;
                $motive =6;
              }
              else {
                $inventory->cantidadElementos -= $request->cantidadProductos;
                $pond->cantidad += $request->cantidadTruchas;
                $motive = 7;
              }
              $inventory->save();
              $pond->save();

              $trout = App\Trucha::findOrFail($pond->idTrucha);
              $history = new App\HistorialEstanque();
              $history->timestamps = false;
              $history->fecha = Carbon::now();
              $history->cantidad = $pond->cantidad;
              $history->nombre = $pond->nombre;
              $history->tipo = $trout->tipo;
              $history->tamano = $pond->tamano;
              $history->peso = $pond->peso;
              $history->temperatura = $pond->temperatura;
              $history->oxigeno = $pond->oxigeno;
              $history->ph = $pond->ph;
              $history->nivelAgua = $pond->volumenAgua;
              $history->fechaIngreso = $pond->fechaIngreso;
              $history->motivo = $motive;
              $history->idEstanque = $pond->id;
              $history->idUsuario = $u->id;
              $history->idTrucha = $pond->idTrucha;
              $history->save();
              DB::commit();
              return redirect('AQUATA/Inventario/Productos')->with('success','El inventario ha sido actualizado correctamente');
            }
            catch (\Exception $e) {
              DB::rollBack();
              return redirect('AQUATA/Inventario/Productos')->with('error', 'No se puedo completar la acción, intentelo de nuevo');
            }
            catch (\Throwable $e) {
              DB::rollBack();
              return redirect('AQUATA/Inventario/Productos')->with('error', 'No se puedo completar la acción, intentelo de nuevo');
            }
         }
         else
         {
           if(auth()->user()->idRol == 3)
           {
             DB::beginTransaction();
              try {
                $inventory = App\Inventario::findOrFail($request->idInventario);
                $pond = App\Estanque::findOrFail($request->estanque);
                $motive=0;
                if ($request->cancelacion == '0') {
                  $inventory->cantidadElementos += $request->cantidadProductos;
                  $pond->cantidad -= $request->cantidadTruchas;
                  $motive =6;
                }
                else {
                  $inventory->cantidadElementos -= $request->cantidadProductos;
                  $pond->cantidad += $request->cantidadTruchas;
                  $motive = 7;
                }
                $inventory->save();
                $pond->save();

                $trout = App\Trucha::findOrFail($pond->idTrucha);
                $history = new App\HistorialEstanque();
                $history->timestamps = false;
                $history->fecha = Carbon::now();
                $history->cantidad = $pond->cantidad;
                $history->nombre = $pond->nombre;
                $history->tipo = $trout->tipo;
                $history->tamano = $pond->tamano;
                $history->peso = $pond->peso;
                $history->temperatura = $pond->temperatura;
                $history->oxigeno = $pond->oxigeno;
                $history->ph = $pond->ph;
                $history->nivelAgua = $pond->volumenAgua;
                $history->fechaIngreso = $pond->fechaIngreso;
                $history->motivo = $motive;
                $history->idEstanque = $pond->id;
                $history->idUsuario = $u->id;
                $history->idTrucha = $pond->idTrucha;
                $history->save();
                DB::commit();
                return redirect('AQUATA/Inventario/Productos')->with('success','El inventario ha sido actualizado correctamente');
              }
              catch (\Exception $e) {
                DB::rollBack();
                return redirect('AQUATA/Inventario/Productos')->with('error', 'No se puedo completar la acción, intentelo de nuevo');
              }
              catch (\Throwable $e) {
                DB::rollBack();
                return redirect('AQUATA/Inventario/Productos')->with('error', 'No se puedo completar la acción, intentelo de nuevo');
              }
           }
           else
           {
             return redirect('AQUATA/inicio')->with('error', '¡Oops! no tienes permiso para hacer eso');
           }
         }
       }
       else
       {
       return redirect('/login');
       }
    }

    public function disasterInventoryProduct($id){
      if(Auth::check()&&auth()->user()->T == 1)
      {
        $inventoryProduct = App\InventarioProducto::findOrFail($id);
        $inventory = App\Inventario::findOrFail($inventoryProduct->idInventario);
        $info = App\InventarioProducto::select('productos.nombre as producto')
                      ->join('productos','productos.id','=','inventario_productos.idProducto')
                      ->where('inventario_productos.id','=',$id)->first();
        $idUser = Auth::id();
        $u=App\Usuario::findOrFail($idUser);
        $user = App\Empleado::findOrFail($u->idEmpleado);
        if(auth()->user()->idRol == 1)
        {
          return view('DisasterInventoryProduct',compact('inventoryProduct','inventory','info','user'));
        }
        else
        {
          if(auth()->user()->idRol == 3)
          {
            return view('DisasterInventoryProduct',compact('inventoryProduct','inventory','info','user'));
          }
          else
          {
            return redirect('AQUATA/inicio')->with('error', '¡Oops! no tienes permiso para hacer eso');
          }
        }
      }
      else
      {
      return redirect('/login');
      }
    }

    public function disasterValidationInventoryProduct(Request $request){
      $idUser = Auth::id();
      $u=App\Usuario::findOrFail($idUser);
      $user = App\Empleado::findOrFail($u->idEmpleado);
      if(Auth::check()&&auth()->user()->T == 1)
      {
        if(auth()->user()->idRol == 1)
        {
          DB::beginTransaction();
          try {
            //return $request;
            // TODO: falta poner el id de usuario que pase por el Request
            $inventoryProduct = App\InventarioProducto::findOrFail($request->id);
            $inventory = App\Inventario::findOrFail($inventoryProduct->idInventario);
            $inventory->cantidadElementos -= $request->cantidad;
            $inventory->save();

            $product = App\Producto::findOrFail($inventoryProduct->idProducto);

            $sell = new App\Venta();
            $sell->descripcion = 'Perecieron '.$request->cantidad.' ('.$request->peso.' gr) de: "'.$product->nombre.'"';
            $sell->fecha = Carbon::now();
            $sell->idUsuario = $request->usuario;
            $sell->T = 1;
            $sell->save();

            $selled = new App\VentaProducto();
            $selled->cantidad = $request->peso;
            $selled->piezas = $request->cantidad;
            $selled->precioVenta = 0;
            $selled->descuento = $selled->precioVenta;
            $selled->idProducto = $product->id;
            $selled->idVenta = $sell->id;
            $selled->idInventarioProducto = $inventoryProduct->id;
            $selled->save();
            DB::commit();
            return redirect('AQUATA/Inventario/Productos')->with('success','El inventario ha sido actualizado correctamente');
          }
          catch (\Exception $e) {
            DB::rollBack();
            return redirect('AQUATA/Inventario/Productos')->with('error', 'No se puedo completar la acción, intentelo de nuevo');
          }
          catch (\Throwable $e) {
            DB::rollBack();
            return redirect('AQUATA/Inventario/Productos')->with('error', 'No se puedo completar la acción, intentelo de nuevo');
          }
        }
        else
        {
          if(auth()->user()->idRol == 3)
          {
            DB::beginTransaction();
            try {
              //return $request;
              // TODO: falta poner el id de usuario que pase por el Request
              $inventoryProduct = App\InventarioProducto::findOrFail($request->id);
              $inventory = App\Inventario::findOrFail($inventoryProduct->idInventario);
              $inventory->cantidadElementos -= $request->cantidad;
              $inventory->save();

              $product = App\Producto::findOrFail($inventoryProduct->idProducto);

              $sell = new App\Venta();
              $sell->descripcion = 'Perecieron '.$request->cantidad.' ('.$request->peso.' gr) de: "'.$product->nombre.'"';
              $sell->fecha = Carbon::now();
              $sell->idUsuario = $request->usuario;
              $sell->T = 1;
              $sell->save();

              $selled = new App\VentaProducto();
              $selled->cantidad = $request->peso;
              $selled->piezas = $request->cantidad;
              $selled->precioVenta = 0;
              $selled->descuento = $selled->precioVenta;
              $selled->idProducto = $product->id;
              $selled->idVenta = $sell->id;
              $selled->idInventarioProducto = $inventoryProduct->id;
              $selled->save();
              DB::commit();
              return redirect('AQUATA/Inventario/Productos')->with('success','El inventario ha sido actualizado correctamente');
            }
            catch (\Exception $e) {
              DB::rollBack();
              return redirect('AQUATA/Inventario/Productos')->with('error', 'No se puedo completar la acción, intentelo de nuevo');
            }
            catch (\Throwable $e) {
              DB::rollBack();
              return redirect('AQUATA/Inventario/Productos')->with('error', 'No se puedo completar la acción, intentelo de nuevo');
            }
          }
          else
          {
            return redirect('AQUATA/inicio')->with('error', '¡Oops! no tienes permiso para hacer eso');
          }
        }
      }
      else
      {
      return redirect('/login');
      }
    }

    public function deleteType($id){
      $idUser = Auth::id();
      $u=App\Usuario::findOrFail($idUser);
      $user = App\Empleado::findOrFail($u->idEmpleado);
      if(Auth::check()&&auth()->user()->T == 1)
      {
        if(auth()->user()->idRol == 1)
        {
          DB::beginTransaction();
          try {
            $nuevo = App\TipoAlimento::findOrFail($id);
            $nuevo->T="0";
            $nuevo->save();
            DB::commit();
            return back()->with('message-delete-type','Eliminacion de tipo exitosa');
          }
          catch (\Exception $e) {
            DB::rollBack();
            return redirect('AQUATA/Inventario/Alimento')->with('error', 'No se puedo completar la acción, intentelo de nuevo');
          }
          catch (\Throwable $e) {
            DB::rollBack();
            return redirect('AQUATA/Inventario/Alimento')->with('error', 'No se puedo completar la acción, intentelo de nuevo');
          }
        }
        else
        {
          if(auth()->user()->idRol == 3)
          {
            DB::beginTransaction();
            try {
              $nuevo = App\TipoAlimento::findOrFail($id);
              $nuevo->T="0";
              $nuevo->save();
              DB::commit();
              return back()->with('message-delete-type','Eliminacion de tipo exitosa');
            }
            catch (\Exception $e) {
              DB::rollBack();
              return redirect('AQUATA/Inventario/Alimento')->with('error', 'No se puedo completar la acción, intentelo de nuevo');
            }
            catch (\Throwable $e) {
              DB::rollBack();
              return redirect('AQUATA/Inventario/Alimento')->with('error', 'No se puedo completar la acción, intentelo de nuevo');
            }
          }
          else
          {
            return redirect('AQUATA/inicio')->with('error', '¡Oops! no tienes permiso para hacer eso');
          }
        }
      }
      else
      {
      return redirect('/login');
      }
    }

    public function deleteArticle($id){
      $idUser = Auth::id();
      $u=App\Usuario::findOrFail($idUser);
      $user = App\Empleado::findOrFail($u->idEmpleado);
      if(Auth::check()&&auth()->user()->T == 1)
      {
        if(auth()->user()->idRol == 1)
        {
          DB::beginTransaction();
          try {
            $nuevo = App\Articulo::findOrFail($id);
            $nuevo->T="0";
            $nuevo->save();
            DB::commit();
            return back()->with('message-delete-article','Articulo eliminado exitosamente');
          }
          catch (\Exception $e) {
            DB::rollBack();
            return redirect('AQUATA/Inventario/Articulos')->with('error', 'No se puedo completar la acción, intentelo de nuevo');
          }
          catch (\Throwable $e) {
            DB::rollBack();
            return redirect('AQUATA/Inventario/Articulos')->with('error', 'No se puedo completar la acción, intentelo de nuevo');
          }
        }
        else
        {
          if(auth()->user()->idRol == 3)
          {
            DB::beginTransaction();
            try {
              $nuevo = App\Articulo::findOrFail($id);
              $nuevo->T="0";
              $nuevo->save();
              DB::commit();
              return back()->with('message-delete-article','Articulo eliminado exitosamente');
            }
            catch (\Exception $e) {
              DB::rollBack();
              return redirect('AQUATA/Inventario/Articulos')->with('error', 'No se puedo completar la acción, intentelo de nuevo');
            }
            catch (\Throwable $e) {
              DB::rollBack();
              return redirect('AQUATA/Inventario/Articulos')->with('error', 'No se puedo completar la acción, intentelo de nuevo');
            }
          }
          else
          {
            return redirect('AQUATA/inicio')->with('error', '¡Oops! no tienes permiso para hacer eso');
          }
        }
      }
      else
      {
      return redirect('/login');
      }
    }

    public function activeArticle($id){
      $idUser = Auth::id();
      $u=App\Usuario::findOrFail($idUser);
      $user = App\Empleado::findOrFail($u->idEmpleado);
      if(Auth::check()&&auth()->user()->T == 1)
      {
        if(auth()->user()->idRol == 1)
        {
          DB::beginTransaction();
          try {
            $nuevo = App\Articulo::findOrFail($id);
            $nuevo->T="1";
            $nuevo->save();
            DB::commit();
            return back()->with('message-active-article','Articulo activado exitosamente');
          }
          catch (\Exception $e) {
            DB::rollBack();
            return redirect('AQUATA/Inventario/Articulos')->with('error', 'No se puedo completar la acción, intentelo de nuevo');
          }
          catch (\Throwable $e) {
            DB::rollBack();
            return redirect('AQUATA/Inventario/Articulos')->with('error', 'No se puedo completar la acción, intentelo de nuevo');
          }
        }
        else
        {
          if(auth()->user()->idRol == 3)
          {
            DB::beginTransaction();
            try {
              $nuevo = App\Articulo::findOrFail($id);
              $nuevo->T="1";
              $nuevo->save();
              DB::commit();
              return back()->with('message-active-article','Articulo activado exitosamente');
            }
            catch (\Exception $e) {
              DB::rollBack();
              return redirect('AQUATA/Inventario/Articulos')->with('error', 'No se puedo completar la acción, intentelo de nuevo');
            }
            catch (\Throwable $e) {
              DB::rollBack();
              return redirect('AQUATA/Inventario/Articulos')->with('error', 'No se puedo completar la acción, intentelo de nuevo');
            }
          }
          else
          {
            return redirect('AQUATA/inicio')->with('error', '¡Oops! no tienes permiso para hacer eso');
          }
        }
      }
      else
      {
      return redirect('/login');
      }
    }

    public function activeType($id){
      $idUser = Auth::id();
      $u=App\Usuario::findOrFail($idUser);
      $user = App\Empleado::findOrFail($u->idEmpleado);
      if(Auth::check()&&auth()->user()->T == 1)
      {
        if(auth()->user()->idRol == 1)
        {
          DB::beginTransaction();
          try {
            $nuevo = App\TipoAlimento::findOrFail($id);
            $nuevo->T="1";
            $nuevo->save();
            DB::commit();
            return back()->with('message-activate-type','Activacion de tipo exitosa');
          }
          catch (\Exception $e) {
            DB::rollBack();
            return redirect('AQUATA/Inventario/Alimento')->with('error', 'No se puedo completar la acción, intentelo de nuevo');
          }
          catch (\Throwable $e) {
            DB::rollBack();
            return redirect('AQUATA/Inventario/Alimento')->with('error', 'No se puedo completar la acción, intentelo de nuevo');
          }
        }
        else
        {
          if(auth()->user()->idRol == 3)
          {
            DB::beginTransaction();
            try {
              $nuevo = App\TipoAlimento::findOrFail($id);
              $nuevo->T="1";
              $nuevo->save();
              DB::commit();
              return back()->with('message-activate-type','Activacion de tipo exitosa');
            }
            catch (\Exception $e) {
              DB::rollBack();
              return redirect('AQUATA/Inventario/Alimento')->with('error', 'No se puedo completar la acción, intentelo de nuevo');
            }
            catch (\Throwable $e) {
              DB::rollBack();
              return redirect('AQUATA/Inventario/Alimento')->with('error', 'No se puedo completar la acción, intentelo de nuevo');
            }
          }
          else
          {
            return redirect('AQUATA/inicio')->with('error', '¡Oops! no tienes permiso para hacer eso');
          }
        }
      }
      else
      {
      return redirect('/login');
      }
    }

    public function editType($id){
      if(Auth::check()&&auth()->user()->T == 1)
      {
        $type = App\TipoAlimento::findOrFail($id);
        $idUser = Auth::id();
        $u=App\Usuario::findOrFail($idUser);
        $user = App\Empleado::findOrFail($u->idEmpleado);
        if(auth()->user()->idRol == 1)
        {
          return view('EditFoodType',compact('type','user'));
        }
        else
        {
          if(auth()->user()->idRol == 3)
          {
            return view('EditFoodType',compact('type','user'));
          }
          else
          {
            return redirect('AQUATA/inicio')->with('error', '¡Oops! no tienes permiso para hacer eso');
          }
        }
      }
      else
      {
      return redirect('/login');
      }
    }

    public function addBag(){
      if(Auth::check()&&auth()->user()->T == 1)
      {
        $foodTypes = App\TipoAlimento::select('id','nombre')->where('T','=','1')->get();
        $idUser = Auth::id();
        $u=App\Usuario::findOrFail($idUser);
        $user = App\Empleado::findOrFail($u->idEmpleado);
        if(auth()->user()->idRol == 1)
        {
          return view('AddBag',compact('foodTypes','user'));
        }
        else
        {
          if(auth()->user()->idRol == 3)
          {
            return view('AddBag',compact('foodTypes','user'));
          }
          else
          {
            return redirect('AQUATA/inicio')->with('error', '¡Oops! no tienes permiso para hacer eso');
          }
        }
      }
      else
      {
      return redirect('/login');
      }
    }
    public function addUpdateBag(Request $request){
      $idUser = Auth::id();
      $u=App\Usuario::findOrFail($idUser);
      $user = App\Empleado::findOrFail($u->idEmpleado);
      if(Auth::check()&&auth()->user()->T == 1)
      {
        if(auth()->user()->idRol == 1)
        {
          DB::beginTransaction();
          try {
            $tag = Carbon::now();
            $random = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
            $random = str_shuffle($random);
            $random = substr($random, 0,3);
            $tag = $tag->toDateString();

            $type = App\TipoAlimento::findOrFail($request->type);
            $expense = new App\Gasto;
            $expense->concepto = 'Se agregaron '.$request->quantity.' costales de "'.$type->nombre.'"';
            $expense->monto = $request->cost;
            $expense->fecha = Carbon::now();
            $expense->idCategoriaGasto = '1';
            $expense->idUsuario = $u->id;
            $expense->save();

            for ($i=1; $i <= $request->quantity; $i++) {
              $bag = new App\Costal;
              $bag->etiqueta = $random.'-'.$tag.' ('.$i.')';
              $bag->tamano = ($request->size*1000);
              $bag->alimentoRestante = ($request->size*1000);
              $bag->idTipoAlimento = $request->type;
              $bag->save();

              $history = new App\HistorialInventarioAlimento;
              $history->alimentoRestante = $bag->alimentoRestante;
              $history->fecha = Carbon::now();
              $history->tamano = $bag->tamano;
              $history->etiqueta = $bag->etiqueta;
              $history->idCostal = $bag->id;
              $history->idMotivo = '3'; //el id 3 corresponde al motivo de agregar un costal
              $history->save();

              $expenseFood = new App\GastoAlimento;
              $expenseFood->idGasto = $expense->id;
              $expenseFood->idCostal = $bag->id;
              $expenseFood->save();
            }

            DB::commit();
            return redirect('AQUATA/Inventario/Alimento')->with('success','El inventario se ha actualizado correctamente');
          }
          catch (\Exception $e) {
            DB::rollBack();
            return redirect('AQUATA/Inventario/Alimento')->with('error','Algo salió mal, intentalo nuevamente');
          }
          catch (\Throwable $e) {
            DB::rollBack();
            return redirect('AQUATA/Inventario/Alimento')->with('error','Algo salió mal, intentalo nuevamente');
          }
        }
        else
        {
          if(auth()->user()->idRol == 3)
          {
            DB::beginTransaction();
            try {
              $tag = Carbon::now();
              $random = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
              $random = str_shuffle($random);
              $random = substr($random, 0,3);
              $tag = $tag->toDateString();

              $type = App\TipoAlimento::findOrFail($request->type);
              $expense = new App\Gasto;
              $expense->concepto = 'Se agregaron '.$request->quantity.' costales de "'.$type->nombre.'"';
              $expense->monto = $request->cost;
              $expense->fecha = Carbon::now();
              $expense->idCategoriaGasto = '1';
              $expense->idUsuario = $u->id;
              $expense->save();

              for ($i=1; $i <= $request->quantity; $i++) {
                $bag = new App\Costal;
                $bag->etiqueta = $random.'-'.$tag.' ('.$i.')';
                $bag->tamano = $request->size;
                $bag->alimentoRestante = $request->size;
                $bag->idTipoAlimento = $request->type;
                $bag->save();

                $history = new App\HistorialInventarioAlimento;
                $history->alimentoRestante = $bag->alimentoRestante;
                $history->fecha = Carbon::now();
                $history->tamano = $bag->tamano;
                $history->etiqueta = $bag->etiqueta;
                $history->idCostal = $bag->id;
                $history->idMotivo = '3'; //el id 3 corresponde al motivo de agregar un costal
                $history->save();

                $expenseFood = new App\GastoAlimento;
                $expenseFood->idGasto = $expense->id;
                $expenseFood->idCostal = $bag->id;
                $expenseFood->save();
              }

              DB::commit();
              return redirect('AQUATA/Inventario/Alimento')->with('success','El inventario se ha actualizado correctamente');
            }
            catch (\Exception $e) {
              DB::rollBack();
              return redirect('AQUATA/Inventario/Alimento')->with('error','Algo salió mal, intentalo nuevamente');
            }
            catch (\Throwable $e) {
              DB::rollBack();
              return redirect('AQUATA/Inventario/Alimento')->with('error','Algo salió mal, intentalo nuevamente');
            }
          }
          else
          {
            return redirect('AQUATA/inicio')->with('error', '¡Oops! no tienes permiso para hacer eso');
          }
        }
      }
      else
      {
      return redirect('/login');
      }
    }

    public function editBag($id){
      if(Auth::check()&&auth()->user()->T == 1)
      {
        $bag = App\Costal::findOrFail($id);
        $types = App\TipoAlimento::select('nombre','id','T')->where('T','=','1')->get();
        $idUser = Auth::id();
        $u=App\Usuario::findOrFail($idUser);
        $user = App\Empleado::findOrFail($u->idEmpleado);
        if(auth()->user()->idRol == 1)
        {
          return view('EditBag',compact('bag', 'types','user'));
        }
        else
        {
          if(auth()->user()->idRol == 3)
          {
            return view('EditBag',compact('bag', 'types','user'));
          }
          else
          {
            return redirect('AQUATA/inicio')->with('error', '¡Oops! no tienes permiso para hacer eso');
          }
        }
      }
      else
      {
      return redirect('/login');
      }
    }

    public function editUnit($id){
      if(Auth::check()&&auth()->user()->T == 1)
      {
        $unit = App\Unidad::findOrFail($id);
        $idUser = Auth::id();
        $u=App\Usuario::findOrFail($idUser);
        $user = App\Empleado::findOrFail($u->idEmpleado);
        if(auth()->user()->idRol == 1)
        {
          return view('EditUnit',compact('unit','user'));
        }
        else
        {
          if(auth()->user()->idRol == 3)
          {
            return view('EditUnit',compact('unit','user'));
          }
          else
          {
            return redirect('AQUATA/inicio')->with('error', '¡Oops! no tienes permiso para hacer eso');
          }
        }
      }
      else
      {
      return redirect('/login');
      }
    }

    public function updateUnit(Request $request){
      $idUser = Auth::id();
      $u=App\Usuario::findOrFail($idUser);
      $user = App\Empleado::findOrFail($u->idEmpleado);
      if(Auth::check()&&auth()->user()->T == 1)
      {
        if(auth()->user()->idRol == 1)
        {
          DB::beginTransaction();
          try {
            $request->validate([
              'nombre'=>'required'
            ]);
            $unit = App\Unidad::findOrFail($request->id);
            $unit->timestamps = false;
            $unit->nombre = $request->nombre;
            $unit->save();
            DB::commit();
            return redirect('AQUATA/Inventario/Articulos')->with('message-edit-unit','Edicion de unidad exitosa');
          }
          catch (\Exception $e) {
            DB::rollBack();
            return redirect('AQUATA/Inventario/Articulos')->with('error', 'No se puedo completar la acción, intentelo de nuevo');
          }
          catch (\Throwable $e) {
            DB::rollBack();
            return redirect('AQUATA/Inventario/Articulos')->with('error', 'No se puedo completar la acción, intentelo de nuevo');
          }
        }
        else
        {
          if(auth()->user()->idRol == 3)
          {
            DB::beginTransaction();
            try {
              $request->validate([
                'nombre'=>'required'
              ]);
              $unit = App\Unidad::findOrFail($request->id);
              $unit->timestamps = false;
              $unit->nombre = $request->nombre;
              $unit->save();
              DB::commit();
              return redirect('AQUATA/Inventario/Articulos')->with('message-edit-unit','Edicion de unidad exitosa');
            }
            catch (\Exception $e) {
              DB::rollBack();
              return redirect('AQUATA/Inventario/Articulos')->with('error', 'No se puedo completar la acción, intentelo de nuevo');
            }
            catch (\Throwable $e) {
              DB::rollBack();
              return redirect('AQUATA/Inventario/Articulos')->with('error', 'No se puedo completar la acción, intentelo de nuevo');
            }
          }
          else
          {
            return redirect('AQUATA/inicio')->with('error', '¡Oops! no tienes permiso para hacer eso');
          }
        }
      }
      else
      {
      return redirect('/login');
      }
    }

    public function deleteBag($id){
      if(Auth::check()&&auth()->user()->T == 1)
      {
        $bag = App\Costal::findOrFail($id);
        $types = App\TipoAlimento::select('nombre','id','T')->where('T','=','1')->get();
        $motives = App\Motivo::select('nombre','id','T','direccion')->where('T','=','1')->where('direccion','=','0')->get();
        $idUser = Auth::id();
        $u=App\Usuario::findOrFail($idUser);
        $user = App\Empleado::findOrFail($u->idEmpleado);
        if(auth()->user()->idRol == 1)
        {
          return view('DeleteBag',compact('bag', 'motives','types','user'));
        }
        else
        {
          if(auth()->user()->idRol == 3)
          {
            return view('DeleteBag',compact('bag', 'motives','types','user'));
          }
          else
          {
            return redirect('AQUATA/inicio')->with('error', '¡Oops! no tienes permiso para hacer eso');
          }
        }
      }
      else
      {
      return redirect('/login');
      }
    }

    public function updateDeleteBag(Request $request){
      $idUser = Auth::id();
      $u=App\Usuario::findOrFail($idUser);
      $user = App\Empleado::findOrFail($u->idEmpleado);
      if(Auth::check()&&auth()->user()->T == 1)
      {
        if(auth()->user()->idRol == 1)
        {
          DB::beginTransaction();
          try {
            $request->validate([
              'etiqueta'=>'required',
              'tamano'=>'required',
              'alimentoRestante'=>'required',
              'idTipoAlimento'=>'required',
              'idCostal'=>'required',
              'idMotivo'=>'required',
            ]);
            $bag = App\Costal::findOrFail($request->idCostal);
            $historyInventaryFood = new App\HistorialInventarioAlimento;
            $bag->alimentoRestante = $request->alimentoRestante;
            $historyInventaryFood->fecha = Carbon::now();
            $historyInventaryFood->alimentoRestante = $request->alimentoRestante;
            $historyInventaryFood->tamano = $request->tamano;
            $historyInventaryFood->etiqueta = $request->etiqueta;
            $historyInventaryFood->idMotivo = $request->idMotivo;
            $historyInventaryFood->idCostal = $request->idCostal;
            $bag->save();
            $historyInventaryFood->save();
            DB::commit();
            return redirect('AQUATA/Inventario/Alimento')->with('message-delete-bag','Eliminacion de costal exitosa');

          }
          catch (\Exception $e) {
            DB::rollBack();
            return redirect('AQUATA/Inventario/Alimento')->with('error', 'No se puedo completar la acción, intentelo de nuevo');
          }
          catch (\Throwable $e) {
            DB::rollBack();
            return redirect('AQUATA/Inventario/Alimento')->with('error', 'No se puedo completar la acción, intentelo de nuevo');
          }
        }
        else
        {
          if(auth()->user()->idRol == 3)
          {
            DB::beginTransaction();
            try {
              $request->validate([
                'etiqueta'=>'required',
                'tamano'=>'required',
                'alimentoRestante'=>'required',
                'idTipoAlimento'=>'required',
                'idCostal'=>'required',
                'idMotivo'=>'required',
              ]);
              $bag = App\Costal::findOrFail($request->idCostal);
              $historyInventaryFood = new App\HistorialInventarioAlimento;
              $bag->alimentoRestante = $request->alimentoRestante;
              $historyInventaryFood->fecha = Carbon::now();
              $historyInventaryFood->alimentoRestante = $request->alimentoRestante;
              $historyInventaryFood->tamano = $request->tamano;
              $historyInventaryFood->etiqueta = $request->etiqueta;
              $historyInventaryFood->idMotivo = $request->idMotivo;
              $historyInventaryFood->idCostal = $request->idCostal;
              $bag->save();
              $historyInventaryFood->save();
              DB::commit();
              return redirect('AQUATA/Inventario/Alimento')->with('message-delete-bag','Eliminacion de costal exitosa');

            }
            catch (\Exception $e) {
              DB::rollBack();
              return redirect('AQUATA/Inventario/Alimento')->with('error', 'No se puedo completar la acción, intentelo de nuevo');
            }
            catch (\Throwable $e) {
              DB::rollBack();
              return redirect('AQUATA/Inventario/Alimento')->with('error', 'No se puedo completar la acción, intentelo de nuevo');
            }
          }
          else
          {
            return redirect('AQUATA/inicio')->with('error', '¡Oops! no tienes permiso para hacer eso');
          }
        }
      }
      else
      {
      return redirect('/login');
      }
    }

    public function updateBag(Request $request){
      $idUser = Auth::id();
      $u=App\Usuario::findOrFail($idUser);
      $user = App\Empleado::findOrFail($u->idEmpleado);
      if(Auth::check()&&auth()->user()->T == 1)
      {
        if(auth()->user()->idRol == 1)
        {
          DB::beginTransaction();
          try {
            $request->validate([
              'etiqueta'=>'required',
              'tamano'=>'required',
              'idTipoAlimento'=>'required'
            ]);
            $bag = App\Costal::findOrFail($request->id);
            //registro de historial
            if(
                $bag->etiqueta        !=  $request->etiqueta        ||
                $bag->tamano          !=  $request->tamano          ||
                $bag->idTipoAlimento  !=   $request->idTipoAlimento
              ){
              $history = new App\HistorialInventarioAlimento;
              $history->alimentoRestante = $bag->alimentoRestante;
              $history->fecha = Carbon::now();
              $history->tamano = $request->tamano;
              $history->etiqueta = $request->etiqueta;
              $history->idCostal = $request->id;
              $history->idMotivo = '4';
              $history->save();
            }

            $bag->timestamps = false;
            $bag->etiqueta = $request->etiqueta;
            $bag->tamano = $request->tamano;
            $bag->idTipoAlimento = $request->idTipoAlimento;
            $bag->save();

            DB::commit();
            return redirect('AQUATA/Inventario/Alimento')->with('message-edit-bag','Edicion de costal exitosa');
          }
          catch (\Exception $e) {
            DB::rollBack();
            return redirect('AQUATA/Inventario/Alimento')->with('error', 'No se puedo completar la acción, intentelo de nuevo');
          }
          catch (\Throwable $e) {
            DB::rollBack();
            return redirect('AQUATA/Inventario/Alimento')->with('error', 'No se puedo completar la acción, intentelo de nuevo');
          }
        }
        else
        {
          if(auth()->user()->idRol == 3)
          {
            DB::beginTransaction();
            try {
              $request->validate([
                'etiqueta'=>'required',
                'tamano'=>'required',
                'idTipoAlimento'=>'required'
              ]);
              $bag = App\Costal::findOrFail($request->id);
              //registro de historial
              if(
                  $bag->etiqueta        !=  $request->etiqueta        ||
                  $bag->tamano          !=  $request->tamano          ||
                  $bag->idTipoAlimento  !=   $request->idTipoAlimento
                ){
                $history = new App\HistorialInventarioAlimento;
                $history->alimentoRestante = $bag->alimentoRestante;
                $history->fecha = Carbon::now();
                $history->tamano = $request->tamano;
                $history->etiqueta = $request->etiqueta;
                $history->idCostal = $request->id;
                $history->idMotivo = '4';
                $history->save();
              }

              $bag->timestamps = false;
              $bag->etiqueta = $request->etiqueta;
              $bag->tamano = $request->tamano;
              $bag->alimentoRestante = $request->tamano;
              $bag->idTipoAlimento = $request->idTipoAlimento;
              $bag->save();

              DB::commit();
              return redirect('AQUATA/Inventario/Alimento')->with('message-edit-bag','Edicion de costal exitosa');
            }
            catch (\Exception $e) {
              DB::rollBack();
              return redirect('AQUATA/Inventario/Alimento')->with('error', 'No se puedo completar la acción, intentelo de nuevo');
            }
            catch (\Throwable $e) {
              DB::rollBack();
              return redirect('AQUATA/Inventario/Alimento')->with('error', 'No se puedo completar la acción, intentelo de nuevo');
            }
          }
          else
          {
            return redirect('AQUATA/inicio')->with('error', '¡Oops! no tienes permiso para hacer eso');
          }
        }
      }
      else
      {
      return redirect('/login');
      }
    }

    public function editMotive($id){
      if(Auth::check()&&auth()->user()->T == 1)
      {
        $motive = App\Motivo::findOrFail($id);
        $idUser = Auth::id();
        $u=App\Usuario::findOrFail($idUser);
        $user = App\Empleado::findOrFail($u->idEmpleado);
        if(auth()->user()->idRol == 1)
        {
          return view('EditFoodMotive',compact('motive','user'));
        }
        else
        {
          if(auth()->user()->idRol == 3)
          {
            return view('EditFoodMotive',compact('motive','user'));
          }
          else
          {
            return redirect('AQUATA/inicio')->with('error', '¡Oops! no tienes permiso para hacer eso');
          }
        }
      }
      else
      {
      return redirect('/login');
      }
    }

    public function editArticleCategory($id){
      if(Auth::check()&&auth()->user()->T == 1)
      {
        $category = App\CategoriaArticulo::findOrFail($id);
        $idUser = Auth::id();
        $u=App\Usuario::findOrFail($idUser);
        $user = App\Empleado::findOrFail($u->idEmpleado);
        if(auth()->user()->idRol == 1)
        {
          return view('EditArticleCategory',compact('category','user'));
        }
        else
        {
          if(auth()->user()->idRol == 3)
          {
            return view('EditArticleCategory',compact('category','user'));
          }
          else
          {
            return redirect('AQUATA/inicio')->with('error', '¡Oops! no tienes permiso para hacer eso');
          }
        }
      }
      else
      {
      return redirect('/login');
      }
    }

    public function updateArticleCategory(Request $request){
      $idUser = Auth::id();
      $u=App\Usuario::findOrFail($idUser);
      $user = App\Empleado::findOrFail($u->idEmpleado);
      if(Auth::check()&&auth()->user()->T == 1)
      {
        if(auth()->user()->idRol == 1)
        {
          DB::beginTransaction();
          try {

            $request->validate([
              'nombre'=>'required',
              'descripcion'=>'required'
            ]);
            $category = App\CategoriaArticulo::findOrFail($request->id);
            $category->timestamps = false;
            $category->nombre = $request->nombre;
            $category->descripcion = $request->descripcion;
            $category->save();
            DB::commit();
            return redirect('AQUATA/Inventario/Articulos')->with('message-edit-category','Edicion de categoria exitosa');
          }
          catch (\Exception $e) {
            DB::rollBack();
            return redirect('AQUATA/Inventario/Articulos')->with('error', 'No se puedo completar la acción, intentelo de nuevo');
          }
          catch (\Throwable $e) {
            DB::rollBack();
            return redirect('AQUATA/Inventario/Articulos')->with('error', 'No se puedo completar la acción, intentelo de nuevo');
          }
        }
        else
        {
          if(auth()->user()->idRol == 3)
          {
            DB::beginTransaction();
            try {

              $request->validate([
                'nombre'=>'required',
                'descripcion'=>'required'
              ]);
              $category = App\CategoriaArticulo::findOrFail($request->id);
              $category->timestamps = false;
              $category->nombre = $request->nombre;
              $category->descripcion = $request->descripcion;
              $category->save();
              DB::commit();
              return redirect('AQUATA/Inventario/Articulos')->with('message-edit-category','Edicion de categoria exitosa');
            }
            catch (\Exception $e) {
              DB::rollBack();
              return redirect('AQUATA/Inventario/Articulos')->with('error', 'No se puedo completar la acción, intentelo de nuevo');
            }
            catch (\Throwable $e) {
              DB::rollBack();
              return redirect('AQUATA/Inventario/Articulos')->with('error', 'No se puedo completar la acción, intentelo de nuevo');
            }
          }
          else
          {
            return redirect('AQUATA/inicio')->with('error', '¡Oops! no tienes permiso para hacer eso');
          }
        }
      }
      else
      {
      return redirect('/login');
      }
    }

    public function updateType(Request $request){
      $idUser = Auth::id();
      $u=App\Usuario::findOrFail($idUser);
      $user = App\Empleado::findOrFail($u->idEmpleado);
      if(Auth::check()&&auth()->user()->T == 1)
      {
        if(auth()->user()->idRol == 1)
        {
          DB::beginTransaction();
          try {
            $request->validate([
              'nombre'=>'required',
              'descripcion'=>'required'
            ]);
            $type = App\TipoAlimento::findOrFail($request->id);
            $type->timestamps = false;
            $type->nombre = $request->nombre;
            $type->descripcion = $request->descripcion;
            $type->save();

            DB::commit();
            return redirect('AQUATA/Inventario/Alimento')->with('message-activate-type','Edicion de tipo exitosa');
          }
          catch (\Exception $e) {
            DB::rollBack();
            return redirect('AQUATA/Inventario/Alimento')->with('error', 'No se puedo completar la acción, intentelo de nuevo');
          }
          catch (\Throwable $e) {
            DB::rollBack();
            return redirect('AQUATA/Inventario/Alimento')->with('error', 'No se puedo completar la acción, intentelo de nuevo');
          }
        }
        else
        {
          if(auth()->user()->idRol == 3)
          {
            DB::beginTransaction();
            try {
              $request->validate([
                'nombre'=>'required',
                'descripcion'=>'required'
              ]);
              $type = App\TipoAlimento::findOrFail($request->id);
              $type->timestamps = false;
              $type->nombre = $request->nombre;
              $type->descripcion = $request->descripcion;
              $type->save();

              DB::commit();
              return redirect('AQUATA/Inventario/Alimento')->with('message-activate-type','Edicion de tipo exitosa');
            }
            catch (\Exception $e) {
              DB::rollBack();
              return redirect('AQUATA/Inventario/Alimento')->with('error', 'No se puedo completar la acción, intentelo de nuevo');
            }
            catch (\Throwable $e) {
              DB::rollBack();
              return redirect('AQUATA/Inventario/Alimento')->with('error', 'No se puedo completar la acción, intentelo de nuevo');
            }
          }
          else
          {
            return redirect('AQUATA/inicio')->with('error', '¡Oops! no tienes permiso para hacer eso');
          }
        }
      }
      else
      {
      return redirect('/login');
      }
    }

    public function addType(){
      if(Auth::check()&&auth()->user()->T == 1)
      {
        $idUser = Auth::id();
        $u=App\Usuario::findOrFail($idUser);
        $user = App\Empleado::findOrFail($u->idEmpleado);
        if(auth()->user()->idRol == 1)
        {
          return view('AddType',compact('user'));
        }
        else
        {
          if(auth()->user()->idRol == 3)
          {
            return view('AddType',compact('user'));
          }
          else
          {
            return redirect('AQUATA/inicio')->with('error', '¡Oops! no tienes permiso para hacer eso');
          }
        }
      }
      else
      {
      return redirect('/login');
      }
    }

    public function addArticleCategory(){
      if(Auth::check()&&auth()->user()->T == 1)
      {
        $idUser = Auth::id();
        $u=App\Usuario::findOrFail($idUser);
        $user = App\Empleado::findOrFail($u->idEmpleado);
        if(auth()->user()->idRol == 1)
        {
          return view('AddArticleCategory',compact('user'));
        }
        else
        {
          if(auth()->user()->idRol == 3)
          {
            return view('AddArticleCategory',compact('user'));
          }
          else
          {
            return redirect('AQUATA/inicio')->with('error', '¡Oops! no tienes permiso para hacer eso');
          }
        }
      }
      else
      {
      return redirect('/login');
      }
    }

    public function addUpdateArticleCategory(Request $request){
      $idUser = Auth::id();
      $u=App\Usuario::findOrFail($idUser);
      $user = App\Empleado::findOrFail($u->idEmpleado);
      if(Auth::check()&&auth()->user()->T == 1)
      {
        if(auth()->user()->idRol == 1)
        {
          DB::beginTransaction();
          try {

            $request->validate([
              'nombre'=>'required',
              'descripcion'=>'required'
            ]);
            $type = new App\CategoriaArticulo;
            $type->timestamps = false;
            $type->nombre = $request->nombre;
            $type->descripcion = $request->descripcion;
            $type->save();
            DB::commit();

            return redirect('AQUATA/Inventario/Articulos')->with('message-add-category','Agregado de categoria exitoso');

          }
          catch (\Exception $e) {
            DB::rollBack();
            return redirect('AQUATA/Inventario/Articulos')->with('error', 'No se puedo completar la acción, intentelo de nuevo');
          }
          catch (\Throwable $e) {
            DB::rollBack();
            return redirect('AQUATA/Inventario/Articulos')->with('error', 'No se puedo completar la acción, intentelo de nuevo');
          }
        }
        else
        {
          if(auth()->user()->idRol == 3)
          {
            DB::beginTransaction();
            try {

              $request->validate([
                'nombre'=>'required',
                'descripcion'=>'required'
              ]);
              $type = new App\CategoriaArticulo;
              $type->timestamps = false;
              $type->nombre = $request->nombre;
              $type->descripcion = $request->descripcion;
              $type->save();
              DB::commit();

              return redirect('AQUATA/Inventario/Articulos')->with('message-add-category','Agregado de categoria exitoso');

            }
            catch (\Exception $e) {
              DB::rollBack();
              return redirect('AQUATA/Inventario/Articulos')->with('error', 'No se puedo completar la acción, intentelo de nuevo');
            }
            catch (\Throwable $e) {
              DB::rollBack();
              return redirect('AQUATA/Inventario/Articulos')->with('error', 'No se puedo completar la acción, intentelo de nuevo');
            }
          }
          else
          {
            return redirect('AQUATA/inicio')->with('error', '¡Oops! no tienes permiso para hacer eso');
          }
        }
      }
      else
      {
      return redirect('/login');
      }
    }

    public function addMotive(){
      if(Auth::check()&&auth()->user()->T == 1)
      {
        $idUser = Auth::id();
        $u=App\Usuario::findOrFail($idUser);
        $user = App\Empleado::findOrFail($u->idEmpleado);
        if(auth()->user()->idRol == 1)
        {
          return view('AddMotive',compact('user'));
        }
        else
        {
          if(auth()->user()->idRol == 3)
          {
            return view('AddMotive',compact('user'));
          }
          else
          {
            return redirect('AQUATA/inicio')->with('error', '¡Oops! no tienes permiso para hacer eso');
          }
        }
      }
      else
      {
      return redirect('/login');
      }
    }

    public function addUnit(){
      if(Auth::check()&&auth()->user()->T == 1)
      {
        $idUser = Auth::id();
        $u=App\Usuario::findOrFail($idUser);
        $user = App\Empleado::findOrFail($u->idEmpleado);
        if(auth()->user()->idRol == 1)
        {
          return view('AddUnit',compact('user'));
        }
        else
        {
          if(auth()->user()->idRol == 3)
          {
            return view('AddUnit',compact('user'));
          }
          else
          {
            return redirect('AQUATA/inicio')->with('error', '¡Oops! no tienes permiso para hacer eso');
          }
        }
      }
      else
      {
      return redirect('/login');
      }
    }

    public function addUpdateUnit(Request $request){
      $idUser = Auth::id();
      $u=App\Usuario::findOrFail($idUser);
      $user = App\Empleado::findOrFail($u->idEmpleado);
      if(Auth::check()&&auth()->user()->T == 1)
      {
        if(auth()->user()->idRol == 1)
        {
          DB::beginTransaction();
          try {

            $request->validate([
              'nombre'=>'required'
            ]);
            $unit = new App\Unidad;
            $unit->timestamps = false;
            $unit->nombre = $request->nombre;
            $unit->save();
            DB::commit();

            return redirect('AQUATA/Inventario/Articulos')->with('message-add-unit','La unidad fue agregada exitosamente');

          }
          catch (\Exception $e) {
            DB::rollBack();
            return redirect('AQUATA/Inventario/Articulos')->with('error', 'No se puedo completar la acción, intentelo de nuevo');
          }
          catch (\Throwable $e) {
            DB::rollBack();
            return redirect('AQUATA/Inventario/Articulos')->with('error', 'No se puedo completar la acción, intentelo de nuevo');
          }
        }
        else
        {
          if(auth()->user()->idRol == 3)
          {
            DB::beginTransaction();
            try {

              $request->validate([
                'nombre'=>'required'
              ]);
              $unit = new App\Unidad;
              $unit->timestamps = false;
              $unit->nombre = $request->nombre;
              $unit->save();
              DB::commit();

              return redirect('AQUATA/Inventario/Articulos')->with('message-add-unit','La unidad fue agregada exitosamente');

            }
            catch (\Exception $e) {
              DB::rollBack();
              return redirect('AQUATA/Inventario/Articulos')->with('error', 'No se puedo completar la acción, intentelo de nuevo');
            }
            catch (\Throwable $e) {
              DB::rollBack();
              return redirect('AQUATA/Inventario/Articulos')->with('error', 'No se puedo completar la acción, intentelo de nuevo');
            }
          }
          else
          {
            return redirect('AQUATA/inicio')->with('error', '¡Oops! no tienes permiso para hacer eso');
          }
        }
      }
      else
      {
      return redirect('/login');
      }
    }

    public function addUpdateMotive(Request $request){
      $idUser = Auth::id();
      $u=App\Usuario::findOrFail($idUser);
      $user = App\Empleado::findOrFail($u->idEmpleado);
      if(Auth::check()&&auth()->user()->T == 1)
      {
        if(auth()->user()->idRol == 1)
        {
          DB::beginTransaction();
          try {

            $request->validate([
              'nombre'=>'required',
              'descripcion'=>'required',
              'direccion'=>'required'
            ]);
            $motive = new App\Motivo;
            $motive->timestamps = false;
            $motive->nombre = $request->nombre;
            $motive->descripcion = $request->descripcion;
            $motive->direccion = $request->direccion;
            $motive->save();
            DB::commit();

            return redirect('AQUATA/Inventario/Alimento')->with('message-add-motive','El motivo fue agregado exitosamente');
          }
          catch (\Exception $e) {
            DB::rollBack();
            return redirect('AQUATA/Inventario/Alimento')->with('error', 'No se puedo completar la acción, intentelo de nuevo');
          }
          catch (\Throwable $e) {
            DB::rollBack();
            return redirect('AQUATA/Inventario/Alimento')->with('error', 'No se puedo completar la acción, intentelo de nuevo');
          }
        }
        else
        {
          if(auth()->user()->idRol == 3)
          {
            DB::beginTransaction();
            try {

              $request->validate([
                'nombre'=>'required',
                'descripcion'=>'required',
                'direccion'=>'required'
              ]);
              $motive = new App\Motivo;
              $motive->timestamps = false;
              $motive->nombre = $request->nombre;
              $motive->descripcion = $request->descripcion;
              $motive->direccion = $request->direccion;
              $motive->save();
              DB::commit();
              return redirect('AQUATA/Inventario/Alimento')->with('message-add-motive','El motivo fue agregado exitosamente');
            }
            catch (\Exception $e) {
              DB::rollBack();
              return redirect('AQUATA/Inventario/Alimento')->with('error', 'No se puedo completar la acción, intentelo de nuevo');
            }
            catch (\Throwable $e) {
              DB::rollBack();
              return redirect('AQUATA/Inventario/Alimento')->with('error', 'No se puedo completar la acción, intentelo de nuevo');
            }
          }
          else
          {
            return redirect('AQUATA/inicio')->with('error', '¡Oops! no tienes permiso para hacer eso');
          }
        }
      }
      else
      {
      return redirect('/login');
      }
    }

    public function addUpdateType(Request $request){
      $idUser = Auth::id();
      $u=App\Usuario::findOrFail($idUser);
      $user = App\Empleado::findOrFail($u->idEmpleado);
      if(Auth::check()&&auth()->user()->T == 1)
      {
        if(auth()->user()->idRol == 1)
        {
          DB::beginTransaction();
          try {

            $request->validate([
              'nombre'=>'required',
              'descripcion'=>'required'
            ]);
            $type = new App\TipoAlimento;
            $type->timestamps = false;
            $type->nombre = $request->nombre;
            $type->descripcion = $request->descripcion;
            $type->save();
            DB::commit();

            return redirect('AQUATA/Inventario/Alimento')->with('message-add-type','Agregado de tipo exitoso');
          }
          catch (\Exception $e) {
            DB::rollBack();
            return redirect('AQUATA/Inventario/Alimento')->with('error', 'No se puedo completar la acción, intentelo de nuevo');
          }
          catch (\Throwable $e) {
            DB::rollBack();
            return redirect('AQUATA/Inventario/Alimento')->with('error', 'No se puedo completar la acción, intentelo de nuevo');
          }
        }
        else
        {
          if(auth()->user()->idRol == 3)
          {
            DB::beginTransaction();
            try {

              $request->validate([
                'nombre'=>'required',
                'descripcion'=>'required'
              ]);
              $type = new App\TipoAlimento;
              $type->timestamps = false;
              $type->nombre = $request->nombre;
              $type->descripcion = $request->descripcion;
              $type->save();
              DB::commit();

              return redirect('AQUATA/Inventario/Alimento')->with('message-add-type','Agregado de tipo exitoso');
            }
            catch (\Exception $e) {
              DB::rollBack();
              return redirect('AQUATA/Inventario/Alimento')->with('error', 'No se puedo completar la acción, intentelo de nuevo');
            }
            catch (\Throwable $e) {
              DB::rollBack();
              return redirect('AQUATA/Inventario/Alimento')->with('error', 'No se puedo completar la acción, intentelo de nuevo');
            }
          }
          else
          {
            return redirect('AQUATA/inicio')->with('error', '¡Oops! no tienes permiso para hacer eso');
          }
        }
      }
      else
      {
      return redirect('/login');
      }
    }

    public function updateMotive(Request $request){
      $idUser = Auth::id();
      $u=App\Usuario::findOrFail($idUser);
      $user = App\Empleado::findOrFail($u->idEmpleado);
      if(Auth::check()&&auth()->user()->T == 1)
      {
        if(auth()->user()->idRol == 1)
        {
          DB::beginTransaction();
          try {

            $request->validate([
              'nombre'=>'required',
              'descripcion'=>'required',
              'direccion'=>'required'
            ]);
            $motive = App\Motivo::findOrFail($request->id);
            $motive->timestamps = false;
            $motive->nombre = $request->nombre;
            $motive->descripcion = $request->descripcion;
            $motive->direccion = $request->direccion;
            $motive->save();
            DB::commit();

            return redirect('AQUATA/Inventario/Alimento')->with('message-edit-motive','Edicion de motivo exitosa');
          }
          catch (\Exception $e) {
            DB::rollBack();
            return redirect('AQUATA/Inventario/Alimento')->with('error', 'No se puedo completar la acción, intentelo de nuevo');
          }
          catch (\Throwable $e) {
            DB::rollBack();
            return redirect('AQUATA/Inventario/Alimento')->with('error', 'No se puedo completar la acción, intentelo de nuevo');
          }
        }
        else
        {
          if(auth()->user()->idRol == 3)
          {
            DB::beginTransaction();
            try {

              $request->validate([
                'nombre'=>'required',
                'descripcion'=>'required',
                'direccion'=>'required'
              ]);
              $motive = App\Motivo::findOrFail($request->id);
              $motive->timestamps = false;
              $motive->nombre = $request->nombre;
              $motive->descripcion = $request->descripcion;
              $motive->direccion = $request->direccion;
              $motive->save();
              DB::commit();
              return redirect('AQUATA/Inventario/Alimento')->with('message-edit-motive','Edicion de motivo exitosa');
            }
            catch (\Exception $e) {
              DB::rollBack();
              return redirect('AQUATA/Inventario/Alimento')->with('error', 'No se puedo completar la acción, intentelo de nuevo');
            }
            catch (\Throwable $e) {
              DB::rollBack();
              return redirect('AQUATA/Inventario/Alimento')->with('error', 'No se puedo completar la acción, intentelo de nuevo');
            }
          }
          else
          {
            return redirect('AQUATA/inicio')->with('error', '¡Oops! no tienes permiso para hacer eso');
          }
        }
      }
      else
      {
      return redirect('/login');
      }
    }

    public function deleteMotive($id){
      $idUser = Auth::id();
      $u=App\Usuario::findOrFail($idUser);
      $user = App\Empleado::findOrFail($u->idEmpleado);
      if(Auth::check()&&auth()->user()->T == 1)
      {
        if(auth()->user()->idRol == 1)
        {
          DB::beginTransaction();
          try {
            $nuevo = App\Motivo::findOrFail($id);
            $nuevo->T="0";
            $nuevo->save();
            DB::commit();
            return back()->with('message-delete-motive','Eliminacion de motivo exitosa');;
          }
          catch (\Exception $e) {
            DB::rollBack();
            return redirect('AQUATA/Inventario/Alimento')->with('error', 'No se puedo completar la acción, intentelo de nuevo');
          }
          catch (\Throwable $e) {
            DB::rollBack();
            return redirect('AQUATA/Inventario/Alimento')->with('error', 'No se puedo completar la acción, intentelo de nuevo');
          }
        }
        else
        {
          if(auth()->user()->idRol == 3)
          {
            DB::beginTransaction();
            try {
              $nuevo = App\Motivo::findOrFail($id);
              $nuevo->T="0";
              $nuevo->save();
              DB::commit();
              return back()->with('message-delete-motive','Eliminacion de motivo exitosa');;
            }
            catch (\Exception $e) {
              DB::rollBack();
              return redirect('AQUATA/Inventario/Alimento')->with('error', 'No se puedo completar la acción, intentelo de nuevo');
            }
            catch (\Throwable $e) {
              DB::rollBack();
              return redirect('AQUATA/Inventario/Alimento')->with('error', 'No se puedo completar la acción, intentelo de nuevo');
            }
          }
          else
          {
            return redirect('AQUATA/inicio')->with('error', '¡Oops! no tienes permiso para hacer eso');
          }
        }
      }
      else
      {
      return redirect('/login');
      }
    }

    public function activeMotive($id){
      $idUser = Auth::id();
      $u=App\Usuario::findOrFail($idUser);
      $user = App\Empleado::findOrFail($u->idEmpleado);
      if(Auth::check()&&auth()->user()->T == 1)
      {
        if(auth()->user()->idRol == 1)
        {
          DB::beginTransaction();
          try {

            $nuevo = App\Motivo::findOrFail($id);
            $nuevo->T="1";
            $nuevo->save();
            DB::commit();

            return back()->with('message-activate-motive','Activacion de motivo exitosa');
          }
          catch (\Exception $e) {
            DB::rollBack();
            return redirect('AQUATA/Inventario/Alimento')->with('error', 'No se puedo completar la acción, intentelo de nuevo');
          }
          catch (\Throwable $e) {
            DB::rollBack();
            return redirect('AQUATA/Inventario/Alimento')->with('error', 'No se puedo completar la acción, intentelo de nuevo');
          }
        }
        else
        {
          if(auth()->user()->idRol == 3)
          {
            DB::beginTransaction();
            try {

              $nuevo = App\Motivo::findOrFail($id);
              $nuevo->T="1";
              $nuevo->save();
              DB::commit();

              return back()->with('message-activate-motive','Activacion de motivo exitosa');
            }
            catch (\Exception $e) {
              DB::rollBack();
              return redirect('AQUATA/Inventario/Alimento')->with('error', 'No se puedo completar la acción, intentelo de nuevo');
            }
            catch (\Throwable $e) {
              DB::rollBack();
              return redirect('AQUATA/Inventario/Alimento')->with('error', 'No se puedo completar la acción, intentelo de nuevo');
            }
          }
          else
          {
            return redirect('AQUATA/inicio')->with('error', '¡Oops! no tienes permiso para hacer eso');
          }
        }
      }
      else
      {
      return redirect('/login');
      }
    }

    public function deleteUnit($id){
      $idUser = Auth::id();
      $u=App\Usuario::findOrFail($idUser);
      $user = App\Empleado::findOrFail($u->idEmpleado);
      if(Auth::check()&&auth()->user()->T == 1)
      {
        if(auth()->user()->idRol == 1)
        {
          DB::beginTransaction();
          try {
            $nuevo = App\Unidad::findOrFail($id);
            $nuevo->T="0";
            $nuevo->save();
            DB::commit();

            return back()->with('message-delete-unit','Eliminacion de unidad exitosa');
          }
          catch (\Exception $e) {
            DB::rollBack();
            return redirect('AQUATA/Inventario/Alimento')->with('error', 'No se puedo completar la acción, intentelo de nuevo');
          }
          catch (\Throwable $e) {
            DB::rollBack();
            return redirect('AQUATA/Inventario/Alimento')->with('error', 'No se puedo completar la acción, intentelo de nuevo');
          }
        }
        else
        {
          if(auth()->user()->idRol == 3)
          {
            DB::beginTransaction();
            try {
              $nuevo = App\Unidad::findOrFail($id);
              $nuevo->T="0";
              $nuevo->save();
              DB::commit();

              return back()->with('message-delete-unit','Eliminacion de unidad exitosa');
            }
            catch (\Exception $e) {
              DB::rollBack();
              return redirect('AQUATA/Inventario/Alimento')->with('error', 'No se puedo completar la acción, intentelo de nuevo');
            }
            catch (\Throwable $e) {
              DB::rollBack();
              return redirect('AQUATA/Inventario/Alimento')->with('error', 'No se puedo completar la acción, intentelo de nuevo');
            }
          }
          else
          {
            return redirect('AQUATA/inicio')->with('error', '¡Oops! no tienes permiso para hacer eso');
          }
        }
      }
      else
      {
      return redirect('/login');
      }
    }

    public function activeUnit($id){
      $idUser = Auth::id();
      $u=App\Usuario::findOrFail($idUser);
      $user = App\Empleado::findOrFail($u->idEmpleado);
      if(Auth::check()&&auth()->user()->T == 1)
      {
        if(auth()->user()->idRol == 1)
        {
          DB::beginTransaction();
          try {
            $nuevo = App\Unidad::findOrFail($id);
            $nuevo->T="1";
            $nuevo->save();
            DB::commit();

            return back()->with('message-active-unit','Activacion de unidad exitosa');
          }
          catch (\Exception $e) {
            DB::rollBack();
            return redirect('AQUATA/Inventario/Articulos')->with('error', 'No se puedo completar la acción, intentelo de nuevo');
          }
          catch (\Throwable $e) {
            DB::rollBack();
            return redirect('AQUATA/Inventario/Articulos')->with('error', 'No se puedo completar la acción, intentelo de nuevo');
          }
        }
        else
        {
          if(auth()->user()->idRol == 3)
          {
            DB::beginTransaction();
            try {
              $nuevo = App\Unidad::findOrFail($id);
              $nuevo->T="1";
              $nuevo->save();
              DB::commit();

              return back()->with('message-active-unit','Activacion de unidad exitosa');
            }
            catch (\Exception $e) {
              DB::rollBack();
              return redirect('AQUATA/Inventario/Articulos')->with('error', 'No se puedo completar la acción, intentelo de nuevo');
            }
            catch (\Throwable $e) {
              DB::rollBack();
              return redirect('AQUATA/Inventario/Articulos')->with('error', 'No se puedo completar la acción, intentelo de nuevo');
            }
          }
          else
          {
            return redirect('AQUATA/inicio')->with('error', '¡Oops! no tienes permiso para hacer eso');
          }
        }
      }
      else
      {
      return redirect('/login');
      }
    }

    public function deleteArticleCategory($id){
      $idUser = Auth::id();
      $u=App\Usuario::findOrFail($idUser);
      $user = App\Empleado::findOrFail($u->idEmpleado);
      if(Auth::check()&&auth()->user()->T == 1)
      {
        if(auth()->user()->idRol == 1)
        {
          DB::beginTransaction();
          try {
            $nuevo = App\CategoriaArticulo::findOrFail($id);
            $nuevo->T="0";
            $nuevo->save();
            DB::commit();
            return back()->with('message-delete-category','Eliminacion de categoria exitosa');
          }
          catch (\Exception $e) {
            DB::rollBack();
            return redirect('AQUATA/Inventario/Articulos')->with('error', 'No se puedo completar la acción, intentelo de nuevo');
          }
          catch (\Throwable $e) {
            DB::rollBack();
            return redirect('AQUATA/Inventario/Articulos')->with('error', 'No se puedo completar la acción, intentelo de nuevo');
          }
        }
        else
        {
          if(auth()->user()->idRol == 3)
          {
            DB::beginTransaction();
            try {
              $nuevo = App\CategoriaArticulo::findOrFail($id);
              $nuevo->T="0";
              $nuevo->save();
              DB::commit();
              return back()->with('message-delete-category','Eliminacion de categoria exitosa');
            }
            catch (\Exception $e) {
              DB::rollBack();
              return redirect('AQUATA/Inventario/Articulos')->with('error', 'No se puedo completar la acción, intentelo de nuevo');
            }
            catch (\Throwable $e) {
              DB::rollBack();
              return redirect('AQUATA/Inventario/Articulos')->with('error', 'No se puedo completar la acción, intentelo de nuevo');
            }
          }
          else
          {
            return redirect('AQUATA/inicio')->with('error', '¡Oops! no tienes permiso para hacer eso');
          }
        }
      }
      else
      {
      return redirect('/login');
      }
    }

    public function activeArticleCategory($id){
      $idUser = Auth::id();
      $u=App\Usuario::findOrFail($idUser);
      $user = App\Empleado::findOrFail($u->idEmpleado);
      if(Auth::check()&&auth()->user()->T == 1)
      {
        if(auth()->user()->idRol == 1)
        {
          DB::beginTransaction();
          try {
            $nuevo = App\CategoriaArticulo::findOrFail($id);
            $nuevo->T="1";
            $nuevo->save();
            DB::commit();
            return back()->with('message-active-category','Activacion de categoria exitosa');
          }
          catch (\Exception $e) {
            DB::rollBack();
            return redirect('AQUATA/Inventario/Articulos')->with('error', 'No se puedo completar la acción, intentelo de nuevo');
          }
          catch (\Throwable $e) {
            DB::rollBack();
            return redirect('AQUATA/Inventario/Articulos')->with('error', 'No se puedo completar la acción, intentelo de nuevo');
          }
        }
        else
        {
          if(auth()->user()->idRol == 3)
          {
            DB::beginTransaction();
            try {
              $nuevo = App\CategoriaArticulo::findOrFail($id);
              $nuevo->T="1";
              $nuevo->save();
              DB::commit();
              return back()->with('message-active-category','Activacion de categoria exitosa');
            }
            catch (\Exception $e) {
              DB::rollBack();
              return redirect('AQUATA/Inventario/Articulos')->with('error', 'No se puedo completar la acción, intentelo de nuevo');
            }
            catch (\Throwable $e) {
              DB::rollBack();
              return redirect('AQUATA/Inventario/Articulos')->with('error', 'No se puedo completar la acción, intentelo de nuevo');
            }
          }
          else
          {
            return redirect('AQUATA/inicio')->with('error', '¡Oops! no tienes permiso para hacer eso');
          }
        }
      }
      else
      {
      return redirect('/login');
      }
    }

    public function addProduct(){
      if(Auth::check()&&auth()->user()->T == 1)
      {
        $idUser = Auth::id();
        $u=App\Usuario::findOrFail($idUser);
        $user = App\Empleado::findOrFail($u->idEmpleado);
        if(auth()->user()->idRol == 1)
        {
          return view('AddProduct',compact('user'));
        }
        else
        {
          if(auth()->user()->idRol == 3)
          {
            return view('AddProduct',compact('user'));
          }
          else
          {
            return redirect('AQUATA/inicio')->with('error', '¡Oops! no tienes permiso para hacer eso');
          }
        }
      }
      else
      {
      return redirect('/login');
      }
    }

    public function addValidationProduct(Request $request){
      $idUser = Auth::id();
      $u=App\Usuario::findOrFail($idUser);
      $user = App\Empleado::findOrFail($u->idEmpleado);
      if(Auth::check()&&auth()->user()->T == 1)
      {
        if(auth()->user()->idRol == 1)
        {
          DB::beginTransaction();
          try {

            $product = new App\Producto;
            $product->nombre = $request->nombre;
            $product->precio = $request->precio;
            $product->descripcion = $request->descripcion;
            $product->T = $request->T;
            $product->imagen = '1';
            $product->save();
            $file= $request->file('imagen');
            $img = 'Producto'.$product->id.'.'.$file->getClientOriginalExtension();
            \Storage::disk('public')->put($img,  \File::get($file));
            $product->imagen = $img;
            //  NOTE: se guarda el registro en la base de datos
            $product->save();
            DB::commit();
            return redirect('AQUATA/Inventario/Productos')->with('success','El inventario ha sido actualizado correctamente');
          }
          catch (\Exception $e) {
            DB::rollBack();
            return redirect('AQUATA/Inventario/Productos')->with('error', 'No se puedo completar la acción, intentelo de nuevo');
          }
          catch (\Throwable $e) {
            DB::rollBack();
            return redirect('AQUATA/Inventario/Productos')->with('error', 'No se puedo completar la acción, intentelo de nuevo');
          }
        }
        else
        {
          if(auth()->user()->idRol == 3)
          {
            DB::beginTransaction();
            try {

              $product = new App\Producto;
              $product->nombre = $request->nombre;
              $product->precio = $request->precio;
              $product->descripcion = $request->descripcion;
              $product->T = $request->T;
              $product->imagen = '1';
              $product->save();
              $file= $request->file('imagen');
              $img = 'Producto'.$product->id.'.'.$file->getClientOriginalExtension();
              \Storage::disk('public')->put($img,  \File::get($file));
              $product->imagen = $img;
              //  NOTE: se guarda el registro en la base de datos
              $product->save();
              DB::commit();
              return redirect('AQUATA/Inventario/Productos')->with('success','El inventario ha sido actualizado correctamente');
            }
            catch (\Exception $e) {
              DB::rollBack();
              return redirect('AQUATA/Inventario/Productos')->with('error', 'No se puedo completar la acción, intentelo de nuevo');
            }
            catch (\Throwable $e) {
              DB::rollBack();
              return redirect('AQUATA/Inventario/Productos')->with('error', 'No se puedo completar la acción, intentelo de nuevo');
            }
          }
          else
          {
            return redirect('AQUATA/inicio')->with('error', '¡Oops! no tienes permiso para hacer eso');
          }
        }
      }
      else
      {
      return redirect('/login');
      }
    }

    public function deleteProduct($id){
      $idUser = Auth::id();
      $u=App\Usuario::findOrFail($idUser);
      $user = App\Empleado::findOrFail($u->idEmpleado);
      if(Auth::check()&&auth()->user()->T == 1)
      {
        if(auth()->user()->idRol == 1)
        {
          DB::beginTransaction();
          try {

            $nuevo = App\Producto::findOrFail($id);
            $nuevo->T="0";
            $nuevo->save();
            DB::commit();
            return back()->with('success','El producto ha sido eliminado correctamente');
          }
          catch (\Exception $e) {
            DB::rollBack();
            return redirect('AQUATA/Inventario/Productos')->with('error', 'No se puedo completar la acción, intentelo de nuevo');
          }
          catch (\Throwable $e) {
            DB::rollBack();
            return redirect('AQUATA/Inventario/Productos')->with('error', 'No se puedo completar la acción, intentelo de nuevo');
          }
        }
        else
        {
          if(auth()->user()->idRol == 3)
          {
            DB::beginTransaction();
            try {

              $nuevo = App\Producto::findOrFail($id);
              $nuevo->T="0";
              $nuevo->save();
              DB::commit();
              return back()->with('success','El producto ha sido eliminado correctamente');
            }
            catch (\Exception $e) {
              DB::rollBack();
              return redirect('AQUATA/Inventario/Productos')->with('error', 'No se puedo completar la acción, intentelo de nuevo');
            }
            catch (\Throwable $e) {
              DB::rollBack();
              return redirect('AQUATA/Inventario/Productos')->with('error', 'No se puedo completar la acción, intentelo de nuevo');
            }
          }
          else
          {
            return redirect('AQUATA/inicio')->with('error', '¡Oops! no tienes permiso para hacer eso');
          }
        }
      }
      else
      {
      return redirect('/login');
      }
    }

    public function activeProduct($id){
      $idUser = Auth::id();
      $u=App\Usuario::findOrFail($idUser);
      $user = App\Empleado::findOrFail($u->idEmpleado);
      if(Auth::check()&&auth()->user()->T == 1)
      {
        if(auth()->user()->idRol == 1)
        {
          DB::beginTransaction();
          try {
            $nuevo = App\Producto::findOrFail($id);
            $nuevo->T="1";
            $nuevo->save();
            DB::commit();
            return back()->with('success','El producto ha sido activado correctamente');
          }
          catch (\Exception $e) {
            DB::rollBack();
            return redirect('AQUATA/Inventario/Productos')->with('error', 'No se puedo completar la acción, intentelo de nuevo');
          }
          catch (\Throwable $e) {
            DB::rollBack();
            return redirect('AQUATA/Inventario/Productos')->with('error', 'No se puedo completar la acción, intentelo de nuevo');
          }
        }
        else
        {
          if(auth()->user()->idRol == 3)
          {
            DB::beginTransaction();
            try {
              $nuevo = App\Producto::findOrFail($id);
              $nuevo->T="1";
              $nuevo->save();
              DB::commit();
              return back()->with('success','El producto ha sido activado correctamente');
            }
            catch (\Exception $e) {
              DB::rollBack();
              return redirect('AQUATA/Inventario/Productos')->with('error', 'No se puedo completar la acción, intentelo de nuevo');
            }
            catch (\Throwable $e) {
              DB::rollBack();
              return redirect('AQUATA/Inventario/Productos')->with('error', 'No se puedo completar la acción, intentelo de nuevo');
            }
          }
          else
          {
            return redirect('AQUATA/inicio')->with('error', '¡Oops! no tienes permiso para hacer eso');
          }
        }
      }
      else
      {
      return redirect('/login');
      }
    }

    public function editProduct($id){
      if(Auth::check()&&auth()->user()->T == 1)
      {
        $product = App\Producto::findOrFail($id);
        $idUser = Auth::id();
        $u=App\Usuario::findOrFail($idUser);
        $user = App\Empleado::findOrFail($u->idEmpleado);
        if(auth()->user()->idRol == 1)
        {
          return view('EditProduct',compact('product','user'));
        }
        else
        {
          if(auth()->user()->idRol == 3)
          {
            return view('EditProduct',compact('product','user'));
          }
          else
          {
            return redirect('AQUATA/inicio')->with('error', '¡Oops! no tienes permiso para hacer eso');
          }
        }
      }
      else
      {
      return redirect('/login');
      }
    }

    public function updateProduct(Request $request){
      $idUser = Auth::id();
      $u=App\Usuario::findOrFail($idUser);
      $user = App\Empleado::findOrFail($u->idEmpleado);
      if(Auth::check()&&auth()->user()->T == 1)
      {
        if(auth()->user()->idRol == 1)
        {
          DB::beginTransaction();
          try {
            //falta validar las imagenes
            $request->validate([
              'id'=>'required',
              'nombre'=>'required',
              'descripcion'=>'required',
              //'imagen'=>'required',
              'T'=>'required',
              'precio'=>'required'
            ]);

            $product = App\Producto::findOrFail($request->id);
            $product->nombre=$request->nombre;
            $product->descripcion=$request->descripcion;
            $product->T=$request->T;
            $product->precio=$request->precio;

            $product->save();

            if($request->file('imagen') != null){
              $file= $request->file('imagen');
              $img = 'Producto'.$request->id.'.'.$file->getClientOriginalExtension();
              \Storage::disk('public')->put($img,  \File::get($file));
              $product->imagen = $img;
              //  NOTE: se guarda el registro en la base de datos
              $product->save();
            }
            DB::commit();
            return redirect('AQUATA/Inventario/Productos')->with('success','El producto ha sido editado correctamente');
          }
          catch (\Exception $e) {
            DB::rollBack();
            return redirect('AQUATA/Inventario/Productos')->with('error', 'No se puedo completar la acción, intentelo de nuevo');
          }
          catch (\Throwable $e) {
            DB::rollBack();
            return redirect('AQUATA/Inventario/Productos')->with('error', 'No se puedo completar la acción, intentelo de nuevo');
          }
        }
        else
        {
          if(auth()->user()->idRol == 3)
          {
            DB::beginTransaction();
            try {
              //falta validar las imagenes
              $request->validate([
                'id'=>'required',
                'nombre'=>'required',
                'descripcion'=>'required',
                //'imagen'=>'required',
                'T'=>'required',
                'precio'=>'required'
              ]);

              $product = App\Producto::findOrFail($request->id);
              $product->nombre=$request->nombre;
              $product->descripcion=$request->descripcion;
              $product->T=$request->T;
              $product->precio=$request->precio;

              $product->save();

              if($request->file('imagen') != null){
                $file= $request->file('imagen');
                $img = 'Producto'.$request->id.'.'.$file->getClientOriginalExtension();
                \Storage::disk('public')->put($img,  \File::get($file));
                $product->imagen = $img;
                //  NOTE: se guarda el registro en la base de datos
                $product->save();
              }
              DB::commit();
              return redirect('AQUATA/Inventario/Productos')->with('success','El producto ha sido editado correctamente');
            }
            catch (\Exception $e) {
              DB::rollBack();
              return redirect('AQUATA/Inventario/Productos')->with('error', 'No se puedo completar la acción, intentelo de nuevo');
            }
            catch (\Throwable $e) {
              DB::rollBack();
              return redirect('AQUATA/Inventario/Productos')->with('error', 'No se puedo completar la acción, intentelo de nuevo');
            }
          }
          else
          {
            return redirect('AQUATA/inicio')->with('error', '¡Oops! no tienes permiso para hacer eso');
          }
        }
      }
      else
      {
      return redirect('/login');
      }
    }
}
// TODO: ERROR EDITAR ARTICULOS
// TODO: error en editar producto
