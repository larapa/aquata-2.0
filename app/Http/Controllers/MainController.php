<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App;

class MainController extends Controller
{
    public function showRecipes()
    {
      $recipes = App\Receta::where('recetas.T','=','1')->get();
      $products = App\Producto::select('productos.*', 'receta_productos.idReceta')
                ->join('receta_productos','productos.id','=','receta_productos.idProducto')
                ->join('recetas','recetas.id','=','receta_productos.idReceta')
                ->where('recetas.T','=','1')
                ->get();

      foreach ($recipes as $recipe) {
        $array = array();
        foreach ($products as $product) {
          if ($product->idReceta == $recipe->id) {
            array_push($array, $product);
          }
        }
        $recipe->productos = $array;
      }

      return view('Main.Recipes',compact('recipes'));
    }
    public function showProducts()
    {
      $products = App\Producto::where('T','=','1')->get();
      return view('Main.Products',compact('products'));
    }
}
