<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App;
use App\Puesto;
use App\Empleado;
use DB;
use Auth;
use Illuminate\Support\Facades\Hash;
use App\Usuario;

class JobsController extends Controller
{
    //  NOTE: consulta para ver todos los puestos y todos los empleados
  public function showInfo($jobs=null,$employes=null){
      if(Auth::check()&&auth()->user()->T == 1){
        if(auth()->user()->idRol == 1||auth()->user()->idRol == 2){
          $jobs = App\Puesto::all();
          $employes = App\Empleado::select('empleados.id','empleados.nombre','aPaterno','aMaterno','telefono','empleados.email','sueldo','imagen','idPuesto', 'usuarios.idEmpleado as idUsuario', 'roles.nombre as rol')
                                  ->leftJoin('usuarios', function ($join) {
                                      $join->on('usuarios.idEmpleado', '=', 'empleados.id');
                                      $join->where('usuarios.T','=','1');
                                    })
                                  ->leftJoin('roles', 'usuarios.idRol', '=', 'roles.id')
                                  ->orderBy('idPuesto')
                                  ->orderBy('empleados.nombre', 'asc')->get();
          return view('Employee',compact('jobs','employes'));
        }
        else{
          return redirect('AQUATA/inicio')->with('error', '¡Oops! No tienes permiso para hacer eso');
        }
      }
      else{
        return redirect('/login');
      }
    }

    //  ------------------------------------------------------------------------
    //  *                   FUNCIONES DE LOS PUESTOS                           *
    //  ------------------------------------------------------------------------

    //  NOTE: funcion que devuelve la vista con el formulario para agregar un puesto
  public function addJobView(){
      return view('AddJob');
    }

    //  NOTE: funcion para agregar un puesto
  public function addJob(Request $request){
      if(Auth::check()&&auth()->user()->T == 1){
        if(auth()->user()->idRol == 1||auth()->user()->idRol == 2){
          DB::beginTransaction();
          try {
            $newJob = new App\Puesto;
            //  NOTE: hay que comprobar que no haya 2 puestos con el mismo nombre
            $currentJobs = App\Puesto::all();
            foreach ($currentJobs as $cJob) {
              if($cJob->nombre == $request->nombre){
                return back()->with('message-duplicated-job','Ya existe un puesto con ese nombre');
              }
            }
            //  NOTE: si no se repitio el nombre se guarda en la base de datos
            $newJob->nombre = $request->nombre;

            $newJob->save();

            DB::commit();
            return redirect('AQUATA/Empleados')->with('message-add-job','El puesto se ha añadido correctamente');
          } catch (\Exception $e) {
            DB::rollBack();
            return redirect('AQUATA/Empleados')->with('bd-error','AS');
          } catch (\Throwable $e) {
            DB::rollBack();
            return redirect('AQUATA/Empleados')->with('bd-error','Algo salio mal, inténtalo nuevamente');
          }
        }
        else{
          return redirect('AQUATA/inicio')->with('error', '¡Oops! No tienes permiso para hacer eso');
        }
      }
      else{
        return redirect('/login');
      }
    }

    //  NOTE: funcion que edita un puesto
  public function editJob($id){
      if(Auth::check()&&auth()->user()->T == 1){
        $job = App\Puesto::findOrFail($id);
        return view('EditJob',compact('job'));
      }
      else{
        return redirect('/login');
      }
    }

    //  NOTE: funcion que actualiza un puesto
  public function updateJob(Request $request){
      if(Auth::check()&&auth()->user()->T == 1){
        if(auth()->user()->idRol == 1||auth()->user()->idRol == 2){
          $request->validate([
            'nombre'=>'required',
          ]);
          DB::beginTransaction();

          try {
            $job = App\Puesto::findOrFail($request->id);
            $job->timestamps = false;
            $job->nombre = $request->nombre;
            $job->save();

            DB::commit();
            return redirect('AQUATA/Empleados')->with('message-update-job','Los datos del puesto se han actualizado correctamente');
          } catch (\Exception $e) {
            DB::rollBack();
            return redirect('AQUATA/Empleados')->with('bd-error','Algo salio mal, inténtalo nuevamente');
          } catch (\Throwable $e) {
            DB::rollBack();
            return redirect('AQUATA/Empleados')->with('bd-error','Algo salio mal, inténtalo nuevamente');
          }
        }
        else{
          return redirect('AQUATA/inicio')->with('error', '¡Oops! No tienes permiso para hacer eso');
        }
      }
      else{
        return redirect('/login');
      }
    }

    //  NOTE: funcion que elimina un puesto
  public function deleteJob($id){
      if(Auth::check()&&auth()->user()->T == 1){
        if(auth()->user()->idRol == 1||auth()->user()->idRol == 2){
          DB::beginTransaction();
          try {
            //  NOTE: PARA PODER ELIMINAR UN PUESTO ESTE NO DEBE TENER EMPLEADOS
            $cant = App\Empleado::select('id')->where('empleados.idPuesto','=',$id)->count();

            if($cant > 0){
              //  hay al menos un empleado con ese puesto
              //  no se puede hacer la eliminacion
              return back()->with('message-delete-job-1','Elimine primero a los empleados con este puesto');
            }
            else{
              //  no hay ningun empleado con ese puesto
              //  puede eliminarse el puesto
              $nuevo = App\Puesto::findOrFail($id);
              $nuevo->T="0";
              $nuevo->save();
              DB::commit();
              return back()->with('message-delete-job-2','El puesto se ha eliminado correctamente');
            }
          } catch (\Exception $e) {
            DB::rollBack();
            return redirect('AQUATA/Empleados')->with('bd-error','Algo salio mal, inténtalo nuevamente');
          } catch (\Throwable $e) {
            DB::rollBack();
            return redirect('AQUATA/Empleados')->with('bd-error','Algo salio mal, inténtalo nuevamente');
          }
        }
        else{
          return redirect('AQUATA/inicio')->with('error', '¡Oops! No tienes permiso para hacer eso');
        }
      }
      else{
        return redirect('/login');
      }
    }

    //  NOTE: funcion que activa un puesto
  public function activateJob($id){
      if(Auth::check()&&auth()->user()->T == 1){
        if(auth()->user()->idRol == 1||auth()->user()->idRol == 2){
          DB::beginTransaction();
          try {
            $nuevo = App\Puesto::findOrFail($id);
            $nuevo->T="1";
            $nuevo->save();
            DB::commit();
            return back()->with('message-activate-job','El puesto se ha reactivado correctamente');
          } catch (\Exception $e) {
            DB::rollBack();
            return redirect('AQUATA/Empleados')->with('bd-error','Algo salio mal, inténtalo nuevamente');
          } catch (\Throwable $e) {
            DB::rollBack();
            return redirect('AQUATA/Empleados')->with('bd-error','Algo salio mal, inténtalo nuevamente');
          }
        }
        else{
          return redirect('AQUATA/inicio')->with('error', '¡Oops! No tienes permiso para hacer eso');
        }
      }
      else{
        return redirect('/login');
      }
    }

    //  ------------------------------------------------------------------------
    //  *                  FUNCIONES DE LOS EMPLEADOS                          *
    //  ------------------------------------------------------------------------

    //  NOTE: funcion que devuelve la view con el formulario para agregar un empleado

  public function addEmployeeView($jobs=null){
    if(Auth::check()&&auth()->user()->T == 1){
      $jobs = App\Puesto::select('id','nombre')->where('puestos.T','=','1')->get();
      $roles = App\Role::all();
      return view('AddEmployee',compact('jobs','roles'));
    }
    else{
      return redirect('/login');
    }
  }

    //  NOTE: funcion para agregar un empleado
  public function addEmployee(Request $request){
    if(Auth::check()&&auth()->user()->T == 1){
      if(auth()->user()->idRol == 1||auth()->user()->idRol == 2){
        DB::beginTransaction();
        try {
          //  NOTE: se crea un nuevo 'objeto' de tipo empleado
          $newEmployee = new App\Empleado;
          //  NOTE: se asigna un valor a cada uno de los campos
          $newEmployee->nombre = $request->nombre;
          $newEmployee->aPaterno = $request->aPaterno;
          $newEmployee->aMaterno = $request->aMaterno;
          $newEmployee->telefono = $request->telefono;
          $newEmployee->email = $request->email;
          $newEmployee->sueldo = $request->sueldo;
          $newEmployee->idPuesto = $request->idPuesto;
          $newEmployee->imagen = '1';
          $newEmployee->save();
          $file= $request->file('imagen');
          $img = 'Empleado'.$newEmployee->id.'.'.$file->getClientOriginalExtension();
          \Storage::disk('public')->put($img,  \File::get($file));
          $newEmployee->imagen = $img;
          //  NOTE: se guarda el registro en la base de datos
          $newEmployee->save();

          if(($request->activo != null) && ($request->password != null) && ($request->idRol != null)){
            if($request->activo == true){
              $newUser = new App\Usuario;
              $newUser->name = $request->nombre;
              $newUser->email = $request->email;
              $newUser->password = Hash::make($request->password);
              $newUser->idEmpleado = $newEmployee->id;
              $newUser->idRol = $request->idRol;
              $newUser->save();
            }
          }

          DB::commit();
          return redirect('AQUATA/Empleados')->with('message-add-employee','El empleado se ha añadido correctamente');
        } catch (\Exception $e) {
          DB::rollBack();
          return redirect('AQUATA/Empleados')->with('bd-error','Algo salio mal, inténtalo nuevamente');
        } catch (\Throwable $e) {
          DB::rollBack();
          return redirect('AQUATA/Empleados')->with('bd-error','Algo salio mal, inténtalo nuevamente');
        }
      }
      else{
        return redirect('AQUATA/inicio')->with('error', '¡Oops! No tienes permiso para hacer eso');
      }
    }
    else{
      return redirect('/login');
    }
  }

    //  NOTE: funcion que edita un empleado
  public function editEmployee($id,$jobs=null){
    if(Auth::check()&&auth()->user()->T == 1){
      $jobs = App\Puesto::select('id','nombre')->where('puestos.T','=','1')->get();
      $employee = App\Empleado::findOrFail($id);
      $user = App\Usuario::where('idEmpleado', '=', $id)->first();
      $roles = App\Role::all();
      return view('EditEmployee',compact('employee','jobs', 'user', 'roles'));
    }
    else{
      return redirect('/login');
    }
  }

    //  NOTE: funcion que actualiza un empleado
  public function updateEmployee(Request $request){
      if(Auth::check()&&auth()->user()->T == 1){
        if(auth()->user()->idRol == 1||auth()->user()->idRol == 2){
          DB::beginTransaction();
          try {
            $request->validate([
              'nombre'=>'required',
              'aPaterno'=>'required',
              'aMaterno'=>'required',
              'telefono'=>'required',
              'email'=>'required',
              'sueldo'=>'required',
              'idPuesto'=>'required'
            ]);

            $employee = App\Empleado::findOrFail($request->id);
            $employee->timestamps = false;
            $employee->nombre = $request->nombre;
            $employee->aPaterno = $request->aPaterno;
            $employee->aMaterno = $request->aMaterno;
            $employee->telefono = $request->telefono;
            $employee->email = $request->email;
            $employee->sueldo = $request->sueldo;
            $employee->idPuesto = $request->idPuesto;
            $employee->save();
            if($request->file('imagen') != null){
              $file= $request->file('imagen');
              $img = 'Empleado'.$request->id.'.'.$file->getClientOriginalExtension();
              \Storage::disk('public')->put($img,  \File::get($file));
              $employee->imagen = $img;
              //  NOTE: se guarda el registro en la base de datos
              $employee->save();
            }


            if($request->activo == true){
              if($request->idU == 0){
                $newUser = new App\Usuario;
                $newUser->name = $request->nombre;
                $newUser->email = $request->email;
                $newUser->password = Hash::make($request->password);
                $newUser->idEmpleado = $employee->id;
                $newUser->idRol = $request->idRol;
                $newUser->save();
              }
              else {
                $updateUser = App\Usuario::findOrFail($request->idU);
                $updateUser->name = $request->nombre;
                $updateUser->email = $request->email;
                $updateUser->idRol = $request->idRol;
                $updateUser->T = 1;
                $updateUser->save();
              }
            }
            else {
              if($request->idU != 0){
                $updateUser = App\Usuario::findOrFail($request->idU);
                $updateUser->name = $request->nombre;
                $updateUser->email = $request->email;
                $updateUser->T = 0;
                $updateUser->idRol = $request->idRol;
                $updateUser->save();
              }
            }

            DB::commit();
            return redirect('AQUATA/Empleados')->with('message-update-employee','Los datos del empleado se han actualizado correctamente');
          } catch (\Exception $e) {
            DB::rollBack();
            return redirect('AQUATA/Empleados')->with('bd-error','Algo salio mal, inténtalo nuevamente');
          } catch (\Throwable $e) {
            DB::rollBack();
            return redirect('AQUATA/Empleados')->with('bd-error','Algo salio mal, inténtalo nuevamente');
          }
        }
        else{
          return redirect('AQUATA/inicio')->with('error', '¡Oops! No tienes permiso para hacer eso');
        }
      }
      else{
        return redirect('/login');
      }
    }

    //  NOTE: funcion para eliminar un empleado
  public function deleteEmployee($id){
    if(Auth::check()&&auth()->user()->T == 1){
      if(auth()->user()->idRol == 1 || auth()->user()->idRol == 2){
        DB::beginTransaction();
        try {
          $nuevo = App\Empleado::findOrFail($id);
          $nuevo->idPuesto="1";
          $nuevo->save();

          $usuario = App\Usuario::where('idEmpleado', '=', $id)->first();
          if($usuario != null){
            $usuario->T = 0;
            $usuario->save();
          }

          DB::commit();
          return back()->with('message-delete-employee','El empleado se ha eliminado correctamente');
        } catch (\Exception $e) {
          DB::rollBack();
          return redirect('AQUATA/Empleados')->with('bd-error','Algo salio mal, inténtalo nuevamente');
        } catch (\Throwable $e) {
          DB::rollBack();
          return redirect('AQUATA/Empleados')->with('bd-error','Algo salio mal, inténtalo nuevamente');
        }
      }
      else {
        return redirect('AQUATA/inicio')->with('error', '¡Oops! No tienes permiso para hacer eso');
      }
    }
    else{
      return redirect('/login');
    }
  }

    //  NOTE: funcion para activar un empleado
  public function activateEmployee($id){
    if(Auth::check()&&auth()->user()->T == 1){
      $jobs = App\Puesto::select('id','nombre')->where('puestos.T','=','1')->get();
      $employee = App\Empleado::findOrFail($id);
      $user = App\Usuario::where('idEmpleado', '=', $id)->first();
      $roles = App\Role::all();
      return view('EditEmployee',compact('employee','jobs', 'user', 'roles'));
    }
    else{
      return redirect('/login');
    }
  }

}
