<?php
// TODO: Faltan las cosas de los usuarios que hacen el cambio
// NOTE: Los motivos son para identificar que se hizo en el historial
// NOTE: 1 agregar, 2 editar, 3 eliminar, 4 alimentar y 5 activar
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App;
use DB;
use Auth;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Auth\Authenticatable;
use App\Http\Middleware;

class PondsController extends Controller
{

  public function show()
  {

    if(Auth::check()&&auth()->user()->T == 1){
      $todayJoin = App\Estanque::select("idEstanque", DB::raw("sum(suministrado) as today"))
                            ->join("truchas","truchas.id","=","estanques.idTrucha")
                            ->rightJoin("registro_alimentacions","registro_alimentacions.idEstanque","=","estanques.id")
                            ->whereDate("fecha","=",Carbon::today())
                            ->groupBy("nombre");
      $minusOneDayJoin = App\Estanque::select("idEstanque", DB::raw("sum(suministrado) as minusOneDay"))
                            ->join("truchas","truchas.id","=","estanques.idTrucha")
                            ->rightJoin("registro_alimentacions","registro_alimentacions.idEstanque","=","estanques.id")
                            ->whereDate("fecha","=",Carbon::today()->subDays(1))
                            ->groupBy("nombre");
      $minusTwoDayJoin = App\Estanque::select("idEstanque", DB::raw("sum(suministrado) as minusTwoDay"))
                            ->join("truchas","truchas.id","=","estanques.idTrucha")
                            ->rightJoin("registro_alimentacions","registro_alimentacions.idEstanque","=","estanques.id")
                            ->whereDate("fecha","=",Carbon::today()->subDays(2))
                            ->groupBy("nombre");
      $minusThreeDayJoin = App\Estanque::select("idEstanque", DB::raw("sum(suministrado) as minusThreeDay"))
                            ->join("truchas","truchas.id","=","estanques.idTrucha")
                            ->rightJoin("registro_alimentacions","registro_alimentacions.idEstanque","=","estanques.id")
                            ->whereDate("fecha","=",Carbon::today()->subDays(3))
                            ->groupBy("nombre");
      $ponds = App\Estanque::select(
                  'estanques.id',
                  'estanques.nombre',
                  'estanques.cantidad',
                  'estanques.tamano',
                  'estanques.peso',
                  'estanques.imagen',
                  'estanques.temperatura',
                  'estanques.oxigeno',
                  'estanques.volumenAgua',
                  'estanques.ph',
                  'estanques.fechaIngreso',
                  'estanques.T',
                  'truchas.tipo',
                  'todayJoin.today',
                  'minusOneDayJoin.minusOneDay',
                  'minusTwoDayJoin.minusTwoDay',
                  'minusThreeDayJoin.minusThreeDay',
                  'truchas.descripcion')
                  ->join("truchas", "truchas.id", '=', 'estanques.idTrucha')
                  ->leftJoinSub($todayJoin,"todayJoin",function($join){
                    $join->on("todayJoin.idEstanque","=","estanques.id");
                  })
                  ->leftJoinSub($minusOneDayJoin,"minusOneDayJoin",function($join){
                    $join->on("minusOneDayJoin.idEstanque","=","estanques.id");
                  })
                  ->leftJoinSub($minusTwoDayJoin,"minusTwoDayJoin",function($join){
                    $join->on("minusTwoDayJoin.idEstanque","=","estanques.id");
                  })
                  ->leftJoinSub($minusThreeDayJoin,"minusThreeDayJoin",function($join){
                    $join->on("minusThreeDayJoin.idEstanque","=","estanques.id");
                  })
                  ->orderBy('estanques.T', 'desc')->orderBy('estanques.nombre', 'asc')
                  ->get();
      //$truchas =  DB::select('select * from ver_truchas');
      $bags = App\Costal::select('costals.id','etiqueta','tamano','alimentoRestante','nombre')
                          ->join('tipo_alimentos','tipo_alimentos.id','=','costals.idTipoAlimento')
                          ->where('costals.T','=','1')->get();
      $today= Carbon::today();

      $days = array(
        "today"=>date_parse_from_format("Y-m-d H:i:s", $today),
        "day-one"=>date_parse_from_format("Y-m-d H:i:s", $today->addDays(1)),
        "day-two"=>date_parse_from_format("Y-m-d H:i:s", $today->addDays(1)),
        "day-three"=>date_parse_from_format("Y-m-d H:i:s", $today->addDays(1)),
        "negative-day-one"=>date_parse_from_format("Y-m-d H:i:s", $today->subDays(4)),
        "negative-day-two"=>date_parse_from_format("Y-m-d H:i:s", $today->subDays(1)),
        "negative-day-three"=>date_parse_from_format("Y-m-d H:i:s", $today->subDays(1))
      );
      $trouts = App\Trucha::all();
      $idUser = Auth::id();
      $u=App\Usuario::findOrFail($idUser);
      $user = App\Empleado::findOrFail($u->idEmpleado);

      return view('Layouts.Estanque',compact('ponds','trouts','bags','user','days'));
    }
    else{
      Auth::logout();
      return redirect('/login');
    }
  }

  public function addForm()
  {
    if(Auth::check()&&auth()->user()->T == 1){
      $trouts = App\Trucha::all();
      return view('AddPond',compact('trouts'));
    }
    else{
      return redirect('/login');
    }
  }

  public function add(Request $request)
  {
    if(Auth::check()&&auth()->user()->T == 1){
      if(auth()->user()->idRol == 1){
        DB::beginTransaction();
        try {
          $idUser = Auth::id();
          $u=App\Usuario::findOrFail($idUser);
          $user = App\Empleado::findOrFail($u->idEmpleado);

          $pond = new App\Estanque;
          $pond->timestamps = false;
          $pond->nombre = $request->nombre;
          $pond->cantidad = $request->cantidad;
          $pond->tamano = $request->tamano;
          $pond->peso = $request->peso;
          $pond->temperatura = $request->temperatura;
          $pond->oxigeno = $request->oxigeno;
          $pond->volumenAgua = $request->volumenAgua;
          $pond->ph = $request->ph;
          $pond->fechaIngreso = $request->fechaIngreso;
          $pond->idTrucha = $request->idTrucha;
          $pond->imagen = '1';
          $pond->save();
          $file= $request->file('imagen');
          $img = 'Estanque'.$pond->id.'.'.$file->getClientOriginalExtension();
          \Storage::disk('public')->put($img,  \File::get($file));
          $pond->imagen = $img;
          $pond->save();

          $nombre = App\Trucha::findOrFail($request->idTrucha);
          $today = new App\HistorialEstanque();
          $today->fecha = Carbon::now();
          $today->timestamps = false;
          $today->cantidad = $pond->cantidad;
          $today->nombre = $pond->nombre;
          $today->tipo = $nombre->tipo;
          $today->tamano = $pond->tamano;
          $today->peso = $pond->peso;
          $today->temperatura = $pond->temperatura;
          $today->oxigeno = $pond->oxigeno;
          $today->nivelAgua = $pond->volumenAgua;
          $today->ph = $pond->ph;
          $today->fechaIngreso = $pond->fechaIngreso;
          $today->idEstanque = $pond->id;
          $today->idusuario = $user->id;
          $today->motivo = 1;
          $today->idTrucha = $pond->idTrucha;
          $today->save();

          DB::commit();
          return redirect('AQUATA/inicio')->with('success', 'Estanque añadido');
        } catch (\Exception $e) {
          DB::rollBack();
          return redirect('AQUATA/inicio')->with('error', 'Algo salió mal, inténtalo nuevamente');
        } catch(\Throwable $ex) {
          DB::rollBack();
          return redirect('AQUATA/inicio')->with('error', 'Algo salió mal, inténtalo nuevamente');
        }
      }
      else{
        return redirect('AQUATA/inicio')->with('error', '¡Oops! No tienes permiso para hacer eso');
      }
    }
    else{
      return redirect('/login');
    }
  }

  public function find(Request $request)
  {
    if(Auth::check()&&auth()->user()->T == 1){
      $pond = App\Estanque::findOrFail($request->id);
      $trouts = App\Trucha::all();
      return view('EditPond',compact('pond','trouts'));
    }
    else{
      return redirect('/login');
    }
  }

  public function delete(Request $request)
  {
    if(Auth::check()&&auth()->user()->T == 1){
      if(auth()->user()->idRol == 1){
        DB::beginTransaction();
        try {
          $idUser = Auth::id();
          $u=App\Usuario::findOrFail($idUser);
          $user = App\Empleado::findOrFail($u->idEmpleado);

          $pondDelete = App\Estanque::findOrFail($request->id);
          $pondDelete->timestamps = false;
          $pondDelete->T = 0;
          $pondDelete->save();

          $nombre = App\Trucha::findOrFail($pondDelete->idTrucha);
          $today = new App\HistorialEstanque();
          $today->fecha = Carbon::now();
          $today->timestamps = false;
          $today->cantidad = $pondDelete->cantidad;
          $today->nombre = $pondDelete->nombre;
          $today->tipo = $nombre->tipo;
          $today->tamano = $pondDelete->tamano;
          $today->peso = $pondDelete->peso;
          $today->temperatura = $pondDelete->temperatura;
          $today->oxigeno = $pondDelete->oxigeno;
          $today->nivelAgua = $pondDelete->volumenAgua;
          $today->ph = $pondDelete->ph;
          $today->fechaIngreso = $pondDelete->fechaIngreso;
          $today->idEstanque = $pondDelete->id;
          $today->idusuario = $user->id;
          $today->motivo = 3;
          $today->idTrucha = $pondDelete->idTrucha;
          $today->save();

          DB::commit();
          return redirect('AQUATA/inicio')->with('success', 'Estanque desactivado');
        } catch (\Exception $e) {
          DB::rollBack();
          return redirect('AQUATA/inicio')->with('error', 'Algo salió mal, inténtalo nuevamente');
        } catch (\Throwable $e) {
          DB::rollBack();
          return redirect('AQUATA/inicio')->with('error', 'Algo salió mal, inténtalo nuevamente');
        }
      }
      else{
        return redirect('AQUATA/inicio')->with('error', '¡Oops! No tienes permiso para hacer eso');
      }
    }
    else{
      return redirect('/login');
    }
  }

  public function pondFeed(Request $request){
    if(Auth::check()&&auth()->user()->T == 1){
      if(auth()->user()->idRol == 1 || auth()->user()->idRol == 3){
        DB::beginTransaction();
        try {
          $idUser = Auth::id();
          $u=App\Usuario::findOrFail($idUser);
          $user = App\Empleado::findOrFail($u->idEmpleado);

          $pondUpdate = App\Estanque::findOrFail($request -> id);
          $pondUpdate->timestamps = false;
          $pondUpdate->nombre = $request -> nombre;
          $pondUpdate->cantidad = $request -> cantidad;
          $pondUpdate->tamano = $request -> tamano;
          $pondUpdate->peso = $request -> peso;
          $pondUpdate->temperatura = $request -> temperatura;
          $pondUpdate->oxigeno = $request -> oxigeno;
          $pondUpdate->volumenAgua = $request -> volumenAgua;
          $pondUpdate->ph = $request -> ph;
          $pondUpdate->fechaIngreso = $request-> fechaIngreso;

          $pondUpdate->save();

          $nombre = App\Trucha::findOrFail($pondUpdate->idTrucha);
          $today = new App\HistorialEstanque();
          $today->fecha = Carbon::now();
          $today->timestamps = false;
          $today->cantidad = $pondUpdate->cantidad;
          $today->nombre = $pondUpdate->nombre;
          $today->tipo = $nombre->tipo;
          $today->tamano = $pondUpdate->tamano;
          $today->peso = $pondUpdate->peso;
          $today->temperatura = $pondUpdate->temperatura;
          $today->oxigeno = $pondUpdate->oxigeno;
          $today->nivelAgua = $pondUpdate->volumenAgua;
          $today->ph = $pondUpdate->ph;
          $today->fechaIngreso = $pondUpdate->fechaIngreso;
          $today->idEstanque = $pondUpdate->id;
          $today->idusuario = $user->id;
          $today->motivo = 4;
          $today->idTrucha = $pondUpdate->idTrucha;
          $today->save();

          $bag = App\Costal::findOrFail($request->idCostal);
          $bag->timestamps = false;
          $bag->alimentoRestante = $bag->alimentoRestante - ($request->quantity * 1000);
          if($bag->alimentoRestante <=5){
            $bag->alimentoRestante =0;
            $bag->T=0;
          }
          $bag->save();

          $history = new App\HistorialInventarioAlimento();
          $history->timestamps = false;
          $history->fecha = Carbon::now();
          $history->alimentoRestante = $bag->alimentoRestante;
          $history->tamano = $bag->tamano;
          $history->etiqueta = $bag->etiqueta;
          $history->idCostal = $bag->id;
          $history->idMotivo = 1; //comprobar que sea este
          $history->save();

          $register = new App\RegistroAlimentacion();
          $register->timestamps = false;
          $register->fecha = Carbon::now();
          $register->recomendado = $request->recomendacion;
          $register->suministrado = ($request->quantity * 1000);
          $register->idEstanque = $pondUpdate->id;
          $register->idUsuario = $user->id;
          $register->idHistorialInventarioAlimento = $history->id;
          $register->save();

          DB::commit();
          return redirect('AQUATA/inicio')->with('success', 'Estanque alimentado');
        } catch (\Exception $e) {
          DB::rollBack();
          return redirect('AQUATA/inicio')->with('error', 'Algo salió mal, inténtalo nuevamente');
        } catch (\Throwable $e) {
          DB::rollBack();
          return redirect('AQUATA/inicio')->with('error', 'Algo salió mal, inténtalo nuevamente');
        }
      }
      else {
        return redirect('AQUATA/inicio')->with('error', '¡Oops! No tienes permiso para hacer eso');
      }
    }
    else{
      return redirect('/login');
    }
  }

  public function update(Request $request)
  {
    if(Auth::check()&&auth()->user()->T == 1){
      if(auth()->user()->idRol == 1){
        DB::beginTransaction();
        try {
          $idUser = Auth::id();
          $u=App\Usuario::findOrFail($idUser);
          $user = App\Empleado::findOrFail($u->idEmpleado);

          $pondUpdate = App\Estanque::findOrFail($request -> id);
          $pondUpdate->timestamps = false;
          $pondUpdate->nombre = $request -> nombre;
          $pondUpdate->cantidad = $request -> cantidad;
          $pondUpdate->tamano = $request -> tamano;
          $pondUpdate->peso = $request -> peso;
          $pondUpdate->temperatura = $request -> temperatura;
          $pondUpdate->oxigeno = $request -> oxigeno;
          $pondUpdate->volumenAgua = $request -> volumenAgua;
          $pondUpdate->ph = $request -> ph;
          $pondUpdate->fechaIngreso = $request-> fechaIngreso;
          $pondUpdate->idTrucha = $request -> idTrucha;
          $pondUpdate->save();

          if($request->file('imagen') != null){
            $file= $request->file('imagen');
            $img = 'Estanque'.$request->id.'.'.$file->getClientOriginalExtension();
            \Storage::disk('public')->put($img,  \File::get($file));
            $pondUpdate->imagen = $img;
            $pondUpdate->save();
          }

          $nombre = App\Trucha::findOrFail($request->idTrucha);
          $today = new App\HistorialEstanque();
          $today->fecha = Carbon::now();
          $today->timestamps = false;
          $today->cantidad = $pondUpdate->cantidad;
          $today->nombre = $pondUpdate->nombre;
          $today->tipo = $nombre->tipo;
          $today->tamano = $pondUpdate->tamano;
          $today->peso = $pondUpdate->peso;
          $today->temperatura = $pondUpdate->temperatura;
          $today->oxigeno = $pondUpdate->oxigeno;
          $today->nivelAgua = $pondUpdate->volumenAgua;
          $today->ph = $pondUpdate->ph;
          $today->fechaIngreso = $pondUpdate->fechaIngreso;
          $today->idEstanque = $pondUpdate->id;
          $today->idusuario = $user->id;
          $today->motivo = 2;
          $today->idTrucha = $pondUpdate->idTrucha;
          $today->save();

          DB::commit();
          return redirect('AQUATA/inicio')->with('success', 'Estanque editado');
        } catch (\Exception $e) {
          DB::rollBack();
          return redirect('AQUATA/inicio')->with('error', 'Algo salió mal, inténtalo nuevamente');
        } catch (\Throwable $e) {
          DB::rollBack();
          return redirect('AQUATA/inicio')->with('error', 'Algo salió mal, inténtalo nuevamente');
        }
      }
      else {
        return redirect('AQUATA/inicio')->with('error', '¡Oops! No tienes permiso para hacer eso');
      }
    }
    else{
      return redirect('/login');
    }
  }

  public function activate(Request $request)
  {
    if(Auth::check()&&auth()->user()->T == 1){
      if(auth()->user()->idRol == 1){
        DB::beginTransaction();
        try {
          $idUser = Auth::id();
          $u=App\Usuario::findOrFail($idUser);
          $user = App\Empleado::findOrFail($u->idEmpleado);

          $pondUpdate = App\Estanque::findOrFail($request->id);
          $pondUpdate->timestamps = false;
          $pondUpdate->T = 1;

          $pondUpdate->save();

          $nombre = App\Trucha::findOrFail($pondUpdate->idTrucha);
          $today = new App\HistorialEstanque();
          $today->fecha = Carbon::now();
          $today->timestamps = false;
          $today->cantidad = $pondUpdate->cantidad;
          $today->nombre = $pondUpdate->nombre;
          $today->tipo = $nombre->tipo;
          $today->tamano = $pondUpdate->tamano;
          $today->peso = $pondUpdate->peso;
          $today->temperatura = $pondUpdate->temperatura;
          $today->oxigeno = $pondUpdate->oxigeno;
          $today->nivelAgua = $pondUpdate->volumenAgua;
          $today->ph = $pondUpdate->ph;
          $today->fechaIngreso = $pondUpdate->fechaIngreso;
          $today->idEstanque = $pondUpdate->id;
          $today->idusuario = $user->id;
          $today->motivo = 5;
          $today->idTrucha = $pondUpdate->idTrucha;
          $today->save();

          DB::commit();
          return redirect('AQUATA/inicio')->with('success', 'Estanque activado');
        } catch (\Exception $e) {
          DB::rollBack();
          return redirect('AQUATA/inicio')->with('error', 'Algo salió mal, inténtalo nuevamente');
        } catch (\Throwable $ex) {
          DB::rollBack();
          return redirect('AQUATA/inicio')->with('error', 'Algo salió mal, inténtalo nuevamente');
        }
      }
      else {
        return redirect('AQUATA/inicio')->with('error', '¡Oops! No tienes permiso para hacer eso');
      }
    }
    else{
      return redirect('/login');
    }

  }

  public function findTrout($id)
  {
    if(Auth::check()&&auth()->user()->T == 1){
      $trout = App\Trucha::findOrFail($id);
      return view('EditTrout',compact('trout'));
    }
    else{
      return redirect('/login');
    }
  }

  public function deleteTrout(Request $request)
  {
    if(Auth::check()&&auth()->user()->T == 1){
      if(auth()->user()->idRol == 1){
        DB::beginTransaction();
        try {
          $troutDelete = App\Trucha::findOrFail($request->id);
          $troutDelete->timestamps = false;
          $troutDelete->T = 0;
          $troutDelete->save();

          DB::commit();
          return redirect('AQUATA/inicio')->with('success', 'Tipo de trucha desactivado');
        } catch (\Exception $e) {
          DB::rollBack();
          return redirect('AQUATA/inicio')->with('error', 'Algo salió mal, inténtalo nuevamente');
        } catch (\Throwable $ex) {
          DB::rollBack();
          return redirect('AQUATA/inicio')->with('error', 'Algo salió mal, inténtalo nuevamente');
        }
      }
      else {
        return redirect('AQUATA/inicio')->with('error', '¡Oops! No tienes permiso para hacer eso');
      }
    }
    else{
      return redirect('/login');
    }
  }

  public function updateTrout(Request $request)
  {
    if(Auth::check()&&auth()->user()->T == 1){
      if(auth()->user()->idRol == 1){
        DB::beginTransaction();
        try {
          $troutUpdate = App\Trucha::findOrFail($request->id);
          $troutUpdate->timestamps = false;
          $troutUpdate->tipo = $request->tipo;
          $troutUpdate->descripcion = $request->descripcion;
          $troutUpdate->save();

          DB::commit();
          return redirect('AQUATA/inicio')->with('success', 'Tipo de trucha editado editado');
        } catch (\Exception $e) {
          DB::rollBack();
          return redirect('AQUATA/inicio')->with('error', 'Algo salió mal, inténtalo nuevamente');
        } catch (\Throwable $ex) {
          DB::rollBack();
          return redirect('AQUATA/inicio')->with('error', 'Algo salió mal, inténtalo nuevamente');
        }
      }
      else {
        return redirect('AQUATA/inicio')->with('error', '¡Oops! No tienes permiso para hacer eso');
      }
    }
    else{
      return redirect('/login');
    }
  }

  public function addFormTrout()
  {
    if(Auth::check()&&auth()->user()->T == 1){
      $trouts = App\Trucha::all();
      return view('AddTrout');
    }
    else{
      return redirect('/login');
    }
  }

  public function addTrout(Request $request)
  {
    if(Auth::check()&&auth()->user()->T == 1){
      if(auth()->user()->idRol == 1){
        DB::beginTransaction();
        try {
          $trout = new App\Trucha;
          $trout->timestamps = false;
          $trout->tipo = $request->tipo;
          $trout->descripcion = $request->descripcion;
          $trout->save();

          DB::commit();
          return redirect('AQUATA/inicio')->with('success', 'Nuevo tipo de trucha añadido');
        } catch (\Exception $e) {
          DB::rollBack();
          return redirect('AQUATA/inicio')->with('error', 'Algo salió mal, inténtalo nuevamente');
        } catch (\Throwable $ex) {
          DB::rollBack();
          return redirect('AQUATA/inicio')->with('error', 'Algo salió mal, inténtalo nuevamente');
        }
      }
      else {
        return redirect('AQUATA/inicio')->with('error', '¡Oops! No tienes permiso para hacer eso');
      }
    }
    else{
      return redirect('/login');
    }
  }

  public function TroutActivate(Request $request)
  {
    if(Auth::check()&&auth()->user()->T == 1){
      if(auth()->user()->idRol == 1){
        DB::beginTransaction();
        try {
          $troutUpdate = App\Trucha::findOrFail($request->id);
          $troutUpdate->timestamps = false;
          $troutUpdate->T = 1;
          $troutUpdate->save();

          DB::commit();
          return redirect('AQUATA/inicio')->with('success', 'Tipo de trucha activado');
        } catch (\Exception $e) {
          DB::rollBack();
          return redirect('AQUATA/inicio')->with('error', 'Algo salió mal, inténtalo nuevamente');
        } catch (\Throwable $ex) {
          DB::rollBack();
          return redirect('AQUATA/inicio')->with('error', 'Algo salió mal, inténtalo nuevamente');
        }
      }
      else {
        return redirect('AQUATA/inicio')->with('error', '¡Oops! No tienes permiso para hacer eso');
      }
    }
    else{
      return redirect('/login');
    }
  }

  public function pondHistory(Request $request){
    if(Auth::check()&&auth()->user()->T == 1){
      DB::beginTransaction();
      try {
        $pond = App\Estanque::findOrFail($request->id);
        $pond = App\Estanque::select('estanques.*','truchas.tipo as etapa')
                              ->join('truchas','truchas.id','=','estanques.idTrucha')
                              ->where('estanques.id','=',$request->id)
                              ->first();
        $history=App\HistorialEstanque::where('idEstanque','=',$request->id)
                                      ->where('historial_estanques.fecha', '>=', Carbon::now()->subMonth())
                                      ->where('historial_estanques.fecha', '<=', Carbon::now())
                                      ->orderBy('fecha','desc')->get();

        DB::commit();
        return view('PondHistory',compact('pond','history'));
      } catch (\Exception $e) {
        DB::rollBack();
        return redirect('AQUATA/inicio')->with('error', 'Algo salió mal, inténtalo nuevamente');
      } catch (\Throwable $ex) {
        DB::rollBack();
        return redirect('AQUATA/inicio')->with('error', 'Algo salió mal, inténtalo nuevamente');
      }
    }
    else{
      return redirect('/login');
    }
  }

  public function pondHistoryByDate(Request $request){
    if(Auth::check()&&auth()->user()->T == 1){
      DB::beginTransaction();
      try {
        if(Carbon::createFromFormat('d/m/Y', $request -> fecha1) >= Carbon::createFromFormat('d/m/Y', $request -> fecha2)){
          return redirect('AQUATA/inicio')->with('error', 'La fecha inicial no puede ser mayor que la final');
        }
        else{
          $pond = App\Estanque::findOrFail($request->id);
          $pond = App\Estanque::select('estanques.*','truchas.tipo as etapa')
                                ->join('truchas','truchas.id','=','estanques.idTrucha')
                                ->where('estanques.id','=',$request->id)
                                ->first();
          $history=App\HistorialEstanque::where('idEstanque','=',$request->id)
                                        ->where('historial_estanques.fecha', '>=', Carbon::createFromFormat('d/m/Y', $request -> fecha1))
                                        ->where('historial_estanques.fecha', '<=', Carbon::createFromFormat('d/m/Y', $request -> fecha2))
                                        ->orderBy('fecha','desc')->get();

          $f1 = $request -> fecha1;
          $f2 = $request -> fecha2;

          DB::commit();
          return view('PondHistory',compact('pond','history','f1','f2'));
        }
      } catch (\Exception $e) {
        DB::rollBack();
        return redirect('AQUATA/inicio')->with('error', 'Algo salió mal, inténtalo nuevamente');
      } catch (\Throwable $ex) {
        DB::rollBack();
        return redirect('AQUATA/inicio')->with('error', 'Algo salió mal, inténtalo nuevamente');
      }
    }
    else{
      return redirect('/login');
    }
  }
}
