<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App;
use App\Receta;
use App\RecetaProducto;
use App\Producto;
use DB;
use Auth;

class RecipeController extends Controller
{
  public function show($recipes=null)
  {
    if(Auth::check()&&auth()->user()->T == 1){
      if(auth()->user()->idRol == 1){
        $recipes = App\Receta::all();
            $products = App\Producto::select('productos.*', 'receta_productos.idReceta')
                      ->join('receta_productos','productos.id','=','receta_productos.idProducto')
                      ->join('recetas','recetas.id','=','receta_productos.idReceta')

                      ->get();

            foreach ($recipes as $recipe) {
              $array = array();
              foreach ($products as $product) {
                if ($product->idReceta == $recipe->id) {
                  array_push($array, $product);
                }
              }
              $recipe->productos = $array;
            }
        return view('Layouts.Recetas',compact('recipes'));
      }
      else{
        return redirect('AQUATA/inicio')->with('error', '¡Oops! No tienes permiso para hacer eso');
      }
    }
    else{
      return redirect('/login');
    }
  }

  public function addForm()
  {
    if(Auth::check()&&auth()->user()->T == 1){
      $recipes = App\Receta::all();
      $products= App\Producto::all();
      return view('AddRecipe',compact('recipes','products'));
    }
    else{
      return redirect('/login');
    }
  }

  public function add(Request $request)
  {
    if(Auth::check()&&auth()->user()->T == 1){
      if(auth()->user()->idRol == 1){
        DB::beginTransaction();
        try {
        $recipe = new App\Receta;
        $recipeP = new App\RecetaProducto;
        $recipe->timestamps = false;
        $recipe->nombre = $request->nombre;
        $recipe->descripcion = $request->descripcion;
        $recipe->tiempo = $request->tiempo;

        $arrI=[];
        json_encode($arrI);
        for($i=0;$i<count($request->input('id_paso'));$i++){
          $id_paso= $request->input('id_paso')[$i];
          $instruccion= $request->input('instruccion')[$i];
          $instrucciones= array('id_paso'=>$id_paso, 'instruccion'=>$instruccion);
          $jinstrucciones=json_encode($instrucciones);
          array_push($arrI,$jinstrucciones);
        }

        $arrCan=[];
        json_encode($arrCan);
        for($i=0;$i<count($request->input('cantidad'));$i++){
          $cantidad= $request->input('cantidad')[$i];
          $ingrediente= $request->input('ingrediente')[$i];
          $ingredientes= array('cantidad'=> $cantidad, 'ingrediente'=> $ingrediente);
          $jingredientes=json_encode($ingredientes);
          array_push($arrCan,$jingredientes);
        }

        $recipe->instrucciones = $arrI;
        $recipe->ingredientes = $arrCan;

        $recipe->porciones = $request->porciones;

        $recipe->imagen = '1';
        $recipe->save();
        $file= $request->file('imagen');
        $img = 'Receta'.$recipe->id.'.'.$file->getClientOriginalExtension();
        \Storage::disk('public')->put($img,  \File::get($file));
        $recipe->imagen = $img;
        //  NOTE: se guarda el registro en la base de datos
        $recipe->save();


        foreach ($request->idProducto as $idP) {
            $recipeP = new App\RecetaProducto;
          // code...
            $recipeP->timestamps = false;
            $recipeP->idReceta = $recipe->id;
            $recipeP->idProducto = $idP;
            $recipeP->save();
        }


        DB::commit();
        return redirect('AQUATA/Recetas')->with('success', 'Receta añadida');
        }
        catch (\Exception $e) {
          DB::rollBack();
          return redirect('AQUATA/Recetas')->with('error','Algo salió mal, intentalo nuevamente');
        }
        catch (\Throwable $e) {
          DB::rollBack();
          return redirect('AQUATA/Recetas')->with('error','Algo salió mal, intentalo nuevamente');
        }
      }
      else{
        return redirect('AQUATA/inicio')->with('error', '¡Oops! No tienes permiso para hacer eso');
      }
    }
    else{
      return redirect('/login');
    }
  }

  public function delete(Request $request)
  {
    if(Auth::check()&&auth()->user()->T == 1){
      if(auth()->user()->idRol == 1){
        DB::beginTransaction();
        try {
        $recipeDelete = App\Receta::findOrFail($request->id);
        $recipeDelete->timestamps = false;
        $recipeDelete->T = 0;
        $recipeDelete->save();

        DB::commit();
        return redirect('AQUATA/Recetas')->with('success', 'Receta desactivada');
        }
        catch (\Exception $e) {
          DB::rollBack();
          return redirect('AQUATA/Recetas')->with('error','Algo salió mal, intentalo nuevamente');
        }
        catch (\Throwable $e) {
          DB::rollBack();
          return redirect('AQUATA/Recetas')->with('error','Algo salió mal, intentalo nuevamente');
        }
      }
      else{
        return redirect('AQUATA/inicio')->with('error', '¡Oops! No tienes permiso para hacer eso');
      }
    }
    else{
      return redirect('/login');
    }
  }

  public function activate(Request $request)
  {
    if(Auth::check()&&auth()->user()->T == 1){
      if(auth()->user()->idRol == 1){
        DB::beginTransaction();
        try {
        $recipeUpdate = App\Receta::findOrFail($request->id);
        $recipeUpdate->timestamps = false;
        $recipeUpdate->T = 1;

        $recipeUpdate->save();

        DB::commit();
        return redirect('AQUATA/Recetas')->with('success', 'Receta activada');
        }
        catch (\Exception $e) {
          DB::rollBack();
          return redirect('AQUATA/Recetas')->with('error','Algo salió mal, intentalo nuevamente');
        }
        catch (\Throwable $e) {
          DB::rollBack();
          return redirect('AQUATA/Recetas')->with('error','Algo salió mal, intentalo nuevamente');
        }
      }
      else {
        return redirect('AQUATA/inicio')->with('error', '¡Oops! No tienes permiso para hacer eso');
      }
    }
    else{
      return redirect('/login');
    }
  }

  public function find(Request $request)
  {
    if(Auth::check()&&auth()->user()->T == 1){
      $recipe = App\Receta::findOrFail($request->id);
      $products= App\Producto::all();
      $productos=App\RecetaProducto::all();
      return view('EditRecipe',compact('recipe','products','productos'));
    }
    else{
      return redirect('/login');
    }
  }

  public function update(Request $request)
  {
    if(Auth::check()&&auth()->user()->T == 1){
      if(auth()->user()->idRol == 1){
        DB::beginTransaction();
        try {
          $recipe = App\Receta::findOrFail($request -> id);

          $recipe->timestamps = false;
          $recipe->nombre = $request-> nombre;
          $recipe->descripcion = $request-> descripcion;
          $recipe->tiempo = $request-> tiempo;

          $arrI=[];
          json_encode($arrI);
          for($i=0;$i<count($request->input('id_paso'));$i++){
            $id_paso= $request->input('id_paso')[$i];
            $instruccion= $request->input('instruccion')[$i];
            $instrucciones= array('id_paso'=>$id_paso, 'instruccion'=>$instruccion);
            $jinstrucciones=json_encode($instrucciones);
            array_push($arrI,$jinstrucciones);
          }

          $arrCan=[];
          json_encode($arrCan);
          for($i=0;$i<count($request->input('cantidad'));$i++){
            $cantidad= $request->input('cantidad')[$i];
            $ingrediente= $request->input('ingrediente')[$i];
            $ingredientes= array('cantidad'=> $cantidad, 'ingrediente'=> $ingrediente);
            $jingredientes=json_encode($ingredientes);
            array_push($arrCan,$jingredientes);
          }

          $recipe->instrucciones = $arrI;
          $recipe->ingredientes = $arrCan;

          $recipe->porciones = $request->porciones;
          $recipe->save();
          if($request->file('imagen') != null){
            $file= $request->file('imagen');
            $img = 'Receta'.$request->id.'.'.$file->getClientOriginalExtension();
            \Storage::disk('public')->put($img,  \File::get($file));
            $recipe->imagen = $img;
            //  NOTE: se guarda el registro en la base de datos
            $recipe->save();
          }

          $validacion = App\RecetaProducto::where('idReceta','=',$request-> id)->count();
          if($validacion==0){
            foreach ($request->idProducto as $idP) {
                $recipeP = new App\RecetaProducto;
              // code...
                $recipeP->timestamps = false;
                $recipeP->idReceta = $recipe->id;
                $recipeP->idProducto = $idP;
                $recipeP->save();
            }
          }
            else{
              $borrarProd = App\RecetaProducto::where('idReceta','=',$request-> id)->forceDelete();
              foreach ($request->idProducto as $idP) {
                $recipeP = new App\RecetaProducto;
              // code...
                $recipeP->timestamps = false;
                $recipeP->idReceta = $recipe->id;
                $recipeP->idProducto = $idP;
                $recipeP->save();
              }
            }
          DB::commit();
          return redirect('AQUATA/Recetas')->with('success', 'Receta editada');
        } catch (\Exception $e) {
          DB::rollBack();
          return redirect('AQUATA/Recetas')->with('error','Algo salió mal, intentalo nuevamente');
        } catch (\Throwable $e) {
          DB::rollBack();
          return redirect('AQUATA/Recetas')->with('error','Algo salió mal, intentalo nuevamente');
        }
      }
      else {
        return redirect('AQUATA/inicio')->with('error', '¡Oops! No tienes permiso para hacer eso');
      }
    }
    else{
      return redirect('/login');
    }
  }



}
