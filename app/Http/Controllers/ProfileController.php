<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App;
use App\Puesto;
use DB;
use Auth;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Auth\Authenticatable;

class ProfileController extends Controller
{
    public function showProfile(){
      if(Auth::check()&&auth()->user()->T == 1){
        $idUser = Auth::id();
        $u=App\Usuario::findOrFail($idUser);
        $user = App\Empleado::findOrFail($u->idEmpleado);
        $job = App\Puesto::findOrFail($user->idPuesto);
        return view('Profile', compact('user','job'));
      }
      else{
        return redirect('/login');
      }
    }
}
