<?php
namespace App\Http\Controllers;
use App;
use Auth;
use DB;
use Carbon\Carbon;
use Illuminate\Http\Request;

class HistoryController extends Controller
{
  public function showArticlesHistory()
  {
    if(Auth::check()&&auth()->user()->T == 1)
    {
      $articlesHistory = App\HistorialInventarioArticulo::select('empleados.nombre', 'empleados.aPaterno', 'empleados.aMaterno', 'historial_inventario_articulos.id', 'historial_inventario_articulos.motivo', 'historial_inventario_articulos.fecha', 'historial_inventario_articulos.articulo', 'historial_inventario_articulos.descripcionArticulo', 'historial_inventario_articulos.unidad', 'historial_inventario_articulos.cantidad', 'historial_inventario_articulos.categoria', 'historial_inventario_articulos.descripcionCategoria', 'historial_inventario_articulos.cantidadElementos', 'historial_inventario_articulos.motivo')
      ->join('usuarios','historial_inventario_articulos.idUsuario', '=', 'usuarios.id')
      ->join('empleados', 'usuarios.idEmpleado', '=', 'empleados.id')
      ->orderBy('historial_inventario_articulos.fecha','desc')
      ->get();
      $idUser = Auth::id();
      $u=App\Usuario::findOrFail($idUser);
      $user = App\Empleado::findOrFail($u->idEmpleado);
      if(auth()->user()->idRol == 1)
      {
        return view('ArticlesHistory', compact('articlesHistory','user'));
      }
      else
      {
        return redirect('AQUATA/inicio')->with('error', '¡Oops! no tienes permiso para hacer eso');
      }
    }
    else
    {
    return redirect('/login');
    }
  }

  public function showArticlesHistoryByDate(Request $request){
    $idUser = Auth::id();
    $u=App\Usuario::findOrFail($idUser);
    $user = App\Empleado::findOrFail($u->idEmpleado);
    if(Auth::check()&&auth()->user()->T == 1)
    {
      if(auth()->user()->idRol == 1)
      {
        DB::beginTransaction();
           try {
             if(Carbon::createFromFormat('d/m/Y', $request -> fecha1) >= Carbon::createFromFormat('d/m/Y', $request -> fecha2)){
               DB::commit();
               return redirect('AQUATA/Historial/Articulos')->with('error', 'La fecha inicial no puede ser mayor que la final');
             }
             else{

               $articlesHistory = App\HistorialInventarioArticulo::select('empleados.nombre', 'empleados.aPaterno', 'empleados.aMaterno', 'historial_inventario_articulos.id', 'historial_inventario_articulos.motivo', 'historial_inventario_articulos.fecha', 'historial_inventario_articulos.articulo', 'historial_inventario_articulos.descripcionArticulo', 'historial_inventario_articulos.unidad', 'historial_inventario_articulos.cantidad', 'historial_inventario_articulos.categoria', 'historial_inventario_articulos.descripcionCategoria', 'historial_inventario_articulos.cantidadElementos')
               ->join('usuarios','historial_inventario_articulos.idUsuario', '=', 'usuarios.id')
               ->join('empleados', 'usuarios.idEmpleado', '=', 'empleados.id')
               ->where('historial_inventario_articulos.fecha', '>=', Carbon::createFromFormat('d/m/Y', $request -> fecha1))
               ->where('historial_inventario_articulos.fecha', '<=', Carbon::createFromFormat('d/m/Y', $request -> fecha2))
               ->orderBy('historial_inventario_articulos.fecha','desc')
               ->get();

               $f1 = $request -> fecha1;
               $f2 = $request -> fecha2;
               DB::commit();
               return view('ArticlesHistory',compact('articlesHistory','f1','f2'));
             }

           }
           catch (\Exception $e) {
             DB::rollBack();
             return redirect('AQUATA/Historial/Articulos')->with('error', 'La fecha inicial no puede ser mayor que la final');
           }
           catch (\Throwable $e) {
             DB::rollBack();
             return redirect('AQUATA/Historial/Articulos')->with('error', 'La fecha inicial no puede ser mayor que la final');
           }
      }
      else
      {
        return redirect('AQUATA/inicio')->with('error', '¡Oops! no tienes permiso para hacer eso');
      }
    }
    else
    {
    return redirect('/login');
    }
  }

  public function showFoodHistory()
  {
    if(Auth::check()&&auth()->user()->T == 1)
    {
      $foodHistory = App\HistorialInventarioAlimento::select('tipo_alimentos.nombre as tipo','tipo_alimentos.descripcion as tipoAlimentoDescripcion','motivos.nombre as motivo','motivos.descripcion as motivoDescripcion', 'motivos.direccion',
      'historial_inventario_alimentos.id', 'historial_inventario_alimentos.fecha', 'historial_inventario_alimentos.alimentoRestante','historial_inventario_alimentos.tamano', 'historial_inventario_alimentos.etiqueta')
      ->join('motivos', 'historial_inventario_alimentos.idMotivo', '=', 'motivos.id')
      ->join('costals', 'costals.id', '=', 'historial_inventario_alimentos.idCostal')
      ->join('tipo_alimentos', 'costals.idTipoAlimento', '=', 'tipo_alimentos.id')
      ->orderBy('historial_inventario_alimentos.fecha','desc')
      ->get();
      $idUser = Auth::id();
      $u=App\Usuario::findOrFail($idUser);
      $user = App\Empleado::findOrFail($u->idEmpleado);
      if(auth()->user()->idRol == 1)
      {
        return view('FoodHistory', compact('foodHistory','user'));
      }
      else
      {
        return redirect('AQUATA/inicio')->with('error', '¡Oops! no tienes permiso para hacer eso');
      }
    }
    else
    {
    return redirect('/login');
    }
  }

  public function showFoodHistoryByDate(Request $request){
    $idUser = Auth::id();
    $u=App\Usuario::findOrFail($idUser);
    $user = App\Empleado::findOrFail($u->idEmpleado);
    if(Auth::check()&&auth()->user()->T == 1)
    {
      if(auth()->user()->idRol == 1)
      {
        DB::beginTransaction();
       try {
         if(Carbon::createFromFormat('d/m/Y', $request -> fecha1) >= Carbon::createFromFormat('d/m/Y', $request -> fecha2)){
           DB::commit();
           return redirect('AQUATA/Historial/Alimento')->with('error', 'La fecha inicial no puede ser mayor que la final');
         }
         else{

           $foodHistory = App\HistorialInventarioAlimento::select('tipo_alimentos.nombre as tipo','tipo_alimentos.descripcion as tipoAlimentoDescripcion','motivos.nombre as motivo','motivos.descripcion as motivoDescripcion', 'motivos.direccion',
           'historial_inventario_alimentos.id', 'historial_inventario_alimentos.fecha', 'historial_inventario_alimentos.alimentoRestante','historial_inventario_alimentos.tamano', 'historial_inventario_alimentos.etiqueta')
           ->join('motivos', 'historial_inventario_alimentos.idMotivo', '=', 'motivos.id')
           ->join('costals', 'costals.id', '=', 'historial_inventario_alimentos.idCostal')
           ->join('tipo_alimentos', 'costals.idTipoAlimento', '=', 'tipo_alimentos.id')
           ->where('historial_inventario_alimentos.fecha', '>=', Carbon::createFromFormat('d/m/Y', $request -> fecha1))
           ->where('historial_inventario_alimentos.fecha', '<=', Carbon::createFromFormat('d/m/Y', $request -> fecha2))
           ->orderBy('historial_inventario_alimentos.fecha','desc')
           ->get();

           $f1 = $request -> fecha1;
           $f2 = $request -> fecha2;
           DB::commit();
           return view('FoodHistory',compact('foodHistory','f1','f2'));
         }
       }
       catch (\Exception $e) {
         DB::rollBack();
         return redirect('AQUATA/Historial/Alimento')->with('error', 'La fecha inicial no puede ser mayor que la final');
       }
       catch (\Throwable $e) {
         DB::rollBack();
         return redirect('AQUATA/Historial/Alimento')->with('error', 'La fecha inicial no puede ser mayor que la final');
       }
      }
      else
      {
        return redirect('AQUATA/inicio')->with('error', '¡Oops! no tienes permiso para hacer eso');
      }
    }
    else
    {
    return redirect('/login');
    }
  }
}
