<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PagesController extends Controller
{
    public function login(Request $request){
      if ($request->email == "AQUATAadmin" && $request->password == "SistemaAQUATA") {
        return redirect('/AQUATA/inicio');
      }
      else {
        return redirect('/Login')->with('error', 'Credenciales incorrectas');
      }
    }
}
