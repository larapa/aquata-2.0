<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InventarioArticulo extends Model
{
    public $timestamps = false;
}
