<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HistorialInventarioAlimento extends Model
{
    public $timestamps = false;
}
