<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Receta extends Model
{
    protected $casts= [
      'ingredientes'=>'array',
      'instrucciones'=>'array'
    ];
    public $timestamps = false;
}
